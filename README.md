# Responsa

## Passos rumo ao bem viver individual e coletivo!

Baixe o app: http://e.eita.org.br/appresponsa
Portal do Consumo Responsável: http://consumoresponsavel.org.br
Facebook: https://www.facebook.com/aplicativore...

## Sobre
Por meio do Responsa você pode encontrar locais, iniciativas e grupos que adotam e fomentam práticas de responsabilidade na produção e consumo. Há iniciativas da **economia solidária**, **grupos de consumo responsável**, **iniciativas de agroecologia**, **centrais de comercialização**, **cooperativas de trabalho**, **feiras orgânicas**, **restaurantes com produtos orgânicos**, **hortas comunitárias** em todo o Brasil.

Estas iniciativas adotam, em variado grau, práticas de produção e comercialização com respeito e cuidado com o trabalhador e com o meio ambiente.

Você pode também interagir com uma comunidade de pessoas interessadas em adotar práticas de consumo responsável. Sugira iniciativas em sua região, marque encontros, lance desafios e caminhe passos em direção ao bem viver individual e coletivo!

## Créditos
O Responsa é uma iniciativa do Instituto Kairós e da cooperativa de trabalho EITA.
Instituto Kairós: http://institutokairos.net/
Cooperativa EITA: http://eita.org.br/

## Saiba mais
Portal do Consumo Responsável: http://consumoresponsavel.org.br
Baixe o app: http://e.eita.org.br/appresponsa
Facebook: https://www.facebook.com/aplicativore...
Instagram: https://www.instagram.com/_responsa
Sound Cloud: https://soundcloud.com/user-589244400
