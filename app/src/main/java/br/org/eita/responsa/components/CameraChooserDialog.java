package br.org.eita.responsa.components;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Environment;
import android.os.Parcelable;
import android.provider.MediaStore;

import com.parse.ParseUser;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import br.org.eita.responsa.R;

public class CameraChooserDialog {

    private Activity callerActivity;
    private CameraDialogReceiver receiver;

    public CameraChooserDialog(Activity callerActivity, CameraDialogReceiver receiver) {
        this.callerActivity = callerActivity;
        this.receiver = receiver;
    }

    //Diálogo para escolher entre camera ou imagem da galeria
    //Fonte: http://stackoverflow.com/questions/4455558/allow-user-to-select-camera-or-gallery-for-image
    private Uri outputFileUri;

    public final static Integer SELECT_PICTURE_REQUEST_CODE = 108 * 108; //random

    public void openImageDialog(Context context) {

        // Determine Uri of camera image to save.
        final File root = new File(Environment.getExternalStorageDirectory() + File.separator + "Responsa" + File.separator);
        root.mkdirs();
        final String fname = "img_"+ ParseUser.getCurrentUser().getObjectId() + "_" + System.currentTimeMillis() + ".jpg";//Utils.getUniqueImageFilename();
        final File sdImageMainDirectory = new File(root, fname);
        outputFileUri = Uri.fromFile(sdImageMainDirectory);

        // Camera.
        final List<Intent> cameraIntents = new ArrayList<Intent>();
        final PackageManager packageManager = callerActivity.getPackageManager();

        final Intent captureIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
        final List<ResolveInfo> listCam = packageManager.queryIntentActivities(captureIntent, 0);
        for(ResolveInfo res : listCam) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(captureIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        //Custom Camera intent
        //Intent cameraIntent = new Intent("br.org.eita.responsa.CAMERA_GET_PHOTO_ACTION");
        //cameraIntent.setComponent(new ComponentName("br.org.eita.responsa","br.org.eita.responsa.view.activity.CameraOpenerActivity"));
        //cameraIntent.setPackage("br.org.eita.responsa");
        //cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);

        //Aplicativos de galeria
        final Intent galleryChooserIntent = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        final List<ResolveInfo> listGal = packageManager.queryIntentActivities(galleryChooserIntent, 0);
        for(ResolveInfo res : listGal) {
            final String packageName = res.activityInfo.packageName;
            final Intent intent = new Intent(galleryChooserIntent);
            intent.setComponent(new ComponentName(res.activityInfo.packageName, res.activityInfo.name));
            intent.setPackage(packageName);
            intent.putExtra(MediaStore.EXTRA_OUTPUT, outputFileUri);
            cameraIntents.add(intent);
        }

        // Filesystem.


        /*final Intent galleryIntent = new Intent();
        galleryIntent.setType("image/*");
        galleryIntent.setAction(Intent.ACTION_GET_CONTENT);*/

        Intent firstIntent = cameraIntents.get(0);
        cameraIntents.remove(0);



        // Chooser of filesystem options.
        final Intent chooserIntent = Intent.createChooser(firstIntent, context.getString(R.string.u_choose_camera_source));

        // Add the camera options.
        chooserIntent.putExtra(Intent.EXTRA_INITIAL_INTENTS, cameraIntents.toArray(new Parcelable[cameraIntents.size()]));

        callerActivity.startActivityForResult(chooserIntent, SELECT_PICTURE_REQUEST_CODE);
    }

    public void handleActivityResult(Intent data) {
        final boolean isCamera;
        if (data == null) {
            isCamera = true;
        } else {
            final String action = data.getAction();
            if (action == null) {
                isCamera = false;
            } else {
                isCamera = action.equals(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            }
        }

        Uri selectedImageUri;
        if (isCamera) {
            selectedImageUri = outputFileUri;
        } else {
            selectedImageUri = data == null ? null : data.getData();
        }

        receiver.receive(selectedImageUri);
    }

    public interface CameraDialogReceiver {
        public void receive(Uri uri);
    }


}
