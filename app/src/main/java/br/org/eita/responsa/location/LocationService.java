package br.org.eita.responsa.location;

import android.Manifest;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;

import com.parse.ParseUser;

import java.util.List;
import java.util.Timer;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.MainActivity;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.data_manager.LastFetchedAt;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.model.UserResponsaObject;
import br.org.eita.responsa.util.ResponsaUtil;

public class LocationService extends Service {
    public static final String BROADCAST_ACTION = "br.org.eita.responsa.LOCATION_BROADCAST";
    private static final int TWO_MINUTES = 1000 * 60 * 2;

    public static int getLocationTrials = 0;
    public static final int MAX_TRIALS_BEFORE_GIVEUP = 5;


    public LocationManager locationManager;
    public ResponsaLocationListener listener;
    public Location previousBestLocation = null;

    private Timer timer;

    Intent intent;
    int counter = 0;

    BroadcastReceiver findNearestInitiativeReceiver;

    BroadcastReceiver responsaObjectFetchedReceiver = null;

    @Override
    public void onCreate() {
        ResponsaUtil.log("ResponsaLocation", "LocationService.onCreate");
        super.onCreate();
        intent = new Intent(BROADCAST_ACTION);

        final LocationService self = this;

        findNearestInitiativeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ResponsaUtil.log("ResponsaLocation", "Receive data for nearest initiative");

                List<ResponsaObject> nearestInitiativeList = ResponsaApplication.responsaDataManager.retrieveLoadedData("nearestInitiative", null);

                if (nearestInitiativeList.size() > 0) {
                    ResponsaObject nearest = nearestInitiativeList.get(0);

                    List<UserResponsaObject> uroList = ResponsaApplication.responsaDataManager.retrieveLoadedData("userResponsaObjectsFromInitiatives", null);

                    ResponsaUtil.log("ResponsaLocationService", "UserResponsaObject list size for nearest initiative " + nearest.getTitle() + "=" + uroList.size());

                    ResponsaUtil.log("ResponsaLocationService", "User Responsa Object by getUserResponsaObject=" + nearest.getUserResponsaObject());

                    ResponsaUtil.log("ResponsaLocationService", "FOUND NEAREST! " + nearest.getTitle());
                    LocationCoordinates currentLocation = LocationCoordinates.getCurrentLocation();

                    /* For debug purposes only:
                    UserResponsaObject nearestURO = nearest.getUserResponsaObject();
                    ResponsaUtil.log("ResponsaLocationService", "nearestURO by direct getUserResponsaObject: " + nearestURO.getObjectId());
                    List<UserResponsaObject> nearestUROList =  ResponsaApplication.responsaDataManager.retrieveLoadedData("userResponsaObjectsFromInitiatives",null);
                    ResponsaUtil.log("ResponsaLocationService", "nearestURO by list: " + nearestUROList.size());
                    */


                    //Na última posição encontrada a mesma iniciativa foi a mais próxima do usuário?
                    Boolean sameNearestInitiative = ResponsaUtil.equalsN(nearest, LocationCoordinates.latestNearest);

                    //O usuário está próximo o suficiente da inciativa, para acontecer o "cheguei"?
                    Boolean userIsNearEnough = nearest.getGeoLocation().distanceInKilometersTo(currentLocation.toParseGeoPoint()) < ((double) Constants.MAX_METERS_TO_NEAREST_TO_ALERT) / 1000;
                    ResponsaUtil.log("ResponsaLocationService", "DISTANCE TO NEAREST: " + ((Double) nearest.getGeoLocation().distanceInKilometersTo(currentLocation.toParseGeoPoint())).toString());


                    Boolean userIsEnoughTimeNearInitiative = sameNearestInitiative ?
                            LocationCoordinates.enteredInNearest != null && System.currentTimeMillis() - LocationCoordinates.enteredInNearest > Constants.TIME_IN_NEAREST_TO_ALERT :
                            false;

                    ResponsaUtil.log("ResponsaLocationService", "sameNearestInitiative: " + sameNearestInitiative.toString() + " | userIsNearEnough: " + userIsNearEnough.toString() + " | userIsEnoughTimeNearInitiative: " + userIsEnoughTimeNearInitiative.toString());

                    if (sameNearestInitiative && userIsNearEnough && userIsEnoughTimeNearInitiative) {
                        //if (true) {
                        nearestInitiativeList.get(0).checkAndSendChegueiBroadcast(self);
                    }

                    if (!sameNearestInitiative) {
                        LocationCoordinates.enteredInNearest = System.currentTimeMillis();
                    }
                    LocationCoordinates.latestNearest = nearestInitiativeList.get(0);
                } else {
                    ResponsaUtil.log("ResponsaLocationService", "DID NOT FOUND NEAREST ");
                }
            }
        };


        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        listener = new ResponsaLocationListener();
        startMonitoringLocationUpdates();

        ResponsaUtil.log("ResponsaLocation", "registering receiver for nearestInitiative");

        registerReceiver(findNearestInitiativeReceiver, new IntentFilter(ResponsaDataManager.getIntentDataUpdatedName("nearestInitiative")));

    }

    public static void restartLocationService(Context context) {
        Intent intent = new Intent(context, LocationService.class);
        context.stopService(intent);
        context.startService(intent);
    }

    public static Boolean locationPermissionGranted (Context context) {
        return (ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(context, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ResponsaUtil.log("ResponsaLocation", "LocationService.onStartCommand");

        return START_NOT_STICKY;
    }

    private void startMonitoringLocationUpdates() {


        String locationProviders = Settings.Secure.getString(getContentResolver(), Settings.Secure.LOCATION_PROVIDERS_ALLOWED);

        if (locationProviders == null || locationProviders.equals("")) {
            finishService();
            return;
        }

        getLocationTrials = 0;

        ResponsaUtil.log("ResponsaLocation", "LocationService.startMonitoringLocationUpdates");
        int minTime = 4000; //miliseconds
        int minDistance = 0; //meters
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, minTime, minDistance, listener);
            }

            if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
                locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, listener);
            }
        } else {
            ResponsaUtil.log("ResponsaLocation", "ANDROID >=6 detected");
            Context appContext = getApplicationContext();
            if (ActivityCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                ResponsaUtil.log("ResponsaLocation", "Has location permission, will request location updates now");

                if (locationManager.getAllProviders().contains(LocationManager.NETWORK_PROVIDER)) {
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, minTime, minDistance, listener);
                }

                if (locationManager.getAllProviders().contains(LocationManager.GPS_PROVIDER)) {
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, minTime, minDistance, listener);
                }
            } else {
                ResponsaUtil.log("ResponsaLocation", "Has NO location permission, will send broadcast ");

                Intent intent = new Intent(MainActivity.REQUEST_PERMISSION_DIALOG_ACTION);
                intent.putExtra("permissionsRequested",new String[]{Manifest.permission.ACCESS_FINE_LOCATION,Manifest.permission.ACCESS_COARSE_LOCATION});
                intent.putExtra("requestCode",MainActivity.PERMISSIONS_LOCATION);
                sendBroadcast(intent);
            }
        }


            /*if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }*/
    }

    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    protected boolean isBetterLocation(Location location, Location currentBestLocation) {
        if (currentBestLocation == null) {
            // A new location is always better than no location
            return true;
        }

        // Check whether the new location fix is newer or older
        long timeDelta = location.getTime() - currentBestLocation.getTime();
        boolean isSignificantlyNewer = timeDelta > TWO_MINUTES;
        boolean isSignificantlyOlder = timeDelta < -TWO_MINUTES;
        boolean isNewer = timeDelta > 0;

        // If it's been more than two minutes since the current location, use the new location
        // because the user has likely moved
        if (isSignificantlyNewer) {
            return true;
            // If the new location is more than two minutes older, it must be worse
        } else if (isSignificantlyOlder) {
            return false;
        }

        // Check whether the new location fix is more or less accurate
        int accuracyDelta = (int) (location.getAccuracy() - currentBestLocation.getAccuracy());
        boolean isLessAccurate = accuracyDelta > 0;
        boolean isMoreAccurate = accuracyDelta < 0;
        boolean isSignificantlyLessAccurate = accuracyDelta > 200;

        // Check if the old and new location are from the same provider
        boolean isFromSameProvider = isSameProvider(location.getProvider(),
                currentBestLocation.getProvider());

        // Determine location quality using a combination of timeliness and accuracy
        if (isMoreAccurate) {
            return true;
        } else if (isNewer && !isLessAccurate) {
            return true;
        } else if (isNewer && !isSignificantlyLessAccurate && isFromSameProvider) {
            return true;
        }
        return false;
    }


    /** Checks whether two providers are the same */
    private boolean isSameProvider(String provider1, String provider2) {
        if (provider1 == null) {
            return provider2 == null;
        }
        return provider1.equals(provider2);
    }


    @Override
    public void onDestroy() {
        ResponsaUtil.log("ResponsaLocation", "LocationService.onDestroy");
        if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
            locationManager.removeUpdates(listener);
        } else {
            Context appContext = getApplicationContext();
            if (ActivityCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED || ActivityCompat.checkSelfPermission(appContext, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED && listener != null) {
                locationManager.removeUpdates(listener);
            }
        }

        ResponsaUtil.log("ResponsaLocation", "unregistering receiver for nearestInitiative");
        unregisterReceiver(findNearestInitiativeReceiver);
        if (responsaObjectFetchedReceiver != null) {
            unregisterReceiver(responsaObjectFetchedReceiver);
        }
        super.onDestroy();
    }

    private void finishService() {
        int threadId = android.os.Process.getThreadPriority(android.os.Process.myTid());
        ResponsaUtil.log("ResponsaLocation", "(" + ((Integer) threadId).toString() + ") service will be finished now...");
        stopSelf();
        //return;
    }


    public class ResponsaLocationListener implements LocationListener
    {
        public void onLocationChanged(final Location loc)
        {
            ResponsaUtil.log("ResponsaLocation","ResponsaLocationListener.onLocationChanged Found location: " + loc.toString());

            getLocationTrials++;

            if (getLocationTrials > MAX_TRIALS_BEFORE_GIVEUP) {
                ResponsaUtil.log("ResponsaLocation", "GIVEUP trying to find location.");

                finishService();
                return;
            } else {
                ResponsaUtil.log("ResponsaLocation", "CONTINUE trying to find location.");
            }

            if(isBetterLocation(loc, previousBestLocation) && loc.getAccuracy() < Constants.MIN_ACCURACY_TO_CHANGE_POSITION_IN_METERS) {
                ResponsaUtil.log("ResponsaLocation", "ResponsaLocationListener.onLocationChanged Found better, valid location ");
                intent.putExtra("Location", loc);
                sendBroadcast(intent);

                LocationCoordinates coordinates = LocationCoordinates.fromLocation(loc);
                coordinates.setCoordinateType(LocationCoordinates.CoordinateType.BY_GPS);

                LocationCoordinates.setCurrentLocation(coordinates);

                ResponsaUtil.log("ResponsaLocation", "Loading nearest initiative... RDM= " + (ResponsaApplication.responsaDataManager == null ? "null" : "not null"));

                // Responsa should only send alerts if the user has already signed up to use the app.
                if (ParseUser.getCurrentUser()!=null) {
                    ResponsaApplication.responsaDataManager.load("nearestInitiative", null, (String) null);
                }
                finishService();
                return;
            } else if (loc.getAccuracy() >= Constants.MIN_ACCURACY_TO_CHANGE_POSITION_IN_METERS) {
                ResponsaUtil.log("ResponsaLocation","ResponsaLocationListener.onLocationChanged Accuracy is bad");
            } else {
                ResponsaUtil.log("ResponsaLocation","ResponsaLocationListener.onLocationChanged Not better location ");
            }
        }

        public void onProviderDisabled(String provider)
        {
            //Toast.makeText(getApplicationContext(), "Gps Disabled", Toast.LENGTH_SHORT).show();
        }


        public void onProviderEnabled(String provider)
        {
            
            //Toast.makeText( getApplicationContext(), "Gps Enabled", Toast.LENGTH_SHORT).show();
        }


        public void onStatusChanged(String provider, int status, Bundle  extras)
        {

        }
    }
}