package br.org.eita.responsa.util;

import java.util.Date;

public interface OnSelectedDateOrTimeCallback {
    public void onSelect(Date newDate);
}
