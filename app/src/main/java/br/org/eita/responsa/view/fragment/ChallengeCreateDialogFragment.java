package br.org.eita.responsa.view.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import br.org.eita.responsa.R;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.model.ChallengeAnswer;

public class ChallengeCreateDialogFragment extends DialogFragment{

    public static ClickDialogYesCallback clickDialogYesCallback;

    public interface ClickDialogYesCallback {
        public void onClick();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        Challenge challenge = Challenge.currentChallenge;

        builder.setTitle(R.string.u_create_challenge_title);

        builder.setMessage(getString(R.string.u_ask_challenge_create, challenge.getTitle(), challenge.getDescription()));

        //TODO set the location for the activity

        builder.setPositiveButton(R.string.u_ask_confirm_create_dialog_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (clickDialogYesCallback != null) {
                    clickDialogYesCallback.onClick();
                }
            }
        });


        builder.setNegativeButton(R.string.u_ask_confirm_answer_dialog_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        return builder.create();
    }
}
