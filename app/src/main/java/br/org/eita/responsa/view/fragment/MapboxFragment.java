package br.org.eita.responsa.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;

import br.org.eita.responsa.R;
import br.org.eita.responsa.location.LocationCoordinates;

public class MapboxFragment extends BaseFragment {


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_mapbox, container, false);

        MapView mv = (MapView) view.findViewById(R.id.mapview);
        //mapView.setTileSource(new MapboxTileLayer("examples.map-i875mjb7"));
        final LocationCoordinates currentLocation = LocationCoordinates.getVisibleLocation();
        if (currentLocation != null) {
            mv.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(MapboxMap mapboxMap) {
                    mapboxMap.setCameraPosition(new CameraPosition.Builder()
                            .target(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()))
                            .zoom(15)
                            .build());
                }
            });
        }

        return view;
    }
}
