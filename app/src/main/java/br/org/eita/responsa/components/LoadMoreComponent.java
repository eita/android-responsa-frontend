package br.org.eita.responsa.components;

import android.app.Activity;
import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.util.ExtraFilters;

public class LoadMoreComponent extends LinearLayout {


    private Activity activity;
    private Integer page = 0;
    private String view;
    private String targetObjectId;
    private String targetView;
    private View loadMoreView;
    private ExtraFilters extraFilters = null;
    Button btnLoadMoreView;
    ProgressBar progressBarLoadMoreView;

    public LoadMoreComponent(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadMoreView = inflater.inflate(R.layout.progress_load_more, this);

        // Load More view, which has the progressBar and the btnLoadMore:
        //loadMoreView = activity.getLayoutInflater().inflate(R.layout.progress_load_more, null);

        // ProgressBarView
        progressBarLoadMoreView = (ProgressBar) this.loadMoreView.findViewById(R.id.progressBar);
        progressBarLoadMoreView.setIndeterminate(true);

        // Button "Load More" view:
        btnLoadMoreView = (Button) this.loadMoreView.findViewById(R.id.btnLoadMore);

        btnLoadMoreView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
            btnLoadMoreView.setVisibility(View.GONE);
            progressBarLoadMoreView.setVisibility(View.VISIBLE);
            ResponsaApplication.responsaDataManager.load(view, targetObjectId , targetView, page + 1, extraFilters);
            }
        });

    }

    public void initParams(String view, String targetObjectId) {
        initParams(view, targetObjectId, view, 0, null);
    }

    public void initParams(String view, String targetObjectId, ExtraFilters extraFilters) {
        initParams(view, targetObjectId, view, 0, extraFilters);
    }

    public void initParams(String view, String targetObjectId, String targetView) {
        initParams(view, targetObjectId, targetView, 0, null);
    }
    public void initParams(String view, String targetObjectId, String targetView, Integer page, ExtraFilters extraFilters) {
        setView(view);
        setTargetObjectId(targetObjectId);
        setTargetView(targetView);
        setPage(page);
        setExtraFilters(extraFilters);
    }
    public void setPage(Integer page) {
        this.page = page;
    }
    public void setView(String view) {
        this.view = view;
    }
    public void setTargetObjectId(String targetObjectId) {
        this.targetObjectId = targetObjectId;
    }
    public void setTargetView(String targetView) {
        this.targetView = targetView;
    }
    public void setExtraFilters(ExtraFilters extraFilters) {
        this.extraFilters = extraFilters;
    }

    public void startProgressBar() {
        btnLoadMoreView.setVisibility(GONE);
        progressBarLoadMoreView.setVisibility(VISIBLE);
    }
    public void stopProgressBar() {
        progressBarLoadMoreView.setVisibility(GONE);
    }
    public void showBtnLoadMore() {
        btnLoadMoreView.setVisibility(VISIBLE);
        progressBarLoadMoreView.setVisibility(GONE);
    }
    public void hideBtnLoadMore() {
        btnLoadMoreView.setVisibility(GONE);
    }



}
