package br.org.eita.responsa.receivers;


import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;

import br.org.eita.responsa.R;
import br.org.eita.responsa.location.LocationService;
import br.org.eita.responsa.location.PeriodicLocationUpdateService;
import br.org.eita.responsa.util.ResponsaUtil;

//Set up Repeating task
// https://technology.jana.com/2014/10/28/periodic-background-tasks-in-android/
public class BootAndUpdateReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ResponsaUtil.log("P-ResponsaLocation", "BOOT AND UPDATE RECEIVER");

        Intent serviceIntent = new Intent(context, PeriodicLocationUpdateService.class);
        context.startService(serviceIntent);
    }
}
