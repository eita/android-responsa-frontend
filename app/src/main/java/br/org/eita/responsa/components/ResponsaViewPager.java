package br.org.eita.responsa.components;


import android.content.Context;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.mapbox.mapboxsdk.maps.MapView;

public class ResponsaViewPager extends ViewPager {

    private Boolean swipeEnabled = true;


    public ResponsaViewPager(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public ResponsaViewPager(Context context) {
        super(context);
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (swipeEnabled)
            return super.onInterceptTouchEvent(ev);
        return false;
    }

    @Override
    public boolean onTouchEvent(MotionEvent ev) {
        if (swipeEnabled)
            return super.onTouchEvent(ev);
        return false;
    }


    public void lockSwipe() {
        this.swipeEnabled = false;
    }

    public void unlockSwipe() {
        this.swipeEnabled = true;
    }

}
