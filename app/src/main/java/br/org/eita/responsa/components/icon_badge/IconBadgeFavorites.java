package br.org.eita.responsa.components.icon_badge;


import android.content.Context;
import android.util.AttributeSet;

import br.org.eita.responsa.R;

public class IconBadgeFavorites extends ModelIconBadge {
    public IconBadgeFavorites(Context context) {
        this(context, null);
    }

    public IconBadgeFavorites(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IconBadgeFavorites(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setIcon(R.xml.ic_fs_g_favoritou);
        setModelField("countFavorites");
    }
}
