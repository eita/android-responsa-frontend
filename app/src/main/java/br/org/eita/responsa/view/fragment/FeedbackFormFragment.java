package br.org.eita.responsa.view.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.parse.ParseException;
import com.parse.ParseUser;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.Feedback;
import br.org.eita.responsa.model.util.ResponsaSaveCallback;
import br.org.eita.responsa.util.ExtraFilters;
import br.org.eita.responsa.util.ResponsaUtil;


public class FeedbackFormFragment extends Fragment {

    EditText userTextInput;
    Button feedbackFormSubmit;

    String view = null;
    String extraFiltersUniqueId = null;
    String viewObjectId = null;
    String viewClass = null;

    public void setData(String view, String viewObjectId, String extraFiltersUniqueId, String viewClass) {
        this.view = view;
        this.viewObjectId = viewObjectId;
        this.extraFiltersUniqueId = extraFiltersUniqueId;
        this.viewClass = viewClass;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)   {
        View inflatedView = inflater.inflate(R.layout.fragment_feedback_form, container, false);

        findAllViewsByIds(inflatedView);

        feedbackFormSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Feedback feedback = sweepData();

                if (feedback.getUserText() == null || feedback.getUserText().isEmpty()) {
                    Toast.makeText(getActivity(), R.string.u_feedback_error_no_message, Toast.LENGTH_SHORT).show();
                } else {
                    ResponsaApplication.responsaDataManager.saveObject("feedback", null, feedback, new ResponsaSaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e != null) {
                                ResponsaUtil.log("feedback","Error on save: " + e.getMessage());
                            }
                        }
                    });

                    Toast.makeText(getActivity(), R.string.u_feedback_sent, Toast.LENGTH_SHORT).show();

                    getActivity().finish();
                }
            }

        });

        return inflatedView;
    }

    private void findAllViewsByIds(View parentView) {
        userTextInput = (EditText) parentView.findViewById(R.id.userTextInput);
        feedbackFormSubmit = (Button) parentView.findViewById(R.id.feedbackFormSubmit);
    }

    private Feedback sweepData() {
        Feedback feedback = new Feedback();

        feedback.setUser(ParseUser.getCurrentUser());
        feedback.setUserText(userTextInput.getText().toString());

        feedback.setView(this.view);
        feedback.setViewClass(this.viewClass);
        feedback.setViewObjectId(this.viewObjectId);
        if (view != null) {
            try {
                feedback.setCachedData(new Gson().toJson(ResponsaApplication.responsaDataManager.retrieveLoadedMetaData(this.view,this.viewObjectId)));
                //feedback.setLastFetchedAt(new Gson().toJson(ResponsaApplication.responsaDataManager.lastFetchedAt.getLastFetchedAtDate(this.view, this.extraFiltersUniqueId)));
            } catch (NullPointerException e) {
                feedback.setCachedData("ERROR: this key could not be found in cached data");
            }

        }

        return feedback;
    }
}
