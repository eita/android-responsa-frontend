package br.org.eita.responsa.model;


import android.content.Intent;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONObject;

import java.util.Date;
import java.util.List;

import br.org.eita.responsa.ResponsaApplication;

@ParseClassName("UserResponsaObject")
public class UserResponsaObject extends ParseObject {

    public ParseObject getResponsaObject() {
        return getParseObject("responsaObject");
    }

    public void setResponsaObject(ResponsaObject responsaObject) {
        if (responsaObject == null) {
            this.put("responsaObject", JSONObject.NULL);
        } else {
            this.put("responsaObject", responsaObject);
            responsaObject.setUserResponsaObject(this);
        }
    }

    public ParseObject getUser() {
        return getParseObject("user");
    }

    public void setUser(ParseObject user) {
        if (user == null) {
            this.put("user", JSONObject.NULL);
        } else {
            this.put("user", user);
        }
    }

    public Date getLastAlertChegada() {
        return getDate("lastAlertChegada");
    }

    public void setLastAlertChegada(Date lastAlertChegada) {
        if (lastAlertChegada == null) {
            this.put("lastAlertChegada", JSONObject.NULL);
        } else {
            this.put("lastAlertChegada", lastAlertChegada);
        }
    }

    public Date getGostou() {
        return getDate("gostou");
    }

    public void setGostou(Date gostou) {
        if (gostou == null) {
            this.put("gostou", JSONObject.NULL);
        } else {
            this.put("gostou", gostou);
        }
    }

    public Boolean getFavoritou() {
        return getBoolean("favoritou");
    }

    public void setFavoritou(Boolean favoritou) {
        Notification.toggleChannelSubscription(Notification.RESPONSAOBJECT_CHANNEL, getResponsaObject().getObjectId(), favoritou);
        this.put("favoritou", favoritou);
    }

    public Date getFavoritouChangedAt() {
        return getDate("favoritouChangedAt");
    }

    public void setFavoritouChangedAt(Date favoritouChangedAt) {
        if (favoritouChangedAt == null) {
            this.put("favoritouChangedAt", JSONObject.NULL);
        } else {
            this.put("favoritouChangedAt", favoritouChangedAt);
        }
    }

    public Boolean getMarcouPresenca() {
        return getBoolean("marcouPresenca");
    }

    public void setMarcouPresenca(Boolean marcouPresenca) {
        if (marcouPresenca == null) {
            this.put("marcouPresenca", JSONObject.NULL);
        } else {
            this.put("marcouPresenca", marcouPresenca);
        }
    }

    public Date getMarcouPresencaChangedAt() {
        return getDate("marcouPresencaChangedAt");
    }

    public void setMarcouPresencaChangedAt(Date marcouPresencaChangedAt) {
        if (marcouPresencaChangedAt == null) {
            this.put("marcouPresencaChangedAt", JSONObject.NULL);
        } else {
            this.put("marcouPresencaChangedAt", marcouPresencaChangedAt);
        }
    }
}
