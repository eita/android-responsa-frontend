package br.org.eita.responsa.view.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.model.ResponsaObject;

public class ArrivalQuestionDialogFragment extends DialogFragment{

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        final ResponsaObject place = LocationCoordinates.latestNearest;

        builder.setTitle(getString(R.string.u_ask_cheguei_dialog_title,place.getTitle()));

        //TODO set the location for the activity

        builder.setPositiveButton(R.string.u_ask_cheguei_dialog_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                ResponsaApplication.responsaViewManager.openFichaSintese(getActivity(), place, true);
                place.chegar(getActivity());
            }
        });


        builder.setNegativeButton(R.string.u_ask_cheguei_dialog_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        return builder.create();
    }
}
