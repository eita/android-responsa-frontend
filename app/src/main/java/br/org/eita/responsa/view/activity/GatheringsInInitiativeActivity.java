package br.org.eita.responsa.view.activity;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;

import br.org.eita.responsa.R;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.view.fragment.ResponsaObjectFormFragment;
import br.org.eita.responsa.view.fragment.ResponsaObjectListFragment;

public class GatheringsInInitiativeActivity extends BaseActivity {

    ResponsaObject placeInitiative;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_gatherings_list);

        placeInitiative = ResponsaObject.currentResponsaObject;

        if (placeInitiative == null) {
            finish();
            return;
        }

        Toolbar rToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(rToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getString(R.string.gatherings_in_location, placeInitiative.getTitle()));

        ResponsaObjectListFragment f = new ResponsaObjectListFragment();


        Bundle args = getIntent().getExtras();
        args.putBoolean("hideHeaderPadding",true);

        f.setArguments(args);

        // Display the fragment as the main content.
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, f)
                .commit();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void openGatheringForm(View view) {
        Intent intent = new Intent(this, ResponsaObjectFormActivity.class);
        intent.putExtra("type","gathering");
        intent.putExtra("inPlaceInitiative",true);
        startActivity(intent);
    }

}
