package br.org.eita.responsa.model;


import android.content.Context;
import android.content.Intent;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.util.ResponsaFindCallback;
import br.org.eita.responsa.model.util.ResponsaSaveCallback;
import br.org.eita.responsa.model.validation.ValidationError;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.view.fragment.FavoritesFragment;

@ParseClassName("Challenge")
public class Challenge extends ParseObject implements VotableAnswerable {

    public static Challenge currentChallenge;

    private ChallengeUserJoin challengeUserJoin;

    private ArrayList<OnVotableChange> onVotableChanges = new ArrayList<OnVotableChange>();
    private ArrayList<OnAnswerableChange> onAnswerableChanges = new ArrayList<OnAnswerableChange>();

    private String layoutTitleViewString = null;



    public ParseUser getAuthor() {
        return getParseUser("author");
    }

    public void setAuthor(ParseUser author) {
        if (author == null) {
            this.put("author", JSONObject.NULL);
        } else {
            this.put("author", author);
        }
    }

    public ParseObject getCorrectAnswer() {
        return getParseObject("correctAnswer");
    }

    public void setCorrectAnswer(ChallengeAnswer correctAnswer) {
        if (correctAnswer == null) {
            this.put("correctAnswer", JSONObject.NULL);
        } else {
            this.put("correctAnswer", correctAnswer);
        }
    }

    public Boolean getPublished() {
        return getBoolean("published");
    }

    public void setPublished(Boolean published) {
        if (published == null) {
            this.put("published", JSONObject.NULL);
        } else {
            this.put("published", published);
        }
    }

    public Integer getUpVotes() {
        return getInt("upVotes");
    }

    public void setUpVotes(Integer upVotes) {
        if (upVotes == null) {
            this.put("upVotes", JSONObject.NULL);
        } else {
            this.put("upVotes", upVotes);
        }
    }

    public Integer getDownVotes() {
        return (getRelevance() - getUpVotes()) * -1;
    }

    public String getTitle() {
        return getString("title");
    }

    public void setTitle(String title) {
        if (title == null) {
            this.put("title", JSONObject.NULL);
        } else {
            this.put("title", title);
        }
    }

    public String getDescription() {
        return getString("description");
    }

    public void setDescription(String description) {
        if (description == null) {
            this.put("description", JSONObject.NULL);
        } else {
            this.put("description", description);
        }
    }

    public String getTerritoryId() {
        return getString("territoryId");
    }

    public void setTerritoryId(String territoryId) {
        if (territoryId == null) {
            this.put("territoryId", JSONObject.NULL);
        } else {
            this.put("territoryId", territoryId);
        }
    }

    public Integer getAnswerCount() {
        return getInt("answerCount");
    }

    public void setAnswerCount(Integer answerCount) {
        if (answerCount == null) {
            this.put("answerCount", JSONObject.NULL);
        } else {
            this.put("answerCount", answerCount);
        }
        propagateAnswerableChanges();
    }

    public String getTerritoryLabel() {
        return getString("territoryLabel");
    }

    public void setTerritoryLabel(String territoryLabel) {
        if (territoryLabel == null) {
            this.put("territoryLabel", JSONObject.NULL);
        } else {
            this.put("territoryLabel", territoryLabel);
        }
    }

    public String getTerritoryRegionId() {
        return getString("territoryRegionId");
    }

    public void setTerritoryRegionId(String territoryRegionId) {
        if (territoryRegionId == null) {
            this.put("territoryRegionId", JSONObject.NULL);
        } else {
            this.put("territoryRegionId", territoryRegionId);
        }
    }

    public ChallengeUserJoin getChallengeUserJoin() {
        if (challengeUserJoin == null) {
            challengeUserJoin = ResponsaApplication.responsaDataManager.createChallengeUserJoin(this);
        }
        return challengeUserJoin;
    }

    public void setChallengeUserJoin(ChallengeUserJoin challengeUserJoin) {
        this.challengeUserJoin = challengeUserJoin;
    }

    public void incrementAnswerCount() {
        this.increment("answerCount");
        this.saveEventually();
        this.propagateAnswerableChanges();
    }

    public void decrementAnswerCount() {
        this.increment("answerCount",-1);
        this.saveEventually();
        this.propagateAnswerableChanges();
    }
/*
    public void markChallengeVoteFetched() {
        this.challengeUserJoinFetched = true;
        for (OnVotableChange ch: onVotableChanges) {
            ch.onChange(this);
        }
    }
    */

    public ArrayList<ValidationError> validate(Context c) {
        ArrayList<ValidationError> errors = new ArrayList<ValidationError>();
        
        String title = this.getTitle();
        if (title == null || title == JSONObject.NULL || (title instanceof String && title.isEmpty())) {
            errors.add(new ValidationError("Challenge Form Submit Error",c.getString(R.string.v_challenge_no_title)));
        }

        String description = this.getDescription();
        if (description == null || description == JSONObject.NULL || (description instanceof String && description.isEmpty())) {
            errors.add(new ValidationError("Challenge Form Submit Error",c.getString(R.string.v_challenge_no_description)));
        }

        return errors;
    }

    public Boolean toggleFavoritar() {
        final ChallengeUserJoin challengeUserJoin = getChallengeUserJoin();
        final Boolean favoritouIsFirstChange = challengeUserJoin.getFavoriteChangedAt() == null;
        challengeUserJoin.setFavorite(!challengeUserJoin.getFavorite());
        challengeUserJoin.setFavoriteChangedAt(new Date());
        final Challenge self = this;
        ResponsaApplication.responsaDataManager.saveObject("userJoinFromChallenge", getObjectId(), "challenge", challengeUserJoin, true, new ResponsaSaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null && challengeUserJoin.getFavorite() && favoritouIsFirstChange) {
                    ResponsaApplication.getAppContext().sendBroadcast(new Intent(FavoritesFragment.ON_FAVORITES_UPDATED));

                    Notification.sendNotification(ResponsaApplication.getAppContext(), "favoriteChallenge", self);
                }
            }
        });

        return challengeUserJoin.getFavorite();
    }

    @Override
    public void setVoteState(Integer newValue) {

        Integer oldValue = getVoteState();

        /*
        old  new         op
        -1   -1           -              -
        -1    0           down--         relevance++
        -1    1           down--, up++   relevance+=2
        0    -1           down++         relevance--
        0     0           -              -
        0     1                   up++   relevance++
        1    -1           down++, up--,  relevance-=2
        1     0                   up--   relevance--
        1     1           -
         */

        if (oldValue != newValue) {
            if (oldValue > 0) this.increment("upVotes",   -1);
            if (newValue > 0) this.increment("upVotes",    1);

            Integer relevanceDiff = newValue - oldValue;
            if (relevanceDiff != 0) this.increment("relevance",relevanceDiff);
        }

        ChallengeUserJoin challengeUserJoin = getChallengeUserJoin();
        challengeUserJoin.setVote(newValue);
        challengeUserJoin.saveEventually();
        ResponsaApplication.responsaDataManager.saveObject("userJoinFromChallenge", this.getObjectId(), "challenge", challengeUserJoin, true, null);

        final Challenge self = this;
        ResponsaApplication.responsaDataManager.saveObject("challenge", this.getObjectId(), this, new ResponsaSaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Notification.sendNotification(ResponsaApplication.getAppContext(), "voteChallenge", self);
                }
            }
        });

        for (OnVotableChange ch: onVotableChanges) {
            ch.onChange(this);
        }
    }

    @Override
    public Integer getVoteState() {
        return getChallengeUserJoin().getVote();
    }

    public void setRelevance(Integer relevance) {
        if (relevance == null) {
            this.put("relevance", JSONObject.NULL);
        } else {
            this.put("relevance", relevance);
        }
    }

    @Override
    public Integer getRelevance() {
        return getInt("relevance");
    }

    @Override
    public void onVotableChange(OnVotableChange onVotableChange) {
        onVotableChanges.add(onVotableChange);
    }

    @Override
    public void onAnswerableChange(OnAnswerableChange onAnswerableChange) {
        onAnswerableChanges.add(onAnswerableChange);
    }

    @Override
    public boolean getVotesEnabled() {
        return true;
    }

    @Override
    public Integer getTotalVotes() {
        //rel - up = -down
        //res = up + down
        //res = up - rel + up
        //res = 2up - rel
        return 2 * this.getUpVotes() - this.getRelevance();
    }

    @Override
    public Integer getTotalAnswers() {
        return (getAnswerCount() == null) ? 0 : getAnswerCount();
    }

    public void propagateAnswerableChanges () {
        for (OnAnswerableChange ch: onAnswerableChanges) {
            ch.onChange(this);
        }
    }

    public static void getById(String objectId, Boolean local, GetCallback<Challenge> cb) {
        ParseQuery<Challenge> query = new ParseQuery(Challenge.class);
        query.include("author");
        if (local) {
            query.fromLocalDatastore();
        }
        query.getInBackground(objectId, cb);
    }

    public String getLayoutTitleViewString() {
        return layoutTitleViewString;
    }

    public void setLayoutTitleViewString(String layoutTitleViewString) {
        this.layoutTitleViewString = layoutTitleViewString;
    }
}
