package br.org.eita.responsa.view.fragment;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.codetroopers.betterpickers.timepicker.TimePickerBuilder;
import com.codetroopers.betterpickers.timepicker.TimePickerDialogFragment;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.components.CategorySpinner;
import br.org.eita.responsa.components.FormAddress;
import br.org.eita.responsa.components.InitiativeDetails;
import br.org.eita.responsa.components.TerritoryChooser;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.InitiativeCategory;
import br.org.eita.responsa.model.Notification;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.model.util.ResponsaSaveCallback;
import br.org.eita.responsa.model.validation.ValidationError;
import br.org.eita.responsa.util.OnSelectedDateOrTimeCallback;
import br.org.eita.responsa.view.activity.ResponsaObjectFormActivity;

public class ResponsaObjectFormFragment extends Fragment {
    EditText roTitle;
    EditText roDescription;
    TerritoryChooser roTerritory;
    EditText roPhone;
    EditText roWebsite;
    EditText roEmail;
    EditText roFacebook;
    Button roFormSubmit;
    FormAddress roAddress;

    RelativeLayout extra_information;
    RelativeLayout initiativeInformation;

    //Initiative only
    CategorySpinner roCategory;

    //Gathering only
    EditText roStartDate;
    EditText roStartTime;
    EditText roFinishDate;
    EditText roFinishTime;

    TextView roLocationLabel;


    HomeTerritory homeTerritory;
    InitiativeCategory selectedCategory = null;
    LocationCoordinates locationCoordinates = null;

    ScrollView scrollView;

    ResponsaObject responsaObject;

    String responsaObjectType;
    String objectId;

    // If we are filling a gathering which belongs to an initative, placeInitiative will be no null
    ResponsaObject placeInitiative;

    InitiativeDetails roPlaceInitiativeDetails;

    Boolean startDateSelected = false;
    Boolean startTimeSelected = false;
    Boolean finishDateSelected = false;
    Boolean finishTimeSelected = false;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        responsaObjectType = getArguments().getString("type");
        objectId = getArguments().getString("objectId");
        placeInitiative = ResponsaObject.currentResponsaObject;

        View view = null;

        if ("initiative".equals(responsaObjectType)) {
            view = inflater.inflate(R.layout.fragment_initiative_form, container, false);
        } else { /* responsaObjectType == "gathering" */
            view = inflater.inflate(R.layout.fragment_gathering_form, container, false);
        }

        findAllViewsByIds(view);

        if (objectId == null) {
            responsaObject = new ResponsaObject();
            responsaObject.setType(responsaObjectType);
            if (placeInitiative == null) {
                setNewTerritory(ResponsaApplication.territoryDb.findTerritoryById(ParseUser.getCurrentUser().getString("territoryId")));
                roTerritory.setSelectedTerritory(homeTerritory);
            }
        } else {
            responsaObject = ResponsaObjectFormActivity.transientResponsaObject;
            ResponsaObjectFormActivity.transientResponsaObject = null;
            populateData(responsaObject);
            roFormSubmit.setText(R.string.finish_edit);
        }

        if ("initiative".equals(responsaObjectType)) {
            roCategory.setOnSelectCategory(new CategorySpinner.OnSelectCategoryCallback() {
                @Override
                public void onSelect(InitiativeCategory initiativeCategory) {
                    selectedCategory = initiativeCategory;
                    validate(false);
                }
            });

        } else { /* responsaObjectType == "gathering" */
            roStartDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDatePicker(responsaObject.getDateTimeStart(), new OnSelectedDateOrTimeCallback() {
                        @Override
                        public void onSelect(Date newDate) {
                            responsaObject.setDateTimeStart(newDate);
                            //TODO I18N
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            roStartDate.setText(sdf.format(newDate));

                            startDateSelected = true;
                            validate(false);

                            if (!startTimeSelected) {
                                openStartTimeDialog();
                            }
                        }
                    });
                }
            });

            roFinishDate.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    showDatePicker(responsaObject.getDateTimeFinish(), new OnSelectedDateOrTimeCallback() {
                        @Override
                        public void onSelect(Date newDate) {
                            responsaObject.setDateTimeFinish(newDate);
                            //TODO I18N
                            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                            roFinishDate.setText(sdf.format(newDate));

                            finishDateSelected = true;
                            validate(false);
                        }
                    });
                }
            });

            roStartTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openStartTimeDialog();

                }
            });

            roFinishTime.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openEndTimeDialog();
                }
            });

            //TODO not working properly in edit..
            if (placeInitiative != null) { //gathering with placeInitiative
                initiativeInformation.setVisibility(View.VISIBLE);
                roPlaceInitiativeDetails.setInitiative(placeInitiative);
                responsaObject.setPlaceInitiative(placeInitiative);
                //roTerritory.setVisibility(View.GONE);
                //roTerritoryLabel.setVisibility(View.GONE);

                setNewTerritory(placeInitiative.getResponsaTerritory());
                roTerritory.setSelectedTerritory(homeTerritory);
                locationCoordinates = placeInitiative.getLocationCoordinates();

                roLocationLabel.setVisibility(View.GONE);
                roTerritory.setVisibility(View.GONE);

                responsaObject.copyAddressFrom(ResponsaObject.currentResponsaObject);
                roAddress.setEditable(false);
                roAddress.setNeighborhoodVisibility(false);

            }
        }

        roAddress.setAddressable(responsaObject);

        roAddress.onChangeTerritory(new FormAddress.OnChangeTerritoryCallback() {
            @Override
            public void onChange(HomeTerritory homeTerritory) {
                //Set Territory ID
                if (homeTerritory != null) {
                    roTerritory.setSelectedTerritory(homeTerritory);
                }
            }
        });

        roFormSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                sweepData();
                if (!validate(true)) return;

                ArrayList<ValidationError> errors = responsaObject.validate(getActivity());
                if (errors != null) {

                } else {
                    responsaObject.setOwner(ParseUser.getCurrentUser());

                    //TODO add moderation procedures
                    responsaObject.setPublished(!"initiative".equals(responsaObject.getType()));

                    if (responsaObject.getType().equals("gathering")) {
                        responsaObject.setCategory("gathering");
                    } else {
                        responsaObject.setSource("responsa");
                    }

                    final Boolean newObject = responsaObject.getObjectId() == null;

                    ResponsaApplication.responsaDataManager.saveObject(responsaObjectType, responsaObject.getObjectId(), responsaObject, new ResponsaSaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            String responsaObjectId = responsaObject.getObjectId();

                            if (newObject) {
                                try {
                                    responsaObject.setLocalId(responsaObjectId);
                                    JSONArray arr = new JSONArray();
                                    JSONObject obj = new JSONObject();
                                    obj.put("local_id",responsaObjectId);
                                    obj.put("source","responsa");
                                    arr.put(obj);
                                    responsaObject.setSources(arr);
                                    responsaObject.saveEventually();
                                } catch (JSONException e1) {
                                    e1.printStackTrace();
                                }
                            }

                            // Subscribe the author to the created ResponsaObject Channel:
                            Notification.subscribeToChannel(Notification.RESPONSAOBJECT_CHANNEL, responsaObjectId);

                            // Send notification to user and channel(s) related to this object:
                            if ("initiative".equals(responsaObject.getType())) {
                                Notification.sendNotification(ResponsaApplication.getAppContext(), "registerInitiative", responsaObject);
                            } else {
                                if (responsaObject.getPlaceInitiative() == null) {
                                    Notification.sendNotification(ResponsaApplication.getAppContext(), "registerGathering", responsaObject);
                                } else {
                                    Notification.sendNotification(ResponsaApplication.getAppContext(), "registerGatheringInInitiative", responsaObject);
                                }
                            }
                        }
                    });

                    if (newObject) {
                        ResponsaApplication.responsaViewManager.openFichaSintese(getActivity(), responsaObject.getType());
                    }

                    getActivity().finish();
                }
            }
        });

        roTerritory.onChangeSelectedTerritory(new TerritoryChooser.OnChangeTerritoryListener() {
            @Override
            public void onChange(HomeTerritory territory) {
                setNewTerritory(territory);
                validate(false);
            }
        });

        TextWatcher tw = new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validate(false);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        };

        roTitle.addTextChangedListener(tw);
        roDescription.addTextChangedListener(tw);
        roPhone.addTextChangedListener(tw);
        roWebsite.addTextChangedListener(tw);
        roFacebook.addTextChangedListener(tw);
        roEmail.addTextChangedListener(tw);

        return view;
    }

    private void openStartTimeDialog() {
        showTimePicker(responsaObject.getDateTimeStart(), new OnSelectedDateOrTimeCallback() {
            @Override
            public void onSelect(Date newDate) {
                responsaObject.setDateTimeStart(newDate);

                //TODO I18N
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                roStartTime.setText(sdf.format(newDate));

                startTimeSelected = true;

                if (responsaObject.getDateTimeFinish() == null) {
                    Calendar cal = Calendar.getInstance(); // creates calendar
                    cal.setTime(newDate); // sets calendar time/date
                    cal.add(Calendar.HOUR_OF_DAY, 1); // adds one hour

                    updateFinishDateTime(cal.getTime());
                }

                validate(false);
            }
        });
    }

    private void updateFinishDateTime(Date finishDateTime) {
        responsaObject.setDateTimeFinish(finishDateTime);

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        roFinishDate.setText(sdf.format(finishDateTime));
        finishDateSelected = true;

        //TODO I18N
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");
        roFinishTime.setText(sdf2.format(finishDateTime));
        finishTimeSelected = true;

    }

    private void openEndTimeDialog() {
        showTimePicker(responsaObject.getDateTimeFinish(), new OnSelectedDateOrTimeCallback() {
            @Override
            public void onSelect(Date newDate) {
                responsaObject.setDateTimeFinish(newDate);

                //TODO I18N
                SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");
                roFinishTime.setText(sdf.format(newDate));

                finishTimeSelected = true;
                validate(false);
            }
        });
    }

    private void showTimePicker(final Date dateTimeStart, final OnSelectedDateOrTimeCallback callback) {
        TimePickerBuilder tpb = new TimePickerBuilder()
                .setFragmentManager(getChildFragmentManager())
                .setStyleResId(R.style.BetterPickersDialogFragment_Light);

        tpb.addTimePickerDialogHandler(new TimePickerDialogFragment.TimePickerDialogHandler() {
            @Override
            public void onDialogTimeSet(int reference, int hour, int minute) {

                Date roDate = dateTimeStart;
                if (roDate == null) roDate = new Date();

                Calendar cal = Calendar.getInstance();

                cal.setTime(roDate);

                cal.set(Calendar.HOUR_OF_DAY, hour);
                cal.set(Calendar.MINUTE, minute);

                callback.onSelect(cal.getTime());
            }
        });

        tpb.show();
    }

    private void showDatePicker(final Date initDate, final OnSelectedDateOrTimeCallback callback ) {

        FragmentManager fm = getChildFragmentManager();

        DatePickerDialogFragment.initDate = initDate;
        DatePickerDialogFragment.callback = callback;

        DatePickerDialogFragment dialogFragment = new DatePickerDialogFragment();

        dialogFragment.show(fm,"fragment_start_datepicker");

        /*
        CalendarDatePickerDialog calendarDatePickerDialog = CalendarDatePickerDialog
                .newInstance(listener, cal.get(Calendar.YEAR), cal.get(Calendar.MONTH),
                        cal.get(Calendar.DAY_OF_MONTH));

        calendarDatePickerDialog.setYearRange(cal.get(Calendar.YEAR), cal.get(Calendar.YEAR) + 3);

        calendarDatePickerDialog.show(fm, "fragment_start_datepicker");
        */
    }

    private void setNewTerritory(HomeTerritory newTerritory) {
        homeTerritory = newTerritory;
        if (newTerritory != null && newTerritory.lat != null && newTerritory.lng != null ) {
            locationCoordinates = newTerritory.getLocationCoordinates();
            responsaObject.setGeoLocation(locationCoordinates);
        } else {
            locationCoordinates = null;
            responsaObject.setGeoLocation((ParseGeoPoint)null);
        }
    }

    private void findAllViewsByIds(View parentView) {
        roTitle = (EditText) parentView.findViewById(R.id.roTitle);
        roDescription = (EditText) parentView.findViewById(R.id.roDescription);
        roTerritory = (TerritoryChooser) parentView.findViewById(R.id.roTerritory);
        roPhone = (EditText) parentView.findViewById(R.id.roPhone);
        roWebsite = (EditText) parentView.findViewById(R.id.roWebsite);
        roEmail = (EditText) parentView.findViewById(R.id.roEmail);
        roFacebook = (EditText) parentView.findViewById(R.id.roFacebook);
        roAddress = (FormAddress) parentView.findViewById(R.id.roAddress);
        roFormSubmit = (Button) parentView.findViewById(R.id.roFormSubmit);

        extra_information = (RelativeLayout) parentView.findViewById(R.id.extra_information);
        initiativeInformation = (RelativeLayout) parentView.findViewById(R.id.initiativeInformation);
        scrollView = (ScrollView) parentView.findViewById(R.id.scroll);

        //Only initiative
        roCategory = (CategorySpinner) parentView.findViewById(R.id.roCategory);

        //Only Gathering
        roStartDate = (EditText) parentView.findViewById(R.id.roStartDate);
        roStartTime = (EditText) parentView.findViewById(R.id.roStartTime);
        roFinishDate = (EditText) parentView.findViewById(R.id.roFinishDate);
        roFinishTime = (EditText) parentView.findViewById(R.id.roFinishTime);

        roLocationLabel = (TextView) parentView.findViewById(R.id.roLocationLabel);

        roPlaceInitiativeDetails = (InitiativeDetails) parentView.findViewById(R.id.roPlaceInitiativeDetails);
    }
    
    private void populateData(ResponsaObject responsaObject) {
        roTitle.setText(responsaObject.getTitle());
        roDescription.setText(responsaObject.getDescription());
        roPhone.setText(responsaObject.getPhone1());
        roWebsite.setText(responsaObject.getWebsite());
        roEmail.setText(responsaObject.getEmail());
        roFacebook.setText(responsaObject.getFacebook());

        homeTerritory = responsaObject.getTerritory();
        roTerritory.setSelectedTerritory(homeTerritory);

        if (responsaObject.getCategory() != null) {
            selectedCategory = InitiativeCategory.categoryBySlug(responsaObject.getCategory());
        }

        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdf2 = new SimpleDateFormat("HH:mm");

        if (responsaObject.getDateTimeStart() != null) {
            roStartDate.setText(sdf.format(responsaObject.getDateTimeStart()));
            startDateSelected = true;

            roStartTime.setText(sdf2.format(responsaObject.getDateTimeStart()));
            startTimeSelected = true;
        }

        if (responsaObject.getDateTimeFinish() != null) {
            roFinishDate.setText(sdf.format(responsaObject.getDateTimeFinish()));
            finishDateSelected = true;

            roFinishTime.setText(sdf2.format(responsaObject.getDateTimeFinish()));
            finishTimeSelected = true;
        }

        validate(false);
/*
        if (selectedCategory != null && selectedCategory.getSlug() != null) responsaObject.setCategory(selectedCategory.getSlug());

            //Set territory ID
            if (homeTerritory != null) {
                responsaObject.setTerritoryId(homeTerritory.id);
                responsaObject.setGeoLocation(locationCoordinates);
                responsaObject.setTerritoryLabel(homeTerritory.label);
                responsaObject.setTerritoryRegionId(homeTerritory.getRegion().id);
            }
        */
    }

    private ResponsaObject sweepData() {
        responsaObject.setTitle(roTitle.getText().toString());
        responsaObject.setDescription(roDescription.getText().toString());
        responsaObject.setPhone1(roPhone.getText().toString());
        responsaObject.setWebsite(roWebsite.getText().toString());
        responsaObject.setEmail(roEmail.getText().toString());
        responsaObject.setFacebook(roFacebook.getText().toString());

        if (selectedCategory != null && selectedCategory.getSlug() != null) responsaObject.setCategory(selectedCategory.getSlug());

        //Set territory ID
        if (homeTerritory != null) {
            responsaObject.setTerritoryId(homeTerritory.id);
            responsaObject.setGeoLocation(locationCoordinates);
            responsaObject.setTerritoryLabel(homeTerritory.label);
            responsaObject.setTerritoryRegionId(homeTerritory.getRegion().id);
        }
        roAddress.sweepForm();

        return responsaObject;
    }

    private Boolean validate(Boolean alert) {
        Boolean result = true;

        String title = roTitle.getText().toString();
        String description = roDescription.getText().toString();

        if (title == null || title.isEmpty()) {
            result = false;
            if (alert) Toast.makeText(ResponsaApplication.getAppContext(),R.string.v_ro_no_title,Toast.LENGTH_SHORT).show();
        } else if (description == null || description.isEmpty()) {
            result = false;
            if (alert) Toast.makeText(ResponsaApplication.getAppContext(),R.string.v_ro_no_description,Toast.LENGTH_SHORT).show();
        } else if ("initiative".equals(responsaObjectType)) {

            String address = roAddress.getAddress();
            String neighborhood = roAddress.getNeighborhood();
            String phone = roPhone.getText().toString();
            String website = roWebsite.getText().toString();
            String email = roEmail.getText().toString();
            String facebook = roFacebook.getText().toString();

            if (selectedCategory == null || selectedCategory.getSlug() == null) {
                result = false;
                if (alert) Toast.makeText(ResponsaApplication.getAppContext(),R.string.roform_category_empty,Toast.LENGTH_SHORT).show();
            } else if (address == null || address.isEmpty()) {
                result = false;
                if (alert) Toast.makeText(ResponsaApplication.getAppContext(),R.string.roform_address_empty,Toast.LENGTH_SHORT).show();
            } else if (neighborhood == null || neighborhood.isEmpty()) {
                result = false;
                if (alert) Toast.makeText(ResponsaApplication.getAppContext(),R.string.roform_neighborhood_empty,Toast.LENGTH_SHORT).show();
            } else if ((phone == null || phone.isEmpty()) && (website == null || website.isEmpty()) && (email == null || email.isEmpty()) && (facebook == null || facebook.isEmpty()) ) {
                result = false;
                if (alert) Toast.makeText(ResponsaApplication.getAppContext(),R.string.roform_contact_empty,Toast.LENGTH_SHORT).show();
            }

        } else { /* responsaObjectType == "gathering" */
            //start & end date
            //The toasts below appear even if alert == false
            if (!startDateSelected || !startTimeSelected || !finishDateSelected || !finishTimeSelected) {
                result = false;
            } else {
                int compDates = responsaObject.getDateTimeStart().compareTo(responsaObject.getDateTimeFinish());
                if (compDates == 0) { //start and end are the same
                    result = false;
                    Toast.makeText(ResponsaApplication.getAppContext(),R.string.u_start_end_dates_equal,Toast.LENGTH_SHORT).show();
                } else if (compDates > 0) { //start after than end
                    result = false;
                    Toast.makeText(ResponsaApplication.getAppContext(),R.string.u_start_after_end,Toast.LENGTH_SHORT).show();
                }

                compDates = responsaObject.getDateTimeFinish().compareTo(new Date());

                if (compDates <= 0) {
                    result = false;
                    Toast.makeText(ResponsaApplication.getAppContext(),R.string.u_end_after_now,Toast.LENGTH_SHORT).show();
                }
            }
        }

        result = result && roTerritory.validate().isEmpty();

        roFormSubmit.setEnabled(result);

        //if (result && placeInitiative == null) {
        //    scrollView.scrollTo(0, scrollView.getBottom());
        //}

        return result;
    }
}
