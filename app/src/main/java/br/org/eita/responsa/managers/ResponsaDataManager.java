package br.org.eita.responsa.managers;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.data_manager.ChallengeFrontPageData;
import br.org.eita.responsa.adapter.data_manager.LastFetchedAt;
import br.org.eita.responsa.adapter.data_manager.QueryResult;
import br.org.eita.responsa.backend_comm.SearchApi;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.model.AppLog;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.model.ChallengeAnswer;
import br.org.eita.responsa.model.ChallengeUserJoin;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.model.UserResponsaObject;
import br.org.eita.responsa.model.search_api.ChallengeFrontPageIds;
import br.org.eita.responsa.model.search_api.ChallengeRankedId;
import br.org.eita.responsa.model.util.LoadChallengesCallback;
import br.org.eita.responsa.model.util.ResponsaSaveCallback;
import br.org.eita.responsa.util.ExtraFilters;
import br.org.eita.responsa.util.ResponsaObjectsMap;
import br.org.eita.responsa.util.ResponsaUtil;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * ResponsaApplication.responsaDataManager (Singleton)
 *
 *
 */
public class ResponsaDataManager {

    public static final String NEW_OBJECT_ID = "__new__";

    public static JSONObject dataConfig;

    public static final String INTENT_GENERAL_PAGE_DATA_UPDATED = "br.org.eita.responsa.__view___PAGE_DATA_UPDATED";
    public static final String PINTAG_GENERAL_NAME = "pintag___key__";

    public ChallengeFrontPageData challengeFrontPageData = new ChallengeFrontPageData();

    public LastFetchedAt lastFetchedAt;

    private Context mContext;

    public ResponsaDataManager(Context context) {
        this.mContext = context;

        initLastFetchedAt();
        loadConfiguration();
    }

    public void init() {
    }

    public void finish() {
        saveLastFetchedAt();
    }

    private void initLastFetchedAt() {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);

        String pref = preferences.getString("lastFetchedAt", "{}");
        ResponsaUtil.log("ResponsaDataManager", "initLastFechedAt prefs: " + pref);

        this.lastFetchedAt = LastFetchedAt.fromString(pref);

        if (this.lastFetchedAt == null) {
            this.lastFetchedAt = new LastFetchedAt();
            //log bug - why is it happening?
            AppLog.record("BUGLOG01",pref);
        }
    }

    public void saveLastFetchedAt() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("lastFetchedAt", this.lastFetchedAt.toString());

        ResponsaUtil.log("ResponsaDataManager", "saveLastFetchedAt prefs: " + this.lastFetchedAt.toString());

        editor.commit();
    }


    public void loadInitialData(ParseUser user) {
        ResponsaUtil.log("ResponsaDataManager", "loadInitialData");

        //Loads remote objects and challenges from location in background
        ResponsaApplication.responsaDataManager.load("notifications", null);
        ResponsaApplication.responsaDataManager.load("initiativesAndGatherings", null);
        ResponsaApplication.responsaDataManager.load("challenges", null);
    }

    /*********************************************************************************
     *                                    VIEW METHODS                               *
     *********************************************************************************/

    public ChallengeFrontPageData getChallengeFrontPageData() {
        ResponsaUtil.log("ResponsaDataManager", "getChallengeFrontPageData");
        return challengeFrontPageData;
    }


    /*********************************************************************************
     *                                  GENERIC METHODS                              *
     *********************************************************************************/

    private HashMap<String, QueryResult> cachedDataForViews = new HashMap<>();
    public static String FORCE_REMOTE = "force_remote";
    public static String FORCE_LOCAL = "force_local";
    public static String IGNORE_CACHED = "ignore_cached";

    public static void loadConfiguration() {
        String json = ResponsaUtil.loadJSONFromAsset("data_config.json", ResponsaApplication.getAppContext());
        try {
            dataConfig = new JSONObject(json);
        } catch (JSONException e) {
            Log.e("Responsa", "erro ao carregar data_config.json");
            e.printStackTrace();
        }
    }

    public void load(String view, String targetViewObjectId) {
        load(view, targetViewObjectId, null, view, view, null, 0, null);
    }

    public void load(String view, String targetViewObjectId, Integer page) {
        load(view, targetViewObjectId, null, view, view, null, page, null);
    }

    public void load(String view, String targetViewObjectId, String targetView, Integer page, ExtraFilters extraFilters) {
        load(view, targetViewObjectId, null, view, targetView, null, page, extraFilters);
    }

    public void load(String view, String targetViewObjectId, String forceLoadOrigin) {
        load(view, targetViewObjectId, forceLoadOrigin, view, view, null, 0, null);
    }

    public void load(String view, String targetViewObjectId, String forceLoadOrigin, String targetView) {
        load(view, targetViewObjectId, forceLoadOrigin, view, targetView, null, 0, null);
    }

    public void load(String view, String targetViewObjectId, String forceLoadOrigin, String targetView, Integer page) {
        load(view, targetViewObjectId, forceLoadOrigin, view, targetView, null, page, null);
    }

    public void load(String view, String targetViewObjectId, ExtraFilters extraFilters) {
        load(view, targetViewObjectId, FORCE_LOCAL, view, view, null, 0, extraFilters);
    }

    public void load(String view, String targetViewObjectId, String forceLoadOrigin, ExtraFilters extraFilters) {
        load(view, targetViewObjectId, forceLoadOrigin, view, view, null, 0, extraFilters);
    }


    /**
     * LOAD
     *
     * Loads data and returns to the user
     *
     * How it works?
     *  1. tries to load data from cachedData (array in memory, current run of the application)
     *  2. tries to load data from local database
     *  3. loads data from remote database
     *
     *  Depending on the expiration policy, the load from local database or from remote database is
     *  forced. However, if not possible to contact the remote database, the local data is returned,
     *  and the user must see that the data is old (some message on the screen).
     *
     *  The load operation got complicated because of:
     *   * The limitations of parse api (limited possibility to make JOINs in a query) - must make
     *     load of dependent queries. The load operation is completed only when the main view and
     *     dependent views are loaded (several operations)
     *   * Asyncronous load - the result is not returned, but a intent is generated when all the data is
     *     retrieved and available. There can be several intents generated, for instance, one when data
     *     is loaded locally, other when data is loaded remotely
     *   * Expiration policy - time to expire the data
     *   * Offline retrieval - the application must return any data to the user, even if s/he is
     *     offline.
     *
     * @param view The view to have data loaded
     * @param targetViewObjectId The ID of the object or the related object
     * @param forceLoadOrigin Force to load from remote or from local, ignoring cachedData
     * @param callerView The original view. It might have dependencies
     * @param targetView The view to where the intent must be broadcast
     * @param page Only used to load more button in lists, like comments or notifications, etc.
     * @param extraFilters ExtraFilters to the query
     */
    private void load(String view, String targetViewObjectId, String forceLoadOrigin, String callerView, String targetView, String threadToken, Integer page, ExtraFilters extraFilters){
        ResponsaUtil.log("NewDataManagerLoad", "----------");
        ResponsaUtil.log("NewDataManagerLoad", "uniqueId: "+LastFetchedAt.getCompoundKey(getViewKey(view, targetViewObjectId), extraFilters)+" | view: " + view + " | targetViewObjectId: " + targetViewObjectId + " | forceLoadOrigin: " + forceLoadOrigin + " | callerView: " + callerView + " | targetView:" + targetView + " | threadToken: " + threadToken + " | page: " + page);
        ResponsaUtil.log("NewDataManagerLoad", "GENERAL LOAD called.");

        String key = getViewKey(view, targetViewObjectId);
        QueryResult cachedDataForView = cachedDataForViews.get(key);
        Integer lastFetchedPage = lastFetchedAt.getLastFetchedAtPage(key, extraFilters);
        Integer listLimit = getListLimit(view);
        Boolean isTopCall = (callerView.equals(view));

        // If view is the callerView, we must reset the information "has_data" and "updateStatus" recursively from all targetview dependencies:
        if (isTopCall) {
            ResponsaUtil.log("NewDataManagerLoad", "RESET all to false: readyToPopulate and updateStatus");
            threadToken = generateThreadToken();
            resetAllReadyToPopulate(targetView, targetViewObjectId, threadToken);
            resetAllUpdateStatus(targetView, targetViewObjectId);
        }

        JSONArray directDependencies = getArrayFromView(view, "dependencies");

        //Load Data for direct dependencies
        if (directDependencies != null) {
            for (int i=0; i < directDependencies.length(); i++) {
                try {
                    load(directDependencies.getString(i), targetViewObjectId, forceLoadOrigin, callerView, targetView, threadToken, page, extraFilters);
                } catch (JSONException e) { continue; }
            }
        }

        // Break the load standard flux if forceLoadOrigin is set to FORCE_REMOTE:
        if (ResponsaUtil.equalsN(forceLoadOrigin, FORCE_REMOTE)) {
            // The needRemoteFetch is not being used here for checking purposes. It is being called to eventually remove the lastfetchedAtDate when user moves more than x km from position when last fetched.
            ResponsaUtil.log("newdatamanagerload", "Force Remote! View: "+view);
            lastFetchedAt.needRemoteFetch(getViewKey(view, targetViewObjectId));

            preloadRemote(view, targetViewObjectId, forceLoadOrigin, callerView, targetView, threadToken, page, extraFilters);
            return;
        }

        Boolean generateNewThreadTokenForLoadRemote = true;
        if (cachedDataForView != null && !ResponsaUtil.equalsN(forceLoadOrigin, FORCE_LOCAL) && page <= lastFetchedPage) {
            // If there is cachedData, it means that there is data, so let's populate to give the user a fast return. But the cache is worthless in 2 cases: if forceLoadOrigin is set to Force_local (gets from PIN), or if the new page is bigger than the last fetch.

            // update updateStatus:
            cachedDataForView.setUpdateStatus(QueryResult.HAS_NO_NEW_DATA);

            // cut the list if it is bigger than page
            /*if (listLimit!=null) {
                cachedDataForView.limitResults(retrieveLoadedData(view, targetViewObjectId), listLimit, page);
            }*/

            cachedDataForViews.put(key, cachedDataForView);

            // Broadcast data to populate from cached data:
            ResponsaUtil.log("NewDataManagerLoad", "----------");
            ResponsaUtil.log("NewDataManagerLoad", "uniqueId: "+LastFetchedAt.getCompoundKey(key, extraFilters)+" | view: " + view + " | targetViewObjectId: " + targetViewObjectId + " | forceLoadOrigin: " + forceLoadOrigin + " | callerView: " + callerView + " | targetView:" + targetView + " | threadToken: " + threadToken + " | page: " + page + " | update satus: " + cachedDataForView.getMetaData().updateStatus);
            ResponsaUtil.log("NewDataManagerLoad", "GOT FROM CACHED. Results: " + String.format("%d",cachedDataForView.getData().size()));
            processLoadedAndPopulate(view, targetViewObjectId, forceLoadOrigin, callerView, targetView, threadToken, page, extraFilters);
        } else if (
                (lastFetchedAt.getLastFetchedAtDate(getViewKey(view,targetViewObjectId), extraFilters) != null && page<=lastFetchedPage)
                || ResponsaUtil.equalsN(forceLoadOrigin, FORCE_LOCAL)
            ){
            // If there is no cache, there might be local data because the user might be just starting the app
            // Another possibility is if forceloadorigin is set to load local (used in saveObject method)
            loadLocal(view, targetViewObjectId, forceLoadOrigin, callerView, targetView, threadToken, page, extraFilters);
        } else {
            generateNewThreadTokenForLoadRemote = false;
        }

        if (lastFetchedAt.needRemoteFetch(key, extraFilters, page)) {
            // Finally, load from Parse if there is the need to fetch remote (be it because of user movement in the ground or because the last remote fetch was long ago). In this case, we must create a new threadToken to have two simultaneous loading processes (threads)
            if (generateNewThreadTokenForLoadRemote) {
                // A new thread must be generated, since it was locally loaded
                threadToken = generateThreadToken();
            }
            preloadRemote(view, targetViewObjectId, forceLoadOrigin, callerView, targetView, threadToken, page, extraFilters);
        }
    }

    public List retrieveLoadedData(String view, String targetViewObjectId) {
        QueryResult cached = cachedDataForViews.get(getViewKey(view, targetViewObjectId));
        if (cached != null) return cached.getData();

        return new ArrayList();
    }

    //May return null in case of objects newly created
    public QueryResult.MetaData retrieveLoadedMetaData(String view, String targetViewObjectId) {
        if (NEW_OBJECT_ID.equals(targetViewObjectId)) {
            QueryResult.MetaData metaData = new QueryResult.MetaData();
            metaData.page = 0;
            metaData.hasMore = false;
            metaData.countResults = 0;
            return metaData;
        } else {
            return cachedDataForViews.get(getViewKey(view, targetViewObjectId)).getMetaData();
        }
    }

    private boolean isOrQuery(JSONObject dataOptions) {
        try{
            if (dataOptions.getJSONArray("or") == null) {
                return false;
            } else {
                return true;
            }
        } catch (JSONException e) {
            return false;
        }
    }

    private void loadLocal(final String view, final String targetViewObjectId, final String forceLoadOrigin, final String callerView, final String targetView, final String threadToken, final Integer page, final ExtraFilters extraFilters) {
        ResponsaUtil.log("NewDataManagerLoad", "loadLocal: " + LastFetchedAt.getCompoundKey(getViewKey(view, targetViewObjectId), extraFilters) + (targetView == null ? "" : " | targetView:" + targetView + " | forceLoadOrigin: " + forceLoadOrigin + " | pin name: " + getPinName(getViewKey(view, targetViewObjectId)) + " | page: " + page));

        try {
            final JSONObject dataOptions = dataConfig.getJSONObject(view);
            final String key = getViewKey(view, targetViewObjectId);

            ParseQuery<ParseObject> query = null;

            if (isOrQuery(dataOptions)) {
                List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();
                JSONArray orArray = dataOptions.getJSONArray("or");
                for (int i=0; i<orArray.length(); i++) {
                    queries.add(ResponsaQueryBuilder.buildLocalQuery(view, dataConfig.getJSONObject(orArray.getString(i)), getPinName(key), targetViewObjectId, page, extraFilters));
                }
                query = ParseQuery.or(queries);
            } else {
                // Build local query:
                query = ResponsaQueryBuilder.buildLocalQuery(view, dataOptions, getPinName(key), targetViewObjectId, page, extraFilters);
            }


            final LocationCoordinates visibleLocationWhenFetched = LocationCoordinates.getVisibleLocation();

            query.findInBackground(new FindCallback() {
                @Override
                public void done(List list, ParseException e) {

                }

                @Override
                public void done(Object objList, Throwable e) {
                    if (e == null) {
                        List list = (List) objList;
                        ResponsaUtil.log("NewDataManagerLoad", "RESULTS RECEIVED loadLocal:  " + LastFetchedAt.getCompoundKey(getViewKey(view, targetViewObjectId), extraFilters) + " , results:"+ String.format("%d",list.size()));

                        Boolean hasMore = false;
                        try {
                            if (dataOptions.getInt("limit") < list.size()) {
                                ResponsaUtil.log("NewDataManagerLoad", "Load local removed last object because hasMore");
                                hasMore = true;
                                list.remove(list.size()-1);
                            }
                        } catch (JSONException e2) {}

                        QueryResult queryResult = new QueryResult(list, retrieveLoadedData(view, targetViewObjectId), QueryResult.Origin.LOCAL, visibleLocationWhenFetched, lastFetchedAt.getLastFetchedAtDate(key, extraFilters), page, hasMore);

                        cachedDataForViews.put(key, queryResult);
                        ResponsaUtil.log("NewDataManagerLoad", "----------");
                        ResponsaUtil.log("NewDataManagerLoad", "uniqueId: "+LastFetchedAt.getCompoundKey(key, extraFilters)+" | view: " + view + " | targetViewObjectId: " + targetViewObjectId + " | forceLoadOrigin: " + forceLoadOrigin + " | callerView: " + callerView + " | targetView:" + targetView + " | threadToken: " + threadToken + " | page: " + page + " | update satus: " + queryResult.getMetaData().updateStatus);

                        ResponsaUtil.log("NewDataManagerLoad", "GOT FROM LOCAL. Results: " + String.format("%d",((List) objList).size()));

                        processLoadedAndPopulate(view, targetViewObjectId, forceLoadOrigin, callerView, targetView, threadToken, page, extraFilters);
                    }
                }
            });

        } catch (JSONException e) {
            ResponsaUtil.log("ResponsaDataManager","ERROR: load: " + e.getMessage());
            e.printStackTrace();
        }
    }

    private void preloadRemote(final String view, final String targetViewObjectId, final String forceLoadOrigin, final String callerView, final String targetView, final String threadToken, final Integer page, final ExtraFilters extraFilters) {
        ResponsaUtil.log("NewDataManagerLoad", "entering preloadRemote");

        try {
            LastFetchedAt logger = new LastFetchedAt();
            final JSONObject dataOptions = dataConfig.getJSONObject(view);
            final String key = getViewKey(view, targetViewObjectId);
            final Date lastFetchedAtDate = lastFetchedAt.getLastFetchedAtDate(key, extraFilters);
            ResponsaUtil.log("NewDataManagerLoad", "preloadRemote: " + LastFetchedAt.getCompoundKey(getViewKey(view, targetViewObjectId), extraFilters) + (targetView == null ? "" : " | lastFetchedAtDate: "+lastFetchedAtDate+" | targetView:" + targetView + " | page: " + page));


            if ("challenges".equals(view)) { //needs data from ajax
                fetchChallengesFromAjax(ParseUser.getCurrentUser().getString("territoryId"), new LoadChallengesCallback() {
                    @Override
                    public void done(List<String> challengeIds) {
                        ParseQuery query = ResponsaQueryBuilder.buildRemoteQuery(view, dataOptions, targetViewObjectId, lastFetchedAtDate, challengeIds, null, page, extraFilters);
                        loadRemote(view, targetViewObjectId, forceLoadOrigin, callerView, targetView, query, threadToken, page, extraFilters);
                    }
                });
            } else {
                List parents = null;
                try {
                    parents = retrieveLoadedData(dataOptions.getString("parentView"), targetViewObjectId);
                } catch (JSONException e) {}

                ParseQuery<ParseObject> query;

                if (isOrQuery(dataOptions)) {
                    List<ParseQuery<ParseObject>> queries = new ArrayList<ParseQuery<ParseObject>>();
                    JSONArray orArray = dataOptions.getJSONArray("or");
                    for (int i=0; i<orArray.length(); i++) {
                        queries.add(ResponsaQueryBuilder.buildRemoteQuery(view, dataOptions, targetViewObjectId, lastFetchedAtDate, null, parents, page, extraFilters));
                    }
                    query = ParseQuery.or(queries);
                } else {
                    query = ResponsaQueryBuilder.buildRemoteQuery(view, dataOptions, targetViewObjectId, lastFetchedAtDate, null, parents, page, extraFilters);
                }

                loadRemote(view, targetViewObjectId, forceLoadOrigin, callerView, targetView, query, threadToken, page, extraFilters);
            }


        } catch (JSONException e) {
            ResponsaUtil.log("newdatamanagerload", "ERROR: load: "+ e.getMessage());
            e.printStackTrace();
        }

    }

    private void loadRemote(final String view, final String targetViewObjectId, final String forceLoadOrigin, final String callerView, final String targetView, ParseQuery query, final String threadToken, final Integer page, final ExtraFilters extraFilters) {
        ResponsaUtil.log("NewDataManagerLoad", "loadRemote: " + LastFetchedAt.getCompoundKey(getViewKey(view, targetViewObjectId), extraFilters) + (targetView == null ? "" : " | targetView:" + targetView) + " | page: " + page);

        try {
            final JSONObject dataOptions = dataConfig.getJSONObject(view);
            final String key = getViewKey(view, targetViewObjectId);

            final LocationCoordinates visibleLocationWhenFetched = LocationCoordinates.getVisibleLocation();

            //final Boolean isWithinGeoBoxQuery = (queryType != null && queryType.equals("withinGeoBox"));
            final LatLngBounds latLngBoundsWhenFetched = ResponsaObjectsMap.mapViewBounds;

            query.findInBackground(new FindCallback() {
                @Override
                public void done(List list, ParseException e) {

                }

                @Override
                public void done(Object objList, Throwable e) {
                    if (e == null) {
                        final List list = (List) objList;
                        Integer listSize = list.size();
                        ResponsaUtil.log("NewDataManagerLoad", "RESULTS RECEIVED loadRemote:  " + LastFetchedAt.getCompoundKey(getViewKey(view, targetViewObjectId), extraFilters) + " , results:" + String.format("%d", listSize));
                        if (listSize==1000) {
                            load(view, targetViewObjectId, forceLoadOrigin, callerView, targetView, threadToken, page+1, extraFilters);
                        }
                            lastFetchedAt.updateLastFetchedAt(key, extraFilters, visibleLocationWhenFetched, threadToken, page, latLngBoundsWhenFetched);

                        // If there is already some data in cache, then we must increment the local data with the remote data received. If not, we broadcast the full list immediately.
                        final Boolean forceLoadLocal = cachedDataForViews.get(getViewKey(view, targetViewObjectId)) != null;

                        ParseObject.pinAllInBackground(getPinName(key), list, new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    if (forceLoadLocal) {
                                        ResponsaUtil.log("NewDataManagerLoad", "----------");
                                        ResponsaUtil.log("NewDataManagerLoad", "uniqueId: "+LastFetchedAt.getCompoundKey(key, extraFilters)+" | view: " + view + " | targetViewObjectId: " + targetViewObjectId + " | forceLoadOrigin: " + forceLoadOrigin + " | callerView: " + callerView + " | targetView:" + targetView + " | threadToken: " + threadToken + " | page: " + page);
                                        ResponsaUtil.log("NewDataManagerLoad", "GOT INCREMENTAL FROM REMOTE. Will not populate now. Calling loadLocal to populate. Results: " + String.format("%d", list.size()));

                                        loadLocal(view, targetViewObjectId, null, callerView, targetView, threadToken, page, extraFilters);
                                    }
                                } else {
                                    ResponsaUtil.log("NewDataManagerLoad", "PIN REMOTE ERROR: (" + String.format("%d", e.getCode()) + ")" + e.getMessage());
                                }
                            }
                        });

                        if (!forceLoadLocal) {
                            Boolean hasMore = false;
                            try {
                                if (dataOptions.getInt("limit") < list.size()) {
                                    hasMore = true;
                                    list.remove(list.size() - 1);
                                }
                            } catch (JSONException e2) {
                                // happens when there is no parameter "limit" in data_config.json for this view.
                            }

                            List previousList = null;
                            QueryResult queryResult = new QueryResult(list, previousList, QueryResult.Origin.REMOTE, visibleLocationWhenFetched, lastFetchedAt.getLastFetchedAtDate(key, extraFilters), page, hasMore);
                            cachedDataForViews.put(key, queryResult);
                            ResponsaUtil.log("NewDataManagerLoad", "----------");
                            ResponsaUtil.log("NewDataManagerLoad", "uniqueId: "+LastFetchedAt.getCompoundKey(key, extraFilters)+" | view: " + view + " | targetViewObjectId: " + targetViewObjectId + " | forceLoadOrigin: " + forceLoadOrigin + " | callerView: " + callerView + " | targetView:" + targetView + " | threadToken: " + threadToken + " | page: " + page + " | update satus: " + queryResult.getMetaData().updateStatus);
                            ResponsaUtil.log("NewDataManagerLoad", "uniqueId: "+LastFetchedAt.getCompoundKey(key, extraFilters)+" | view: " + view + " - GOT FROM REMOTE WITHOUT cachedData. Results: " + String.format("%d", list.size()));
                            processLoadedAndPopulate(view, targetViewObjectId, forceLoadOrigin, callerView, targetView, threadToken, page, extraFilters);
                        }

                    } else {
                        ResponsaUtil.log("NewDataManagerLoad", "LOAD REMOTE ERROR: " + e.getMessage());
                    }
                }
            });
        } catch (JSONException e) {
            ResponsaUtil.log("NewDataManagerLoad", "LOAD REMOTE ERROR: " + e.getMessage());
        }
    }

    private void updateRelatedList(final String view, final ParseObject object) {
        ResponsaUtil.log("NewDataManagerLoad", "Entered saveObject in relatedListView: " + view +" | object to be saved: " + object.getClassName());
        try {
            JSONObject dataOptions = dataConfig.getJSONObject(view);
            final JSONArray relatedListViews = dataOptions.getJSONArray("relatedListViews");
            for(int i=0; i<relatedListViews.length(); i++) {
                JSONArray relatedListViewArray = relatedListViews.getJSONArray(i);
                final String relatedListView = relatedListViewArray.getString(0);
                ParseObject relatedListViewInitiative;
                if (relatedListViewArray.length()>1 && relatedListViewArray.getString(1)!=null) {
                    relatedListViewInitiative = object.getParseObject(relatedListViewArray.getString(1));
                } else {
                    relatedListViewInitiative = null;
                }
                final String relatedListViewObjectId = (relatedListViewInitiative == null)
                        ? null
                        : relatedListViewInitiative.getObjectId();
                ResponsaUtil.log("NewDataManagerLoad", "saveObject in relatedListView:" + relatedListView + " | ObjectId= " + (relatedListViewObjectId == null ? "null" : relatedListViewObjectId) + " | object: " + object.getClassName());

                object.pinInBackground(getPinName(getViewKey(relatedListView, relatedListViewObjectId)), new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            load(relatedListView, relatedListViewObjectId, FORCE_LOCAL);
                        } else {
                            ResponsaUtil.log("NewDataManagerLoad", "saveObject in list: PIN IN BACKGROUND: FAILURE , object: " + object.getClassName() + " targetViewObjectId: " + (object.getObjectId() == null ? "null" : object.getObjectId()));
                        }
                    }
                });
            }
        } catch (JSONException e) {
            ResponsaUtil.log("NewDataManagerLoad", e.getMessage());
            //if there is no relatedListView
        }
    }

    public void saveObject(String view, String relatedObjectId, ParseObject object, ResponsaSaveCallback cb) {
        saveObject(view, relatedObjectId, view, object, true, cb);
    }

    public void saveObject(final String view, final String relatedObjectId, final ParseObject object, final Boolean loadAfterSave, final ResponsaSaveCallback cb) {
        saveObject(view, relatedObjectId, view, object, loadAfterSave, cb);
    }

    /**
     * SAVE
     * @param view
     * @param targetViewObjectId
     * @param targetView
     * @param object
     * @param loadAfterSave Will load the --view-- and also any relatedListView related to the view, after saving.
     * @param cb
     */
    public void saveObject(final String view, final String targetViewObjectId, final String targetView, final ParseObject object, final Boolean loadAfterSave, final ResponsaSaveCallback cb) {

        SaveCallback saveCallback = new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (cb != null) {
                    cb.done(e);
                    ResponsaUtil.log("NewDataManagerLoad", "----------");
                    ResponsaUtil.log("NewDataManagerLoad", "view: " + view + " | targetViewObjectId: " + targetViewObjectId + " | callerView: " + view + " | targetView:" + targetView);
                    ResponsaUtil.log("NewDataManagerLoad", "OBJECT SAVED.");
                    /*if (loadAfterSave != null) {
                        load(view, targetViewObjectId, FORCE_LOCAL, view, targetView, null, 0);
                        updateRelatedList(view);
                    }*/
                }
            }
        };

        if (targetViewObjectId == null) {
            //Creates temporary cached data to open view before saving confirmation
            ArrayList<ParseObject> data = new ArrayList<>();
            data.add(object);
            List previousList = null;
            cachedDataForViews.put(getViewKey(view, NEW_OBJECT_ID), new QueryResult(data, previousList, QueryResult.Origin.NEW_OBJECT, LocationCoordinates.getVisibleLocation(), new Date() , 0, false));
        }

        object.saveEventually(saveCallback);

        object.pinInBackground(getPinName(getViewKey(view, targetViewObjectId)), new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    if (loadAfterSave != null && loadAfterSave==true) {
                        load(view, targetViewObjectId, FORCE_LOCAL, view, targetView, null, 0, null);
                    }

                    ResponsaUtil.log("NewDataManagerLoad", "----------");
                    ResponsaUtil.log("NewDataManagerLoad", "view: " + view + " | targetViewObjectId: " + targetViewObjectId + " | callerView: " + view + " | targetView:" + targetView);
                    ResponsaUtil.log("NewDataManagerLoad", "SAVE: OBJECT PINNED IN BACKGROUND.");
                } else {
                    ResponsaUtil.log("NewDataManagerLoad", "saveObject: PIN IN BACKGROUND: FAILURE , object: " + object.getClassName() + " targetViewObjectId: " + (object.getObjectId() == null ? "null" : object.getObjectId()));
                }
            }
        });

        updateRelatedList(view, object);
    }

    public void saveList(final String view, final String targetViewObjectId, List objectList, final Boolean loadAfterSave, final ResponsaSaveCallback cb) {
        ParseObject.saveAllInBackground(objectList, new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (cb != null && loadAfterSave) {
                    load(view, targetViewObjectId);
                    cb.done(e);
                }
            }
        });

        ParseObject.pinAllInBackground(getPinName(getViewKey(view, targetViewObjectId)), objectList);
    }

    private void processLoadedAndPopulate(String view, String targetViewObjectId, String forceLoadOrigin, String callerView, String targetView, String threadToken, Integer page, ExtraFilters extraFilters) {

        ResponsaUtil.log("newDataManagerLoad", "Entered processAndPopulate...");
        //Synchronize if needed
        synchronizeCachedData(view, targetViewObjectId);

        // Remember that this specific key is ready to populate:
        String threadTokenKey = getThreadTokenKey(view, targetViewObjectId, threadToken);
        ResponsaUtil.log("newDataManagerLoad", "LastFetchedAt.updateLastFetchedAt: inserting new token key, " + threadTokenKey);
        lastFetchedAt.readyToPopulate.put(threadTokenKey, true);

        // Set updated page:
        lastFetchedAt.setLastFetchedAtPage(getViewKey(view, targetViewObjectId), extraFilters, page);

        JSONArray directHierarchicalDependencies = getArrayFromView(view, "hierarchicalDependencies");

        //Loads dependencies
        if (directHierarchicalDependencies != null && directHierarchicalDependencies.length() > 0) {
            ResponsaUtil.log("NewDataManagerLoad", "Did NOT populate: view '"+view+"' must still load hierarchicalDependencies '"+directHierarchicalDependencies.toString()+"'");
            for (int i = 0; i < directHierarchicalDependencies.length(); i++) {
                try {
                    load(directHierarchicalDependencies.getString(i), targetViewObjectId, forceLoadOrigin, callerView, targetView, threadToken, page, extraFilters);
                } catch (JSONException e) { continue; }
            }
            return;
        }

        //Check if all keys related to this key are ready to populate
        if (readyToPopulate(callerView, targetViewObjectId, threadToken)) {
            //Populate through intent. The intent name is always referred to the targetView.
            ResponsaUtil.log("NewDataManagerLoad", "Populate!");
            broadcastIntent(getIntentDataUpdatedName(targetView), targetView);
        } else {
            ResponsaUtil.log("NewDataManagerLoad", "Did NOT populate!");
        }
    }

    private void synchronizeCachedData(String view, String targetViewObjectId) {
        try {
            JSONObject constraints = dataConfig.getJSONObject(view).getJSONObject("constraints");

            //Loads dependencies
            if (constraints != null) {
                String className = null;
                String constraintField = null;

                for (Iterator<String> i = constraints.keys(); i.hasNext();) {
                    constraintField = i.next();
                    if ("in_PARENTS".equals(constraints.getJSONArray(constraintField).getString(0))) {
                        className =  dataConfig.getJSONObject(view).getString("className");
                        break;
                    }
                }

                if (className == null) return;

                List<ParseObject> cachedObjects = retrieveLoadedData(view, targetViewObjectId);

                for (int i=0; i<cachedObjects.size(); i++) {
                    try {
                        ParseObject cachedObject = cachedObjects.get(i);
                        ParseObject parent =  cachedObject.getParseObject(constraintField);
                        if (parent == null) {
                            ResponsaUtil.log("NewDataManagerLoad", "ERROR: LOADING PARENT OBJECT FROM IN_PARENTS: PARENT NOT FOUND, key:" + getViewKey(view,targetViewObjectId));
                        } else {
                            Class<?> parentClass = parent.getClass();
                            //Class params[] = {cachedObject.getClass()};
                            Method setObjectOnParent = parentClass.getDeclaredMethod("set" + className, cachedObject.getClass());
                            setObjectOnParent.invoke(parent, cachedObject);
                        }
                    } catch (NoSuchMethodException e) {
                        //e.printStackTrace();
                    } catch (InvocationTargetException e) {
                        //e.printStackTrace();
                    } catch (IllegalAccessException e) {
                        //e.printStackTrace();
                    } catch (NullPointerException e) {

                    }
                }
            }

        } catch (JSONException e) {

        }
    }

    List<ChallengeRankedId> challengesRanks = new ArrayList<ChallengeRankedId>();

    public void fetchChallengesFromAjax(final String territoryId, final LoadChallengesCallback cb) {

        RestAdapter restAdapter;
        SearchApi searchApi;

        //Gson gson = new GsonBuilder().registerTypeAdapter(Date.class, new DateAdapter()).create();
        //Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd HH:mm:ss").create();


        restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.searchServiceUrl)
                        //.setLogLevel(RestAdapter.LogLevel.HEADERS_AND_ARGS)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                        //.setConverter(new GsonConverter(gson))
                .build();

        searchApi = restAdapter.create(SearchApi.class);

        searchApi.queryChallengesFirstPageIds(territoryId, null, new Callback<ChallengeFrontPageIds>() {
            @Override
            public void success(final ChallengeFrontPageIds challengeFrontPageIds, Response response) {
                challengeFrontPageIds.saveToPreferences();

                List<String> challengeIds = new ArrayList<String>();

                if (challengeFrontPageIds == null) return;

                for (int i = 0; i < challengeFrontPageIds.challenges.size(); i++) {
                    challengeIds.add(challengeFrontPageIds.challenges.get(i).objectId);
                }
                for (int i = 0; i < challengeFrontPageIds.discoveries.size(); i++) {
                    challengeIds.add(challengeFrontPageIds.discoveries.get(i).objectId);
                }

                cb.done(challengeIds);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    //Sees if current view and descendants are ready to populate
    private Boolean readyToPopulate(String callerView, String targetViewObjectId, String threadToken) {

        if (lastFetchedAt.readyToPopulate.get(getThreadTokenKey(callerView, targetViewObjectId, threadToken)) == null) {
            return false;
        }

        JSONArray deps[] = getAllViewDependencies(callerView);

        if (deps[0].length()>0 || deps[1].length()>0) {
            for (int i = 0; i < deps.length; i++) {
                // check if each dependency is ready
                for (int j = 0; j < deps[i].length(); j++) {
                    String dependentView = null;
                    try {
                        dependentView = deps[i].getString(j);
                        String threadTokenKey = getThreadTokenKey(dependentView, targetViewObjectId, threadToken);
                        if (lastFetchedAt.readyToPopulate.get(threadTokenKey) == null) {
                            ResponsaUtil.log("NewDataManagerLoad", "readyToPopulate: Already haves threadTokenKey: " + threadTokenKey + " , so it is NOT ready to populate");

                            return false;
                        }
                    } catch (JSONException e) {
                        continue;
                    }
                }
            }
        }
        // All ready! Let's reset all dependencies before returning true, so that next time this check can be made again:
        resetAllReadyToPopulate(callerView, targetViewObjectId, threadToken);

        return true;
    }

    private void resetAllUpdateStatus(String targetView, String targetViewObjectId) {
        resetUpdateStatus(getAllKeysFromTargetViewTree(targetView, targetViewObjectId));
    }
    private void resetAllReadyToPopulate(String targetView, String targetViewObjectId, String threadToken) {
        lastFetchedAt.resetReadyToPopulate(getAllThreadTokenKeysFromTargetViewTree(targetView, targetViewObjectId, threadToken));
    }

    private ArrayList getAllThreadTokenKeysFromTargetViewTree(String targetView, String targetViewObjectId, String threadToken) {
        JSONArray[] allDependencies = getAllViewDependencies(targetView);
        ArrayList depKeys = new ArrayList();
        depKeys.add(getThreadTokenKey(targetView, targetViewObjectId, threadToken));
        for (int i=0; i < allDependencies.length; i++) {
            for (int j=0; j < allDependencies[i].length(); j++) {
                try {
                    depKeys.add(getThreadTokenKey(allDependencies[i].getString(j), targetViewObjectId, threadToken));
                } catch (JSONException e) { continue; }
            }
        }
        return depKeys;
    }

    private ArrayList getAllKeysFromTargetViewTree(String targetView, String targetViewObjectId) {
        JSONArray[] allDependencies = getAllViewDependencies(targetView);
        ArrayList depKeys = new ArrayList();
        depKeys.add(getViewKey(targetView, targetViewObjectId));
        for (int i=0; i < allDependencies.length; i++) {
            for (int j=0; j < allDependencies[i].length(); j++) {
                try {
                    depKeys.add(getViewKey(allDependencies[i].getString(j), targetViewObjectId));
                } catch (JSONException e) { continue; }
            }
        }
        return depKeys;
    }

    /**
     * Gets an array parameter from a (data) view
     * @param view Name of the view (in the data_config.json)
     * @param type Name of the array parameter of the view
     * @return
     */
    private JSONArray getArrayFromView(String view, String type) {
        JSONArray directDeps = null;
        try {
            directDeps = dataConfig.getJSONObject(view).getJSONArray(type);
        } catch (JSONException e) { }
        return directDeps;
    }

    private JSONArray[] getAllViewDependencies(String view) {
        return getAllViewDependencies(view, new JSONArray[] {new JSONArray(), new JSONArray()});
    }

    private JSONArray[] getAllViewDependencies(String view, JSONArray[] result) {
        JSONArray directDependencies = getArrayFromView(view, "dependencies");
        JSONArray directHierarchicalDependencies = getArrayFromView(view, "hierarchicalDependencies");

        JSONArray deps[] = {directDependencies, directHierarchicalDependencies};
        for (int i=0; i< deps.length; i++) {
            if (deps[i] == null) {
                continue;
            }

            // recursively find decendants
            for (int j=0; j<deps[i].length(); j++) {
                String dependentView = null;
                try{
                    dependentView = deps[i].getString(j);
                    result[i].put(dependentView);
                } catch (JSONException e) {
                    continue;
                }
                JSONArray[] subDeps = getAllViewDependencies(dependentView);
                for (int ii=0; ii< subDeps.length; ii++) {
                    if (subDeps[ii] == null) {
                        continue;
                    }
                    for (int jj=0; jj<subDeps[ii].length(); jj++) {
                        try {
                            result[ii].put(subDeps[ii].getString(jj));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }
        }

        return result;
    }

    public String getThreadTokenKey(String view, String targetViewObjectId, String threadToken) {
        return getThreadTokenKey(getViewKey(view, targetViewObjectId), threadToken);
    }
    public String getThreadTokenKey(String key, String threadToken) {
        return key+"_"+threadToken;
    }

    private String generateThreadToken() {
        return String.valueOf(System.currentTimeMillis());
    }

    public Integer getListLimit(String view) {
        Integer limit = null;
        try {
            limit = Integer.parseInt(dataConfig.getJSONObject(view).getString("limit"));
        } catch (JSONException e) { }
        return limit;
    }

    public static String getIntentDataUpdatedName(String view) {
        return INTENT_GENERAL_PAGE_DATA_UPDATED.replaceFirst("__view__",view);
    }

    public static String getPinName(String key) {
        return PINTAG_GENERAL_NAME.replaceFirst("__key__",key);
    }

    public static String getViewKey(String view, String targetViewObjectId) {
        if (view == null) return null;
        
        String key = view;
        if (targetViewObjectId != null) key += targetViewObjectId;
        return key;
    }

    private void resetUpdateStatus(ArrayList keys) {
        for (int i=0; i<keys.size(); i++) {
            QueryResult cachedDataForView = cachedDataForViews.get(keys.get(i));
            if (cachedDataForView != null) {
                cachedDataForView.setUpdateStatus(QueryResult.HAS_NO_NEW_DATA);
            }
        }
    }

    public void clearLocal(String key, ExtraFilters extraFilters) {
        lastFetchedAt.removeLastFetchedAtDate(key, extraFilters);
        ParseObject.unpinAllInBackground(getPinName(key));

    }

    public void clearLocal(String view, String targetViewObjectId, ExtraFilters extraFilters) {
        clearLocal(getViewKey(view, targetViewObjectId), extraFilters);
    }

    public void clearCached(String view, String targetViewObjectId) {
        cachedDataForViews.remove(getViewKey(view, targetViewObjectId));
    }


    /*********************************************************************************
     *                                LOAD REMOTE METHODS                            *
     *********************************************************************************/


    private void broadcastIntent(String intentName, String view) {
        ResponsaUtil.log("ResponsaDataManager", "broadcastIntent " + intentName);

        Intent intent = new Intent(intentName);
        intent.putExtra("targetView",view);
        ResponsaApplication.getAppContext().sendBroadcast(intent);
    }


    public UserResponsaObject createUserResponsaObject(ResponsaObject responsaObject) {
        UserResponsaObject userResponsaObject;
        ResponsaUtil.log("obj_debug", "Creating new userResponsaObject for responsaObject: "+responsaObject.getTitle());
        userResponsaObject = new UserResponsaObject();
        userResponsaObject.setUser(ParseUser.getCurrentUser());
        userResponsaObject.setResponsaObject(responsaObject);
        return userResponsaObject;
    }

    public ChallengeUserJoin createChallengeUserJoin(Challenge challenge) {
        ChallengeUserJoin challengeUserJoin;
        ResponsaUtil.log("obj_debug", "Creating new challengeUserJoin for challenge: "+ challenge.getTitle());
        challengeUserJoin = new ChallengeUserJoin();
        challengeUserJoin.setUser(ParseUser.getCurrentUser());
        challengeUserJoin.setChallenge(challenge);
        return challengeUserJoin;
    }

    public ChallengeUserJoin createChallengeUserJoin(ChallengeAnswer challengeAnswer) {
        ChallengeUserJoin challengeUserJoin;
        ResponsaUtil.log("obj_debug", "Creating new challengeUserJoin for challengeAnswer: " + challengeAnswer.getDescription());
        challengeUserJoin = new ChallengeUserJoin();
        challengeUserJoin.setUser(ParseUser.getCurrentUser());
        challengeUserJoin.setChallengeAnswer(challengeAnswer);
        return challengeUserJoin;
    }

    public boolean hasMore(String view, String targetViewObjectId) {
        QueryResult.MetaData metaData = retrieveLoadedMetaData(view, targetViewObjectId);

        return metaData.hasMore;
    }
}
