package br.org.eita.responsa.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseUser;

import java.util.Arrays;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.ChallengeAnswersAdapter;
import br.org.eita.responsa.adapter.data_manager.QueryResult;
import br.org.eita.responsa.components.LoadMoreComponent;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.components.EmojiconKeyboard;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.model.ChallengeAnswer;
import br.org.eita.responsa.model.ChallengeUserJoin;
import br.org.eita.responsa.model.Notification;
import br.org.eita.responsa.model.util.ResponsaSaveCallback;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.view.fragment.ChallengeAnswerDialogFragment;
import br.org.eita.responsa.view.fragment.ChallengeViewFragment;

public class ChallengeViewActivity extends BaseActivity {

    private ChallengeViewFragment challengeViewFragment;
    private ActionBar actionBar;

    private ListView answersListView;

    EmojiconKeyboard emojiconKeyboard;

    private Challenge challenge;
    private ChallengeAnswer editingChallengeAnswer = null;
    private QueryResult.MetaData challengeMetaData;
    private ChallengeUserJoin challengeUserJoin;
    private List<ChallengeAnswer> challengeAnswers;
    private QueryResult.MetaData challengeAnswersMetaData;
    private ChallengeAnswersAdapter challengeAnswersAdapter;

    public static final String INTENT_OBJECT_ID_KEY = "objectId";

    private BroadcastReceiver challengeFetchedReceiver;

    String challengeId = null;

    Boolean firstLoad = true;

    Toolbar rToolbar;

    private LoadMoreComponent loadMoreComponent;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        rToolbar.inflateMenu(R.menu.menu_feedback);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_feedback:
                intent = new Intent(this, FeedbackActivity.class);
                intent.putExtra("view", "challenge");
                intent.putExtra("viewObjectId",challengeId);
                intent.putExtra("viewClass",getClass().getSimpleName());
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_challenge_view_2);

        // Add toolbar and actionbar
        rToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(rToolbar);
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.u_challenge_view_title));

        // Challenge answers list:
        answersListView = (ListView) findViewById(R.id.answersListView);
        challengeAnswersAdapter = new ChallengeAnswersAdapter(this,this);

        // Add emoji keyboard:
        emojiconKeyboard = (EmojiconKeyboard) findViewById(R.id.emojiconKeyboard);
        emojiconKeyboard.setEmojiKeyboardEnabled(false);

        // Get challenge ID from intent on create view:
        Bundle extras = null;
        Intent intent = getIntent();
        if (intent != null) {
            extras = intent.getExtras();
            if (extras != null) {
                challengeId = extras.getString(ChallengeViewActivity.INTENT_OBJECT_ID_KEY);
            }
        }

        challengeFetchedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                List<Challenge> challenges = ResponsaApplication.responsaDataManager.retrieveLoadedData("challenge", challengeId);
                if (challenges.size() == 0) {
                    ResponsaUtil.log("NewDataManager ChallengeViewActivity", "Didn't find any cachedData at challengeId=" + challengeId);
                    return;
                }

                challenge = challenges.get(0);
                challengeMetaData = ResponsaApplication.responsaDataManager.retrieveLoadedMetaData("challenge", challengeId);

                challengeUserJoin = challenge.getChallengeUserJoin();

                challengeAnswers = ResponsaApplication.responsaDataManager.retrieveLoadedData("challengeAnswers", challengeId);
                challengeAnswersMetaData = ResponsaApplication.responsaDataManager.retrieveLoadedMetaData("challengeAnswers", challengeId);

                populateView();

                if (firstLoad) {
                    addEmojiconKeyboard();

                    // provisorily add user to this challenge push channel, so that new comments from other users will be updated automatically to him/her:
                    Notification.subscribeToChannel(Notification.CHALLENGE_CHANNEL, challengeId, true);
                }

                if (firstLoad || challengeAnswersMetaData.updateStatus== QueryResult.HAS_NEW_DATA) {
                    Boolean showKeyboard = true;
                    Iterator<ChallengeAnswer> it = challengeAnswers.iterator();
                    while (it.hasNext()) {
                        ChallengeAnswer answer = it.next();
                        if (answer.getAuthor().equals(ParseUser.getCurrentUser())) {
                            showKeyboard = false;
                        }
                    }
                    emojiconKeyboard.setEmojiKeyboardEnabled(!ParseUser.getCurrentUser().equals(challenge.getAuthor()) && showKeyboard);
                }

                if (firstLoad) {
                    firstLoad = false;
                }

            }
        };

        // Load data. When loaded, it will broadcast an intent to the receiver registered below:
        registerReceiver(challengeFetchedReceiver, new IntentFilter(new IntentFilter(ResponsaDataManager.getIntentDataUpdatedName("challenge"))));
        ResponsaApplication.responsaDataManager.load("challenge", challengeId);


    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        unregisterReceiver(challengeFetchedReceiver);
        // Remove user to the temporary challenge push channel if he/she is not owner nor did favorite this challenge:
        Notification.unsubscribeToChannel(Notification.CHALLENGE_CHANNEL, challengeId, true);
    }

    private void populateView() {
        // Set challenge being viewed by user:
        Challenge.currentChallenge = challenge;

        if (firstLoad) {
            LayoutInflater inflater = getLayoutInflater();
            View header = inflater.inflate(R.layout.fragment_challenge_view_header_outer, null);

            answersListView.addHeaderView(header);

            challengeViewFragment = (ChallengeViewFragment)
                    getSupportFragmentManager()
                            .findFragmentById(R.id.headerFragmentEmbedded);

            // On first load, the page must be zero
            // TODO: make this automatically via load
            challengeAnswersMetaData.page = 0;

            loadMoreComponent = new LoadMoreComponent(this, null, R.layout.progress_load_more);
            loadMoreComponent.initParams("challengeAnswers", challengeId, "challenge");
            answersListView.addFooterView(loadMoreComponent);
            answersListView.setAdapter(challengeAnswersAdapter);
        }

        loadMoreComponent.setPage(challengeAnswersMetaData.page);
        if (ResponsaApplication.responsaDataManager.hasMore("challengeAnswers", challengeId)) {
            loadMoreComponent.showBtnLoadMore();
        } else {
            loadMoreComponent.stopProgressBar();
            loadMoreComponent.hideBtnLoadMore();
        }

        if (firstLoad || challengeMetaData.updateStatus==QueryResult.HAS_NEW_DATA) {
            if (challenge.getCorrectAnswer() == null) {
                actionBar.setTitle(getResources().getString(R.string.u_challenge_view_title_challenge));
            } else {
                actionBar.setTitle(getResources().getString(R.string.u_challenge_view_title_discovery));
            }

            challengeViewFragment.updateChallenge(challenge, challengeUserJoin);
        }

        if (firstLoad || challengeMetaData.updateStatus==QueryResult.HAS_NEW_DATA || challengeAnswersMetaData.updateStatus==QueryResult.HAS_NEW_DATA) {
            challengeAnswersAdapter.populate(challenge, challengeAnswers, ResponsaApplication.responsaDataManager.retrieveLoadedMetaData("challengeAnswers", challengeId));
        }
    }

    public void addEmojiconKeyboard() {
        //Save text comment if sent
        emojiconKeyboard.setOnSubmitListener(new EmojiconKeyboard.OnEmojiKeyboardSubmitCallback() {
            @Override
            public void onSubmit(String text) {
                final Boolean editing = editingChallengeAnswer != null;

                ChallengeAnswer cha = null;
                if (editing) {
                    cha = editingChallengeAnswer;
                    editingChallengeAnswer = null;
                } else {
                    cha = new ChallengeAnswer();
                }
                final ChallengeAnswer challengeAnswer = cha;

                if (!editing) {
                    challengeAnswer.setAuthor(ParseUser.getCurrentUser());
                    challengeAnswer.setChallenge(challenge);
                    challengeAnswer.setLocalCreatedAt(new Date());
                    challengeAnswer.setChosenAnswer(false);
                }

                challengeAnswer.setDescription(text);


                ChallengeAnswer.currentChallengeAnswer = challengeAnswer;

                ChallengeAnswerDialogFragment.clickDialogYesCallback = new ChallengeAnswerDialogFragment.ClickDialogYesCallback() {
                    @Override
                    public void onClick() {
                        ChallengeAnswerDialogFragment.clickDialogYesCallback = null;
                        emojiconKeyboard.clearInput();

                        answersListView.smoothScrollToPositionFromTop(1, 100);
                        hideKeyboard();
                        if (!editing) {
                            challengeAnswersAdapter.insert(challengeAnswer, 0);
                            challenge.incrementAnswerCount();
                        }

                        ResponsaApplication.responsaDataManager.saveObject("challengeAnswers", challengeId, "challenge", challengeAnswer, true, new ResponsaSaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                //emojiconKeyboard.stopProgressBar();
                            }
                        });

                        ResponsaApplication.responsaDataManager.saveObject("challenge", challengeId, challenge, true, new ResponsaSaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    if (!editing) {
                                        String challengeAnswerDescription = challengeAnswer.getDescription();
                                        Notification.sendNotification(ResponsaApplication.getAppContext(), "answerChallenge", challenge, Arrays.asList(challengeAnswerDescription), Arrays.asList(challengeAnswerDescription));
                                    }
                                }
                            }
                        });

                        emojiconKeyboard.setEmojiKeyboardEnabled(false);
                    }
                };

                (new ChallengeAnswerDialogFragment()).show(getSupportFragmentManager(), "ChallengeAnswerDialogFragment");
            }
        });

        emojiconKeyboard.setPhotoEnabled(false);
        emojiconKeyboard.setHint(getString(R.string.u_answer_action_hint));
    }

    public void editChallengeAnswer(ChallengeAnswer ans) {
        emojiconKeyboard.setEmojiKeyboardEnabled(true);
        emojiconKeyboard.setText(ans.getDescription());
        editingChallengeAnswer = ans;
    }

    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public void onClickFavoritar(View view) {
        //ResponsaApplication.vibrate();
        Boolean newFavoritarState = challenge.toggleFavoritar();
        challengeViewFragment.populateFavoritar(newFavoritarState);
    }

}
