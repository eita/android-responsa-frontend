package br.org.eita.responsa.adapter;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.shamanland.fonticon.FontIconTextView;

import java.util.ArrayList;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.InitiativeCategory;
import br.org.eita.responsa.model.ResponsaObject;

//Gets pinned user objects that go to event
public class UserListAdapter extends ArrayAdapter<ParseUser> {

    public UserListAdapter(Context context) {
        super(context, 0, new ArrayList<ParseUser>());
    }

    public void populateAdapter(List list) {
        this.clear();
        this.addAll(list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ParseUser user = getItem(position);

        convertView = View.inflate(getContext(), android.R.layout.simple_list_item_1, null);

        TextView titleView = (TextView) convertView.findViewById(android.R.id.text1);
        titleView.setText(user.getString("name"));

        return convertView;
    }
}
