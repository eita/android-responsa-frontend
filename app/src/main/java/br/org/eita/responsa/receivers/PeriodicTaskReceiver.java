package br.org.eita.responsa.receivers;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.SystemClock;
import android.preference.PreferenceManager;

import com.google.common.base.Strings;

import br.org.eita.responsa.location.LocationService;
import br.org.eita.responsa.util.ResponsaUtil;

public class PeriodicTaskReceiver extends BroadcastReceiver {

    private static final String TAG = "P-ResponsaLocation";
    public static final String INTENT_FETCH_LOCATION_NOW = "br.org.eita.responsa.FETCH_LOCATION_NOW";

    public static final String BACKGROUND_SERVICE_BATTERY_CONTROL = "org.eita.responsa.backgroundServiceBatteryControl";

    @Override
    public void onReceive(Context context, Intent intent) {
        if (!Strings.isNullOrEmpty(intent.getAction())) {
            ResponsaUtil.log("P-ResponsaLocation", "PeriodicTaskReceiver.onReceive ACTION="+intent.getAction());

            SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

            //ResponsaApplication myApplication = (ResponsaApplication) context.getApplicationContext();
            //SharedPreferences sharedPreferences = myApplication.getSharedPreferences();

            if (intent.getAction().equals("android.intent.action.BATTERY_LOW")) {
                sharedPreferences.edit().putBoolean(BACKGROUND_SERVICE_BATTERY_CONTROL, false).apply();
                stopPeriodicTask(context);
            } else if (intent.getAction().equals("android.intent.action.BATTERY_OKAY")) {
                sharedPreferences.edit().putBoolean(BACKGROUND_SERVICE_BATTERY_CONTROL, true).apply();
                restartPeriodicTask(context);
            } else if (intent.getAction().equals(INTENT_FETCH_LOCATION_NOW)) {
                doPeriodicTask(context);
            }
        }
    }

    private void doPeriodicTask(Context context) {
        ResponsaUtil.log("P-ResponsaLocation", "PeriodicTaskReceiver.doPeriodicTask");

        // Periodic task(s) go here: Start Location Service
        Intent serviceIntent = new Intent(context, LocationService.class);
        context.startService(serviceIntent);
    }

    public void restartPeriodicTask(Context context) {
        ResponsaUtil.log("P-ResponsaLocation", "PeriodicTaskReceiver.restartPeriodicTask");

        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);

        boolean isBatteryOk = sharedPreferences.getBoolean(BACKGROUND_SERVICE_BATTERY_CONTROL, true);
        Intent alarmIntent = new Intent(context, PeriodicTaskReceiver.class);
        boolean isAlarmUp = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_NO_CREATE) != null;

        if (isBatteryOk && !isAlarmUp) {

            Integer timerMinutes = Integer.parseInt(sharedPreferences.getString("p_interval_to_find_position", "10"));
            if (timerMinutes == 0) {
                ResponsaUtil.log("ResponsaLocation","ALARM - NOT SETTING DUE TO PREFERENCES");
                return;
            }

            ResponsaUtil.log("ResponsaLocation","ALARM SETUP TO MINS: " + timerMinutes.toString());


            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmIntent.setAction(INTENT_FETCH_LOCATION_NOW);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), timerMinutes * 60000, pendingIntent);
        } else {
            ResponsaUtil.log("ResponsaLocation","ALARM - NO NEED TO SETUP");
        }
    }

    public void stopPeriodicTask(Context context) {
        ResponsaUtil.log("P-ResponsaLocation", "ALARM DESTROY");

        AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
        Intent alarmIntent = new Intent(context, PeriodicTaskReceiver.class);
        alarmIntent.setAction(INTENT_FETCH_LOCATION_NOW);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
        alarmManager.cancel(pendingIntent);
    }
}