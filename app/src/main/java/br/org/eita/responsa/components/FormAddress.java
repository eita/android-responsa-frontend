package br.org.eita.responsa.components;

import android.content.Context;
import android.location.Address;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.model.Addressable;
import br.org.eita.responsa.model.CepAddress;
import br.org.eita.responsa.model.GeocoderAddress;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.util.ResponsaUtil;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


public class FormAddress extends RelativeLayout{

    EditText homeCep;
    EditText homeAddress;
    EditText homeNeighborhood;

    TextView homeNeighborhoodLabel;

    Button registerAddressButton;
    ImageView fsMapView;

    //MapScrollView mapScroll;

    Address address;
    private Addressable addressable;
    private OnChangeTerritoryCallback onChangeTerritoryCallback;

    public FormAddress(Context context) {
        this(context, null);
    }

    public FormAddress(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FormAddress(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.component_form_address, this);

        findAllViewsByIds(view);


        homeCep.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean hasFocus) {
                if (!hasFocus) {
                    CepAddress.buscaCep(homeCep.getText().toString(), new Callback<CepAddress>() {
                        @Override
                        public void success(CepAddress cepAddress, Response response) {

                            addressable.fromCepAddress(cepAddress);

                            if (onChangeTerritoryCallback != null) {
                                onChangeTerritoryCallback.onChange(addressable.getResponsaTerritory());
                            }

                            fsMapView.setVisibility(View.GONE);

                            searchCoordinatesByAddressAndUpdateView(cepAddress.toString());

                            populateForm();
                        }

                        @Override
                        public void failure(RetrofitError error) {
                            //does nothing
                        }
                    });

                }
            }
        });

        homeAddress.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    String endereco = ((EditText) view).getText().toString();
                    String localidade = null;

                    HomeTerritory location = addressable.getResponsaTerritory();
                    if (location != null) {
                        localidade = location.title;
                        searchCoordinatesByAddressAndUpdateView(endereco + ", " + localidade + ", Brazil" );
                    }
                }
            }
        });
    }

    public String getAddress() {
        return homeAddress.getText().toString();
    }

    public String getNeighborhood() {
        return homeNeighborhood.getText().toString();
    }

    public String getCep() {
        return homeCep.getText().toString();
    }

    private void searchCoordinatesByAddressAndUpdateView(String address) {
        GeocoderAddress.searchCoordinates(address, new Callback<List<GeocoderAddress>>() {
            @Override
            public void success(List<GeocoderAddress> geocoderAddressList, Response response) {
                if (geocoderAddressList.size() > 0) {
                    addressable.fromGeocoderAddress(geocoderAddressList.get(0));
                    if (onChangeTerritoryCallback != null) {
                        onChangeTerritoryCallback.onChange(addressable.getResponsaTerritory());
                    }
                    showMap(addressable.getCoordinates());
                    //Toast.makeText(getActivity(), geocoderAddress.lat.toString() + ":" + geocoderAddress.lon.toString() , Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void failure(RetrofitError error) {
                ResponsaUtil.log("responsa_geocoding", "error in searching for address");
            }
        });
    }

    public void setAddressable(Addressable addressable) {
        this.addressable = addressable;
        populateForm();
    }

    private void findAllViewsByIds(View parentView) {
        homeCep = (EditText) parentView.findViewById(R.id.homeCep);
        homeAddress = (EditText) parentView.findViewById(R.id.homeAddress);
        homeNeighborhood = (EditText) parentView.findViewById(R.id.homeNeighborhood);
        registerAddressButton = (Button) parentView.findViewById(R.id.registerAddressButton);
        fsMapView = (ImageView) parentView.findViewById(R.id.fsMapView);
        homeNeighborhoodLabel = (TextView) parentView.findViewById(R.id.homeNeighborhoodLabel);

    }

    private void populateForm() {
        homeCep.setText(addressable.getCep());
        homeAddress.setText(addressable.getRuaENumero());
        homeNeighborhood.setText(addressable.getBairro());
        showMap(addressable.getCoordinates());
    }

    public void sweepForm() {
        addressable.setCep(homeCep.getText().toString());
        addressable.setRuaENumero(homeAddress.getText().toString());
        addressable.setBairro(homeNeighborhood.getText().toString());
    }

    private void showMap(LocationCoordinates coordinates) {
        if (coordinates != null) {
            new DownloadMapViewTask(fsMapView,coordinates,0.3F).execute();
        } else {
            fsMapView.setVisibility(View.GONE);
        }
    }

    public void onChangeTerritory(OnChangeTerritoryCallback callback) {
        this.onChangeTerritoryCallback = callback;
    }

    public interface OnChangeTerritoryCallback {
        public void onChange(HomeTerritory homeTerritory);
    }

    public void setEditable(Boolean editable) {
        homeCep.setEnabled(editable);
        homeAddress.setEnabled(editable);
        homeNeighborhood.setEnabled(editable);
    }

    public void setNeighborhoodVisibility(Boolean visible) {
        if (visible) {
            homeNeighborhood.setVisibility(VISIBLE);
            homeNeighborhoodLabel.setVisibility(VISIBLE);
        } else {
            homeNeighborhood.setVisibility(GONE);
            homeNeighborhoodLabel.setVisibility(GONE);
        }
    }

}
