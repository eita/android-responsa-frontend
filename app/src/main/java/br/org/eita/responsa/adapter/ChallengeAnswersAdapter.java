package br.org.eita.responsa.adapter;


import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.parse.DeleteCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.shamanland.fonticon.FontIconTextView;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.data_manager.QueryResult;
import br.org.eita.responsa.components.VoteUpDownWidget;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.model.ChallengeAnswer;
import br.org.eita.responsa.model.ChallengeUserJoin;
import br.org.eita.responsa.model.Notification;
import br.org.eita.responsa.model.util.ResponsaSaveCallback;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.view.activity.ChallengeViewActivity;
import br.org.eita.responsa.view.fragment.ChallengeAnswerApprovalDialogFragment;
import br.org.eita.responsa.view.fragment.ChallengeAnswerDialogFragment;

public class ChallengeAnswersAdapter extends ArrayAdapter<ChallengeAnswer> {

    private Challenge challenge;
    private ChallengeViewActivity activity;

    public ChallengeAnswersAdapter(Context context, ChallengeViewActivity activity) {
        super(context, 0, new ArrayList<ChallengeAnswer>());
        this.activity = activity;
    }

    public void populate(Challenge challenge, List<ChallengeAnswer> challengeAnswers, QueryResult.MetaData challengeAnswersMetaData) {
        this.challenge = challenge;
        Integer page = challengeAnswersMetaData.page;
        ResponsaUtil.log("challengeAnswersAdapter", "loadMore -> page=" + page + ", limit=" + ResponsaApplication.responsaDataManager.getListLimit("challengeAnswers"));
        this.clear();
        this.addAll(challengeAnswers);
        /*if (page == 0) {
            this.clear();
            this.addAll(challengeAnswers);
        } else {*/
        /*
            Integer limit = ResponsaApplication.responsaDataManager.getListLimit("challengeAnswers");
            Integer begin = page * limit;
            Integer end = begin + limit;
            for (int i=begin; i<end; i++) {
                this.add(challengeAnswers.get(i));
            }
        */
        /*}*/

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ChallengeAnswer challengeAnswer = getItem(position);

        convertView = View.inflate(getContext(), R.layout.challenge_answer_item_view, null);

        TextView challengeAnswerText = (TextView) convertView.findViewById(R.id.challengeAnswerText);
        final BootstrapCircleThumbnail userImageView = (BootstrapCircleThumbnail) convertView.findViewById(R.id.userImage);

        final VoteUpDownWidget voteUpDownWidget = (VoteUpDownWidget) convertView.findViewById(R.id.voteUpDownWidget);
        voteUpDownWidget.setVotable(challengeAnswer);

        voteUpDownWidget.setVoteEnabled(false, null);

        TextView textUserNameDate = (TextView) convertView.findViewById(R.id.textUserNameDate);

        Date createdAtTime = challengeAnswer.getCreatedAt();
        String dateString = (createdAtTime == null) ? getContext().getString(R.string.u_in_this_moment) : DateUtils.getRelativeTimeSpanString(createdAtTime.getTime()).toString();

        textUserNameDate.setText(challengeAnswer.getAuthor().getString("name") + ", " + dateString);

        RelativeLayout answerContent =  (RelativeLayout) convertView.findViewById(R.id.answerContent);

        answerContent.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (challengeAnswer.getAuthor().equals(ParseUser.getCurrentUser())) {
                    CharSequence actions[] = new CharSequence[] {"Editar", "Apagar"};

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Resposta");
                    builder.setItems(actions, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0: //Editar
                                    activity.editChallengeAnswer(challengeAnswer);
                                    break;
                                case 1:
                                    challengeAnswer.deleteInBackground(new DeleteCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                challenge.decrementAnswerCount();
                                                ResponsaApplication.responsaDataManager.saveObject("challenge", challenge.getObjectId(), challenge, true, null);

                                                Toast.makeText(getContext(), "Resposta apagada", Toast.LENGTH_LONG).show();
                                                //ResponsaApplication.responsaDataManager.load("challenge",challenge.getObjectId(),"force_remote");
                                            } else {
                                                Toast.makeText(getContext(), "Resposta não pôde ser apagada. Tente novamente mais tarde.", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                                    break;
                            }
                        }
                    });
                    builder.show();
                }
            }
        });


        if (challengeAnswer.equals(this.challenge.getCorrectAnswer())) {
            FontIconTextView solvedIcon = (FontIconTextView) convertView.findViewById(R.id.solvedIcon);
            solvedIcon.setVisibility(View.VISIBLE);
        }

        if (this.challenge.getAuthor().equals(ParseUser.getCurrentUser()) && this.challenge.getCorrectAnswer() == null) {
            FontIconTextView solveChallengeButton = (FontIconTextView) convertView.findViewById(R.id.solveChallengeButton);
            solveChallengeButton.setVisibility(View.VISIBLE);
            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ChallengeAnswerApprovalDialogFragment.clickDialogYesCallback = new ChallengeAnswerApprovalDialogFragment.ClickDialogYesCallback() {
                        @Override
                        public void onClick() {
                            ChallengeAnswerApprovalDialogFragment.clickDialogYesCallback = null;
                            challengeAnswer.choose();
                        }
                    };
                    ChallengeAnswer.currentChallengeAnswer = challengeAnswer;

                    (new ChallengeAnswerApprovalDialogFragment()).show(((FragmentActivity) getContext()).getSupportFragmentManager(), "ChallengeAnswerApprovalDialogFragment");

                }
            });
        }

        challengeAnswer.findAnswerVote(new ChallengeAnswer.FindAnswerVoteCallback() {
            @Override
            public void onAnswerVoteFetchCompleted(ChallengeUserJoin challengeUserJoin, ParseException e) {
                voteUpDownWidget.setVotable(challengeAnswer);
                Boolean userIsAnswerAuthor = ParseUser.getCurrentUser().equals(challengeAnswer.getAuthor());
                String disabledMessage = (userIsAnswerAuthor) ? getContext().getString(R.string.u_vote_answer_disabled_user_is_author) : null;
                voteUpDownWidget.setVoteEnabled(!userIsAnswerAuthor, disabledMessage);
            }
        });

        //challengeAnswer text
        String strChallengeAnswerText = challengeAnswer.getDescription();
        if (strChallengeAnswerText != null && !strChallengeAnswerText.isEmpty()) {
            challengeAnswerText.setText(strChallengeAnswerText);
            challengeAnswerText.setVisibility(View.VISIBLE);
        }

        return convertView;
    }
}

    /*
   private Challenge challenge;


    public HashMap<String, Bitmap> cachedImages = new HashMap<String,Bitmap>();

    public ChallengeAnswersAdapter(Context context, final Challenge challenge) {
        super(context, new QueryFactory<ChallengeAnswer>() {
            @Override
            public ParseQuery<ChallengeAnswer> create() {
                ParseQuery<ChallengeAnswer> query = new ParseQuery(ChallengeAnswer.class);
                query.whereEqualTo("challenge", challenge);
                //query.fromPin(challengeAnswer.getPinName(responsaObject));
                query.orderByDescending("chosenAnswer");
                query.orderByDescending("localCreatedAt");
                query.setLimit(10);
                query.fromLocalDatastore();
                query.include("author");
                return query;
            }
        });
        this.challenge = challenge;
    }

    @Override
    public View getItemView(final ChallengeAnswer challengeAnswer, View v, ViewGroup parent) {
        v = View.inflate(getContext(), R.layout.challenge_answer_item_view, null);
        super.getItemView(challengeAnswer, v, parent);

        TextView challengeAnswerText = (TextView) v.findViewById(R.id.challengeAnswerText);
        final BootstrapCircleThumbnail userImageView = (BootstrapCircleThumbnail) v.findViewById(R.id.userImage);

        final VoteUpDownWidget voteUpDownWidget = (VoteUpDownWidget) v.findViewById(R.id.voteUpDownWidget);
        voteUpDownWidget.setVotable(challengeAnswer);

        voteUpDownWidget.setVoteEnabled(false, null);

        TextView textUserNameDate = (TextView) v.findViewById(R.id.textUserNameDate);

        Date createdAtTime = challengeAnswer.getChallenge().getCreatedAt();
        String dateString = (createdAtTime == null) ? getContext().getString(R.string.u_in_this_moment) : DateUtils.getRelativeTimeSpanString(createdAtTime.getTime()).toString();

        textUserNameDate.setText(challengeAnswer.getAuthor().getString("name") + ", " + dateString);

        if (challengeAnswer.equals(this.challenge.getCorrectAnswer())) {
            FontIconTextView solvedIcon = (FontIconTextView) v.findViewById(R.id.solvedIcon);
            solvedIcon.setVisibility(View.VISIBLE);
        }

        if (this.challenge.getAuthor().equals(ParseUser.getCurrentUser()) && this.challenge.getCorrectAnswer() == null) {
            FontIconTextView solveChallengeButton = (FontIconTextView) v.findViewById(R.id.solveChallengeButton);
            solveChallengeButton.setVisibility(View.VISIBLE);
            v.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    ChallengeAnswerApprovalDialogFragment.clickDialogYesCallback = new ChallengeAnswerApprovalDialogFragment.ClickDialogYesCallback() {
                        @Override
                        public void onClick() {
                            ChallengeAnswerApprovalDialogFragment.clickDialogYesCallback = null;
                            challengeAnswer.choose();
                        }
                    };
                    ChallengeAnswer.currentChallengeAnswer = challengeAnswer;

                    (new ChallengeAnswerApprovalDialogFragment()).show(((FragmentActivity) getContext()).getSupportFragmentManager(), "ChallengeAnswerApprovalDialogFragment");

                }
            });
        }

        challengeAnswer.findAnswerVote(new ChallengeAnswer.FindAnswerVoteCallback() {
            @Override
            public void onAnswerVoteFetchCompleted(ChallengeUserJoin challengeUserJoin, ParseException e) {
                voteUpDownWidget.setVotable(challengeAnswer);
                Boolean userIsAnswerAuthor = ParseUser.getCurrentUser().equals(challengeAnswer.getAuthor());
                String disabledMessage = (userIsAnswerAuthor) ? getContext().getString(R.string.u_vote_answer_disabled_user_is_author) : null;
                voteUpDownWidget.setVoteEnabled(!userIsAnswerAuthor, disabledMessage);
            }
        });

        //challengeAnswer text
        String strChallengeAnswerText = challengeAnswer.getDescription();
        if (strChallengeAnswerText != null && !strChallengeAnswerText.isEmpty()) {
            challengeAnswerText.setText(strChallengeAnswerText);
            challengeAnswerText.setVisibility(View.VISIBLE);
        }

        return v;
    }

}
*/
