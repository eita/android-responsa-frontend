package br.org.eita.responsa.model;


import br.org.eita.responsa.location.LocationCoordinates;

public interface Addressable {

    public void setCep(String cep);
    public void setRuaENumero(String logradouro);
    public void setBairro(String bairro);

    //Gets data from a given cepAddress
    void fromCepAddress(CepAddress cepAddress);

    //Gets data from a given geocoderAddress
    void fromGeocoderAddress(GeocoderAddress geocoderAddress);

    public String getCep();
    public String getRuaENumero();
    public String getBairro();
    public LocationCoordinates getCoordinates();
    public HomeTerritory getResponsaTerritory();
}
