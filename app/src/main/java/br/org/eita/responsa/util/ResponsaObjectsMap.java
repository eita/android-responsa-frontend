package br.org.eita.responsa.util;

import android.graphics.Color;

import com.google.common.math.DoubleMath;
import com.google.common.primitives.Booleans;
import com.mapbox.mapboxsdk.annotations.PolygonOptions;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapboxMap;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.data_manager.LastFetchedAt;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.model.ResponsaObject;

public class ResponsaObjectsMap {

    public static LatLngBounds mapViewBounds;

    public static Boolean containsLatLngBounds(List<LatLngBounds> containerArray, LatLngBounds internal) {
        ResponsaUtil.log("colchaderetalhos", "-----------------------");
        ResponsaUtil.log("colchaderetalhos", "entered containsLatLngBounds to check box "+internal.toString());
        if (containerArray==null || containerArray.size()==0) {
            ResponsaUtil.log("colchaderetalhos", "returned false because container is empty");
            return false;
        }
        ResponsaUtil.log("colchaderetalhos", "checking containsLatLngBounds of "+containerArray.size()+" boxes");
        ResponsaUtil.log("colchaderetalhos", "boxes: "+containerArray.toString());
        for(int i=0; i<containerArray.size(); i++) {
            if (containsLatLngBounds(containerArray.get(i), internal)) {
                ResponsaUtil.log("colchaderetalhos", "returned true, it's contained in container "+i);
                return true;
            }
        }
        ResponsaUtil.log("colchaderetalhos", "returned false because no container contains this internal box"+internal.toString());
        return false;
    }

    public static Boolean containsLatLngBounds(LatLngBounds container, LatLngBounds internal) {
        if (container == null) {
            return false;
        }
        LatLng SW = new LatLng(internal.getLatSouth(),internal.getLonWest());
        LatLng NE = new LatLng(internal.getLatNorth(),internal.getLonEast());

        if (!container.contains(SW) || !container.contains(NE)) {
            return false;
        }
        return true;
    }

    /**
     * This function removes latLngBounds which are included in some other latlngbound, to keep clean the memory of latlngbounds
     * @param latLngBoundsList
     * @return
     */
    public static List<LatLngBounds> cleanLatLngBoundsArray(List<LatLngBounds> latLngBoundsList) {
        ResponsaUtil.log("colchaderetalhos", "-------------------");
        ResponsaUtil.log("colchaderetalhos", "entered cleanLatLngBoundsArray");
        if (latLngBoundsList==null || latLngBoundsList.size()<=1) {
            ResponsaUtil.log("colchaderetalhos", "latLngBoundsList is empty or has only one element. returned.");
            return latLngBoundsList;
        }
        ResponsaUtil.log("colchaderetalhos", "latLngBoundsList: "+latLngBoundsList.size());
        int toBeRemoved[] = new int[latLngBoundsList.size()];
        for (int i=0; i<toBeRemoved.length; i++) {
            toBeRemoved[i] = -1;
        }
        for (int i=0; i<latLngBoundsList.size(); i++) {
            for (int j=0; j<latLngBoundsList.size(); j++) {
                if (i!=j && containsLatLngBounds(latLngBoundsList.get(i), latLngBoundsList.get(j)) && toBeRemoved[j]==-1) {
                    toBeRemoved[j] = 1;
                }
            }
        }
        List<LatLngBounds> newLatLngBoundsList = new ArrayList<LatLngBounds>();
        for (int i=0; i<latLngBoundsList.size(); i++) {
            if (toBeRemoved[i]==-1) {
                newLatLngBoundsList.add(latLngBoundsList.get(i));
            } else {
                ResponsaUtil.log("colchaderetalhos", "Removed item " + i + ": "+latLngBoundsList.get(i));
            }
        }
        ResponsaUtil.log("colchaderetalhos", "final latLngBoundsList: "+newLatLngBoundsList.size());
        return newLatLngBoundsList;
    }

}
