package br.org.eita.responsa;

import android.app.Activity;
import android.app.Application;
import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.os.Vibrator;

import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.SaveCallback;
import com.shamanland.fonticon.FontIconTypefaceHolder;

import java.util.Date;

import br.org.eita.responsa.location.PeriodicLocationUpdateService;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.managers.ResponsaViewManager;
import br.org.eita.responsa.model.AppLog;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.model.ChallengeAnswer;
import br.org.eita.responsa.model.ChallengeUserJoin;
import br.org.eita.responsa.model.Chegada;
import br.org.eita.responsa.model.Comment;
import br.org.eita.responsa.model.Feedback;
import br.org.eita.responsa.model.Notification;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.model.User;
import br.org.eita.responsa.model.UserResponsaObject;
import br.org.eita.responsa.model.Version;
import br.org.eita.responsa.storage.queries.TerritoryDb;
import br.org.eita.responsa.util.ResponsaUtil;


public class ResponsaApplication extends Application {

    public static String installationObjectId = "";

    public static User currentUser = null;

    //Used in opening show views
    public static Boolean RESPONSA_IS_IN_FOREGROUND = false;

    private static LocationCoordinates lastLocation = null;

    //Static reference to context, for static methods
    //Use: ResponsaApplication.getAppContext();
    private static Context context;

    public static TerritoryDb territoryDb;
    public static ResponsaDataManager responsaDataManager;
    public static ResponsaViewManager responsaViewManager;

    @Override
    public void onCreate() {

        // http://stackoverflow.com/questions/704311/android-how-do-i-investigate-an-anr
        /*StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder()
                .detectAll()
                .penaltyLog()
                .penaltyDeath()
                .build()); */

        super.onCreate();

        Parse.enableLocalDatastore(this);
        ParseObject.registerSubclass(Notification.class);
        ParseObject.registerSubclass(ResponsaObject.class);
        ParseObject.registerSubclass(UserResponsaObject.class);
        ParseObject.registerSubclass(Chegada.class);
        ParseObject.registerSubclass(Comment.class);
        ParseObject.registerSubclass(Challenge.class);
        ParseObject.registerSubclass(ChallengeAnswer.class);
        ParseObject.registerSubclass(ChallengeUserJoin.class);
        ParseObject.registerSubclass(Feedback.class);
        ParseObject.registerSubclass(Version.class);
        ParseObject.registerSubclass(AppLog.class);

        Parse.initialize(new Parse.Configuration.Builder(this)
                        .applicationId(Constants.sashidoApplicationId)
                        .clientKey(Constants.sashidoClientKey)
                        .server(Constants.sashidoEndpoint)
                        .enableLocalDataStore()
                        .build()
        );

        final ParseInstallation installation = ParseInstallation.getCurrentInstallation();

        //Updates installation data, like appVersion and others
        //installation.put("updatedAt",new Date());

        installation.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) { //success
                    ResponsaApplication.installationObjectId = installation.getObjectId();
                } else {
                    ResponsaUtil.log("Installation error", "Did not succeed creating new installation");
                }
            }
        });

        ResponsaApplication.currentUser = User.getCurrentUser(this);

        ResponsaApplication.context = getApplicationContext();

        //Start Ibge Location Database
        territoryDb = new TerritoryDb(this);
        territoryDb.start();

        ResponsaApplication.initResponsaDataManager();
        ResponsaApplication.initResponsaViewManager(getApplicationContext());

        FontIconTypefaceHolder.init(getAssets(), "iconesresponsa.ttf");

        Notification.loadConfiguration();

        //Starts periodic location update service
        Intent serviceIntent = new Intent(context, PeriodicLocationUpdateService.class);
        context.startService(serviceIntent);


        /*registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks() {
            @Override
            public void onActivityCreated(Activity activity, Bundle bundle) {
                activity.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
            }

            @Override
            public void onActivityStarted(Activity activity) {

            }

            @Override
            public void onActivityResumed(Activity activity) {

            }

            @Override
            public void onActivityPaused(Activity activity) {

            }

            @Override
            public void onActivityStopped(Activity activity) {

            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle bundle) {

            }

            @Override
            public void onActivityDestroyed(Activity activity) {

            }
        });*/
    }

    @Override
    public void onTerminate() {
        finishResponsaDataManager();
        super.onTerminate();
        RESPONSA_IS_IN_FOREGROUND = false;
    }

    public static Context getAppContext() {
        return ResponsaApplication.context;
    }

    public static boolean isNetworkAvailable(Context c) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) c.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void vibrate() {
        Vibrator v = (Vibrator) getAppContext().getSystemService(Context.VIBRATOR_SERVICE);
        v.vibrate(200);
    }

    public static Boolean isAppInForeground() {
        Context appContext = ResponsaApplication.getAppContext();
        if (appContext == null) return false;

        return RESPONSA_IS_IN_FOREGROUND;
    }

    //Creates responsaViewManager and starts to fetch data
    public static void initResponsaViewManager(Context context) {
        if (ResponsaApplication.responsaViewManager == null) {
            ResponsaApplication.responsaViewManager = new ResponsaViewManager(context);
        }
    }

    //Creates responsaDataManager and starts to fetch data
    public static void initResponsaDataManager() {
        if (ResponsaApplication.responsaDataManager == null && ResponsaApplication.territoryDb != null) {
            ResponsaApplication.responsaDataManager = new ResponsaDataManager(getAppContext());
            ResponsaApplication.responsaDataManager.init();
        }
    }

    public static void finishResponsaDataManager() {
        ResponsaApplication.responsaDataManager.finish();
    }
}
