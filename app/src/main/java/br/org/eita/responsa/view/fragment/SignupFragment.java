package br.org.eita.responsa.view.fragment;


import android.app.AlertDialog;
import android.app.Fragment;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.ParseUser;

import java.util.ArrayList;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.components.DDDButton;
import br.org.eita.responsa.components.TerritoryChooser;
import br.org.eita.responsa.components.ResponsaPhoneNumber;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.User;
import br.org.eita.responsa.model.validation.ValidationError;
import br.org.eita.responsa.view.activity.SignupActivity;
import retrofit.RetrofitError;

public class SignupFragment extends Fragment implements View.OnClickListener, User.Registrator, DDDButton.OnDDDChangeListener {

    public static String receivedConfirmationCode = null;

    public EditText userIdInput;
    public Button signupButton;

    public EditText confirmationCodeInput;
    public Button sendCodeButton;

    public LinearLayout enterPhoneNumberPanel;
    public LinearLayout pendingConfirmationPanel;
    public LinearLayout userNameAndTerritoryQueryPanel;
    public LinearLayout smsFailurePanel;


    public BroadcastReceiver smsConfirmationReceiver;

    public Button wrongNumberButton;
    public Button sendSmsAgainButton;
    public Button sendSmsAgainButton2;
    public Button backToFillNumberButton;
    public DDDButton dddButton;
    public TextView textViewSmsTimeout;
    public TextView textViewSignupPending;
    public TextView textViewSmsFailure;

    public EditText userNickname;
    public TerritoryChooser userTerritory;
    public Button userNameAndTerritorySubmitButton;

    ProgressDialog progress;

    Boolean nameValidated = false;
    HomeTerritory selectedTerritory = null;

    @Override
    public void onDestroyView() {
        getActivity().unregisterReceiver(smsConfirmationReceiver);
        super.onDestroyView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_signup, container, false);

        signupButton = (Button) view.findViewById(R.id.signupButton);
        signupButton.setOnClickListener(this);

        userIdInput = (EditText) view.findViewById(R.id.userId);
        //userIdInput.addTextChangedListener(new PhoneNumberFormattingTextWatcher());
        userIdInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validateRegistrationForm(charSequence, null);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        confirmationCodeInput = (EditText) view.findViewById(R.id.confirmationCode);
        confirmationCodeInput.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                sendCodeButton.setEnabled(charSequence.length() >= 6);
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        sendCodeButton = (Button) view.findViewById(R.id.sendCodeButton);
        sendCodeButton.setOnClickListener(this);

        dddButton = (DDDButton) view.findViewById(R.id.dddButton);
        dddButton.setOnDDDChangeListener(this);


        enterPhoneNumberPanel = (LinearLayout) view.findViewById(R.id.enterPhoneNumberPanel);
        pendingConfirmationPanel = (LinearLayout) view.findViewById(R.id.pendingConfirmationPanel);
        userNameAndTerritoryQueryPanel = (LinearLayout) view.findViewById(R.id.userNameAndLocationQueryPanel);
        smsFailurePanel = (LinearLayout) view.findViewById(R.id.smsFailurePanel);

        wrongNumberButton = (Button) view.findViewById(R.id.wrongNumberButton);
        wrongNumberButton.setOnClickListener(this);

        ResponsaPhoneNumber phoneNumber = ResponsaApplication.currentUser.getPhoneNumber();
        if (phoneNumber != null) {
            Resources res = getActivity().getResources();
            wrongNumberButton.setText(String.format(res.getString(R.string.u_signup_button_my_number_is_wrong), new String[]{phoneNumber.getFormattedPhoneWithAreaCode()}));
        }

        sendSmsAgainButton = (Button) view.findViewById(R.id.sendSmsAgainButton);
        sendSmsAgainButton.setOnClickListener(this);

        sendSmsAgainButton2 = (Button) view.findViewById(R.id.sendSmsAgainButton2);
        sendSmsAgainButton2.setOnClickListener(this);

        backToFillNumberButton = (Button) view.findViewById(R.id.backToFillNumberButton);
        backToFillNumberButton.setOnClickListener(this);

        textViewSmsTimeout = (TextView) view.findViewById(R.id.textViewSmsTimeout);
        textViewSignupPending = (TextView) view.findViewById(R.id.textViewSignupPending);
        textViewSmsFailure = (TextView) view.findViewById(R.id.textViewSmsFailure);


        userNickname = (EditText) view.findViewById(R.id.userNickname);;
        userTerritory = (TerritoryChooser) view.findViewById(R.id.userLocation);
        userNameAndTerritorySubmitButton = (Button) view.findViewById(R.id.userNameAndTerritorySubmit);
        userNameAndTerritorySubmitButton.setOnClickListener(this);

        userTerritory.onChangeSelectedTerritory(new TerritoryChooser.OnChangeTerritoryListener() {
            @Override
            public void onChange(HomeTerritory territory) {
                selectedTerritory = territory;
                validateNameAndTerritory();
            }
        });

        userNickname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                nameValidated = charSequence.length() > 0;
                validateNameAndTerritory();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });
/*
        String[] countries = { "Belgium", "France", "Italy", "Germany", "Spain"};

        ArrayAdapter<String> frontPageAdapter = new ArrayAdapter<String>(getActivity(), android.R.layout.simple_dropdown_item_1line, countries);

        userTerritory.setAdapter(frontPageAdapter);

*/

        smsConfirmationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                receivedConfirmationCode = intent.getStringExtra("sms");
                User.State userState = ResponsaApplication.currentUser.getState();
                if (userState == User.State.CONFIRMED || userState == User.State.CONFIRMATION_TIMEOUT) {
                    confirmUser(receivedConfirmationCode);
                }
            }
        };
        getActivity().registerReceiver(smsConfirmationReceiver, new IntentFilter(Constants.smsAction));

        changeViewOnUserState();

        //Já tem os dados necessários pra fazer login no Parse
        if (ResponsaApplication.currentUser.getState() == User.State.CONFIRMED && ParseUser.getCurrentUser() == null) {
            ResponsaApplication.currentUser.parseLogInBackground(ResponsaApplication.currentUser.getFirstUserSetupCallback(null,this));

        }


        return view;
    }

    private ArrayList<ValidationError> validateNameAndTerritory() {
        ArrayList<ValidationError> errors = new ArrayList<>();
        if (!nameValidated) {
            errors.add(new ValidationError("User Form error", getString(R.string.v_user_no_nickname)));
        }
        errors.addAll(userTerritory.validate());
        userNameAndTerritorySubmitButton.setEnabled(errors.isEmpty());
        return errors;
    }

    private void validateRegistrationForm(CharSequence userId, Boolean isDDDFilled) {
        if (isDDDFilled == null) {
            isDDDFilled = dddButton.getDddSelected();
        }

        if (!isDDDFilled) {
            userIdInput.setVisibility(View.GONE);
            signupButton.setEnabled(false);
            return;
        } else {
            userIdInput.setVisibility(View.VISIBLE);
        }

        if (userId == null) {
            userId = userIdInput.getText().toString();
        }

        ResponsaPhoneNumber phoneNumber = new ResponsaPhoneNumber(getActivity());

        //makes validation
        if (phoneNumber.setPhone(userId)) {
            signupButton.setEnabled(true);
        } else {
            signupButton.setEnabled(false);
        }
    }

    private void changeViewOnUserState() {
        User.State userState = ResponsaApplication.currentUser.getState();

        if (userState == User.State.NEW || userState == User.State.PHONE_NUMBER_RESETED) {
            enterPhoneNumberPanel.setVisibility(View.VISIBLE);
            pendingConfirmationPanel.setVisibility(View.GONE);
            userNameAndTerritoryQueryPanel.setVisibility(View.GONE);
            smsFailurePanel.setVisibility(View.GONE);
            userIdInput.requestFocus();

        } else if(userState == User.State.PENDING_NAME_AND_TERRITORY_INFO) {
            enterPhoneNumberPanel.setVisibility(View.GONE);
            pendingConfirmationPanel.setVisibility(View.GONE);
            smsFailurePanel.setVisibility(View.GONE);
            userNameAndTerritoryQueryPanel.setVisibility(View.VISIBLE);

        } else if (userState == User.State.SMS_SENDING_FAILURE) {
            smsFailurePanel.setVisibility(View.VISIBLE);
            textViewSmsFailure.setText(ResponsaApplication.currentUser.getSmsFailureMessage());
            enterPhoneNumberPanel.setVisibility(View.GONE);
            pendingConfirmationPanel.setVisibility(View.GONE);
            userNameAndTerritoryQueryPanel.setVisibility(View.GONE);

        } else if (receivedConfirmationCode != null) {
            confirmUser(receivedConfirmationCode);
        } else if (userState == User.State.PENDING_CONFIRMATION) {

            enterPhoneNumberPanel.setVisibility(View.GONE);
            pendingConfirmationPanel.setVisibility(View.VISIBLE);
            userNameAndTerritoryQueryPanel.setVisibility(View.GONE);
            smsFailurePanel.setVisibility(View.GONE);

            wrongNumberButton.setVisibility(View.GONE);
            sendSmsAgainButton.setVisibility(View.GONE);

            textViewSmsTimeout.setVisibility(View.GONE);
            textViewSignupPending.setVisibility(View.VISIBLE);

            confirmationCodeInput.requestFocus();

        } else if (userState == User.State.CONFIRMATION_TIMEOUT || ParseUser.getCurrentUser() == null) {
            enterPhoneNumberPanel.setVisibility(View.GONE);
            pendingConfirmationPanel.setVisibility(View.VISIBLE);
            userNameAndTerritoryQueryPanel.setVisibility(View.GONE);
            smsFailurePanel.setVisibility(View.GONE);

            wrongNumberButton.setVisibility(View.VISIBLE);
            sendSmsAgainButton.setVisibility(View.VISIBLE);

            textViewSmsTimeout.setVisibility(View.VISIBLE);
            textViewSignupPending.setVisibility(View.GONE);

            confirmationCodeInput.requestFocus();
        } else if (userState == User.State.CONFIRMED) {
            ((SignupActivity) getActivity()).openMainActivity();
        }

    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.signupButton:
                registerPhoneNumber(getUnformattedPhone());
                break;
            case R.id.sendCodeButton:
                confirmUser(confirmationCodeInput.getText().toString());
                break;
            case R.id.sendSmsAgainButton:
                sendSmsAgain();
                break;
            case R.id.sendSmsAgainButton2:
                clearSmsFailureHistory();
                sendSmsAgain();
                break;
            case R.id.wrongNumberButton:
                resetNumber();
                break;
            case R.id.userNameAndTerritorySubmit:
                registerNameAndTerritory(userNickname.getText().toString(), selectedTerritory);
                break;
            case R.id.backToFillNumberButton:
                clearSmsFailureHistory();
                resetNumber();
                break;
        }
    }

    private void clearSmsFailureHistory() {
        User user = ResponsaApplication.currentUser;
        user.clearSmsFailureHistory();
    }

    private void registerNameAndTerritory(String name, HomeTerritory selectedTerritory) {
        int i=1;
        //Save
        User user = ResponsaApplication.currentUser;
        user.storeNameAndTerritory(name, selectedTerritory);
        if (receivedConfirmationCode != null) {
            confirmUser(receivedConfirmationCode);
        }
        changeViewOnUserState();
    }

    private String getUnformattedPhone() {
        ResponsaPhoneNumber phoneNumber = new ResponsaPhoneNumber(getActivity());

        phoneNumber.setDdd(dddButton.getText().toString());
        phoneNumber.setPhone(userIdInput.getText().toString());

        return phoneNumber.toString();
    }

    private void resetNumber() {
        User user = ResponsaApplication.currentUser;
        String oldUid = user.getPhoneNumber().getFormattedPhone();
        user.resetNumber();
        userIdInput.setText(oldUid);
        changeViewOnUserState();
    }

    private void sendSmsAgain() {
        User user = ResponsaApplication.currentUser;
        user.resendSms(this);
        progress = ProgressDialog.show(getActivity(), "", getString(R.string.u_loading_register_smsagain), true);
        changeViewOnUserState();
    }

    /***
     * Registers new user in backend
     * @param userId phone number
     */
    public void registerPhoneNumber(String userId) {
        User user = ResponsaApplication.currentUser;
        user.storeNumber(userId, this);
        progress = ProgressDialog.show(getActivity(), null, getString(R.string.u_loading_register_phone), true);
        Resources res = getActivity().getResources();
    }

    //Evita um problema que estamos tendo de duplicação de usuários. Só pode confirmar usuário se ainda não estiver confirmando...
    private Boolean confirmingUser = false;
    private Boolean userConfirmed = false;
    /***
     * Sends confirmation code to backend
     * @param confirmationCode confirmation code received via SMS, entered manually
     */
    public void confirmUser(String confirmationCode) {
        if (confirmingUser || userConfirmed) return;
        confirmingUser = true;
        User user = ResponsaApplication.currentUser;
        user.confirm(confirmationCode, this);
        progress = ProgressDialog.show(getActivity(), "", getString(R.string.u_loading_register_confirm_user), true);
    }

    @Override
    public void onUserConfirmationSuccess() {
        confirmingUser = false;
        userConfirmed = true;
        if (progress != null) progress.dismiss();
        ((SignupActivity) getActivity()).openMainActivity();
    }

    @Override
    public void onUserConfirmationFailure(String error) {
        confirmingUser = false;
        if (progress != null) progress.dismiss();
        new AlertDialog.Builder(getActivity()).setMessage(error).setNeutralButton("Close", null).show();
    }

    @Override
    public void onUserConfirmationTimeout() {
        if (progress != null) progress.dismiss();
        changeViewOnUserState();
    }

    @Override
    public void onUserRegistrationRequestSuccess() {
        if (progress != null) progress.dismiss();
        wrongNumberButton.setText(String.format(getString(R.string.u_signup_button_my_number_is_wrong), new String[]{ResponsaApplication.currentUser.getPhoneNumber().getFormattedPhoneWithAreaCode()}));
        changeViewOnUserState();
    }

    @Override
    public void onUserRegistrationRequestFailure(RetrofitError error) {
        if (progress != null) progress.dismiss();
        if (error.getKind() == RetrofitError.Kind.NETWORK) {
            new AlertDialog.Builder(getActivity()).setMessage(R.string.u_error_network).setNeutralButton("Close", null).show();
        } else {
            new AlertDialog.Builder(getActivity()).setMessage(R.string.u_register_error_phone).setNeutralButton("Close", null).show();
        }
        //TODO deixar melhor o tratamento de erro
        //Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSmsResendSuccess() {
        if (progress != null) progress.dismiss();
        changeViewOnUserState();
    }

    @Override
    public void onSmsResendFailure(RetrofitError error) {
        if (progress != null) progress.dismiss();
        new AlertDialog.Builder(getActivity()).setMessage(R.string.u_register_error_sms).setNeutralButton("Close", null).show();
        //TODO deixar melhor o tratamento de erro
        Toast.makeText(getActivity(),error.getMessage(),Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onSmsFetchStatusSuccess(String smsStatusGroup, String smsStatusName, String smsErrorGroup, String smsErrorName) {
        if ("HANDSET_ERRORS".equals(smsErrorGroup) || "USER_ERRORS".equals(smsErrorGroup) || "OPERATOR_ERRORS".equals(smsErrorGroup)) {
            textViewSmsFailure.setText(ResponsaApplication.currentUser.getSmsFailureMessage());
        }
        changeViewOnUserState();
    }

    @Override
    public void onSmsFetchStatusFailure(RetrofitError error) {
        int i=1;
    }

    @Override
    public void onDDDChange(String ddd, Boolean isDDDFilled) {
        validateRegistrationForm(null, isDDDFilled);
    }

}
