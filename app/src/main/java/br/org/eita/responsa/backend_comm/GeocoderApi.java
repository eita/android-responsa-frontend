package br.org.eita.responsa.backend_comm;

import java.util.List;

import br.org.eita.responsa.model.GeocoderAddress;
import br.org.eita.responsa.model.GeocoderReverseAddress;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;
import retrofit.http.Query;

public interface GeocoderApi {

    //Reverse geocoding
    // reference: http://wiki.openstreetmap.org/wiki/Nominatim#Reverse_Geocoding_.2F_Address_lookup
    @GET("/reverse?format=json&zoom=18&addressdetails=1")
    void reverse(@Query("lat") Double latitude, @Query("lon") Double longitude, Callback<GeocoderReverseAddress> cb);

    //Find coordinates based on address
    @GET("/search/{address}?format=json&limit=1&addressdetails=0")
    void search(@Path("address") String address, Callback<List<GeocoderAddress>> cb);


}
