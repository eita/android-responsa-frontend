package br.org.eita.responsa.adapter.data_manager;

import java.util.ArrayList;

public class ArrayListWithMeta<E> extends ArrayList<E> {
    private String title;
    private DataOrigin origin;

    public static class DataOrigin {
        public static final DataOrigin LOCAL =  new DataOrigin();
        public static final DataOrigin REMOTE = new DataOrigin();
    }

    public DataOrigin getOrigin() {
        return origin;
    }

    public void setOrigin(DataOrigin dataOrigin) {
        this.origin = dataOrigin;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
