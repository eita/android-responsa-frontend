package br.org.eita.responsa.model.util;

import com.parse.ParseException;

import java.util.List;

public interface ResponsaFindCallback<T> {
    public void done(List<T> list, ParseException e);
}
