package br.org.eita.responsa.components.icon_badge;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.shamanland.fonticon.FontIconDrawable;
import com.shamanland.fonticon.FontIconTextView;

import br.org.eita.responsa.R;
import br.org.eita.responsa.util.ResponsaUtil;

public class FontIconBadge extends RelativeLayout {

    FontIconTextView iconBadge;
    TextView iconCount;

    public FontIconBadge(Context context) {
        this(context,null);
    }

    public FontIconBadge(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FontIconBadge(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.component_font_icon_badge, this);

        findAllViewsByIds(view);
    }

    private void findAllViewsByIds(View parentView) {
        iconBadge = (FontIconTextView) parentView.findViewById(R.id.iconBadge);
        iconCount = (TextView) parentView.findViewById(R.id.iconCount);
    }

    /**
     *
     * @param icon a Xml resource
     */
    public void setIcon(int icon) {
        Drawable dIcon = FontIconDrawable.inflate(getContext(), icon);
        iconBadge.setCompoundDrawables(dIcon, null, null, null);
    }

    public void setValue(Integer value) {
        iconCount.setText(ResponsaUtil.humanFormatInteger(value));
    }

}
