package br.org.eita.responsa.model;

import java.util.List;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.backend_comm.GeocoderApi;
import retrofit.Callback;
import retrofit.RestAdapter;

public class GeocoderAddress {

    public Double lat;
    public Double lon;

    public static void searchCoordinates(String address, Callback<List<GeocoderAddress>> cb) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.geocoderUrl)
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .build();

        GeocoderApi geocoderApi = restAdapter.create(GeocoderApi.class);

        geocoderApi.search(address, cb);
    }
}
