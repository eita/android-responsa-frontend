package br.org.eita.responsa.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.TextView;

import java.util.ArrayList;

import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.storage.queries.TerritoryDb;

public class ResponsaTerritoryAutocompleteAdapter extends ArrayAdapter<HomeTerritory> {
    private final String MY_DEBUG_TAG = "ResponsaLocationAdapter";
    private ArrayList<HomeTerritory> items;
    private ArrayList<HomeTerritory> itemsAll;
    private ArrayList<HomeTerritory> suggestions;
    private int viewResourceId;




    public ResponsaTerritoryAutocompleteAdapter(Context context, int viewResourceId, ArrayList<HomeTerritory> items) {
        super(context, viewResourceId, items);
        this.items = items;
        this.itemsAll = (ArrayList<HomeTerritory>) items.clone();
        this.suggestions = new ArrayList<HomeTerritory>();
        this.viewResourceId = viewResourceId;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater vi = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = vi.inflate(viewResourceId, null);
        }
        HomeTerritory homeTerritory = items.get(position);
        if (homeTerritory != null) {
            TextView responsaTerritoryLabel = (TextView) v.findViewById(android.R.id.text1);
            responsaTerritoryLabel.setSingleLine(false);
            if (responsaTerritoryLabel != null) {
                responsaTerritoryLabel.setText(homeTerritory.label);
            }
        }
        return v;
    }

    @Override
    public Filter getFilter() {
        return nameFilter;
    }

    Filter nameFilter = new Filter() {
        @Override
        public String convertResultToString(Object resultValue) {
            //String str = ((LocationCoordinates)(resultValue)).title + " / " + ((LocationCoordinates)(resultValue)).state;
            String str = ((HomeTerritory)(resultValue)).label;
            return str;
        }
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            if(constraint != null) {
                suggestions.clear();
                for (HomeTerritory homeTerritory : itemsAll) {
                    //if(homeTerritory.label.toLowerCase().startsWith(constraint.toString().toLowerCase())){
                        suggestions.add(homeTerritory);
                    //}
                }
                FilterResults filterResults = new FilterResults();
                filterResults.values = suggestions;
                filterResults.count = suggestions.size();
                return filterResults;
            } else {
                return new FilterResults();
            }
        }
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            ArrayList<HomeTerritory> filteredList = (ArrayList<HomeTerritory>) results.values;
            if(results != null && results.count > 0) {
                clear();
                for (HomeTerritory c : filteredList) {
                    add(c);
                }
                notifyDataSetChanged();
            }
        }
    };

}