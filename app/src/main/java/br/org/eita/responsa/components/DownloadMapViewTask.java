package br.org.eita.responsa.components;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.os.AsyncTask;
import android.util.Log;
import android.view.Display;
import android.view.View;
import android.view.WindowManager;
import android.widget.ImageView;

import com.parse.ParseGeoPoint;

import java.io.InputStream;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.util.ResponsaUtil;

public class DownloadMapViewTask extends AsyncTask<String, Void, Bitmap> {
    ImageView bmImage;
    LocationCoordinates coordinates;
    Float heightPercentage;

    Integer imageWidth;
    Integer imageHeight;

    public DownloadMapViewTask(ImageView bmImage, LocationCoordinates coordinates, Float heightPercentage) {
        this.bmImage = bmImage;
        this.coordinates = coordinates;
        this.heightPercentage = heightPercentage;
    }

    protected Bitmap doInBackground(String... unused) {
        calcImageSize();
        Bitmap mIcon11 = null;
        try {
            InputStream in = new java.net.URL(getUrl()).openStream();
            mIcon11 = BitmapFactory.decodeStream(in);
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            e.printStackTrace();
        }
        return mIcon11;
    }

    protected void onPostExecute(Bitmap result) {
        if (result != null && bmImage != null) {
            bmImage.setImageBitmap(result);
            bmImage.getLayoutParams().height = imageHeight;
            bmImage.requestLayout();
            bmImage.setVisibility(View.VISIBLE);
        }
    }

    protected String getUrl() {
        String strLocation = String.format("%f", coordinates.getLongitude()).replace(',','.') + "," +
                String.format("%f", coordinates.getLatitude()).replace(',','.');

        StringBuffer urlBuffer = new StringBuffer();
        urlBuffer.append("http://api.tiles.mapbox.com/v4/viniciuscb.9a8d5d54/");
        urlBuffer.append("pin-l-garden+285A98("+strLocation+")/"); //Marker
        urlBuffer.append(strLocation + ",17/"); //Location
        urlBuffer.append(imageWidth + "x"+ imageHeight +".png"); //Dimensions
        urlBuffer.append("?access_token=sk.eyJ1IjoidmluaWNpdXNjYiIsImEiOiI5YzI2MjY4MmNhY2M1YjRhMTEyYjM0NjA2MDE4ZmQ5NCJ9.q2-O09nXSruHe4xk1FND-Q"); //key

        ResponsaUtil.log("imageUrl", urlBuffer.toString());
        return urlBuffer.toString();
    }

    protected void calcImageSize() {
        //get Screen sizes
        WindowManager wm = (WindowManager) ResponsaApplication.getAppContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;
        int screenHeight = size.y;

        imageHeight = Math.round(screenHeight * heightPercentage);
        imageWidth = screenWidth;
    }
}
