package br.org.eita.responsa.adapter;

import android.content.Context;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.backend_comm.SearchApi;
import br.org.eita.responsa.model.search_api.ChallengeSummary;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;
import retrofit.converter.GsonConverter;

//Gets pinned responsa objects near location
public class ChallengesSearchAjaxAdapter extends ChallengesAdapter {

    RestAdapter restAdapter;
    SearchApi searchApi;
    private OnQueryCompletedCallback onQueryCompleted;

    String currentUserId;

    public ChallengesSearchAjaxAdapter(Context context) {
        super(context, 0, new ArrayList<ChallengeSummary>());

        Gson gson = new GsonBuilder().setDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'").create();

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.searchServiceUrl)
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .setConverter(new GsonConverter(gson))
                .build();

        searchApi = restAdapter.create(SearchApi.class);

        currentUserId = ParseUser.getCurrentUser().getObjectId();

    }

    public void setKeywordQuery(final String keywordQuery) {
        final ChallengesSearchAjaxAdapter self = this;
        searchApi.queryChallengeSummaries(keywordQuery, currentUserId, new Callback<List<ChallengeSummary>>() {
            @Override
            public void success(List<ChallengeSummary> responsaObjectSummaries, Response response) {
                self.clear();
                self.addAll(responsaObjectSummaries);
                if (onQueryCompleted != null)
                    onQueryCompleted.onQueryComplete(keywordQuery);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    public void setOnQueryCompleted(OnQueryCompletedCallback completed) {
        this.onQueryCompleted = completed;
    }

    public interface OnQueryCompletedCallback {
        public void onQueryComplete(String queryText);
    }


}
