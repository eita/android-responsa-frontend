package br.org.eita.responsa.backend_comm;

import java.util.Date;
import java.util.List;

import br.org.eita.responsa.model.search_api.ChallengeFrontPageIds;
import br.org.eita.responsa.model.search_api.ChallengeFrontPageSummary;
import br.org.eita.responsa.model.search_api.ChallengeSummary;
import br.org.eita.responsa.model.search_api.RegionContext;
import br.org.eita.responsa.model.search_api.ResponsaObjectSummary;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Search api - searches in our mirror
 */
public interface SearchApi {

    //type: can be 'initiative' or 'gathering'
    @GET("/responsa_datamining/search_api.php")
    void queryResponsaObjectSummaries(@Query("q") String q, @Query("userId") String userId, @Query("model") String type, Callback<List<ResponsaObjectSummary>> cb);

    @GET("/responsa_datamining/search_api.php?model=Challenge")
    void queryChallengeSummaries(@Query("q") String q, @Query("userId") String userId, Callback<List<ChallengeSummary>> cb);

    @GET("/responsa_datamining/challenge_main_api.php?show_titles=1")
    void queryChallengesFirstPageIds(@Query("territoryId") String territoryId, @Query("lastFetchedAt") String lastFetchedAt, Callback<ChallengeFrontPageIds> cb);

    @GET("/responsa_datamining/region_context_api.php")
    void queryRegionContext(@Query("territoryId") String territoryId, Callback<RegionContext> cb);
}
