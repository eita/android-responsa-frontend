package br.org.eita.responsa.view.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.widget.DatePicker;

import java.util.Calendar;
import java.util.Date;

import br.org.eita.responsa.R;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.util.OnSelectedDateOrTimeCallback;

public class DatePickerDialogFragment extends DialogFragment{

    public static Date initDate;

    public static OnSelectedDateOrTimeCallback callback;


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final DatePickerDialog.OnDateSetListener listener  = new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year, int month, int dayOfMonth) {
                if (datePicker.isShown()) {
                    Date roDate = initDate;
                    if (roDate == null) roDate = new Date();

                    Calendar cal = Calendar.getInstance();
                    cal.setTime(roDate);

                    cal.set(Calendar.YEAR,year);
                    cal.set(Calendar.MONTH, month);
                    cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);

                    if (callback != null) {
                        callback.onSelect(cal.getTime());
                    }
                }
            }
        };

        Date startDate = initDate;
        if (startDate == null) startDate = new Date();

        final Calendar cal = Calendar.getInstance();
        cal.setTime(startDate);


        // Use the current date as the default date in the picker
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(getActivity(), listener, year, month, day);
    }
}
