/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.org.eita.responsa.view.fragment;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.SearchView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.annotations.MarkerOptions;
import com.mapbox.mapboxsdk.camera.CameraPosition;
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.mapbox.mapboxsdk.maps.MapboxMap;
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback;


import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.TimerTask;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.MainActivity;
import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.ResponsaObjectListAdapter;
import br.org.eita.responsa.adapter.ResponsaObjectsSearchAjaxAdapter;
import br.org.eita.responsa.adapter.data_manager.QueryResult;
import br.org.eita.responsa.components.LoadMoreComponent;
import br.org.eita.responsa.components.ResponsaViewPager;
import br.org.eita.responsa.components.TerritoryChooser;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.InitiativeCategory;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.util.ExtraFilters;
import br.org.eita.responsa.util.ResponsaObjectsMap;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.view.activity.AboutActivity;
import br.org.eita.responsa.view.activity.FeedbackActivity;

public class ResponsaObjectListFragment extends BaseListFragment {

    private BroadcastReceiver updatesReceiver;

    ResponsaObjectListAdapter originalAdapter;
    //LocalResponsaObjectsByProximityAdapter originalAdapter;
    ResponsaObjectsSearchAjaxAdapter ajaxAdapter;
    ListAdapter currentAdapter;

    TextView headerText;
    String originalHeaderTextValue;
    ObservableListView listView;
    LoadMoreComponent loadMoreComponent;

    SwipeRefreshLayout swipeLayout;

    View headerPadding;

    String viewType;
    String objectId = null;

    Boolean firstPopulate = null;

    HomeTerritory filteredTerritory = null;

    RelativeLayout emptyListItemLabel = null;

    MapView mapView = null;
    ProgressBar progressBar = null;

    private Timer timer;
    private CameraPosition cameraPosition;

    // Marker Id, ResponsaObject Id
    Map<Marker, ResponsaObject> markerDictionary;

    MenuItem searchItem = null;

    //Extra Query filters
    ExtraFilters extraFilters = null;

    Boolean redrawMarkers = false;
    Boolean populatedOnce = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        viewType = getArguments().getString("view");
        objectId = getArguments().getString("objectId");
        firstPopulate = true;

        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).responsaObjectListFragment = this;
        }

        extraFilters = new ExtraFilters();

        //Map initialization
        markerDictionary = new HashMap<Marker, ResponsaObject>();

        Boolean hidePadding = getArguments().getBoolean("hideHeaderPadding");

        Integer layout = 0;
        String headerString = null;
        if ("initiativesAndGatherings".equals(viewType) || "favorites".equals(viewType) ) {
            layout = R.layout.fragment_initiatives_and_gatherings;
        } else if ("gatheringsFromInitiative".equals(viewType)) { /* viewType == "gathering" */
            layout = R.layout.fragment_gatherings_from_initiative;
        } else {
            ResponsaUtil.log("ResponsaErrors", "error inflating list viewType");
        }

        final View view = inflater.inflate(layout, container, false);


        setHasOptionsMenu(true);

        Activity parentActivity = getActivity();
        listView = (ObservableListView) view.findViewById(R.id.scroll);

        if (!hidePadding) {
            View padding = inflater.inflate(R.layout.padding, listView, false);
            headerPadding = padding.findViewById(R.id.headerPadding);
            listView.addHeaderView(padding);
        }

        //Adds title
        headerText = (TextView) inflater.inflate(R.layout.list_title,listView,false);
        listView.addHeaderView(headerText);
        headerText.setText(headerString);

        emptyListItemLabel = (RelativeLayout) inflater.inflate(R.layout.empty_list_item,listView,false);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                //String objectId = ResponsaObject.currentResponsaObject == null ? null :  ResponsaObject.currentResponsaObject.getObjectId();
                LocationCoordinates.requestLocationUpdate(true);
                ResponsaApplication.responsaDataManager.load(viewType, objectId, ResponsaDataManager.FORCE_REMOTE, extraFilters);
            }
        });

        swipeLayout.setProgressViewOffset(false, 0, 160);

        ajaxAdapter = new ResponsaObjectsSearchAjaxAdapter(getActivity(), this.viewType);
        ajaxAdapter.setOnQueryCompleted(new ResponsaObjectsSearchAjaxAdapter.OnQueryCompletedCallback() {
            @Override
            public void onQueryComplete(String queryText) {
                listView.removeHeaderView(emptyListItemLabel);
                if (ajaxAdapter.isEmpty()) {
                    listView.addHeaderView(emptyListItemLabel);
                }
                loadMoreComponent.setVisibility(View.GONE);

                headerText.setText(getString(R.string.u_search_results_text_robj,queryText));
            }
        });

        listView.setTouchInterceptionViewGroup((ViewGroup) parentActivity.findViewById(R.id.container));

        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            listView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }

        originalAdapter = createNewAdapter();
        currentAdapter = originalAdapter;
        loadMoreComponent = new LoadMoreComponent(getActivity(), null, R.layout.progress_load_more);
        loadMoreComponent.initParams(viewType, null, extraFilters);
        listView.addFooterView(loadMoreComponent);
        listView.setAdapter(originalAdapter);


        //Location receiver
        updatesReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                String view = intent.getStringExtra("targetView");
                ResponsaUtil.log("POPULATEMAP","received intent, view="+view);
                populate(view);
                swipeLayout.setRefreshing(false);
            }
        };

        IntentFilter filter =  new IntentFilter(ResponsaDataManager.getIntentDataUpdatedName(viewType));
        filter.addAction(ResponsaDataManager.getIntentDataUpdatedName("initiativesAndGatheringsWithTerritory"));
        filter.addAction(ResponsaDataManager.getIntentDataUpdatedName("initiativesAndGatheringsWithinGeoBox"));


        getActivity().registerReceiver(updatesReceiver,filter);

        ResponsaApplication.responsaDataManager.load(viewType, objectId, extraFilters);

        View filterCategoriesButton = getActivity().findViewById(R.id.filterCategories);

        // Only show the option to filter categories if viewType is initiativesAndGatherings:
        if (filterCategoriesButton != null) {
            filterCategoriesButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openFilterCategoriesDialog(view);
                }
            });
        }

        View filterbyTerritoryButton = getActivity().findViewById(R.id.filterbyTerritory);

        // Only show the option to filter categories if viewType is initiativesAndGatherings:
        if (filterbyTerritoryButton != null) {
            filterbyTerritoryButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    openFilterTerritoryDialog(view);
                }
            });
        }        

        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ResponsaViewPager vp = null;
        if (getActivity() instanceof MainActivity) {
            vp = ((MainActivity) getActivity()).getResponsaViewPager();
        }

        final ResponsaViewPager viewPager = vp;

        //Init progressbar
        progressBar = (ProgressBar) view.findViewById(R.id.progress_bar);
        //Init mapview
        mapView = (MapView) view.findViewById(R.id.mapView);
        if (mapView != null) {
            mapView.onCreate(savedInstanceState);


            mapView.setOnTouchListener(new View.OnTouchListener() {
                @Override
                public boolean onTouch(View view, MotionEvent motionEvent) {
                    switch (motionEvent.getAction()) {
                        case MotionEvent.ACTION_MOVE:
                            viewPager.requestDisallowInterceptTouchEvent(true);
                            break;
                        case MotionEvent.ACTION_CANCEL:
                            viewPager.requestDisallowInterceptTouchEvent(false);
                            break;
                        case MotionEvent.ACTION_SCROLL:
                            viewPager.requestDisallowInterceptTouchEvent(false);
                            break;

                    }
                    return mapView.onTouchEvent(motionEvent);
                }
            });

            final MainActivity activity = (MainActivity) getActivity();

            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(final MapboxMap mapboxMap) {
                    mapboxMap.setOnInfoWindowClickListener(new MapboxMap.OnInfoWindowClickListener() {
                        @Override
                        public boolean onInfoWindowClick(@NonNull Marker marker) {
                            ResponsaApplication.responsaViewManager.openFichaSintese(getActivity(),markerDictionary.get(marker));
                            return false;
                        }
                    });

                    // first cameraPosition to prevent null values.
                    cameraPosition = mapboxMap.getCameraPosition();

                    mapboxMap.setOnCameraChangeListener(new MapboxMap.OnCameraChangeListener() {
                        @Override
                        public void onCameraChange(CameraPosition position) {
                            if (timer != null) {
                                timer.cancel();
                                activity.runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        progressBar.setVisibility(View.VISIBLE);
                                        activity.floatingActionsMenu.setVisibility(View.GONE);
                                        activity.mapViewToggleButton.setVisibility(View.GONE);
                                    }
                                });
                            } else {
                                // In case it's the first time, the progressbar must be hidden
                                timer = new Timer();
                                timer.schedule(new TimerTask() {
                                    @Override
                                    public void run() {
                                        activity.runOnUiThread(new Runnable() {
                                            @Override
                                            public void run() {
                                                progressBar.setVisibility(View.GONE);
                                                activity.floatingActionsMenu.setVisibility(View.VISIBLE);
                                                activity.mapViewToggleButton.setVisibility(View.VISIBLE);
                                            }
                                        });
                                    }
                                }, 1000);
                            }
                            timer = new Timer();
                            timer.schedule(new TimerTask() {
                                @Override
                                public void run() {
                                    CameraPosition newCameraPosition = mapboxMap.getCameraPosition();
                                    Double distanceMoved =  cameraPosition.target.distanceTo(newCameraPosition.target);
                                    if (distanceMoved>0) {
                                        ResponsaUtil.log("timer", "distanceMoved: "+distanceMoved.toString());
                                        ResponsaObjectsMap.mapViewBounds = mapboxMap.getProjection().getVisibleRegion().latLngBounds;
                                        ResponsaApplication.responsaDataManager.load("initiativesAndGatheringsWithinGeoBox", null, ResponsaDataManager.FORCE_LOCAL, extraFilters);
                                    }
                                    cameraPosition = newCameraPosition;
                                }
                            },1000);
                        }
                    });
                }
            });

            if (getActivity() instanceof MainActivity) {
                setMapShown(((MainActivity)getActivity()).mapShown);
            }
        }
    }

    public void setMapShown(Boolean mapVisibility) {
        if (mapView != null) {
            if (mapVisibility) {
                mapView.setVisibility(View.VISIBLE);
                swipeLayout.setVisibility(View.GONE);
                if (searchItem != null) {
                    searchItem.setVisible(false);
                }
            } else {
                mapView.setVisibility(View.GONE);
                swipeLayout.setVisibility(View.VISIBLE);
                if (searchItem != null) {
                    searchItem.setVisible(true);
                }
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mapView != null) {
            mapView.onResume();
        }
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (mapView != null) {
            mapView.onSaveInstanceState(outState);
        }
    }

    @Override
    public void onPause() {
        if (mapView != null) {
            mapView.onPause();
        }
        super.onPause();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        if (mapView != null) {
            mapView.onLowMemory();
        }
    }

    private void populate(String view) {
        Activity activity = getActivity();
        Boolean mapShown = false;
        if (activity instanceof MainActivity) {
            mapShown = ((MainActivity) activity).mapShown;
        }

        ResponsaUtil.log("ResponsaObjectListFragment", "start populate: viewType="+ viewType+"; view="+view);
        //Esconde o progressbar do mapa, mostra novamente os botões
        if (progressBar != null) {
            progressBar.setVisibility(View.GONE);
            ((MainActivity) getActivity()).floatingActionsMenu.setVisibility(View.VISIBLE);
            ((MainActivity) getActivity()).mapViewToggleButton.setVisibility(View.VISIBLE);
        }
        if (view.equals("initiativesAndGatheringsWithinGeoBox")) {
            List data = ResponsaApplication.responsaDataManager.retrieveLoadedData(view, null);
            QueryResult.MetaData metaData = ResponsaApplication.responsaDataManager.retrieveLoadedMetaData(view, null);
            populateMap(data, metaData, false, redrawMarkers);
            redrawMarkers = false;
        } else {
            List data = ResponsaApplication.responsaDataManager.retrieveLoadedData(viewType, objectId);
            QueryResult.MetaData metaData = ResponsaApplication.responsaDataManager.retrieveLoadedMetaData(viewType, objectId);

            populateList(data, metaData);
            populateMap(data, metaData, !mapShown || !populatedOnce, true);
            populatedOnce = true;
        }

    }

    private void populateList(List data, QueryResult.MetaData metaData) {
        listView.removeHeaderView(emptyListItemLabel);
        if (data.size() == 0) {
            listView.addHeaderView(emptyListItemLabel);
        }

        originalAdapter.populateAdapter(data);

        loadMoreComponent.setPage(metaData.page);
        if (ResponsaApplication.responsaDataManager.hasMore(viewType, objectId)) {
            loadMoreComponent.showBtnLoadMore();
        } else {
            loadMoreComponent.stopProgressBar();
            loadMoreComponent.hideBtnLoadMore();
        }

        if (MainActivity.currentState != MainActivity.State.SEARCH) {
            headerText.setText(getListTitle(metaData, viewType));
        } else {
            originalHeaderTextValue = getListTitle(metaData, viewType);
        }

        if (firstPopulate) {
            firstPopulate = false;
        }
    }

    private void populateMap(final List<ResponsaObject> data, final QueryResult.MetaData metaData, final Boolean centerAndZoom, final Boolean deletePreviousMarkers) {

        if (mapView == null) return;

        ResponsaUtil.log("POPULATEMAP","Total points: "+data.size() + " centerAndZoom=" + centerAndZoom);

        final MainActivity mainActivity = (MainActivity) getActivity();

        mapView.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(MapboxMap mapboxMap) {
                if (deletePreviousMarkers) {
                    mapboxMap.clear();
                    markerDictionary.clear();
                }

                Iterator<ResponsaObject> dataIterator = data.iterator();

                LatLngBounds.Builder latLngBoundsBuilder = new LatLngBounds.Builder();
                Integer pointsAdded = 0;

                while(dataIterator.hasNext()) {
                    ResponsaObject responsaObject = dataIterator.next();

                    if (!markerDictionary.containsValue(responsaObject)) {
                        Marker marker = mapboxMap.addMarker(new MarkerOptions()
                                        .position(responsaObject.getCoordinates().getLatLng())
                                        .title(responsaObject.getTitle())
                                        .snippet(ResponsaUtil.ellipsize(responsaObject.getDescription(),80))
                                //.icon(responsaObject.getMapboxIcon(getActivity()))
                        );

                        markerDictionary.put(marker,responsaObject);

                        if (centerAndZoom==true) {
                            latLngBoundsBuilder.include(responsaObject.getCoordinates().getLatLng());
                            if (pointsAdded==0) {
                                // Add dummy point to prevent crash in builder in case there is only one single point
                                LatLng dummyLatLng = new LatLng(responsaObject.getCoordinates().getLatLng().getLatitude()+0.015, responsaObject.getCoordinates().getLatLng().getLongitude()+0.015);
                                latLngBoundsBuilder.include(dummyLatLng);
                            }
                            pointsAdded += 1;
                        }
                    }
                }

                if (centerAndZoom) {
                    LatLngBounds firstLatLngBounds = null;
                    if (pointsAdded == 0) {
                        ResponsaUtil.log("POPULATEMAP", "NO POINTS ADDED");
                    } else {
                        ResponsaUtil.log("POPULATEMAP",pointsAdded.toString() + " POINTS ADDED");
                        firstLatLngBounds = latLngBoundsBuilder.build();
                    }

                    //Initial position and zoom (first populate only):
                    if (firstLatLngBounds != null && centerAndZoom) {
                        //Points may be too wide. Here we check this and zoom in a small area without all points if necessary
                        if (firstLatLngBounds.getLongitudeSpan() > 0.015) {
                            mapboxMap.setCameraPosition(new CameraPosition.Builder()
                                    .target(metaData.visibleLocationWhenFetched.getLatLng())
                                    .zoom(14)
                                    .build()
                            );
                            if (!mainActivity.mapViewCentered) {
                                mainActivity.mapViewToggleButton.setVisibility(View.VISIBLE);
                                mainActivity.mapViewCentered = true;
                            }
                        } else { //Zoom focus all points
                            mapboxMap.moveCamera(CameraUpdateFactory.newLatLngBounds(firstLatLngBounds,50));
                        }
                        ResponsaObjectsMap.mapViewBounds = mapboxMap.getProjection().getVisibleRegion().latLngBounds;
                        // first cameraPosition to prevent null values.
                        cameraPosition = mapboxMap.getCameraPosition();
                    }
                }

            }
        });
    }

    private String getListTitle(QueryResult.MetaData metaData, String view) {
        String listTitle = "";
        String now = (metaData.lastFetchedAt == null || System.currentTimeMillis() < metaData.lastFetchedAt.getTime() + Constants.MAX_TIME_TO_CONSIDER_NOW) ? "" :
                " " + DateUtils.getRelativeTimeSpanString(metaData.lastFetchedAt.getTime()).toString();

        if ("gatheringsFromInitiative".equals(viewType)) {
            listTitle = getString(R.string.u_list_title_gatheringsFromInitiative, ResponsaObject.currentResponsaObject.getTitle());
        } else if ("favorites".equals(viewType)) {
            listTitle = getString(R.string.u_list_title_favorites);
        } else {
            String type  = getString(ResponsaUtil.getResourceIdByName("u_list_title_" + viewType, getActivity()));

            if (filteredTerritory != null) {
                listTitle = getString(R.string.u_list_title_in_territory, type, filteredTerritory.label);
            } else if (metaData.visibleLocationWhenFetched.getCoordinateType().equals(LocationCoordinates.CoordinateType.BY_GPS)) {
                listTitle = getString(R.string.u_list_title_near_you, type);
            } else { //Territory_id
                HomeTerritory homeTerritory = metaData.visibleLocationWhenFetched.getLocation();
                listTitle = getString(R.string.u_list_title_in_your_region, type, homeTerritory.getCity().title);
            }
        }

        listTitle += now;
        return listTitle;
    }


    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(updatesReceiver);
        if (hasSelectedCategories) {
            ResponsaApplication.responsaDataManager.clearCached(viewType, objectId);
        }
        if (mapView != null) {
            mapView.onDestroy();
        }
        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).responsaObjectListFragment = null;
        }
        super.onDestroy();
    }


    protected ResponsaObjectListAdapter createNewAdapter() {
        ResponsaObjectListAdapter responsaObjectListAdapter = new ResponsaObjectListAdapter(getActivity());
        return responsaObjectListAdapter;
    }

    private CharSequence getTitle() {
        Date now = new Date();
        return DateUtils.getRelativeTimeSpanString(now.getTime()-10000*60*60, now.getTime(), 0);
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_iniciativas_fragment, menu);

        inflater.inflate(R.menu.menu_feedback, menu);

        searchItem = menu.findItem(R.id.action_busca);

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                if (getActivity() instanceof MainActivity) {
                    MainActivity activity = ((MainActivity) getActivity());
                    activity.setState(MainActivity.State.SEARCH);
                    headerPadding.setVisibility(View.GONE);
                }
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                if (getActivity() instanceof MainActivity) {
                    ((MainActivity) getActivity()).setState(MainActivity.State.BROWSE);
                    headerPadding.setVisibility(View.VISIBLE);
                }
                return true;
            }
        });

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setQueryHint(getString(R.string.search_hint));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                if (newText.isEmpty() && currentAdapter != originalAdapter) {
                    listView.setAdapter(originalAdapter);
                    listView.removeHeaderView(emptyListItemLabel);
                    headerText.setText(originalHeaderTextValue);
                } else if (!newText.isEmpty()) {
                    if (!(currentAdapter instanceof ResponsaObjectsSearchAjaxAdapter)) {
                        originalHeaderTextValue = headerText.getText().toString();
                    }
                    listView.setAdapter(ajaxAdapter);
                    ajaxAdapter.setKeywordQuery(newText);
                    currentAdapter = ajaxAdapter;
                }

                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id) {
            case R.id.action_feedback:
                intent = new Intent(getActivity(), FeedbackActivity.class);
                intent.putExtra("view", viewType);
                intent.putExtra("viewObjectId",objectId);
                intent.putExtra("viewClass",getClass().getSimpleName());
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(getActivity(), AboutActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    boolean[] checkedCategories = new boolean[InitiativeCategory.CATEGORIES_COUNT + 2] ;
    Boolean hasSelectedCategories = false;

    public void openFilterCategoriesDialog(View view) {

        AlertDialog dialog;

        final ArrayList<String> categoryNames = new ArrayList<String>();
        categoryNames.add(getString(R.string.show_all_categories));
        categoryNames.addAll(InitiativeCategory.getCategoryNames(getActivity()));
        categoryNames.add(getString(R.string.gatherings));

        final CharSequence[] categoryArray = categoryNames.toArray(new CharSequence[categoryNames.size()]);

        final boolean[] actualCheckedCategories = checkedCategories.clone();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.filter_categories));

        builder.setMultiChoiceItems(categoryArray, checkedCategories,
                new DialogInterface.OnMultiChoiceClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int indexSelected,
                                        boolean isChecked) {
                        ListView list = ((AlertDialog) dialog).getListView();

                        if (indexSelected == 0) {
                            for (int i=0; i < list.getCount(); i++) {
                                list.setItemChecked(i, isChecked);
                                checkedCategories[i] = isChecked;
                            }
                        } else {
                            checkedCategories[indexSelected] = isChecked;
                            if (!isChecked) {
                                checkedCategories[0] = false;
                                list.setItemChecked(0, false);
                            } else {
                                boolean checkFirst = true;
                                for (int i=1; i<checkedCategories.length; i++) {
                                    checkFirst = checkFirst && checkedCategories[i];
                                }
                                if (checkFirst) {
                                    checkedCategories[0] = true;
                                    list.setItemChecked(0, true);
                                }
                            }
                        }
                    }
                })
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        final ArrayList<InitiativeCategory> categories = InitiativeCategory.getCategoryList();

                        //TODO: find a way to include gathering inside InitiativeCategory class
                        categories.add(new InitiativeCategory("gathering",R.string.gatherings,0));

                        ArrayList<String> categoriesSlug = new ArrayList<String>();
                        for (int i=1; i<checkedCategories.length; i++) {
                            if (checkedCategories[i]) {
                                categoriesSlug.add(categories.get(i-1).getSlug());
                                hasSelectedCategories = true;
                            }
                        }
                        if (categoriesSlug.size() > 0) {
                            extraFilters.add("category","in",categoriesSlug);
                        } else {
                            extraFilters.remove("category");
                        }
                        redrawMarkers = true;
                        ResponsaApplication.responsaDataManager.load(viewType, objectId, extraFilters);
                        if (getActivity() instanceof MainActivity) {
                            MainActivity activity = (MainActivity) getActivity();
                            activity.floatingActionsMenu.collapse();
                            if (activity.mapShown) {
                                ResponsaApplication.responsaDataManager.load("initiativesAndGatheringsWithinGeoBox",null, extraFilters);
                            }
                        }
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        checkedCategories = actualCheckedCategories;
                        if (getActivity() instanceof MainActivity) {
                            ((MainActivity)getActivity()).floatingActionsMenu.collapse();
                        }
                    }
                });


        dialog = builder.create();
        dialog.show();
    }

    public void openFilterTerritoryDialog(View view) {

        AlertDialog dialog;

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(getString(R.string.filter_by_territory));

        final TerritoryChooser territoryChooser = new TerritoryChooser(getActivity());

        territoryChooser.setSelectedTerritory(filteredTerritory);


        builder.setView(territoryChooser)
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        setFilteredTerritory(territoryChooser.getSelectedTerritory());
                    }
                })
                .setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        if (getActivity() instanceof MainActivity) {
                            ((MainActivity)getActivity()).floatingActionsMenu.collapse();
                        }
                    }
                });

        dialog = builder.create();
        dialog.show();
        territoryChooser.focus();
    }

    public void setFilteredTerritory(final HomeTerritory territory) {

        filteredTerritory = territory;

        if (territory == null) {
            extraFilters.remove("territoryId");
        } else {
            extraFilters.add("territoryId","STARTS_WITH",territory.getTerritoryOrCity().id);
            mapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(MapboxMap mapboxMap) {
                    mapboxMap.setCameraPosition(new CameraPosition.Builder()
                            .target(territory.getLocationCoordinates().getLatLng())
                            .zoom(13)
                            .build()
                    );
                    cameraPosition = mapboxMap.getCameraPosition();
                }
            });
        }
        ResponsaApplication.responsaDataManager.load(viewType, objectId, extraFilters);

        if (getActivity() instanceof MainActivity) {
            ((MainActivity) getActivity()).floatingActionsMenu.collapse();
        }
    }

    /*
    @Override

    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
                //Toast.makeText(getActivity(),"notifications invisible", Toast.LENGTH_SHORT).show();
            } else {
                //Toast.makeText(getActivity(),"notifications visible", Toast.LENGTH_SHORT).show();
            }
        }
    }
    */



}
