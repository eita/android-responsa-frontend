package br.org.eita.responsa.storage.queries;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.widget.Toast;

import com.google.gson.Gson;

import org.json.JSONArray;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

import br.org.eita.responsa.MainActivity;
import br.org.eita.responsa.R;
import br.org.eita.responsa.storage.ResponsaDbHelper;
import br.org.eita.responsa.storage.ResponsaStorageContract;

public class NotificationDb {

    private ResponsaDbHelper dbHelper;
    private Context mContext;

    public NotificationDb(Context context) {
        mContext = context;
        this.dbHelper = new ResponsaDbHelper(context);
    }

    public void insert(int messageId, String data, String target) {
        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getWritableDatabase();

        // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(ResponsaStorageContract.Notification.COLUMN_NAME_MESSAGE_ID, messageId);
        values.put(ResponsaStorageContract.Notification.COLUMN_NAME_DATA, data);
        values.put(ResponsaStorageContract.Notification.COLUMN_NAME_TARGET, target);

        // Insert the new row, returning the primary key value of the new row
        long newRowId;
        newRowId = db.insert(
                ResponsaStorageContract.Notification.TABLE_NAME,
                null,
                values);
    }

    public int deleteAll() {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        return db.delete(ResponsaStorageContract.Notification.TABLE_NAME," 1=1 ",new String[]{});
    }

    public Cursor getNotificationsList(boolean unread_only, Integer offset, Integer limit) {
        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                ResponsaStorageContract.Notification._ID,
                ResponsaStorageContract.Notification.COLUMN_NAME_MESSAGE_ID,
                ResponsaStorageContract.Notification.COLUMN_NAME_DATA,
                ResponsaStorageContract.Notification.COLUMN_NAME_TARGET
        };

// How you want the results sorted in the resulting Cursor
        String sortOrder =
                ResponsaStorageContract.Notification.COLUMN_NAME_DATA + " ASC";

        String limitOffset = null;
        if (offset != null && limit != null) {
            limitOffset = offset.toString()+','+limit.toString();
        } else if (limit != null) {
            limitOffset = limit.toString();
        }

        Cursor c = db.query(
                ResponsaStorageContract.Notification.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder,                                 // The sort order
                limitOffset
        );
        return c;
    }


    public Cursor selectAll() {
        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                ResponsaStorageContract.Notification._ID,
                ResponsaStorageContract.Notification.COLUMN_NAME_MESSAGE_ID,
                ResponsaStorageContract.Notification.COLUMN_NAME_DATA,
                ResponsaStorageContract.Notification.COLUMN_NAME_TARGET
        };

// How you want the results sorted in the resulting Cursor
        String sortOrder =
                ResponsaStorageContract.Notification.COLUMN_NAME_DATA + " ASC";

        Cursor c = db.query(
                ResponsaStorageContract.Notification.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );
        return c;
    }

    public boolean insertSample() {

        deleteAll();

        createNotification(R.string.u_welcome_with_steps, new String[]{"10","20","30","40"},"t1");
        createNotification(R.string.u_initiative_created_near_steps, new String[]{"abc","20"},"t2");
        createNotification(R.string.u_place_created_near_steps, new String[]{"abc","20"},"t3");
        createNotification(R.string.u_initiative_approved_steps,new String[]{"abc","20"},"t4");
        createNotification(R.string.u_comment_on_meeting,new String[]{"João","Encontro","abc"},"t5");
        createNotification(R.string.u_place_approved_steps,new String[]{"Feira da rocinha","30"},"t6");
        createNotification(R.string.u_favorite_steps,new String[]{"10","novo local"},"t7");
        createNotification(R.string.u_comment_on_initiative,new String[]{"10","20","30"},"t8");
        createNotification(R.string.u_place_suggested_steps,new String[]{"10"},"t9");
        createNotification(R.string.u_challenge_created_steps,new String[]{"10","20"},"t10");
        createNotification(R.string.u_comment_on_place,new String[]{"10","20","30"},"t11");
        createNotification(R.string.u_comment_on_initiative,new String[]{"a","b","c"},"t8");
        createNotification(R.string.u_place_suggested_steps,new String[]{"d"},"t9");
        createNotification(R.string.u_challenge_created_steps,new String[]{"e","f"},"t10");
        createNotification(R.string.u_comment_on_place,new String[]{"g","h","i"},"t11");
        return true;
        
    }

    public void createNotification(int notificationMessageId, String[] args, String targetActivity) {
        //JSON serialization
        Gson gson = new Gson();
        insert(notificationMessageId,gson.toJson(args),targetActivity);
    }

    public String getNotificationMessage(Cursor mCursor) {
        Gson gson = new Gson();
        Resources res = mContext.getResources();

        Integer messageId = mCursor.getInt(mCursor.getColumnIndex(ResponsaStorageContract.Notification.COLUMN_NAME_MESSAGE_ID));
        String message = res.getString(messageId);
        String argumentsStr = mCursor.getString(mCursor.getColumnIndex(ResponsaStorageContract.Notification.COLUMN_NAME_DATA));
        String[] arguments = gson.fromJson(argumentsStr,String[].class);

        return String.format(message, arguments);
    }

}
