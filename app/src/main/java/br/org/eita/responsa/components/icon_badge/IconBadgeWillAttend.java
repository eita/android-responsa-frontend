package br.org.eita.responsa.components.icon_badge;

import android.content.Context;
import android.support.v4.app.FragmentActivity;
import android.util.AttributeSet;
import android.view.View;

import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.BaseResponsaModel;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.model.UserResponsaObject;
import br.org.eita.responsa.view.fragment.ChallengeCreateDialogFragment;
import br.org.eita.responsa.view.fragment.UserListDialogFragment;

public class IconBadgeWillAttend extends ModelIconBadge {

    public IconBadgeWillAttend(Context context) {
        this(context, null);
    }

    public IconBadgeWillAttend(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IconBadgeWillAttend(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setIcon(R.xml.ic_fs_g_confirmou_presenca);
        setModelField("countWillAttendGathering");
    }

}
