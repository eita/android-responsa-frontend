package br.org.eita.responsa.sms;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.telephony.SmsMessage;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.org.eita.responsa.Constants;

public class SmsReceiver extends BroadcastReceiver {

    private static final String ACTION_SMS_RECEIVED = "android.provider.Telephony.SMS_RECEIVED";
    private Context mContext;
    private Intent mIntent;

    @Override
    public void onReceive(Context context, Intent intent) {

        mContext = context;
        mIntent = intent;

        String action = intent.getAction();

        if(action.equals(ACTION_SMS_RECEIVED)){

            String address, str = "", message, confirmationCode = "";
            Boolean isResponsaMsg;
            int contactId = -1;

            SmsMessage[] msgs = getMessagesFromIntent(mIntent);
            if (msgs != null) {
                for (int i = 0; i < msgs.length; i++) {
                    message = msgs[i].getMessageBody().toString();
                    if (message.matches("Responsa(.*)")) {
                        Pattern p = Pattern.compile("[0-9]{6}");
                        Matcher m = p.matcher(message);
                        while(m.find()) {
                            confirmationCode = m.group(0);
                        }

                        if (confirmationCode != "") {

                            //Finishes confirmation process with confirmation code
                            Intent broadcastIntent = new Intent();
                            broadcastIntent.setAction(Constants.smsAction);
                            broadcastIntent.putExtra("sms", confirmationCode);
                            context.sendBroadcast(broadcastIntent);

                        }
                    }
                }
            }

            /*if(contactId != -1){
                showNotification(contactId, str);
            }*/

            // ---send a broadcast intent to update the SMS received in the
            // activity---
           /* Intent broadcastIntent = new Intent();
            broadcastIntent.setAction("SMS_RECEIVED_ACTION");
            broadcastIntent.putExtra("sms", str);
            context.sendBroadcast(broadcastIntent); */


        }
    }

    public static SmsMessage[] getMessagesFromIntent(Intent intent) {
        Object[] messages = (Object[]) intent.getSerializableExtra("pdus");
        byte[][] pduObjs = new byte[messages.length][];

        for (int i = 0; i < messages.length; i++) {
            pduObjs[i] = (byte[]) messages[i];
        }
        byte[][] pdus = new byte[pduObjs.length][];
        int pduCount = pdus.length;
        SmsMessage[] msgs = new SmsMessage[pduCount];
        for (int i = 0; i < pduCount; i++) {
            pdus[i] = pduObjs[i];
            msgs[i] = SmsMessage.createFromPdu(pdus[i]);
        }
        return msgs;
    }
}
