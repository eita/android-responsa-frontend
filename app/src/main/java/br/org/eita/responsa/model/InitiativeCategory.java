package br.org.eita.responsa.model;

import android.content.Context;

import java.util.ArrayList;

import br.org.eita.responsa.R;

public class InitiativeCategory {
    private String slug;
    private int textResource;
    private int explanation;

    public static final String FEIRAS = "feiras";
    public static final String GRUPOS_DE_CONSUMO_RESPONSAVEL = "grupos-de-consumo-responsavel";
    public static final String INICIATIVAS_DE_ECONOMIA_SOLIDARIA = "iniciativas-de-economia-solidaria";
    public static final String INICIATIVAS_DE_AGROECOLOGIA = "iniciativas-de-agroecologia";
    public static final String RESTAURANTES = "restaurantes";
    public static final String LOJAS = "lojas";
    public static final String COLETIVOS_CULTURAIS = "coletivos-culturais";
    public static final String AGRICULTURA_URBANA = "agricultura-urbana";


    public static final Integer CATEGORIES_COUNT = 8;

    public static final ArrayList<InitiativeCategory> getCategoryList() {
        ArrayList<InitiativeCategory> categories = new ArrayList<InitiativeCategory>();

        categories.add(new InitiativeCategory(InitiativeCategory.FEIRAS,R.string.category_feiras, R.string.category_feiras_exp));
        categories.add(new InitiativeCategory(InitiativeCategory.GRUPOS_DE_CONSUMO_RESPONSAVEL,R.string.category_grupos_de_consumo_responsavel, R.string.category_grupos_de_consumo_responsavel_exp));
        categories.add(new InitiativeCategory(InitiativeCategory.INICIATIVAS_DE_ECONOMIA_SOLIDARIA, R.string.category_iniciativas_de_economia_solidaria, R.string.category_iniciativas_de_economia_solidaria_exp));
        categories.add(new InitiativeCategory(InitiativeCategory.INICIATIVAS_DE_AGROECOLOGIA, R.string.category_iniciativas_de_agroecologia, R.string.category_iniciativas_de_agroecologia_exp));
        categories.add(new InitiativeCategory(InitiativeCategory.RESTAURANTES,R.string.category_restaurantes, R.string.category_restaurantes_exp));
        categories.add(new InitiativeCategory(InitiativeCategory.LOJAS,R.string.category_lojas, R.string.category_lojas_exp));
        categories.add(new InitiativeCategory(InitiativeCategory.COLETIVOS_CULTURAIS,R.string.category_coletivos_culturais, R.string.category_coletivos_culturais_exp));
        categories.add(new InitiativeCategory(InitiativeCategory.AGRICULTURA_URBANA,R.string.category_agricultura_urbana, R.string.category_agricultura_urbana_exp));

        return categories;
    }

    public static final ArrayList<String> getCategoryNames(Context context) {
        ArrayList<String> categoryNames = new ArrayList<String>();

        ArrayList<InitiativeCategory> categories = getCategoryList();

        for (int i=0; i<categories.size(); i++) {
            categoryNames.add(context.getString(categories.get(i).getTextResource()));
        }
        return categoryNames;
    }

    public InitiativeCategory(String slug, int textResource, int explanation) {
        this.slug = slug;
        this.textResource = textResource;
        this.explanation = explanation;
    }

    public String getSlug() {
        return slug;
    }

    public void setSlug(String slug) {
        this.slug = slug;
    }

    public int getTextResource() {
        return textResource;
    }

    public void setTextResource(int textResource) {
        this.textResource = textResource;
    }

    public int getExplanation() {
        return explanation;
    }

    public void setExplanation(int explanation) {
        this.explanation = explanation;
    }

    public static InitiativeCategory categoryBySlug(String slug) {
        ArrayList<InitiativeCategory> categories = getCategoryList();
        for (int i=0; i<categories.size();i++) {
            if (categories.get(i).getSlug().equals(slug)) {
                return categories.get(i);
            }
        }
        return null;
    }
}
