package br.org.eita.responsa.view.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.parse.ParseException;
import com.parse.ParseUser;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.Feedback;
import br.org.eita.responsa.model.util.ResponsaSaveCallback;
import br.org.eita.responsa.util.ResponsaUtil;
import us.feras.mdv.MarkdownView;


public class AboutFragment extends Fragment {

    MarkdownView aboutText;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)   {
        View inflatedView = inflater.inflate(R.layout.fragment_about, container, false);

        findAllViewsByIds(inflatedView);
        aboutText.loadMarkdownFile("file:///android_asset/about.md");

        return inflatedView;
    }

    private void findAllViewsByIds(View parentView) {
        aboutText = (MarkdownView) parentView.findViewById(R.id.aboutText);
    }
}
