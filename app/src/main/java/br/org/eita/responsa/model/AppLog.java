package br.org.eita.responsa.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

import org.json.JSONObject;

@ParseClassName("AppLog")
public class AppLog extends ParseObject {

    /**
     * Records Log in database, for future analysis
     * @param id identification of the log, to help find / filter it
     * @param text description or text related to the error
     */
    public static void record(String id, String text) {
        AppLog appLog = new AppLog();
        appLog.setUser(ParseUser.getCurrentUser());
        appLog.setLogId(id);
        appLog.setLogText(text);
        appLog.saveEventually();
    }

    public ParseUser getUser() {
        return getParseUser("user");
    }

    public void setUser(ParseUser user) {
        if (user == null) {
            this.put("user", JSONObject.NULL);
        } else {
            this.put("user", user);
        }
    }

    public String getLogId() {
        return getString("logId");
    }

    public void setLogId(String logId) {
        if (logId == null) {
            this.put("logId", JSONObject.NULL);
        } else {
            this.put("logId", logId);
        }
    }

    public String getLogText() {
        return getString("logText");
    }

    public void setLogText(String logText) {
        if (logText == null) {
            this.put("logText", JSONObject.NULL);
        } else {
            this.put("logText", logText);
        }
    }    
}
