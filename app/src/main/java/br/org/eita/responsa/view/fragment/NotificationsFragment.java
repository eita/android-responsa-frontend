/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.org.eita.responsa.view.fragment;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;

import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.readystatesoftware.viewbadger.BadgeView;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.NotificationAdapter;
import br.org.eita.responsa.adapter.data_manager.QueryResult;
import br.org.eita.responsa.components.LoadMoreComponent;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.model.Notification;
import br.org.eita.responsa.push.ResponsaPushReceiver;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.view.activity.AboutActivity;
import br.org.eita.responsa.view.activity.FeedbackActivity;

public class NotificationsFragment extends BaseFragment   {

    public static final String ARG_INITIAL_POSITION = "ARG_INITIAL_POSITION";

    private BroadcastReceiver notificationReceiver;

    Boolean firstPopulate = null;

    NotificationAdapter notificationAdapter;
    private LoadMoreComponent loadMoreComponent = null;
    TextView listTitle;

    SwipeRefreshLayout swipeLayout;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_notificacoes, container, false);
        firstPopulate = true;

        setHasOptionsMenu(true);

        Activity parentActivity = getActivity();
        final ObservableListView listView = (ObservableListView) view.findViewById(R.id.scroll);
        notificationAdapter = new NotificationAdapter(getActivity());

        listView.addHeaderView(inflater.inflate(R.layout.padding, listView, false));

        //Adds title
        listView.addHeaderView(inflater.inflate(R.layout.list_title,listView,false));
        listTitle = (TextView) view.findViewById(R.id.listTitle);
        listTitle.setText(R.string.u_notifications_list_title);

        //Swipe refresher
        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ResponsaApplication.responsaDataManager.load("notifications", null, ResponsaDataManager.FORCE_REMOTE);
            }
        });

        swipeLayout.setProgressViewOffset(false, 0, 160);


        loadMoreComponent = new LoadMoreComponent(getActivity(), null, R.layout.progress_load_more);
        loadMoreComponent.initParams("notifications", null);
        listView.addFooterView(loadMoreComponent);

        listView.setAdapter(notificationAdapter);
        listView.setTouchInterceptionViewGroup((ViewGroup) parentActivity.findViewById(R.id.container));

        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            listView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }

        ResponsaUtil.log("responsa", "notificationfragment.createView()");

        notificationReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                QueryResult.MetaData metaData = ResponsaApplication.responsaDataManager.retrieveLoadedMetaData("notifications", null);
                notificationAdapter.clear();
                notificationAdapter.addAll(ResponsaApplication.responsaDataManager.retrieveLoadedData("notifications", null));
                swipeLayout.setRefreshing(false);

                loadMoreComponent.setPage(metaData.page);
                if (ResponsaApplication.responsaDataManager.hasMore("notifications", null)) {
                    loadMoreComponent.showBtnLoadMore();
                } else {
                    loadMoreComponent.stopProgressBar();
                    loadMoreComponent.hideBtnLoadMore();
                }

                if (firstPopulate) {
                    firstPopulate = false;
                }
            }
        };

        getActivity().registerReceiver(notificationReceiver, new IntentFilter(ResponsaDataManager.getIntentDataUpdatedName("notifications")));
        ResponsaApplication.responsaDataManager.load("notifications", null);

        return view;
    }

    @Override
    public void onDestroyView() {
        getActivity().unregisterReceiver(notificationReceiver);
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.menu_feedback, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id) {
            case R.id.action_feedback:
                intent = new Intent(getActivity(), FeedbackActivity.class);
                intent.putExtra("view", "notifications");
                intent.putExtra("viewObjectId",(String)null);
                intent.putExtra("viewClass",getClass().getSimpleName());
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(getActivity(), AboutActivity.class);
                startActivity(intent);
                return true;
        }


        return super.onOptionsItemSelected(item);
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
                //Toast.makeText(getActivity(),"notifications invisible", Toast.LENGTH_SHORT).show();
                // TODO stop audio playback
            } else {
                Notification.markAllLocalAsRead();

                //Toast.makeText(getActivity(),"notifications visible", Toast.LENGTH_SHORT).show();
            }
        }
    }

}