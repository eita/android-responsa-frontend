package br.org.eita.responsa.util;


import java.util.ArrayList;
import java.util.Collections;


public class ExtraFilters extends ArrayList<ExtraFilters.Argument> {
    public class Argument {
        public String field;
        public String operation;
        public Object value;

        public Argument(String field, String operation, Object value) {
            this.field = field;
            this.operation = operation;
            this.value = value;
        }

        public String getId() { //String identification of this argument
            StringBuilder builder = new StringBuilder();
            builder.append(this.field);
            builder.append("_");
            if (this.value instanceof String) {
                builder.append(value);
            } else if (this.value instanceof ArrayList) {
                ArrayList list = (ArrayList) this.value;
                Collections.sort(list);
                for (int i=0; i < list.size(); i++) {
                    Object elm = list.get(i);
                    if (elm instanceof String && elm != null && !((String) elm).isEmpty()) {
                        if (i>0) {
                            builder.append("_");
                        }
                        builder.append((String) elm);
                    }
                }
            }
            return builder.toString();
        }
    }

    public String getUniqueId() {
        ArrayList<String> list = new ArrayList<>();
        for (int i=0; i<this.size(); i++) {
            list.add(this.get(i).getId());
        }
        Collections.sort(list);
        StringBuilder builder = new StringBuilder();
        for (int i=0; i<list.size(); i++) {
            if (i>0) {
                builder.append("_");
            }
            builder.append(list.get(i));
        }
        return builder.toString();
    }

    public Argument remove(String field) {
        for (int i=0; i<size(); i++) {
            Argument elm = get(i);
            if (elm.field.equals(field)) {
                return remove(i);
            }
        }

        return null;
    }

    public boolean add(String field, String operation, Object value) {
        //if there is already an item with same field, change it
        for (int i=0; i<size(); i++) {
            Argument elm = get(i);
            if (elm.field.equals(field)) {
                elm.operation = operation;
                elm.value = value;
                return true;
            }
        }
        return add(new Argument(field,operation,value));
    }



}
