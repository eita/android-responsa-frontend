package br.org.eita.responsa.model;

public interface VotableAnswerable extends Votable, Answerable {
}
