package br.org.eita.responsa.view.activity;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.Uri;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.ScrollUtils;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.CommentsAdapter;
import br.org.eita.responsa.adapter.data_manager.QueryResult;
import br.org.eita.responsa.components.LoadMoreComponent;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.components.CameraChooserDialog;
import br.org.eita.responsa.components.EmojiconKeyboard;
import br.org.eita.responsa.model.Comment;
import br.org.eita.responsa.model.Notification;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.model.UserResponsaObject;
import br.org.eita.responsa.model.util.ResponsaSaveCallback;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.view.fragment.FavoritesFragment;
import br.org.eita.responsa.view.fragment.FichaSinteseBaseFragment;

public class FichaSinteseActivity extends BaseActivity implements ObservableScrollViewCallbacks {

    //private View mImageView;
    private Toolbar mToolbarView;
    //private View mListBackgroundView;
    private ObservableListView commentsListView;
    private int mParallaxImageHeight;

    private ResponsaObject responsaObject;
    private UserResponsaObject userResponsaObject;

    private ActionBar actionBar;

    CommentsAdapter commentsAdapter;
    private LoadMoreComponent loadMoreComponent = null;

    BroadcastReceiver responsaObjectFetchedReceiver = null;

    FichaSinteseBaseFragment fichaSinteseFragment;

    Boolean descriptionHidden = true;
    Boolean favoritar = false;
    Boolean marcarPresenca = false;

    EmojiconKeyboard emojiconKeyboard;

    public static final String INTENT_OBJECT_ID_KEY = "objectId";
    public static final String INTENT_VIEW_KEY = "view";
    public static final String INTENT_CHEGUEI = "cheguei";

    Boolean parallaxEnabled = false;

    Boolean inflated = false;

    String responsaObjectId = null;
    String viewType = null;
    Boolean cheguei = false;

    public static List<ParseUser> gatheringAttendeesList;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_ficha_sintese_2) ;

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));
        actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        mToolbarView = (Toolbar) findViewById(R.id.toolbar);

        //-----------------------------------------------------//
        // Declaração do commentsListView
        commentsListView = (ObservableListView) findViewById(R.id.commentsListView);
        commentsListView.setScrollViewCallbacks(this);

        //-----------------------------------------------------//

        commentsAdapter = new CommentsAdapter(this);

        Bundle extras = null;
        Intent intent = getIntent();
        if (intent != null) {
            extras = intent.getExtras();
            if (extras != null) {
                responsaObjectId = extras.getString(FichaSinteseActivity.INTENT_OBJECT_ID_KEY);
                viewType = extras.getString(FichaSinteseActivity.INTENT_VIEW_KEY);
                cheguei = extras.getBoolean(FichaSinteseActivity.INTENT_CHEGUEI);
            }
        }

        ResponsaUtil.log("ResponsaNewDataManager", "Entered fichaSintese with objectId=" + responsaObjectId);

        responsaObjectFetchedReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ResponsaUtil.log("ResponsaNewDataManager", "Fetched intent in fichaSintese with objectId=" + responsaObjectId);
                List<ResponsaObject> responsaObjects = ResponsaApplication.responsaDataManager.retrieveLoadedData(viewType, responsaObjectId);
                if (responsaObjects.size() == 0) return;
                responsaObject = responsaObjects.get(0);


                List<UserResponsaObject> userResponsaObjects = ResponsaApplication.responsaDataManager.retrieveLoadedData("userResponsaObject", responsaObjectId);
                if (userResponsaObjects.size() > 0) {
                    userResponsaObject = (UserResponsaObject) userResponsaObjects.get(0);
                }

                // Populate list of people who might attend to the gathering:
                List attendeesResponsaObjects = ResponsaApplication.responsaDataManager.retrieveLoadedData("peopleWhoConfirmedInResponsaObject", responsaObject.getObjectId());
                gatheringAttendeesList = new ArrayList<ParseUser>();
                ParseUser user;
                UserResponsaObject attendeeResponsaObject;
                for (int i=0; i<attendeesResponsaObjects.size(); i++) {
                    attendeeResponsaObject = (UserResponsaObject) attendeesResponsaObjects.get(i);
                    user = attendeeResponsaObject.getParseUser("user");
                    gatheringAttendeesList.add(user);
                }


                if (responsaObject == null) {
                    //Erro!! Objeto não encontrado
                }
                if (userResponsaObject == null) {
                    userResponsaObject = ResponsaApplication.responsaDataManager.createUserResponsaObject(responsaObject);
                }
                initViewWithResponsaObject(responsaObject, userResponsaObject);
            }
        };

        registerReceiver(responsaObjectFetchedReceiver, new IntentFilter(ResponsaDataManager.getIntentDataUpdatedName(viewType)));

        ResponsaUtil.log("obj_debug", "will load responsaObject to ficha sintese: viewType=" + viewType + "objectId= " + (responsaObjectId == null ? "null" : responsaObjectId));

        ResponsaApplication.responsaDataManager.load(viewType, responsaObjectId);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (responsaObject != null && "initiative".equals(responsaObject.getType())) {
            ResponsaObject.currentResponsaObject = null;
        }
        unregisterReceiver(responsaObjectFetchedReceiver);
        // Remove user to the temporary challenge push channel if he/she is not owner nor did favorite this challenge:
        Notification.unsubscribeToChannel(Notification.RESPONSAOBJECT_CHANNEL, responsaObjectId, true);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        mToolbarView.inflateMenu(R.menu.menu_feedback);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onResume() {
        super.onResume();
    }

    public void hideKeyboard() {
        InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);

        inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_NOT_ALWAYS);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        onScrollChanged(commentsListView.getCurrentScrollY(), false, false);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        if (parallaxEnabled) {
            View c = commentsListView.getChildAt(0);
            int fixedScrollY = getScroll();
            int baseColor = getResources().getColor(R.color.primary);
            float alpha = Math.min(1, (float) fixedScrollY / mParallaxImageHeight);
            mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(alpha, baseColor));
        }

        //ViewHelper.setTranslationY(mImageView, -scrollY / 2);

        // Translate list background
        //ViewHelper.setTranslationY(mListBackgroundView, Math.max(0, -scrollY + mParallaxImageHeight));
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
    }

    private void initViewWithResponsaObject(ResponsaObject rObj, UserResponsaObject urObj) {
        this.responsaObject = rObj;
        this.userResponsaObject = urObj;

        ResponsaUtil.log("obj_debug", "init ficha sintese with responsaObject: " + responsaObject.getObjectId() + " - " + responsaObject.getTitle());


        if ("initiative".equals(responsaObject.getType())) {
            ResponsaObject.currentResponsaObject = responsaObject;

            enableParallax();
            //mImageView = findViewById(R.id.image);
        }

        //-----------------------------------------------------//
        // Populate data
        if ("gathering".equals(responsaObject.getType())) {
            actionBar.setTitle(R.string.gatheringFsTitle);
        } else {
            actionBar.setTitle(responsaObject.getTitle());
        }

        initFichaSinteseFragment(responsaObject.getType());

        fichaSinteseFragment.updateResponsaObject(responsaObject);
        populateUserResponsaObject(userResponsaObject);

        //-----------------------------------------------------//
        // Bind dos dados com commentListView

        final FichaSinteseActivity self = this;

        //ResponsaApplication.responsaDataManager.load("comments",responsaObject.getObjectId(), true);

        emojiconKeyboard = (EmojiconKeyboard) findViewById(R.id.emojiconKeyboard);

        emojiconKeyboard.setVisibility(rObj.getPublished() ? View.VISIBLE : View.GONE);
        //Save text comment
        emojiconKeyboard.setOnSubmitListener(new EmojiconKeyboard.OnEmojiKeyboardSubmitCallback() {
            @Override
            public void onSubmit(String text) {
                hideKeyboard();

                final Comment comment = preSaveComment(text);

                ResponsaApplication.responsaDataManager.saveObject("comments", responsaObjectId, viewType, comment, true, new ResponsaSaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        if (e == null) {
                            emojiconKeyboard.stopProgressBar();
                            String commentBody = comment.getString("comment");
                            if ("initiative".equals(responsaObject.getType())) {
                                Notification.sendNotification(ResponsaApplication.getAppContext(), "commentInitiative", responsaObject, Arrays.asList(commentBody), Arrays.asList(commentBody));
                            } else {
                                Notification.sendNotification(ResponsaApplication.getAppContext(), "commentGathering", responsaObject, Arrays.asList(commentBody), Arrays.asList(commentBody));
                            }
                        }
                    }
                });
                emojiconKeyboard.clearInput();
            }
        });

        //Save photo comment
        emojiconKeyboard.setActivityForCameraDialogResult(this, new CameraChooserDialog.CameraDialogReceiver() {
            @Override
            public void receive(Uri uri) {
                if (uri != null) {
                    hideKeyboard();
                    //start progress bar
                    emojiconKeyboard.startProgressBar();

                    final Comment comment = preSaveComment(null);

                    comment.setImageFromUri(uri, getContentResolver(), new Comment.AfterImageSetCallback() {
                        @Override
                        public void executeAfterImageSet() {
                            ResponsaApplication.responsaDataManager.saveObject("comments", responsaObjectId, viewType, comment, true, new ResponsaSaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null) {
                                        //finish progress bar
                                        emojiconKeyboard.stopProgressBar();
                                        if ("initiative".equals(responsaObject.getType())) {
                                            Notification.sendNotification(ResponsaApplication.getAppContext(), "commentInitiativeWithImage", responsaObject);
                                        } else {
                                            Notification.sendNotification(ResponsaApplication.getAppContext(), "commentGatheringWithImage", responsaObject);
                                        }
                                    }
                                }
                            });

                        }
                    });
                }
            }
        });
    }

    private Comment preSaveComment(String text) {
        Comment comment = new Comment();
        if (text != null) {
            comment.setComment(text);
        }
        comment.setUser(ParseUser.getCurrentUser());
        comment.setResponsaObject(responsaObject);
        comment.put("localCreatedAt", new Date());
        commentsAdapter.insert(comment, 0);
        //TODO make better this distance
        commentsListView.smoothScrollToPositionFromTop(1, 180);

        responsaObject.increment("countComments");
        ResponsaApplication.responsaDataManager.saveObject(viewType, responsaObjectId, responsaObject, true, null);

        return comment;
    }

    private void enableParallax() {
        parallaxEnabled = true;
        mToolbarView.setBackgroundColor(ScrollUtils.getColorWithAlpha(0, getResources().getColor(R.color.primary)));

        mParallaxImageHeight = getResources().getDimensionPixelSize(R.dimen.parallax_image_height);
    }
    private Dictionary<Integer, Integer> listViewItemHeights = new Hashtable<Integer, Integer>();

    private int getScroll() {
        View c = commentsListView.getChildAt(0); //this is the first visible row
        int scrollY = -c.getTop();
        listViewItemHeights.put(commentsListView.getFirstVisiblePosition(), c.getHeight());
        for (int i = 0; i < commentsListView.getFirstVisiblePosition(); ++i) {
            if (listViewItemHeights.get(i) != null) // (this is a sanity check)
                scrollY += listViewItemHeights.get(i); //add all heights of the views that are gone
        }
        return scrollY;
    }

    private void initFichaSinteseFragment(String type) {
        LayoutInflater inflater = getLayoutInflater();
        View header = null;
        List<Comment> comments = ResponsaApplication.responsaDataManager.retrieveLoadedData("comments", responsaObjectId);
        commentsAdapter.populate(comments);
        QueryResult.MetaData commentsMetaData = ResponsaApplication.responsaDataManager.retrieveLoadedMetaData("comments", responsaObjectId);
        if (!inflated) {

            // provisorily add user to this responsaObject push channel, so that new comments from other users will be updated automatically to him/her:
            Notification.subscribeToChannel(Notification.RESPONSAOBJECT_CHANNEL, responsaObjectId, true);

            commentsMetaData.page = 0;
            loadMoreComponent = new LoadMoreComponent(this, null, R.layout.progress_load_more);
            loadMoreComponent.initParams("comments", responsaObjectId, type);

            if ("initiative".equals(type)) {
                header = inflater.inflate(R.layout.fragment_ficha_sintese_header_initiative, null);

                commentsListView.addHeaderView(header);

                fichaSinteseFragment = (FichaSinteseBaseFragment)
                        getSupportFragmentManager()
                                .findFragmentById(R.id.headerFragmentEmbeddedInitiative);
            } else {
                header = inflater.inflate(R.layout.fragment_ficha_sintese_header_gathering, null);


                commentsListView.addHeaderView(header);

                fichaSinteseFragment = (FichaSinteseBaseFragment)
                        getSupportFragmentManager()
                                .findFragmentById(R.id.headerFragmentEmbeddedGathering);
            }

            commentsListView.addFooterView(loadMoreComponent);
            commentsListView.setAdapter(commentsAdapter);
            inflated = true;
        } else {
            loadMoreComponent.setPage(commentsMetaData.page);
            loadMoreComponent.stopProgressBar();
        }

        if (ResponsaApplication.responsaDataManager.hasMore("comments", responsaObjectId)) {
            loadMoreComponent.showBtnLoadMore();
        } else {
            loadMoreComponent.hideBtnLoadMore();
        }
    }

    private void populateUserResponsaObject(UserResponsaObject userResponsaObject) {
        populateFavoritar(userResponsaObject.getFavoritou());
        populateMarcarPresenca(userResponsaObject.getMarcouPresenca());
    }

    private void populateFavoritar(Boolean newFavoritarState) {
        fichaSinteseFragment.setFavoritar(newFavoritarState);
    }

    private void populateMarcarPresenca(Boolean newMarcarPresencaState) {
        fichaSinteseFragment.setMarcarPresenca(newMarcarPresencaState);
    }

    public void onClickDescription(View view) {

        if (descriptionHidden) {
            ResponsaUtil.resetGradient(((TextView) view));
            ((TextView) view).setMaxLines(Integer.MAX_VALUE);
            ((TextView) view).setEllipsize(null);
        } else {
            ResponsaUtil.applyGradient(((TextView) view),this);
            ((TextView) view).setMaxLines(3);
            ((TextView) view).setEllipsize(TextUtils.TruncateAt.END);
        }
        descriptionHidden = !descriptionHidden;
    }

    public void onClickFavoritar(View view) {

        if (responsaObject == null || !responsaObject.getPublished()) {
            return;
        }

        //ResponsaApplication.vibrate();
        final Boolean favoritouIsFirstChange = userResponsaObject.getFavoritouChangedAt() == null;
        favoritar = !userResponsaObject.getFavoritou();
        userResponsaObject.setFavoritou(favoritar);
        userResponsaObject.setFavoritouChangedAt(new Date());
        ResponsaApplication.responsaDataManager.saveObject("userResponsaObject", responsaObjectId, userResponsaObject, new ResponsaSaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    ResponsaApplication.getAppContext().sendBroadcast(new Intent(FavoritesFragment.ON_FAVORITES_UPDATED));
                    if (e == null && favoritar && favoritouIsFirstChange) {
                        if ("initiative".equals(responsaObject.getType())) {
                            Notification.sendNotification(ResponsaApplication.getAppContext(), "favoriteInitiative", responsaObject);
                        } else {
                            Notification.sendNotification(ResponsaApplication.getAppContext(), "favoriteGathering", responsaObject);
                        }
                    }
                }
            }
        });
        responsaObject.increment("countFavorites", favoritar ? 1 : -1);
        ResponsaApplication.responsaDataManager.saveObject(viewType, responsaObjectId, responsaObject, null, null);
        populateFavoritar(favoritar);
    }

    public void onClickCompartilhar(View view) {

        if (responsaObject == null || !responsaObject.getPublished()) {
            return;
        }

        Intent share = new Intent(android.content.Intent.ACTION_SEND);
        share.setType("text/plain");
        share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);

        // Add data to the intent, the receiving app will decide
        // what to do with it.
        share.putExtra(Intent.EXTRA_SUBJECT, responsaObject.getTitle());
        share.putExtra(Intent.EXTRA_TEXT, responsaObject.getShareUrl());

        startActivity(Intent.createChooser(share, "Compartilhar link"));

    }

    public void onClickExportar(View view) {
        responsaObject.exportEvent(this);
    }

    public void onClickMarcarPresenca(View view) {
        //ResponsaApplication.vibrate();
        final Boolean marcouPresencaIsFirstChange = userResponsaObject.getMarcouPresencaChangedAt() == null;
        marcarPresenca = !userResponsaObject.getMarcouPresenca();
        userResponsaObject.setMarcouPresenca(marcarPresenca);
        userResponsaObject.setMarcouPresencaChangedAt(new Date());

        updateGatheringAttendeesList(marcarPresenca);

        ResponsaApplication.responsaDataManager.saveObject("userResponsaObject", responsaObjectId, viewType, userResponsaObject, false, new ResponsaSaveCallback() {
            @Override
            public void done(ParseException e) {

            }
        });

        responsaObject.increment("countWillAttendGathering", marcarPresenca ? 1 : -1);
        ResponsaApplication.responsaDataManager.saveObject(viewType, responsaObjectId, responsaObject, false, new ResponsaSaveCallback() {
            @Override
            public void done(ParseException e) {
                // Only send notification if the person is confirming presence for the first time
                if (e == null)
                    if (userResponsaObject.getMarcouPresenca() && marcouPresencaIsFirstChange) {
                        Notification.sendNotification(ResponsaApplication.getAppContext(), "confirmGatheringParticipation", responsaObject);

                    } else {
                        //send silent notification: just to update others' list
                        Notification.sendNotification(ResponsaApplication.getAppContext(), "confirmGatheringParticipationSilent", responsaObject);

                    }
            }
        });
        populateMarcarPresenca(marcarPresenca);

        //shows dialog asking user to put event in personal calendar

        if (marcarPresenca) {
            final FichaSinteseActivity self = this;

            DialogInterface.OnClickListener dialogClickListener = new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    switch (which){
                        case DialogInterface.BUTTON_POSITIVE:
                            responsaObject.exportEvent(self);
                            break;

                        case DialogInterface.BUTTON_NEGATIVE:
                            //No button clicked
                            break;
                    }
                }
            };

            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Deseja adicionar este evento no calendário do seu celular?").setPositiveButton("Sim", dialogClickListener)
                    .setNegativeButton("Não", dialogClickListener).show();
        }

    }

    private void updateGatheringAttendeesList(Boolean marcarPresenca) {
        if (marcarPresenca) {
            gatheringAttendeesList.add(ParseUser.getCurrentUser());
        } else {
            for (int i=0;i<gatheringAttendeesList.size();i++) {
                if (gatheringAttendeesList.get(i).equals(ParseUser.getCurrentUser())) {
                    gatheringAttendeesList.remove(i);
                    break;
                }
            }
        }
    }

    public void onClickEncontros(View view) {

        if (responsaObject == null || !responsaObject.getPublished()) {
            return;
        }

        ResponsaObject.currentResponsaObject = responsaObject;
        Intent intent = new Intent(this, GatheringsInInitiativeActivity.class);
        intent.putExtra("view", "gatheringsFromInitiative");
        intent.putExtra("objectId", responsaObject.getObjectId());
        startActivity(intent);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (resultCode == Activity.RESULT_OK) {
            if (requestCode == CameraChooserDialog.SELECT_PICTURE_REQUEST_CODE) {
                emojiconKeyboard.handleCameraResultIntent(data);
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_feedback:
                intent = new Intent(this, FeedbackActivity.class);
                intent.putExtra("view", viewType);
                intent.putExtra("viewObjectId",responsaObjectId);
                intent.putExtra("viewClass",getClass().getSimpleName());
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

}