/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.org.eita.responsa.view.fragment;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.SearchView;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;

import java.util.List;

import br.org.eita.responsa.MainActivity;
import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.ChallengesFirstPageAdapter;
import br.org.eita.responsa.adapter.ChallengesSearchAjaxAdapter;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.util.ExtraFilters;
import br.org.eita.responsa.view.activity.AboutActivity;
import br.org.eita.responsa.view.activity.FeedbackActivity;

public class ChallengeListFragment extends BaseListFragment {

    ChallengesFirstPageAdapter frontPageAdapter;
    ChallengesSearchAjaxAdapter searchAdapter;
    ListAdapter currentAdapter;

    ObservableListView listView;

    SwipeRefreshLayout swipeLayout;

    View headerPadding;

    BroadcastReceiver updatesReceiver;

    RelativeLayout emptyListItemLabel = null;
    TextView headerText;

    ExtraFilters extraFilters = null;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_challenges_2, container, false);

        setHasOptionsMenu(true);

        extraFilters = new ExtraFilters();

        Activity parentActivity = getActivity();
        listView = (ObservableListView) view.findViewById(R.id.scroll);

        View padding = inflater.inflate(R.layout.padding, listView, false);
        headerPadding = padding.findViewById(R.id.headerPadding);
        listView.addHeaderView(padding);


        //Adds title
        headerText = (TextView) inflater.inflate(R.layout.list_title,listView,false);
        headerText.setText(R.string.u_challenges_and_discoveries);

        emptyListItemLabel = (RelativeLayout) inflater.inflate(R.layout.empty_list_item,listView,false);


        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ResponsaApplication.responsaDataManager.load("challenges", null, ResponsaDataManager.FORCE_REMOTE, extraFilters);
            }
        });

        swipeLayout.setProgressViewOffset(false, 0, 160);

        frontPageAdapter = new ChallengesFirstPageAdapter(getActivity(), ResponsaApplication.responsaDataManager.getChallengeFrontPageData());

        searchAdapter = new ChallengesSearchAjaxAdapter(getActivity());
        searchAdapter.setOnQueryCompleted(new ChallengesSearchAjaxAdapter.OnQueryCompletedCallback() {
            @Override
            public void onQueryComplete(String queryText) {
                listView.removeHeaderView(emptyListItemLabel);
                if (searchAdapter.isEmpty()) {
                    listView.addHeaderView(emptyListItemLabel);
                }
            }
        });


        listView.setTouchInterceptionViewGroup((ViewGroup) parentActivity.findViewById(R.id.container));

        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            listView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }

        currentAdapter = frontPageAdapter;
        listView.setAdapter(frontPageAdapter);

        updatesReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                List<Challenge> data = ResponsaApplication.responsaDataManager.retrieveLoadedData("challenges", null);
                frontPageAdapter.populate(data);

                listView.removeHeaderView(emptyListItemLabel);
                if (data.size() == 0) {
                    listView.addHeaderView(emptyListItemLabel);
                }

                swipeLayout.setRefreshing(false);
            }
        };

        getActivity().registerReceiver(updatesReceiver, new IntentFilter(ResponsaDataManager.getIntentDataUpdatedName("challenges")));
        ResponsaApplication.responsaDataManager.load("challenges", null, extraFilters);

        final View showChallengesButton = getActivity().findViewById(R.id.showChallenges);
        final View showDiscoveriesButton = getActivity().findViewById(R.id.showDiscoveries);
        final View showChallengesAndDiscoveriesButton = getActivity().findViewById(R.id.showChallengesAndDiscoveries);

        showChallengesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                extraFilters.add("correctAnswer","IS NULL",null);
                ResponsaApplication.responsaDataManager.load("challenges", null, extraFilters);
                ((MainActivity)getActivity()).floatingActionsMenu.collapse();
                showChallengesAndDiscoveriesButton.setVisibility(View.VISIBLE);
                showChallengesButton.setVisibility(View.GONE);
                showDiscoveriesButton.setVisibility(View.VISIBLE);
            }
        });
        showDiscoveriesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                extraFilters.add("correctAnswer","IS NOT NULL",null);
                ResponsaApplication.responsaDataManager.load("challenges", null, extraFilters);
                ((MainActivity)getActivity()).floatingActionsMenu.collapse();
                showChallengesAndDiscoveriesButton.setVisibility(View.VISIBLE);
                showChallengesButton.setVisibility(View.VISIBLE);
                showDiscoveriesButton.setVisibility(View.GONE);
            }
        });
        showChallengesAndDiscoveriesButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                extraFilters.remove("correctAnswer");
                ResponsaApplication.responsaDataManager.load("challenges", null, extraFilters);
                ((MainActivity)getActivity()).floatingActionsMenu.collapse();
                showChallengesAndDiscoveriesButton.setVisibility(View.GONE);
                showChallengesButton.setVisibility(View.VISIBLE);
                showDiscoveriesButton.setVisibility(View.VISIBLE);
            }
        });



        return view;
    }

    @Override
    public void onDestroyView() {
        getActivity().unregisterReceiver(updatesReceiver);
        super.onDestroyView();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.menu_iniciativas_fragment, menu);

        inflater.inflate(R.menu.menu_feedback, menu);

        MenuItem searchItem = menu.findItem(R.id.action_busca);

        MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                ((MainActivity) getActivity()).setState(MainActivity.State.SEARCH);
                headerPadding.setVisibility(View.GONE);
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                ((MainActivity) getActivity()).setState(MainActivity.State.BROWSE);
                headerPadding.setVisibility(View.VISIBLE);
                return true;
            }
        });

        SearchView searchView = (SearchView) MenuItemCompat.getActionView(searchItem);

        searchView.setQueryHint(getString(R.string.search_hint));

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                listView.removeHeaderView(emptyListItemLabel);

                if (newText.isEmpty() && currentAdapter != frontPageAdapter) {
                    if (currentAdapter  != frontPageAdapter) {
                        listView.removeHeaderView(headerText);
                    }

                    listView.setAdapter(frontPageAdapter);
                    currentAdapter = frontPageAdapter;

                } else if (!newText.isEmpty()) {
                    if (currentAdapter  != searchAdapter) {
                        listView.addHeaderView(headerText);
                    }

                    listView.setAdapter(searchAdapter);
                    currentAdapter = searchAdapter;

                    searchAdapter.setKeywordQuery(newText);
                }

                return false;
            }
        });
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id) {
            case R.id.action_feedback:
                intent = new Intent(getActivity(), FeedbackActivity.class);
                intent.putExtra("view", "challenges");
                intent.putExtra("viewObjectId",(String) null);
                intent.putExtra("viewClass",getClass().getSimpleName());
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(getActivity(), AboutActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
    /*
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);

        // Make sure that we are currently visible
        if (this.isVisible()) {
            // If we are becoming invisible, then...
            if (!isVisibleToUser) {
                //Toast.makeText(getActivity(),"notifications invisible", Toast.LENGTH_SHORT).show();
            } else {
                //Toast.makeText(getActivity(),"notifications visible", Toast.LENGTH_SHORT).show();
            }
        }
    }
    */
}