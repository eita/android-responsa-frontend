package br.org.eita.responsa.adapter;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.Notification;

public class NotificationAdapter extends ArrayAdapter<Notification> {

    public NotificationAdapter(Context context) {
        super(context, 0, new ArrayList<Notification>());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Notification notification = getItem(position);

        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.item_notification, null);
        }

        if (!notification.getRead()) {
            convertView.setBackgroundColor(getContext().getResources().getColor(R.color.readNotification));
        }

        TextView titleView = (TextView) convertView.findViewById(R.id.notificationTitle);
        titleView.setText(notification.getString("title"));

        TextView shortDescriptionView = (TextView) convertView.findViewById(R.id.notificationShortDescription);
        shortDescriptionView.setText(notification.getShortMessage());

        TextView createdAtView = (TextView) convertView.findViewById(R.id.notificationDate);
        Date createdAt = notification.getCreatedAt();
        if (createdAt == null) {
            createdAtView.setText(R.string.u_now);
        } else {
            createdAtView.setText(DateUtils.getRelativeTimeSpanString(notification.getCreatedAt().getTime()));
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notification.openActivity(getContext());
            }
        });

        return convertView;
    }
}
