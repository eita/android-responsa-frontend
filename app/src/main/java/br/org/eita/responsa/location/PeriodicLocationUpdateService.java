package br.org.eita.responsa.location;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.os.BatteryManager;
import android.os.IBinder;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.util.Log;

import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.receivers.PeriodicTaskReceiver;
import br.org.eita.responsa.util.ResponsaUtil;

public class PeriodicLocationUpdateService extends Service {

    private static final String TAG = "PResponsaLocation";

    PeriodicTaskReceiver mPeriodicTaskReceiver = new PeriodicTaskReceiver();

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        ResponsaUtil.log("P-ResponsaLocation", "Start periodic Location Update Service");


        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);


        IntentFilter batteryStatusIntentFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatusIntent = registerReceiver(null, batteryStatusIntentFilter);

        if (batteryStatusIntent != null) {
            int level = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            int scale = batteryStatusIntent.getIntExtra(BatteryManager.EXTRA_SCALE, -1);
            float batteryPercentage = level / (float) scale;
            float lowBatteryPercentageLevel = 0.14f;

            try {
                int lowBatteryLevel = Resources.getSystem().getInteger(Resources.getSystem().getIdentifier("config_lowBatteryWarningLevel", "integer", "android"));
                lowBatteryPercentageLevel = lowBatteryLevel / (float) scale;
            } catch (Resources.NotFoundException e) {
                ResponsaUtil.log(TAG,"Missing low battery threshold resource");
            }

            sharedPreferences.edit().putBoolean(PeriodicTaskReceiver.BACKGROUND_SERVICE_BATTERY_CONTROL, batteryPercentage >= lowBatteryPercentageLevel).apply();
        } else {
            sharedPreferences.edit().putBoolean(PeriodicTaskReceiver.BACKGROUND_SERVICE_BATTERY_CONTROL, true).apply();
        }


        /*
        Context context = this;
        Intent alarmIntent = new Intent(context, LocationService.class);
        boolean isAlarmUp = PendingIntent.getBroadcast(context, 0, alarmIntent, PendingIntent.FLAG_NO_CREATE) != null;


        if (!isAlarmUp) {
            ResponsaUtil.log("ResponsaLocation","ALARM SETUP");
            AlarmManager alarmManager = (AlarmManager) context.getSystemService(Context.ALARM_SERVICE);
            alarmIntent.setAction(PeriodicTaskReceiver.INTENT_FETCH_LOCATION_NOW);
            PendingIntent pendingIntent = PendingIntent.getBroadcast(context, 0, alarmIntent, 0);
            alarmManager.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, SystemClock.elapsedRealtime(), 90000, pendingIntent);
        } else {
            ResponsaUtil.log("ResponsaLocation","ALARM - NO NEED TO SETUP");
        }*/


        mPeriodicTaskReceiver.restartPeriodicTask(this);
        //Intent serviceIntent = new Intent(this, LocationService.class);
        //startService(serviceIntent);

        return START_STICKY;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }
}

