package br.org.eita.responsa.adapter;


import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.parse.DeleteCallback;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.Comment;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.view.activity.FichaSinteseActivity;
import br.org.eita.responsa.view.fragment.FullImageDialogFragment;

public class CommentsAdapter extends ArrayAdapter<Comment> {

    public HashMap<String, Bitmap> cachedImages = new HashMap<String,Bitmap>();
    FichaSinteseActivity fichaSinteseActivity;

    public CommentsAdapter(FichaSinteseActivity fichaSinteseActivity) {
        super(fichaSinteseActivity, 0, new ArrayList<Comment>());
        this.fichaSinteseActivity = fichaSinteseActivity;
    }

    @Override
    public View getView(int position, View convertView, final ViewGroup parent) {

        final Comment comment = getItem(position);
        //ResponsaUtil.log("NewDataManager","comment: " + (comment == null ? "null" : "not null") + ", user:" + (comment != null && comment.getUser() != null ? "not null" : "null") + ", parseUser:" + (ParseUser.getCurrentUser() == null ? "null" : "not null"));
        final Boolean myself = comment.getUser().equals(ParseUser.getCurrentUser());
        ParseFile commentThumbnailFile = comment.getThumbnail();
        final ParseFile commentFullImageFile = comment.getImage();
        String strCommentText = comment.getComment();
        final Boolean hasImage = commentThumbnailFile != null;
        final Boolean hasText = strCommentText != null && !strCommentText.isEmpty();

        convertView = myself
            ? hasImage
                ? View.inflate(getContext(), R.layout.comment_item_image_myself_view, null)
                : View.inflate(getContext(), R.layout.comment_item_myself_view, null)
            : hasImage
                ? View.inflate(getContext(), R.layout.comment_item_image_view, null)
                : View.inflate(getContext(), R.layout.comment_item_view, null);

        final BootstrapCircleThumbnail userImageView = (BootstrapCircleThumbnail) convertView.findViewById(R.id.userImage);

        if (!myself) {
            TextView textUserName = (TextView) convertView.findViewById(R.id.textUserName);
            textUserName.setText(comment.getUser().getString("name"));
        }

        TextView textDate = (TextView) convertView.findViewById(R.id.textDate);
        textDate.setText(DateUtils.getRelativeTimeSpanString(comment.getLocalCreatedAt().getTime()));

        RelativeLayout commentContent = (RelativeLayout) convertView.findViewById(R.id.commentContent);

        /* Part done: edit and delete comment
        commentContent.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (comment.getUser().equals(ParseUser.getCurrentUser())) {
                    CharSequence actions[] = new CharSequence[] {"Editar", "Apagar"};

                    AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                    builder.setTitle("Comentário");
                    builder.setItems(actions, new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            switch (which) {
                                case 0: //Editar
                                    fichaSinteseActivity.editComment(comment);
                                    break;
                                case 1:
                                    comment.deleteInBackground(new DeleteCallback() {
                                        @Override
                                        public void done(ParseException e) {
                                            if (e == null) {
                                                challenge.decrementAnswerCount();
                                                ResponsaApplication.responsaDataManager.saveObject("challenge", challenge.getObjectId(), challenge, true, null);

                                                Toast.makeText(getContext(), "Resposta apagada", Toast.LENGTH_LONG).show();
                                                //ResponsaApplication.responsaDataManager.load("challenge",challenge.getObjectId(),"force_remote");
                                            } else {
                                                Toast.makeText(getContext(), "Resposta não pôde ser apagada. Tente novamente mais tarde.", Toast.LENGTH_LONG).show();
                                            }
                                        }
                                    });
                                    break;
                            }
                        }
                    });
                    builder.show();
                }

                return false;
            }
        });
        */


        if (hasText && !hasImage) {
            //Comment text
            TextView commentText = (TextView) convertView.findViewById(R.id.commentText);
            commentText.setText(strCommentText);
        } else if (hasImage) {
            //Comment photo
            final ImageView commentThumbnail = (ImageView) convertView.findViewById(R.id.commentThumbnail);
            final Bitmap cachedImage = cachedImages.get(comment.getObjectId());
            if (cachedImage != null) {
                commentThumbnail.setImageBitmap(cachedImage);
                commentThumbnail.invalidate();
            } else {
                commentThumbnailFile.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] bytes, ParseException e) {
                        if (e != null) {
                            ResponsaUtil.log("ImageOpenError",e.getMessage());
                        }
                        if (bytes != null) { //Problem when transfering endpoint and not yet files...
                            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            if (bitmap != null) {
                                if (comment.getImageOrientation() != null) {
                                    ResponsaUtil.rotateBitmap(bitmap, comment.getImageOrientation());
                                }
                                commentThumbnail.setImageBitmap(bitmap);
                                commentThumbnail.invalidate();
                                cachedImages.put(comment.getObjectId(),bitmap);
                            }
                        }
                    }
                });
            }


            commentThumbnail.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View v) {

                    final FullImageDialogFragment fullImageDialogFragment = new FullImageDialogFragment();


                    Bitmap cachedImage = cachedImages.get(comment.getObjectId()+"_fullImage");
                    if (cachedImage != null) {
                        ResponsaUtil.log("CommentsAdapter", "Image clicked!");
                        fullImageDialogFragment.setImage(cachedImage);
                        fullImageDialogFragment.show(((AppCompatActivity)parent.getContext()).getSupportFragmentManager(), "view_image_fullscreen");
                    } else {
                        commentFullImageFile.getDataInBackground(new GetDataCallback() {
                            @Override
                            public void done(byte[] bytes, ParseException e) {
                                if (e != null) {
                                    ResponsaUtil.log("ImageOpenError",e.getMessage());
                                }
                                if (bytes != null) { //Problem when transfering endpoint and not yet files...
                                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                                    if (bitmap != null) {
                                        if (comment.getImageOrientation() != null) {
                                            ResponsaUtil.rotateBitmap(bitmap, comment.getImageOrientation());
                                        }
                                        cachedImages.put(comment.getObjectId()+"_fullImage",bitmap);
                                        fullImageDialogFragment.setImage(bitmap);
                                        fullImageDialogFragment.show(((AppCompatActivity) parent.getContext()).getSupportFragmentManager(), "view_image_fullscreen");
                                    }
                                }
                            }
                        });
                    }

                    /*FrameLayout layout = (FrameLayout) findViewById(R.layout.activity_ficha_sintese_2);
                    layout.setBackground(commentThumbnail.getDrawable());*/

                }
            });

        }


        //User name
        //aqui

        //User avatar
        /*
        ParseFile imgUser = comment.getTargetUser().getParseFile("avatar");
        if (imgUser != null) {
            imgUser.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                    if (bitmap != null) {
                        userImageView.setImage(bitmap);
                    }
                }
            });
        }
        */
        return convertView;
    }

    public void populate(List<Comment> comments) {
        this.clear();
        this.addAll(comments);
    }

}

