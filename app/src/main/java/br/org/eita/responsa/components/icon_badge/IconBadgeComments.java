package br.org.eita.responsa.components.icon_badge;

import android.content.Context;
import android.util.AttributeSet;

import br.org.eita.responsa.R;
import br.org.eita.responsa.model.OnUpdateValueCallback;
import br.org.eita.responsa.model.ResponsaObject;

public class IconBadgeComments extends ModelIconBadge {
    private ResponsaObject responsaObject;

    public IconBadgeComments(Context context) {
        this(context, null);
    }

    public IconBadgeComments(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public IconBadgeComments(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        setIcon(R.xml.ic_fs_g_comentarios);
        setModelField("countComments");
    }
}

