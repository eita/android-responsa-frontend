package br.org.eita.responsa.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.SpinnerAdapter;

import java.util.ArrayList;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.storage.queries.TerritoryDb;

public class SubCityRegionSpinner extends FrameLayout {

    static TerritoryDb territoryDb;

    HomeTerritory selectedSubCityRegion;
    HomeTerritory city = null;

    //Adapter for dropdown:
    static SubCityRegionSpinnerAdapter subCityRegionSpinnerAdapter;
    ArrayList<HomeTerritory> territories = new ArrayList<>();

    //TODO this should not be static
    public static Spinner subCityRegionSpinner;

    public SubCityRegionSpinner(Context context) {
        this(context, null);
    }

    public SubCityRegionSpinner(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SubCityRegionSpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.component_subcityregion_spinner, this);

        territoryDb = new TerritoryDb(context);

        subCityRegionSpinner = (Spinner) view.findViewById(R.id.subCityRegionInput);

        refreshSubCityRegionSpinnerAdapter(city);

    }

    static public void refreshSubCityRegionSpinnerAdapter(HomeTerritory city) {
        ArrayList<HomeTerritory> loadedTerritories = territoryDb.queryTerritoriesByCity(city);
        subCityRegionSpinnerAdapter = new SubCityRegionSpinnerAdapter(ResponsaApplication.getAppContext(), loadedTerritories);
        subCityRegionSpinner.setAdapter(subCityRegionSpinnerAdapter);
    }

    public static void SetSelectedSubCityRegion(HomeTerritory selectedTerritory) {
        SpinnerAdapter adapter = subCityRegionSpinner.getAdapter();
        final int count = adapter.getCount();
        for (int pos = 0; pos < count; pos++) {
            if (selectedTerritory.equals(adapter.getItem(pos))) {
                subCityRegionSpinner.setSelection(pos);
                return;
            }
        }
    }
}
