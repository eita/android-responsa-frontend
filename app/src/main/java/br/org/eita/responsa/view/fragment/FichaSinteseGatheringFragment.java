package br.org.eita.responsa.view.fragment;

import br.org.eita.responsa.R;

public class FichaSinteseGatheringFragment extends FichaSinteseBaseFragment {
    @Override
    protected int getFragmentResource() {
        return R.layout.fragment_ficha_sintese_gathering;
    }
}
