package br.org.eita.responsa.model;


import com.parse.ParseClassName;
import com.parse.ParseObject;

import org.json.JSONObject;

import java.util.Date;

@ParseClassName("Version")
public class Version extends ParseObject {

    public String getVersionName() {
        return getString("versionName");
    }

    public void setVersionName(String versionName) {
        if (versionName == null) {
            this.put("versionName", JSONObject.NULL);
        } else {
            this.put("versionName", versionName);
        }
    }

    public Date getExpires() {
        return getDate("expires");
    }

    public void setExpires(Date expires) {
        if (expires == null) {
            this.put("expires", JSONObject.NULL);
        } else {
            this.put("expires", expires);
        }
    }

    public String getPlatform() {
        return getString("platform");
    }

    public void setPlatform(String platform) {
        if (platform == null) {
            this.put("platform", JSONObject.NULL);
        } else {
            this.put("platform", platform);
        }
    }
}
