/*
 * Copyright 2014 Soichiro Kashima
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package br.org.eita.responsa.view.fragment;


import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.github.ksoichiro.android.observablescrollview.ObservableListView;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.FavoritesAdapter;
import br.org.eita.responsa.components.LoadMoreComponent;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.view.activity.AboutActivity;
import br.org.eita.responsa.view.activity.FeedbackActivity;

public class FavoritesFragment extends BaseListFragment {

    public static final String ON_FAVORITES_UPDATED = "on_favorites_updated";

    private BroadcastReceiver updatesReceiver;
    private BroadcastReceiver onFavoritesChangeReceiver;

    FavoritesAdapter adapter;

    TextView headerText;
    String originalHeaderTextValue;
    ObservableListView listView;
    LoadMoreComponent loadMoreComponent;

    SwipeRefreshLayout swipeLayout;

    View headerPadding;

    String viewType1 = "favoriteResponsaObjects";
    String viewType2 = "favoriteChallenges";

    Boolean cleanListBeforeRefresh = false;

    Boolean firstPopulate = null;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        firstPopulate = true;

        Boolean hidePadding = getArguments().getBoolean("hideHeaderPadding");

        Integer layout = 0;
        String headerString = null;
        layout = R.layout.fragment_favorites;

        final View view = inflater.inflate(layout, container, false);

        setHasOptionsMenu(true);

        Activity parentActivity = getActivity();
        listView = (ObservableListView) view.findViewById(R.id.scroll);

        if (!hidePadding) {
            View padding = inflater.inflate(R.layout.padding, listView, false);
            headerPadding = padding.findViewById(R.id.headerPadding);
            listView.addHeaderView(padding);
        }

        //Adds title
        headerText = (TextView) inflater.inflate(R.layout.list_title,listView,false);
        listView.addHeaderView(headerText);
        headerText.setText(headerString);

        swipeLayout = (SwipeRefreshLayout) view.findViewById(R.id.swipe_container);

        swipeLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                ResponsaApplication.responsaDataManager.load(viewType1, null, ResponsaDataManager.FORCE_REMOTE);
                cleanListBeforeRefresh = true;
            }
        });

        swipeLayout.setProgressViewOffset(false, 0, 160);

        listView.setTouchInterceptionViewGroup((ViewGroup) parentActivity.findViewById(R.id.container));

        if (parentActivity instanceof ObservableScrollViewCallbacks) {
            listView.setScrollViewCallbacks((ObservableScrollViewCallbacks) parentActivity);
        }

        /*
        LocationCoordinates visibleLocation = LocationCoordinates.getVisibleLocation();


        if (LocationCoordinates.getCurrentLocation() == null && "initiative".equals(viewType)) {
            CharSequence relativeTimeSpan = DateUtils.getRelativeTimeSpanString(visibleLocation.getFetchTime(), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS);
            headerText.setText(getResources().getString(R.string.u_near_you_in_the_past,relativeTimeSpan));
            //swipeLayout.setRefreshing(true);
        }
        */

        adapter = createNewAdapter();
        //loadMoreComponent = new LoadMoreComponent(getActivity(), null, R.layout.progress_load_more);
        //loadMoreComponent.initParams(viewType1, null);
        //listView.addFooterView(loadMoreComponent);
        listView.setAdapter(adapter);


        updatesReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                populateList();
                swipeLayout.setRefreshing(false);
            }
        };

        getActivity().registerReceiver(updatesReceiver, new IntentFilter(ResponsaDataManager.getIntentDataUpdatedName(viewType1)));

        ResponsaApplication.responsaDataManager.load(viewType1, null);
        cleanListBeforeRefresh = true;

        onFavoritesChangeReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                ResponsaApplication.responsaDataManager.load(viewType1, null, ResponsaDataManager.FORCE_REMOTE);
                cleanListBeforeRefresh = true;
            }
        };

        getActivity().registerReceiver(onFavoritesChangeReceiver, new IntentFilter(FavoritesFragment.ON_FAVORITES_UPDATED));


        return view;
    }

    private void populateList() {
        List data1 = new ArrayList(ResponsaApplication.responsaDataManager.retrieveLoadedData(viewType1, null));
        List data2 = ResponsaApplication.responsaDataManager.retrieveLoadedData(viewType2, null);

        data1.addAll(data2);

        Collections.sort(data1, new Comparator() {
            @Override
            public int compare(Object obj1, Object obj2) {
                ParseObject po1 = (ParseObject) obj1;
                ParseObject po2 = (ParseObject) obj2;

                String title1, title2;

                if ("UserResponsaObject".equals(po1.getClass().getSimpleName())) {
                    title1 = po1.getParseObject("responsaObject").getString("title");
                } else {
                    title1 = po1.getParseObject("challenge").getString("title");
                }

                if ("UserResponsaObject".equals(po2.getClass().getSimpleName())) {
                    title2 = po2.getParseObject("responsaObject").getString("title");
                } else {
                    title2 = po2.getParseObject("challenge").getString("title");
                }

                return title1.compareTo(title2);
            }
        });

        //QueryResult.MetaData metaData = ResponsaApplication.responsaDataManager.retrieveLoadedMetaData(view, null);

        adapter.populateAdapter(data1, true);
        cleanListBeforeRefresh = false;

        /*
        loadMoreComponent.setPage(metaData.page);
        if (ResponsaApplication.responsaDataManager.hasMore(viewType1, null)) {
            loadMoreComponent.showBtnLoadMore();
        } else {
            loadMoreComponent.stopProgressBar();
            loadMoreComponent.hideBtnLoadMore();
        }*/

        headerText.setText(getString(R.string.u_list_title_favorites));

        if (firstPopulate) {
            firstPopulate = false;
        }
    }



    @Override
    public void onDestroy() {
        getActivity().unregisterReceiver(updatesReceiver);
        getActivity().unregisterReceiver(onFavoritesChangeReceiver);
        super.onDestroy();
    }


    protected FavoritesAdapter createNewAdapter() {
        FavoritesAdapter favoritesAdapter = new FavoritesAdapter(getActivity());
        return favoritesAdapter;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        //inflater.inflate(R.menu.menu_iniciativas_fragment, menu);

        inflater.inflate(R.menu.menu_feedback, menu);

        //MenuItem searchItem = menu.findItem(R.id.action_busca);

        /*MenuItemCompat.setOnActionExpandListener(searchItem, new MenuItemCompat.OnActionExpandListener() {
            @Override
            public boolean onMenuItemActionExpand(MenuItem item) {
                if (getActivity() instanceof MainActivity) {
                    ((MainActivity) getActivity()).setState(MainActivity.State.SEARCH);
                    headerPadding.setVisibility(View.GONE);
                }
                return true;
            }

            @Override
            public boolean onMenuItemActionCollapse(MenuItem item) {
                if (getActivity() instanceof MainActivity) {
                    ((MainActivity) getActivity()).setState(MainActivity.State.BROWSE);
                    headerPadding.setVisibility(View.VISIBLE);
                }
                return true;
            }
        });*/
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent;
        switch (id) {
            case R.id.action_feedback:
                intent = new Intent(getActivity(), FeedbackActivity.class);
                intent.putExtra("view", "favorites");
                intent.putExtra("viewObjectId","");
                intent.putExtra("viewClass",getClass().getSimpleName());
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(getActivity(), AboutActivity.class);
                startActivity(intent);
                return true;
        }

        return super.onOptionsItemSelected(item);
    }


}