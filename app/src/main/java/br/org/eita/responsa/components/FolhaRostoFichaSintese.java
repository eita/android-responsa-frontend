package br.org.eita.responsa.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import br.org.eita.responsa.R;
import br.org.eita.responsa.model.InitiativeCategory;

public class FolhaRostoFichaSintese extends RelativeLayout {
    public FolhaRostoFichaSintese(Context context) {
        this(context, null);
    }

    public FolhaRostoFichaSintese(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public FolhaRostoFichaSintese(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    public void setCategory(String category) {



        int viewResource = 0;

        if (category == null) {
            viewResource = R.layout.fs_header_none;
        } else if (InitiativeCategory.FEIRAS.equals(category)) {
            viewResource = R.layout.fs_header_feiras;
        } else if (InitiativeCategory.GRUPOS_DE_CONSUMO_RESPONSAVEL.equals(category)) {
            viewResource = R.layout.fs_header_grupos_de_consumo_responsavel;
        } else if (InitiativeCategory.INICIATIVAS_DE_ECONOMIA_SOLIDARIA.equals(category)) {
            viewResource = R.layout.fs_header_iniciativas_de_economia_solidaria;
        } else if (InitiativeCategory.INICIATIVAS_DE_AGROECOLOGIA.equals(category)) {
            viewResource = R.layout.fs_header_iniciativas_de_agroecologia;
        } else if (InitiativeCategory.RESTAURANTES.equals(category)) {
            viewResource = R.layout.fs_header_restaurantes;
        } else if (InitiativeCategory.AGRICULTURA_URBANA.equals(category)) {
            viewResource = R.layout.fs_header_agricultura_urbana;
        } else if (InitiativeCategory.LOJAS.equals(category)) {
            viewResource = R.layout.fs_header_lojas;
        } else if (InitiativeCategory.COLETIVOS_CULTURAIS.equals(category)) {
            viewResource = R.layout.fs_header_coletivos_culturais;
        } else {
            viewResource = R.layout.fs_header_none;
        }



        LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(viewResource, this);

    }
}
