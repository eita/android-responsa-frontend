package br.org.eita.responsa.model;

import com.parse.ParseClassName;
import com.parse.ParseObject;
import com.parse.ParseUser;

import org.json.JSONObject;

@ParseClassName("Feedback")
public class Feedback extends ParseObject{

    public ParseUser getUser() {
        return getParseUser("user");
    }

    public void setUser(ParseUser user) {
        if (user == null) {
            this.put("user", JSONObject.NULL);
        } else {
            this.put("user", user);
        }
    }

    public String getUserText() {
        return getString("userText");
    }

    public void setUserText(String userText) {
        if (userText == null) {
            this.put("userText", JSONObject.NULL);
        } else {
            this.put("userText", userText);
        }
    }

    public String getViewClass() {
        return getString("viewClass");
    }

    public void setViewClass(String viewClass) {
        if (viewClass == null) {
            this.put("viewClass", JSONObject.NULL);
        } else {
            this.put("viewClass", viewClass);
        }
    }

    public String getView() {
        return getString("view");
    }

    public void setView(String view) {
        if (view == null) {
            this.put("view", JSONObject.NULL);
        } else {
            this.put("view", view);
        }
    }

    public String getViewObjectId() {
        return getString("viewObjectId");
    }

    public void setViewObjectId(String viewObjectId) {
        if (viewObjectId == null) {
            this.put("viewObjectId", JSONObject.NULL);
        } else {
            this.put("viewObjectId", viewObjectId);
        }
    }

    public String getCachedData() {
        return getString("cachedData");
    }

    public void setCachedData(String cachedData) {
        if (cachedData == null) {
            this.put("cachedData", JSONObject.NULL);
        } else {
            this.put("cachedData", cachedData);
        }
    }

    public String getLastFetchedAt() {
        return getString("lastFetchedAt");
    }

    public void setLastFetchedAt(String lastFetchedAt) {
        if (lastFetchedAt == null) {
            this.put("lastFetchedAt", JSONObject.NULL);
        } else {
            this.put("lastFetchedAt", lastFetchedAt);
        }
    }
}
