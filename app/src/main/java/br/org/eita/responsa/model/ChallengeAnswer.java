package br.org.eita.responsa.model;

import android.content.Intent;

import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.model.util.ResponsaSaveCallback;

@ParseClassName("ChallengeAnswer")
public class ChallengeAnswer extends ParseObject implements Votable {

    public static ChallengeAnswer currentChallengeAnswer;
    private Boolean challengeVoteFetched = false;

    private ChallengeUserJoin challengeUserJoin;

    private ArrayList<OnVotableChange> onVotableChanges = new ArrayList<OnVotableChange>();

    public ParseObject getChallenge() {
        return getParseObject("challenge");
    }

    public void setChallenge(Challenge challenge) {
        if (challenge == null) {
            this.put("challenge", JSONObject.NULL);
        } else {
            this.put("challenge", challenge);
        }
    }

    public String getDescription() {
        return getString("description");
    }

    public void setDescription(String description) {
        if (description == null) {
            this.put("description", JSONObject.NULL);
        } else {
            this.put("description", description);
        }
    }

    public Integer getUpVotes() {
        return getInt("upVotes");
    }

    public void setUpVotes(Integer upVotes) {
        if (upVotes == null) {
            this.put("upVotes", JSONObject.NULL);
        } else {
            this.put("upVotes", upVotes);
        }
    }

    public Integer getDownVotes() {
        return (getRelevance() - getUpVotes()) * -1;
    }

    public ParseUser getAuthor() {
        return getParseUser("author");
    }

    public void setAuthor(ParseUser author) {
        if (author == null) {
            this.put("author", JSONObject.NULL);
        } else {
            this.put("author", author);
        }
    }

    public Date getLocalCreatedAt() {
        return getDate("localCreatedAt");
    }

    public void setLocalCreatedAt(Date localCreatedAt) {
        if (localCreatedAt == null) {
            this.put("localCreatedAt", JSONObject.NULL);
        } else {
            this.put("localCreatedAt", localCreatedAt);
        }
    }

    public Boolean getChosenAnswer() {
        return getBoolean("chosenAnswer");
    }

    public void setChosenAnswer(Boolean chosenAnswer) {
        if (chosenAnswer == null) {
            this.put("chosenAnswer", JSONObject.NULL);
        } else {
            this.put("chosenAnswer", chosenAnswer);
        }
    }


    public ChallengeUserJoin getChallengeUserJoin() {
        return challengeUserJoin;
    }

    public void setChallengeUserJoin(ChallengeUserJoin challengeUserJoin) {
        this.challengeUserJoin = challengeUserJoin;
    }

    /**
     * Chooses this answer
     */
    public void choose() {
        Challenge challenge = (Challenge) this.getChallenge();
        challenge.setCorrectAnswer(this);
        this.setChosenAnswer(true);
        ResponsaApplication.responsaDataManager.saveObject("challengeAnswers", challenge.getObjectId(), "challenge", this, true, null);
        final ChallengeAnswer self = this;
        ResponsaApplication.responsaDataManager.saveObject("challenge", challenge.getObjectId(), challenge, true, new ResponsaSaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Notification.sendNotification(ResponsaApplication.getAppContext(), "answerChallengeChosen", self.getChallenge());
                }
            }
        });
    }

    public void markAnswerVoteFetched() {
        this.challengeVoteFetched = true;
        for (OnVotableChange ch: onVotableChanges) {
            ch.onChange(this);
        }
    }

    public void findAnswerVote(final FindAnswerVoteCallback cb) {
        if (challengeVoteFetched) {
            if (cb != null) {
                cb.onAnswerVoteFetchCompleted(challengeUserJoin, null);
            }
        } else {
            ParseQuery<ChallengeUserJoin> query = ParseQuery.getQuery(ChallengeUserJoin.class);
            query.whereEqualTo("user",ParseUser.getCurrentUser());
            query.whereEqualTo("challengeAnswer",this);
            query.getFirstInBackground(new GetCallback<ChallengeUserJoin>() {
                @Override
                public void done(ChallengeUserJoin loadedChallengeUserJoin, ParseException e) {
                    if (e == null || e.getCode() == ParseException.OBJECT_NOT_FOUND) {
                        if (loadedChallengeUserJoin != null) {
                            challengeUserJoin = loadedChallengeUserJoin;
                        }
                        markAnswerVoteFetched();
                        if (cb != null) {
                            cb.onAnswerVoteFetchCompleted(loadedChallengeUserJoin, e);
                        }
                    }
                }
            });
        }
    }

    public interface FindAnswerVoteCallback {
        public void onAnswerVoteFetchCompleted(ChallengeUserJoin challengeUserJoin, ParseException e);
    }

    //----------------------------------------------------------------//
    // Votable methods

    @Override
    public void setVoteState(Integer newValue) {
        if (!this.challengeVoteFetched) { //Object is not votable
            return;
        }

        Integer oldValue = getVoteState();

        /*
        old  new         op
        -1   -1           -              -
        -1    0           down--         relevance++
        -1    1           down--, up++   relevance+=2
        0    -1           down++         relevance--
        0     0           -              -
        0     1                   up++   relevance++
        1    -1           down++, up--,  relevance-=2
        1     0                   up--   relevance--
        1     1           -
         */

        if (oldValue != newValue) {
            if (oldValue > 0) this.increment("upVotes",   -1);
            if (newValue > 0) this.increment("upVotes",    1);

            Integer relevanceDiff = newValue - oldValue;
            if (relevanceDiff != 0) this.increment("relevance",relevanceDiff);
        }

        if (challengeUserJoin == null) {
            challengeUserJoin = ResponsaApplication.responsaDataManager.createChallengeUserJoin(this);
        }
        challengeUserJoin.setVote(newValue);
        challengeUserJoin.saveEventually();
        this.saveEventually(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Notification.sendNotification(ResponsaApplication.getAppContext(), "voteChallenge", challengeUserJoin.getChallengeAnswer().getParseObject("challenge"));
                }
            }
        });
        for (OnVotableChange ch: onVotableChanges) {
            ch.onChange(this);
        }
    }

    @Override
    public Integer getVoteState() {
        if (challengeUserJoin == null) {
            return 0;
        } else {
            return challengeUserJoin.getVote();
        }
    }

    @Override
    public Integer getRelevance() {
        return getInt("relevance");
    }

    public void setRelevance(Integer relevance) {
        if (relevance == null) {
            this.put("relevance", JSONObject.NULL);
        } else {
            this.put("relevance", relevance);
        }
    }

    @Override
    public void onVotableChange(OnVotableChange onVotableChange) {
        onVotableChanges.add(onVotableChange);
    }

    @Override
    public boolean getVotesEnabled() {
        return this.challengeVoteFetched;
    }

    @Override
    public Integer getTotalVotes() {
        //rel - up = -down
        //res = up + down
        //res = up - rel + up
        //res = 2up - rel
        return 2 * this.getUpVotes() - this.getRelevance();
    }


}
