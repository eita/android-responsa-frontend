package br.org.eita.responsa.components;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffXfermode;
import android.util.AttributeSet;
import android.view.Display;
import android.view.WindowManager;
import android.widget.ImageView;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;

public class MaskImage extends ImageView {
	int mImageSource=0;
	int mMaskSource=0;
	int mBackgroundSource=0;
	RuntimeException mException;

    TypedArray a;

	public MaskImage(Context context, AttributeSet attrs) {
		super(context, attrs);
		a = getContext().obtainStyledAttributes(attrs, R.styleable.MaskImage, 0, 0);
		mImageSource = a.getResourceId(R.styleable.MaskImage_image, 0);
		mMaskSource = a.getResourceId(R.styleable.MaskImage_mask, 0);
		mBackgroundSource = a.getResourceId(R.styleable.MaskImage_frame, 0);

		if (mImageSource == 0 || mMaskSource == 0 || mBackgroundSource == 0) {
			mException = new IllegalArgumentException(a.getPositionDescription() + 
					": The content attribute is required and must refer to a valid image.");
		}

		if (mException != null) 
			throw mException;

        Bitmap original = BitmapFactory.decodeResource(getResources(), mImageSource);
        applyMaskInImage(original);

    }

    public void applyMaskInImage(Bitmap original) {
        WindowManager wm = (WindowManager) ResponsaApplication.getAppContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int screenWidth = size.x;
        int width = screenWidth;

        float ratio = (float) original.getHeight() / original.getWidth();
        int height = Math.round(ratio * screenWidth);
        if ((float) height< getResources().getDimension(R.dimen.parallax_image_height)) {
            height = Math.round(getResources().getDimension(R.dimen.parallax_image_height));
            width = Math.round(height / ratio);
        }

        Bitmap originalScaled = Bitmap.createScaledBitmap(original, width, height, true);

        Bitmap mask = BitmapFactory.decodeResource(getResources(), mMaskSource);
        ratio = (float) mask.getHeight() / mask.getWidth();
        height = Math.round(ratio * screenWidth);
        Bitmap maskScaled = Bitmap.createScaledBitmap(mask, screenWidth, height, true);


        Bitmap result = Bitmap.createBitmap(screenWidth, height, Config.ARGB_8888);
        Canvas mCanvas = new Canvas(result);
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        paint.setXfermode(new PorterDuffXfermode(PorterDuff.Mode.DST_IN));
        mCanvas.drawBitmap(originalScaled, 0, 0, null);
        mCanvas.drawBitmap(maskScaled, 0, 0, paint);
        paint.setXfermode(null);
        setImageBitmap(result);
        setScaleType(ScaleType.CENTER);
        setBackgroundResource(mBackgroundSource);

        a.recycle();
    }
}
