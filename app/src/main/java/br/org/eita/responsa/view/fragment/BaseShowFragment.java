package br.org.eita.responsa.view.fragment;


import android.support.v4.app.Fragment;
import android.view.View;

/**
 * Used in show screens
 */
public abstract class BaseShowFragment extends Fragment  {

    //Vinicius: to maintain consistency
    abstract protected void findAllViewsByIds(View parentView);

    abstract protected void populateViewData();

    abstract protected void sweepViewData();
}
