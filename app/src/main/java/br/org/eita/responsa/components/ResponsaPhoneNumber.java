package br.org.eita.responsa.components;

import android.content.Context;
import android.telephony.TelephonyManager;
import android.util.Log;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static android.telephony.PhoneNumberUtils.extractNetworkPortion;
import static android.telephony.PhoneNumberUtils.formatNumber;

public class ResponsaPhoneNumber {
    private String countryCode = "55";
    private String ddd;
    private String phone;

    private Context mContext;

    public ResponsaPhoneNumber(Context context) {
        this.mContext = context;
    }


    public String getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }

    public String getDdd() {
        return ddd;
    }

    public void setDdd(String ddd) {
        Pattern p = Pattern.compile("[0-9]");
        Matcher m = p.matcher(ddd);

        int count=0;
        while (m.find()) {
            count++;
        }

        if (count != 2)
            this.ddd = null;

        this.ddd = ddd;
    }

    public String getPhone() {
        return phone;
    }

    /***
     *
     * @param phone
     * @return boolean phone is valid or not
     */
    public Boolean setPhone(CharSequence phone) {
        Pattern p = Pattern.compile("[0-9]");
        Matcher m = p.matcher(phone);

        String unformattedPhoneNumber = "";

        int count=0;
        while (m.find()) {
            count++;
            unformattedPhoneNumber += m.group(0);
        }

        if (count < 8 || count > 9) {
            this.phone = null;
            return false;
        }

        this.phone = unformattedPhoneNumber;
        return true;
    }


    @Override
    public String toString() {
        if (this.countryCode != null && this.ddd != null && this.phone != null) {
            return this.countryCode + this.ddd + this.phone;
        }
        return null;
    }

    /***
     *
     * @param completePhoneNumber
     * @return boolean success/failure
     */
    public Boolean fromString(String completePhoneNumber) {
        //TODO make for others that are not Brazil
        try {
            this.countryCode = completePhoneNumber.substring(0,2);
            this.ddd = completePhoneNumber.substring(2,4);
            this.phone = completePhoneNumber.substring(4);
            return true;

        } catch (Exception e) {
            this.countryCode = null;
            this.ddd = null;
            this.phone = null;
            return false;
        }
    }

    public String getFormattedPhone() {
        TelephonyManager manager = (TelephonyManager) this.mContext.getSystemService(Context.TELEPHONY_SERVICE);

        //getNetworkCountryIso
        String CountryID = manager.getSimCountryIso().toUpperCase();

        return formatNumber(this.phone,CountryID);
    }

    public String getFormattedPhoneWithAreaCode() {
        if (this.ddd != null && this.phone != null) {
            return "(" + this.ddd + ") " + getFormattedPhone();
        }
        return "";
    }
}
