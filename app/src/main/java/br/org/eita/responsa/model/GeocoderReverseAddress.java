package br.org.eita.responsa.model;

import com.google.gson.annotations.SerializedName;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.backend_comm.GeocoderApi;
import br.org.eita.responsa.location.LocationCoordinates;
import retrofit.Callback;
import retrofit.RestAdapter;

public class GeocoderReverseAddress {

    public LocationCoordinates location;

    public Double lat;

    public Double lng;

    @SerializedName("display_name")
    public String displayName;

    public AddressDetails address;

    public class AddressDetails {
        @SerializedName("house_number")
        public String houseNumber;

        public String road;

        public String suburb;

        public String hamlet;

        public String city;

        public String town;

        @SerializedName("state_district")
        public String stateDistrict;

        public String state;

        public String postcode;

        public String county;

        public String country;

        @SerializedName("country_code")
        public String countryCode;
    }

    public static void reverseGeocode(Double latitude, Double longitude, Callback<GeocoderReverseAddress> cb) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.geocoderUrl)
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .build();
        GeocoderApi geocoderApi = restAdapter.create(GeocoderApi.class);

        geocoderApi.reverse(latitude, longitude, cb);
    }
}
