package br.org.eita.responsa.managers;

import android.content.Context;
import android.content.Intent;

import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.view.activity.ChallengeViewActivity;
import br.org.eita.responsa.view.activity.FichaSinteseActivity;

public class ResponsaViewManager {

    private Context mContext;

    public ResponsaViewManager(Context mContext) {
        this.mContext = mContext;
    }


    public void openFichaSintese(Context context, String viewType) {
        openFichaSintese(context, ResponsaDataManager.NEW_OBJECT_ID, viewType, false);
    }

    public void openFichaSintese(Context context, ResponsaObject responsaObject) {
        openFichaSintese(context, responsaObject.getObjectId(), responsaObject.getType(), false);
    }

    public void openFichaSintese(Context context, ResponsaObject responsaObject, Boolean cheguei) {
        openFichaSintese(context, responsaObject.getObjectId(), responsaObject.getType(), cheguei);
    }

    public void openFichaSintese(Context context, String responsaObjectId, String viewType) {
        openFichaSintese(context, responsaObjectId, viewType, false);
    }

    private void openFichaSintese(Context context, String responsaObjectId, String viewType, Boolean cheguei) {
        Intent intent = new Intent(context, FichaSinteseActivity.class);

        if (responsaObjectId != null) {
            intent.putExtra(FichaSinteseActivity.INTENT_OBJECT_ID_KEY, responsaObjectId);
        }
        intent.putExtra(FichaSinteseActivity.INTENT_VIEW_KEY, viewType);
        intent.putExtra(FichaSinteseActivity.INTENT_CHEGUEI, cheguei);
        context.startActivity(intent);
    }

    public void openChallengeView(Context context) {
        openChallengeView(context, ResponsaDataManager.NEW_OBJECT_ID);
    }

    public void openChallengeView(Context context, Challenge challenge) {
        openChallengeView(context, challenge.getObjectId());
    }

    public void openChallengeView(Context context, String challengeId) {
        Intent intent = new Intent(context, ChallengeViewActivity.class);

        if (challengeId != null) {
            intent.putExtra(ChallengeViewActivity.INTENT_OBJECT_ID_KEY, challengeId);
        }
        context.startActivity(intent);
    }


}
