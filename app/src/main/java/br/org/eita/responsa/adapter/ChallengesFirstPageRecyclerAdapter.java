package br.org.eita.responsa.adapter;


import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.shamanland.fonticon.FontIconTextView;

import java.util.ArrayList;

import br.org.eita.responsa.R;
import br.org.eita.responsa.adapter.data_manager.ChallengeFrontPageData;
import br.org.eita.responsa.components.VotableStatsWidget;
import br.org.eita.responsa.model.Challenge;

public class ChallengesFirstPageRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    LayoutInflater mInflater;
    ChallengeFrontPageData frontPageData;
    ArrayList<Challenge> mChallenges = new ArrayList<Challenge>();
    Context mContext;

    public ChallengesFirstPageRecyclerAdapter(Context context, ChallengeFrontPageData frontPageData) {
        mInflater = LayoutInflater.from(context);
        this.frontPageData = frontPageData;

        mChallenges.clear();
        mChallenges.addAll(frontPageData.challenges);

        //Challenge title = new Challenge();
        //title.layoutTitleViewString = R.string.u_discoveries;
        //this.add(title);

        mChallenges.addAll(frontPageData.discoveries);
        mContext = context;


    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ChallengeViewHolder(mInflater.inflate(R.layout.challenge_item_view, parent, false));
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {

        ChallengeViewHolder viewHolder = (ChallengeViewHolder) holder;

        final Challenge challenge = mChallenges.get(position);

        /*
        String authorLocation = challenge.getAuthor().getString("name");
        if (challenge.getLocationLabel() != null) {
            authorLocation += " " + mContext.getString(R.string.in_location, challenge.getLocationLabel());
        }*/

        viewHolder.challengeTitle.setText(challenge.getTitle());

        //viewHolder.challengeDescription.setText(challenge.getDescription());

        /*
        if (challenge.getCreatedAt() != null) {
            viewHolder.challengeCreatedAt.setText(DateUtils.getRelativeTimeSpanString(ResponsaUtil.getDateCurrentTimeZone(challenge.getCreatedAt().getTime()), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS));
            //challengeCreatedAt.setText(ResponsaUtil.getDateCurrentTimeZone(summary.createdAt.getTime() / 1000));
        } else {
            viewHolder.challengeCreatedAt.setVisibility(View.GONE);
        }
        */
        //viewHolder.challengeAuthor.setText(authorLocation);

        //viewHolder.votableStatsWidget.setVotableAnswerable(challenge);
        /*
        if (challenge.getCorrectAnswer() != null) {
            ResponsaUtil.log("ResponsaDataManager", "desafio tem resposta");
            viewHolder.challengeSelectedAnswer.setText(challenge.getCorrectAnswer().getString("description"));
        } else {
            viewHolder.challengeSelectedAnswer.setVisibility(View.GONE);
        }*/

        /*
        viewHolder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Challenge.currentChallenge = challenge;

                Intent intent = new Intent(mContext, ChallengeViewActivity.class);
                //intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);
                intent.putExtra(ChallengeViewActivity.INTENT_OBJECT_ID_KEY, challenge.getObjectId());
                mContext.startActivity(intent);
            }
        }); */


    }

    @Override
    public int getItemCount() {
        return mChallenges.size();
    }

    static class ChallengeViewHolder extends RecyclerView.ViewHolder {
        TextView challengeTitle;
        TextView challengeCreatedAt;
        TextView challengeDescription;
        TextView challengeAuthor;
        VotableStatsWidget votableStatsWidget;
        FontIconTextView challengeSelectedAnswer;

        public ChallengeViewHolder(View view) {
            super(view);

            challengeTitle = (TextView) view.findViewById(R.id.challengeTitle);
            challengeCreatedAt = (TextView) view.findViewById(R.id.challengeCreatedAt);
            challengeDescription = (TextView) view.findViewById(R.id.challengeDescription);
            challengeAuthor = (TextView) view.findViewById(R.id.challengeAuthor);
            votableStatsWidget = (VotableStatsWidget) view.findViewById(R.id.votableStatsWidget);
            challengeSelectedAnswer = (FontIconTextView) view.findViewById(R.id.challengeSelectedAnswer);

        }
    }

    static class TitleViewHolder extends RecyclerView.ViewHolder {

        public TitleViewHolder(View view) {
            super(view);
        }
    }
}
