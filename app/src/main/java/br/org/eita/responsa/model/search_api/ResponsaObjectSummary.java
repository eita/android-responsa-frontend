package br.org.eita.responsa.model.search_api;

import java.util.Date;

public class ResponsaObjectSummary {
    public String objectId;
    public String title;
    public String description;
    public String label;
    public String type;
    public String avatarImage;
    public Integer countComments;
    public Date dateTimeStart;
    public Date dateTimeFinish;
    public String ownerId;
    public String ownerName;
    public String category;
    public String territoryId;

}
