package br.org.eita.responsa.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.parse.ParseUser;
import com.shamanland.fonticon.FontIconTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.backend_comm.SearchApi;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.InitiativeCategory;
import br.org.eita.responsa.model.search_api.ResponsaObjectSummary;
import br.org.eita.responsa.util.ResponsaUtil;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

//Gets pinned responsa objects near location
public class ResponsaObjectsSearchAjaxAdapter extends ArrayAdapter<ResponsaObjectSummary> {

    RestAdapter restAdapter;
    SearchApi searchApi;
    private OnQueryCompletedCallback onQueryCompleted;

    String currentUserId;

    String responsaObjectType;

    public ResponsaObjectsSearchAjaxAdapter(Context context, String viewType) {
        super(context, 0, new ArrayList<ResponsaObjectSummary>());

        if ("gatheringsFromInitiative".equals(viewType)){
            responsaObjectType = "gathering";
        } else if ("initiativesAndGatherings".equals(viewType)) {
            responsaObjectType = "ResponsaObject";
        }

        restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.searchServiceUrl)
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .build();

        searchApi = restAdapter.create(SearchApi.class);

        currentUserId = ParseUser.getCurrentUser().getObjectId();

    }

    public void setKeywordQuery(final String keywordQuery) {
        final ResponsaObjectsSearchAjaxAdapter self = this;
        searchApi.queryResponsaObjectSummaries(keywordQuery, currentUserId, responsaObjectType, new Callback<List<ResponsaObjectSummary>>() {
            @Override
            public void success(List<ResponsaObjectSummary> responsaObjectSummaries, Response response) {
                self.clear();
                self.addAll(responsaObjectSummaries);
                if (onQueryCompleted != null)
                    onQueryCompleted.onQueryComplete(keywordQuery);
            }

            @Override
            public void failure(RetrofitError error) {

            }
        });
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ResponsaObjectSummary summary = getItem(position);


        if ("initiative".equals(summary.type)) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_initiative, parent, false);
        } else if ("gathering".equals(summary.type)) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_gathering, parent, false);
        }


        TextView titleView = (TextView) convertView.findViewById(R.id.responsaObjectTitle);
        titleView.setText(summary.title);

        if (summary.description != null && !summary.description.isEmpty()) {
            TextView descriptionView = (TextView) convertView.findViewById(R.id.responsaObjectDescription);
            descriptionView.setText(summary.description);
            descriptionView.setVisibility(View.VISIBLE);
        }

        TextView distanceView = (TextView) convertView.findViewById(R.id.responsaObjectDistance);
        distanceView.setVisibility(View.GONE);
        //distanceView.setText(summary.label);

        TextView locationView = (TextView) convertView.findViewById(R.id.responsaObjectLocation);
        HomeTerritory homeTerritory = ResponsaApplication.territoryDb.findTerritoryById(summary.territoryId);

        if (homeTerritory == null) {
            locationView.setVisibility(View.GONE);
        } else {
            locationView.setText(homeTerritory.label);
        }

        if ("gathering".equals(summary.type)) {
            TextView dateView = (TextView) convertView.findViewById(R.id.responsaObjectDate);
            if (summary.dateTimeStart != null) {
                SimpleDateFormat sdf = new SimpleDateFormat("EEE, d 'de' MMMM");
                dateView.setText(sdf.format(summary.dateTimeStart));
            }

            FontIconTextView fsTimeGathering = (FontIconTextView) convertView.findViewById(R.id.fsTimeGathering);
            fsTimeGathering.setText(ResponsaUtil.humanReadableTimeForEvent(summary.dateTimeStart,summary.dateTimeFinish));
        }


        final BootstrapCircleThumbnail thumbnail = (BootstrapCircleThumbnail) convertView.findViewById(R.id.responsaObjectImage);

        if ("gathering".equals(summary.type)) {
            thumbnail.setImage(R.drawable.encontros);
        } else if (InitiativeCategory.FEIRAS.equals(summary.category)) {
            thumbnail.setImage(R.drawable.feiras);
        } else if (InitiativeCategory.GRUPOS_DE_CONSUMO_RESPONSAVEL.equals(summary.category)) {
            thumbnail.setImage(R.drawable.grupos_de_consumo_responsavel);
        } else if (InitiativeCategory.INICIATIVAS_DE_ECONOMIA_SOLIDARIA.equals(summary.category)) {
            thumbnail.setImage(R.drawable.iniciativas_de_economia_solidaria);
        } else if (InitiativeCategory.INICIATIVAS_DE_AGROECOLOGIA.equals(summary.category)) {
            thumbnail.setImage(R.drawable.iniciativas_de_agroecologia);
        } else if (InitiativeCategory.RESTAURANTES.equals(summary.category)) {
            thumbnail.setImage(R.drawable.restaurantes);
        } else {
            thumbnail.setImage(R.drawable.avatar_responsa);
        }

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResponsaApplication.responsaViewManager.openFichaSintese(getContext(), summary.objectId, summary.type);
            }
        });

        return convertView;

    }

    public void setOnQueryCompleted(OnQueryCompletedCallback completed) {
        this.onQueryCompleted = completed;
    }

    public interface OnQueryCompletedCallback {
        public void onQueryComplete(String queryText);
    }


}
