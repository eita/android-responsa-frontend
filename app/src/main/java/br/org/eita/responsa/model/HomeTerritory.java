package br.org.eita.responsa.model;


import android.util.Log;

import com.parse.ParseUser;

import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.util.ResponsaUtil;

public class HomeTerritory {
    public String id;
    public String type;
    public String title;
    public String alias;
    public String parentId;
    public String firstChilds;
    public Double lat;
    public Double lng;
    public String state;

    public String label;

    public LocationCoordinates getLocationCoordinates() {
        LocationCoordinates locationCoordinates = null;

        HomeTerritory loc = this;
        //ResponsaUtil.log("getLocationCoordinates","entered");
        //ResponsaUtil.log("getLocationCoordinates",(loc== null)? "null" : String.format("%s, %s, %f, %f",loc.id, loc.title, loc.lat, loc.lng));
        while (locationCoordinates == null) {
            //ResponsaUtil.log("getLocationCoordinates",(loc== null)? "null" : String.format("%s, %s, %f, %f",loc.id, loc.title, loc.lat, loc.lng));
            if (loc.lat != null && loc.lat != 0D) {
                ResponsaUtil.log("getLocationCoordinates", "a");
                locationCoordinates = new LocationCoordinates();
                locationCoordinates.setLatitude(loc.lat);
                locationCoordinates.setLongitude(loc.lng);
                locationCoordinates.setAccuracy(0F);
                locationCoordinates.setFetchTime(System.currentTimeMillis());
                locationCoordinates.setCoordinateType(LocationCoordinates.CoordinateType.BY_TERRITORY_ID);
                locationCoordinates.setLocation(this);
                break;
            } else if (loc.parentId != null){
                ResponsaUtil.log("getLocationCoordinates", "b");
                loc = ResponsaApplication.territoryDb.findTerritoryById(loc.parentId);
            } else {
                ResponsaUtil.log("getLocationCoordinates", "c");
                return null;
            }
        }

        return locationCoordinates;
    }

    public HomeTerritory getRegion() {
        HomeTerritory loc = this;
        while (loc.id.length() > 4) {
            loc = ResponsaApplication.territoryDb.findTerritoryById(loc.parentId);
        }
        return loc;
    }

    public HomeTerritory getCity() {
        HomeTerritory loc = this;
        if (loc.id.length() < 7) {
            // return empty, since it's a state or region above city level
            loc = new HomeTerritory();
        } else if (loc.id.length()>7) {
            while (!"city".equals(loc.type)) {
                loc = ResponsaApplication.territoryDb.findTerritoryById(loc.parentId);
            }
        }
        return loc;
    }

    //Returns the whole city if the territory_id is the city itself or a subcity territory if not
    public HomeTerritory getTerritoryOrCity() {
        HomeTerritory city = this.getCity();
        return (city.title.equals(title)) ? city : this;

    }

    public String get(String field) {
        if ("label".equals(field)) {
            return label;
        } else if ("title".equals(field)) {
            return title;
        } else if ("alias".equals(field)) {
            return alias;
        } else if ("type".equals(field)) {
            return type;
        }
        return null;
    }

    public static void onUpdateTerritory() {
        ResponsaUtil.log("ResponsaDataManager", "updateLocation");

        String territoryId = ParseUser.getCurrentUser().getString("territoryId");
        HomeTerritory territory = ResponsaApplication.territoryDb.findTerritoryById(territoryId);
        LocationCoordinates.setVisibleLocation(territory.getLocationCoordinates());
        ResponsaApplication.responsaDataManager.clearLocal("challenges",null);
        ResponsaApplication.responsaDataManager.load("challenges", null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        HomeTerritory that = (HomeTerritory) o;

        if (id != null ? !id.equals(that.id) : that.id != null) return false;
        if (type != null ? !type.equals(that.type) : that.type != null) return false;
        if (title != null ? !title.equals(that.title) : that.title != null) return false;
        if (alias != null ? !alias.equals(that.alias) : that.alias != null) return false;
        if (parentId != null ? !parentId.equals(that.parentId) : that.parentId != null)
            return false;
        if (firstChilds != null ? !firstChilds.equals(that.firstChilds) : that.firstChilds != null)
            return false;
        if (lat != null ? !lat.equals(that.lat) : that.lat != null) return false;
        if (lng != null ? !lng.equals(that.lng) : that.lng != null) return false;
        if (state != null ? !state.equals(that.state) : that.state != null) return false;
        return !(label != null ? !label.equals(that.label) : that.label != null);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (type != null ? type.hashCode() : 0);
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (alias != null ? alias.hashCode() : 0);
        result = 31 * result + (parentId != null ? parentId.hashCode() : 0);
        result = 31 * result + (firstChilds != null ? firstChilds.hashCode() : 0);
        result = 31 * result + (lat != null ? lat.hashCode() : 0);
        result = 31 * result + (lng != null ? lng.hashCode() : 0);
        result = 31 * result + (state != null ? state.hashCode() : 0);
        result = 31 * result + (label != null ? label.hashCode() : 0);
        return result;
    }
}
