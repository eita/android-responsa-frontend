package br.org.eita.responsa.model;

public interface Votable {
    /**
     * @param newValue -1, 0 or 1
     */
    public void setVoteState(Integer newValue);

    /**
     * Returns the vote state
     *
     * @return -1, 0 or 1
     */
    public Integer getVoteState();

    /**
     * Returns the upvotes - downvotes
     *
     * @return
     */
    public Integer getRelevance();

    public void onVotableChange(OnVotableChange onVotableChange);

    public boolean getVotesEnabled();

    public Integer getTotalVotes();

    interface OnVotableChange {
        public void onChange(Votable v);
    }
}
