package br.org.eita.responsa.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.org.eita.responsa.R;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TutorialTrajetoriaFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TutorialTrajetoriaFragment extends Fragment {


    public TutorialTrajetoriaFragment() {
        // Required empty public constructor
    }

    public static TutorialTrajetoriaFragment newInstance() {
        TutorialTrajetoriaFragment fragment = new TutorialTrajetoriaFragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.tutorial_trajetoria, container, false);


        return fragmentView;
    }

}
