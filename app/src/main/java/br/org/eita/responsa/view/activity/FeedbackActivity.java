package br.org.eita.responsa.view.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import br.org.eita.responsa.R;
import br.org.eita.responsa.util.ExtraFilters;
import br.org.eita.responsa.view.fragment.ChallengeFormFragment;
import br.org.eita.responsa.view.fragment.FeedbackFormFragment;

public class FeedbackActivity extends BaseActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feedback_form);

        Toolbar rToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(rToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.action_feedback));

        FeedbackFormFragment feedbackFormFragment = new FeedbackFormFragment();

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, feedbackFormFragment)
                .commit();

        String view = null;
        String viewObjectId = null;
        String extraFiltersUniqueId = null;
        String viewClass = null;

        Bundle extras = null;
        Intent intent = getIntent();
        if (intent != null) {
            extras = intent.getExtras();
            if (extras != null) {
                view = extras.getString("view");
                viewObjectId = extras.getString("viewObjectId");
                extraFiltersUniqueId = extras.getString("extraFiltersUniqueId");
                viewClass = extras.getString("viewClass");
            }
        }

        feedbackFormFragment.setData(view, viewObjectId, extraFiltersUniqueId, viewClass);

    }
}
