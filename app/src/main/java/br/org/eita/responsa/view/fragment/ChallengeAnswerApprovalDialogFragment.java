package br.org.eita.responsa.view.fragment;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import br.org.eita.responsa.R;
import br.org.eita.responsa.model.ChallengeAnswer;

public class ChallengeAnswerApprovalDialogFragment extends DialogFragment{

    public static ClickDialogYesCallback clickDialogYesCallback;

    public interface ClickDialogYesCallback {
        public void onClick();
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

        ChallengeAnswer currentChallengeAnswer = ChallengeAnswer.currentChallengeAnswer;

        builder.setTitle(R.string.u_ask_choose_answer_title);

        builder.setMessage(currentChallengeAnswer.getDescription());

        builder.setPositiveButton(R.string.u_ask_choose_answer_dialog_yes, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (clickDialogYesCallback != null) {
                    clickDialogYesCallback.onClick();
                }
            }
        });


        builder.setNegativeButton(R.string.u_ask_choose_answer_dialog_no, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

            }
        });

        return builder.create();
    }
}
