package br.org.eita.responsa.backend_comm;

import br.org.eita.responsa.model.User;
import retrofit.Callback;
import retrofit.http.Body;
import retrofit.http.Field;
import retrofit.http.FormUrlEncoded;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Path;
import retrofit.http.Query;

public interface BackendApi {

    @Headers("Accept: application/json")
    @POST("/users")
    void createUser( @Query("user[uid]") String uid, @Query("user[parse_installation_object_id]") String installationObjectId, @Body String body, Callback<User> cb);

    @Headers("Accept: application/json")
    @POST("/auth/sign_in")
    void confirmUser(@Query("uid") String uid, @Query("confirmation_token") String confirmationCode, @Body String body, Callback<User> cb);
    //void confirmUser(@Query("uid") String uid, @Query("confirmation_token") String confirmationCode, Callback<User> cb);

    @Headers("Accept: application/json")
    @GET("/users/{id}/resend_sms")
    void resendSms( @Path("id") Integer id, Callback<User> cb);


    @Headers("Accept: application/json")
    @PUT("/users/{id}")
    void changeNumber( @Query("user[uid]") String uid, @Path("id") Integer id, @Body String body, Callback<User> cb);

    @Headers("Accept: application/json")
    @GET("/users/wipe_dummy")
    void wipeDummy( @Path("uid") String uid, Callback<User> cb);

    @Headers("Accept: application/json")
    @GET("/users/{id}/sms_status")
    void smsStatus( @Path("id") Integer id, Callback<User> cb);

}
