package br.org.eita.responsa.view.fragment;

import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParsePush;
import com.parse.ParseUser;

import java.util.ArrayList;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.components.TerritoryChooser;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.Notification;
import br.org.eita.responsa.model.util.ResponsaSaveCallback;
import br.org.eita.responsa.model.validation.ValidationError;
import br.org.eita.responsa.view.activity.ChallengeFormActivity;


public class ChallengeFormFragment extends Fragment {

    EditText challengeFormTitleInput;
    EditText challengeFormDescriptionInput;
    TerritoryChooser challengeFormTerritoryInput;
    Button   challengeFormSubmit;

    HomeTerritory homeTerritory;

    Challenge challenge;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_challenge_form, container, false);

        fetchChallenge();

        findAllViewsByIds(view);

        populateForm();

        challengeFormSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final Challenge challenge = sweepData();
                final Boolean isNew = challenge.getCreatedAt() == null;

                ArrayList<ValidationError> errors = new ArrayList<ValidationError>();
                errors.addAll(challenge.validate(getActivity()));
                if (errors.isEmpty()) {
                    if (isNew) {
                        challenge.setPublished(true);
                    }
                    Challenge.currentChallenge = challenge;

                    ChallengeCreateDialogFragment.clickDialogYesCallback = new ChallengeCreateDialogFragment.ClickDialogYesCallback() {
                        @Override
                        public void onClick() {
                            ChallengeCreateDialogFragment.clickDialogYesCallback = null;

                            ResponsaApplication.responsaDataManager.saveObject("challenge", null, challenge, new ResponsaSaveCallback() {
                                @Override
                                public void done(ParseException e) {
                                    if (e == null && isNew) {
                                        Notification.subscribeToChannel(Notification.CHALLENGE_CHANNEL, challenge.getObjectId());
                                        if (challenge.getTerritoryId() != null) {
                                            Notification.sendNotification(ResponsaApplication.getAppContext(), "registerChallengeWithTerritoryId", challenge);
                                        } else {
                                            Notification.sendNotification(ResponsaApplication.getAppContext(), "registerChallengeWithoutTerritoryId", challenge);
                                        }
                                    }
                                }
                            });

                            ResponsaApplication.responsaViewManager.openChallengeView(getActivity());

                            getActivity().finish();
                        }
                    };

                    (new ChallengeCreateDialogFragment()).show(((FragmentActivity) getActivity()).getSupportFragmentManager(), "ChallengeCreateDialogFragment");
                } else {
                    Toast.makeText(getActivity(), getString(R.string.u_please_fill_all_data), Toast.LENGTH_SHORT).show();
                }
            }
        });

        challengeFormTerritoryInput.onChangeSelectedTerritory(new TerritoryChooser.OnChangeTerritoryListener() {
            @Override
            public void onChange(HomeTerritory territory) {
                homeTerritory = territory;
            }
        });

        return view;
    }

    private void fetchChallenge() {
        if (ChallengeFormActivity.transientChallenge == null) {
            challenge = createChallenge();
        } else {
            challenge = ChallengeFormActivity.transientChallenge;
            ChallengeFormActivity.transientChallenge = null;
        }
    }

    private void findAllViewsByIds(View parentView) {
        challengeFormTitleInput = (EditText) parentView.findViewById(R.id.challengeFormTitleInput);
        challengeFormDescriptionInput = (EditText) parentView.findViewById(R.id.challengeFormDescriptionInput);
        challengeFormTerritoryInput = (TerritoryChooser) parentView.findViewById(R.id.challengeFormLocationInput);
        challengeFormSubmit = (Button) parentView.findViewById(R.id.challengeFormSubmit);
    }

    private Challenge sweepData() {

        challenge.setTitle(challengeFormTitleInput.getText().toString());
        challenge.setDescription(challengeFormDescriptionInput.getText().toString());

        //Set location ID
        if (homeTerritory == null) {
            challenge.setTerritoryId(null);
            challenge.setTerritoryRegionId(null);
            challenge.setTerritoryLabel(null);
        } else {
            challenge.setTerritoryId(homeTerritory.id);
            challenge.setTerritoryRegionId(homeTerritory.getRegion().id);
            challenge.setTerritoryLabel(homeTerritory.label);
        }
        return challenge;
    }

    private void populateForm() {
        challengeFormTitleInput.setText(challenge.getTitle());
        challengeFormDescriptionInput.setText(challenge.getDescription());
        if (challenge.getTerritoryId() != null) {
            homeTerritory = ResponsaApplication.territoryDb.findTerritoryById(challenge.getTerritoryId());
            challengeFormTerritoryInput.setSelectedTerritory(homeTerritory);
        }
    }

    private Challenge createChallenge() {
        Challenge chal = new Challenge();

        chal.setAuthor(ParseUser.getCurrentUser());

        chal.setRelevance(0);
        chal.setUpVotes(0);
        chal.setAnswerCount(0);

        return chal;
    }
}
