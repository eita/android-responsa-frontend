package br.org.eita.responsa.components;

import android.content.Context;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.FrameLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.model.InitiativeCategory;

public class CategorySpinner extends FrameLayout {

    Spinner categoryInput;

    ArrayList<InitiativeCategory> inputOptions;

    InitiativeCategory selectedCategory;

    OnSelectCategoryCallback onSelectCategoryCallback = null;

    public CategorySpinner(Context context) {
        this(context, null);
    }

    public CategorySpinner(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CategorySpinner(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.component_category_spinner, this);

        categoryInput = (Spinner) view.findViewById(R.id.categoryInput);

        inputOptions = getObjectList();

        categoryInput.setAdapter(new CategorySpinnerAdapter(getContext(), inputOptions));

        categoryInput.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                selectedCategory = inputOptions.get(i);
                if (onSelectCategoryCallback != null) {
                    onSelectCategoryCallback.onSelect(selectedCategory);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });

    }

    private class CategorySpinnerAdapter extends ArrayAdapter<InitiativeCategory> {

        public CategorySpinnerAdapter(Context context, List<InitiativeCategory> objects) {
            super(context, R.layout.simple_spinner_item, objects);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position,convertView,parent, R.layout.simple_spinner_item);
        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {
            return getCustomView(position,convertView,parent, R.layout.category_spinner_item);
        }

        public View getCustomView(int position, View convertView, ViewGroup parent, int resourceId) {
            final InitiativeCategory category = getItem(position);

            // Check if an existing view is being reused, otherwise inflate the view
            if (convertView == null) {
                convertView = LayoutInflater.from(getContext()).inflate(resourceId, parent, false);
            }

            TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
            textView.setText(category.getTextResource());


            TextView categoryDetail = (TextView) convertView.findViewById(R.id.categoryDetail);
            if (categoryDetail != null) {
                categoryDetail.setText(category.getExplanation());
            }

            return convertView;
        }
    }

    private ArrayList<InitiativeCategory> getObjectList() {
        ArrayList<InitiativeCategory> categories = new ArrayList<InitiativeCategory>();
        categories.add(new InitiativeCategory(null, R.string.select_category, R.string.empty));
        categories.addAll(InitiativeCategory.getCategoryList());
        return categories;
    }

    public void setOnSelectCategory(OnSelectCategoryCallback onSelectCategoryCallback) {
        this.onSelectCategoryCallback = onSelectCategoryCallback;
    }

    public interface OnSelectCategoryCallback {
        public void onSelect(InitiativeCategory initiativeCategory);
    }

}
