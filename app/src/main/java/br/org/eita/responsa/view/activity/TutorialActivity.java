package br.org.eita.responsa.view.activity;

import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;


import com.github.paolorotolo.appintro.AppIntro;
import com.github.paolorotolo.appintro.AppIntroFragment;

import br.org.eita.responsa.model.User;
import br.org.eita.responsa.view.fragment.TutorialDesafiosFragment;
import br.org.eita.responsa.view.fragment.TutorialEncontrosFragment;
import br.org.eita.responsa.view.fragment.TutorialSlide2Fragment;

import br.org.eita.responsa.R;
import br.org.eita.responsa.view.fragment.TutorialTrajetoriaFragment;

public class TutorialActivity extends AppIntro {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        addSlide(AppIntroFragment.newInstance(getString(R.string.tutorial_1_title), getString(R.string.tutorial_1_description), R.drawable.avatar_responsa, R.color.primaryDark));

        addSlide(TutorialSlide2Fragment.newInstance());

        addSlide(TutorialDesafiosFragment.newInstance());

        addSlide(TutorialEncontrosFragment.newInstance());

        addSlide(TutorialTrajetoriaFragment.newInstance());

        setSkipText(getString(R.string.tutorial_skip_text));
        setDoneText(getString(R.string.tutorial_done_text));

        /*
        addSlide(new SimpleSlide.Builder()
                .title(R.string.tutorial_1_title)
                .description(R.string.tutorial_1_description)
                .image(R.drawable.avatar_responsa)
                .background(R.color.primary)
                .backgroundDark(R.color.primaryDark)
                .build());

        addSlide(new FragmentSlide.Builder()
                .background(R.color.pinkLight)
                .backgroundDark(R.color.pink)
                .fragment(TutorialSlide2Fragment.newInstance())
                .build()
                );

        addSlide(new SimpleSlide.Builder()
                .title(R.string.tutorial_2_title)
                .description(R.string.tutorial_2_description)
                .image(R.drawable.listamapa)
                .background(R.color.pinkLight)
                .backgroundDark(R.color.pink)
                .scrollable(true)
                .layout(R.layout.tutorial_slide_2)
                .build());

        addSlide(new SimpleSlide.Builder()
                .description(R.string.tutorial_3_description)
                //.image(R.drawable.lista)
                .background(R.color.primary)
                .backgroundDark(R.color.primaryDark)
                .build());

        addSlide(new SimpleSlide.Builder()
                .description(R.string.tutorial_4_description)
                //.image(R.drawable.image_1)
                .background(R.color.pinkLight)
                .backgroundDark(R.color.pink)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("Slide 3")
                .description("Descrição Slide 2")
                //.image(R.drawable.image_1)
                .background(R.color.pinkLight)
                .backgroundDark(R.color.pink)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("Slide 4")
                .description("Descrição Slide 2")
                //.image(R.drawable.image_1)
                .background(R.color.pinkLight)
                .backgroundDark(R.color.pink)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("Slide 5")
                .description("Descrição Slide 2")
                //.image(R.drawable.image_1)
                .background(R.color.pinkLight)
                .backgroundDark(R.color.pink)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("Slide 6")
                .description("Descrição Slide 2")
                //.image(R.drawable.image_1)
                .background(R.color.pinkLight)
                .backgroundDark(R.color.pink)
                .build());

        addSlide(new SimpleSlide.Builder()
                .title("Slide 7")
                .description("Descrição Slide 2")
                //.image(R.drawable.image_1)
                .background(R.color.pinkLight)
                .backgroundDark(R.color.pink)
                .build());
*/
        //setContentView(R.layout.activity_tutorial);
    }

    @Override
    public void onSkipPressed(Fragment currentFragment) {
        super.onSkipPressed(currentFragment);

        dismissUserAndFinish();
        // Do something when users tap on Skip button.
    }

    @Override
    public void onDonePressed(Fragment currentFragment) {
        super.onDonePressed(currentFragment);

        dismissUserAndFinish();
        // Do something when users tap on Done button.
    }

    @Override
    public void onSlideChanged(@Nullable Fragment oldFragment, @Nullable Fragment newFragment) {
        super.onSlideChanged(oldFragment, newFragment);
        // Do something when the slide changes.
    }

    private void dismissUserAndFinish() {
        User user = User.getCurrentUser(this);
        user.tutorialAppeared = true;
        user.savePreferences();
        finish();
    }
}
