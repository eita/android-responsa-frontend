package br.org.eita.responsa.components;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.List;
import br.org.eita.responsa.R;
import br.org.eita.responsa.model.HomeTerritory;

public class SubCityRegionSpinnerAdapter extends ArrayAdapter<HomeTerritory> {

    public List<HomeTerritory> territories;

    public SubCityRegionSpinnerAdapter(Context context, List<HomeTerritory> objects) {
        super(context, R.layout.simple_spinner_item, objects);
        territories = objects;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position,convertView,parent, R.layout.simple_spinner_item);
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        return getCustomView(position,convertView,parent, R.layout.subcityregion_spinner_item);
    }

    public View getCustomView(int position, View convertView, ViewGroup parent, int resourceId) {
        final HomeTerritory subCityRegion = getItem(position);

        // Check if an existing view is being reused, otherwise inflate the view
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(resourceId, parent, false);
        }

        TextView textView = (TextView) convertView.findViewById(android.R.id.text1);
        textView.setText(subCityRegion.title);

        return convertView;
    }
}