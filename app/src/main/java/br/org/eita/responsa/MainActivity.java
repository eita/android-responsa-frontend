package br.org.eita.responsa;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.graphics.drawable.Drawable;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.os.Bundle;
import android.support.v4.view.MenuItemCompat;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.ImageSpan;
import android.text.util.Linkify;
import android.view.Menu;


import android.support.v4.view.ViewCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.github.ksoichiro.android.observablescrollview.CacheFragmentStatePagerAdapter;
import com.github.ksoichiro.android.observablescrollview.ObservableScrollViewCallbacks;
import com.github.ksoichiro.android.observablescrollview.ScrollState;
import com.github.ksoichiro.android.observablescrollview.Scrollable;

import com.google.common.base.Strings;
import com.mapbox.mapboxsdk.MapboxAccountManager;
import com.mikepenz.fontawesome_typeface_library.FontAwesome;
import com.mikepenz.iconics.IconicsDrawable;
import com.nineoldandroids.view.ViewHelper;

import br.org.eita.responsa.components.ResponsaViewPager;
import br.org.eita.responsa.location.LocationCoordinates;

import br.org.eita.responsa.location.LocationService;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.model.AppLog;
import br.org.eita.responsa.model.Notification;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.model.User;
import br.org.eita.responsa.model.Version;
import br.org.eita.responsa.push.ResponsaPushReceiver;
import br.org.eita.responsa.storage.ResponsaDbHelper;
import br.org.eita.responsa.ui.widget.SlidingTabLayout;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.view.activity.BaseActivity;
import br.org.eita.responsa.view.activity.ChallengeFormActivity;
import br.org.eita.responsa.view.activity.ResponsaObjectFormActivity;
import br.org.eita.responsa.view.activity.SettingsActivity;
import br.org.eita.responsa.view.activity.SignupActivity;
import br.org.eita.responsa.view.activity.TrajectoryActivity;
import br.org.eita.responsa.view.activity.TutorialActivity;
import br.org.eita.responsa.view.activity.UpgradeLockActivity;
import br.org.eita.responsa.view.activity.UserFormActivity;
import br.org.eita.responsa.view.fragment.ArrivalQuestionDialogFragment;
import br.org.eita.responsa.view.fragment.ChallengeListFragment;
import br.org.eita.responsa.view.fragment.FavoritesFragment;
import br.org.eita.responsa.view.fragment.ResponsaObjectListFragment;
import br.org.eita.responsa.view.fragment.NotificationsFragment;

import br.org.eita.responsa.view.fragment.ViewPagerTabListViewFragment;

import com.nineoldandroids.view.ViewPropertyAnimator;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.shamanland.fonticon.FontIconTextView;

import net.i2p.android.ext.floatingactionbutton.FloatingActionButton;
import net.i2p.android.ext.floatingactionbutton.FloatingActionsMenu;

import java.util.List;

import static android.telephony.PhoneNumberUtils.formatNumber;



/**
 * This is an example of ViewPager + SlidingTab + ListView/ScrollView.
 * This example shows how to handle scroll events for several different fragments.
 * <p/>
 * SlidingTabLayout and SlidingTabStrip are from google/iosched:
 * https://github.com/google/iosched
 */
public class MainActivity extends BaseActivity implements ObservableScrollViewCallbacks{

    public static final String  REQUEST_PERMISSION_DIALOG_ACTION = "br.org.eita.responsa.REQUEST_PERMISSION_DIALOG_ACTION";
    public static final int PERMISSIONS_LOCATION = 1;
    public static final int PERMISSIONS_DIAL_PHONE = 2;


    private BroadcastReceiver versionCheckReceiver = null;

    private View mHeaderView;
    private Toolbar mToolbarView;
    private Toolbar searchableToolbar;

    private int mBaseTranslationY;
    private ViewPager mPager;
    private NavigationAdapter mPagerAdapter;

    public FloatingActionsMenu floatingActionsMenu;
    public FloatingActionButton mapViewToggleButton;
    public int mapViewToggleButtonVisibility = View.VISIBLE;

    private View overlayView;

    private ResponsaDbHelper rDbHelper;

    private BroadcastReceiver chegueiDetectReceiver;
    Boolean chegueiDetectIsRegistered = false;

    private BroadcastReceiver requestPermissionsReceiver;
    Boolean requestPermissionsIsRegistered = false;

    private  ResponsaViewPager responsaViewPager;

    public ResponsaObjectListFragment responsaObjectListFragment = null;

    public static TextView stepsTextView;
    public static FontIconTextView stepsIcon;

    static int fabmapBottomMargin = 0;
    static int fabBottomMargin = 0;

    public static class State {
        public final static State BROWSE = new State();
        public final static State SEARCH = new State();
    }

    public static State currentState = State.BROWSE;

    public Boolean mapShown = false;
    public Boolean mapViewCentered = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        /*
        ParseUser user = ParseUser.getCurrentUser();
        user.put("territoryId","");
        user.saveInBackground(); */




        final MainActivity self = this;

        MapboxAccountManager.start(this, Constants.MAPBOX_SECRET_TOKEN);

        setContentView(R.layout.activity_main);

        setSupportActionBar((Toolbar) findViewById(R.id.toolbar));

        mHeaderView = findViewById(R.id.header);
        ViewCompat.setElevation(mHeaderView, getResources().getDimension(R.dimen.toolbar_elevation));
        mToolbarView = (Toolbar) findViewById(R.id.toolbar);

        searchableToolbar = (Toolbar) findViewById(R.id.searchableToolbar);

        mPagerAdapter = new NavigationAdapter(getSupportFragmentManager());
        mPager = (ViewPager) findViewById(R.id.pager);


        responsaViewPager = (ResponsaViewPager) mPager;
        mPager.setAdapter(mPagerAdapter);

        PageListener pageListener = new PageListener();
        pageListener.setActivity(this);
        mPager.addOnPageChangeListener((ViewPager.OnPageChangeListener) pageListener);


        rDbHelper = new ResponsaDbHelper(getApplicationContext());
        LocationCoordinates.requestLocationUpdate(false);

        SlidingTabLayout slidingTabLayout = (SlidingTabLayout) findViewById(R.id.sliding_tabs);
        slidingTabLayout.setCustomTabView(R.layout.custom_tab, 0);
        slidingTabLayout.setSelectedIndicatorColors(getResources().getColor(R.color.accent));
        slidingTabLayout.setDistributeEvenly(true);
        slidingTabLayout.setViewPager(mPager);

        propagateToolbarState(toolbarIsShown());

        chegueiDetectReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                openAskUserArrivedDialog();
            }
        };

        requestPermissionsReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Integer requestCode = intent.getIntExtra("requestCode",0);
                if (requestCode == 0) return;
                ActivityCompat.requestPermissions(self, intent.getStringArrayExtra("permissionsRequested"),requestCode);
            }
        };

        overlayView = this.findViewById(R.id.full_overlay);
        floatingActionsMenu = (FloatingActionsMenu) findViewById(R.id.floating_action_button);
        mapViewToggleButton = (FloatingActionButton) findViewById(R.id.mapview_toggle);

        fabmapBottomMargin = ((RelativeLayout.LayoutParams) mapViewToggleButton.getLayoutParams()).bottomMargin;

        fabBottomMargin = ((RelativeLayout.LayoutParams) floatingActionsMenu.getLayoutParams()).bottomMargin;


        floatingActionsMenu.setOnFloatingActionsMenuUpdateListener(new FloatingActionsMenu.OnFloatingActionsMenuUpdateListener() {
            @Override
            public void onMenuExpanded() {
                overlayView.setVisibility(View.VISIBLE);
                overlayView.setAlpha(0.0f);
                overlayView.animate()
                        .alpha(1.0f);
                mapViewToggleButtonVisibility = mapViewToggleButton.getVisibility();
                mapViewToggleButton.setVisibility(View.GONE);
            }

            @Override
            public void onMenuCollapsed() {
                overlayView.setVisibility(View.GONE);
                mapViewToggleButton.setVisibility(mapViewToggleButtonVisibility);
                /*overlayView.animate()
                    .alpha(0.0f)
                    .setListener(new AnimatorListenerAdapter() {
                        @Override
                        public void onAnimationEnd(Animator animation) {
                            super.onAnimationEnd(animation);
                            overlayView.setVisibility(View.GONE);
                        }
                    });*/
            }
        });
        overlayView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                floatingActionsMenu.collapse();
            }
        });

        //appTabs.showFloatingActionButton(this, 0);

        if (userInitialize()) {
            migrateApp();
            if (runTutorialFirstTime()) {
                checkForExpiration();
            }
        }

        LocationService.restartLocationService(this);



        //Adds current user to administrator role
        /*
        ParseQuery<ParseRole> query = new ParseQuery(ParseRole.class);

        query.whereEqualTo("name","Administrator");

        query.getFirstInBackground(new GetCallback<ParseRole>() {
            @Override
            public void done(final ParseRole parseRole, ParseException e) {

                if (e == null) {
                    ParseQuery<ParseUser> uq = new ParseQuery(ParseUser.class);

                    List<String> adminPhones = Arrays.asList("5535998664571","5535991588784","553598582890","5511981215188","5511995027947");

                    uq.whereContainedIn("phone",adminPhones);

                    uq.findInBackground(new FindCallback<ParseUser>() {
                        @Override
                        public void done(List<ParseUser> list, ParseException e) {
                            if (e == null) {
                                for (ParseUser u : list) {
                                    parseRole.getUsers().add(u);
                                }
                                parseRole.saveInBackground();
                            }
                        }
                    });


                }
            }
        });
        */
    }


    public static final String LAST_VERSION = "LAST_VERSION";
    private void migrateApp() {

        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        SharedPreferences.Editor editor = prefs.edit();
        Integer actualVersion = 0;

        //Gets actual version
        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            actualVersion = pInfo.versionCode;
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }

        final Context mContext = this;
        int last = prefs.getInt(LAST_VERSION, 0);
        if (last < actualVersion) {

            if (last <= 37 && actualVersion > 37) {
                //Version 37 had an installation problem, so that current user was not registered
                ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                ParseUser currentUser = ParseUser.getCurrentUser();
                if (currentUser != null) {
                    installation.put("user",currentUser);
                    installation.saveInBackground(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            /*
                            String msg = "MAC ok!";
                            if (e != null) {
                                msg = e.getMessage();
                            }
                            Toast.makeText(mContext,msg,Toast.LENGTH_LONG).show();
                            */

                        }
                    });
                    String territoryId = currentUser.getString("territoryId");
                    if (territoryId != null) {
                        Notification.subscribeToChannel(Notification.TERRITORY_CHANNEL, territoryId);
                    }
                }

            }
            //Parse endpoint change to sashido - must relogin user
            if (last <= 46 && actualVersion > 46) {
                //ResponsaApplication.currentUser.parseLogout();
                ResponsaApplication.currentUser.parseLogInBackground(ResponsaApplication.currentUser.getFirstUserSetupCallback(null,null));
            }

            editor.putInt(LAST_VERSION, actualVersion);
            editor.commit();
        }
    }

    @Override
    protected void onDestroy() {
        if (versionCheckReceiver != null) {
            unregisterReceiver(versionCheckReceiver);
        }
        super.onDestroy();
    }

    public ResponsaViewPager getResponsaViewPager() {
        return responsaViewPager;
    }

    /**
     * Init preferences and signup activity, if user is not logged in
     * @return true if user is already logged in
     */
    private Boolean userInitialize() {
        //Set default preferences
        PreferenceManager.setDefaultValues(this, R.xml.settings, true);

        if (ResponsaApplication.currentUser.appToken == null || ParseUser.getCurrentUser() == null) {
            Intent intent = new Intent(this, SignupActivity.class);
            startActivity(intent);
            finish();
            return false;
        } else if (Strings.isNullOrEmpty(ParseUser.getCurrentUser().getString("territoryId")) || Strings.isNullOrEmpty(ParseUser.getCurrentUser().getString("name"))){
            Intent intent = new Intent(this, UserFormActivity.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
            startActivity(intent);
            return false;
        }
        return true;
    }

    /**
     * Runs tutorial at a first time, if it had not run already
     * @return true if tutorial runned before
     */
    private Boolean runTutorialFirstTime() {
        User user = User.getCurrentUser(this);
        if (!user.tutorialAppeared) {
            Intent intent = new Intent(this, TutorialActivity.class);
            startActivity(intent);
            return false;
        }
        return true;
    }

    private void checkForExpiration() {
        PackageInfo pInfo = null;
        try {
            //Registers receiver

            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            final String versionName = pInfo.versionName;

            final MainActivity self = this;

            versionCheckReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    List<Version> versions = ResponsaApplication.responsaDataManager.retrieveLoadedData("version", versionName);
                    if (versions.size() == 0) return;
                    Version ver = versions.get(0);

                    if (ver.getExpires() != null) {
                        Intent intent1 = new Intent(self, UpgradeLockActivity.class);
                        startActivity(intent1);
                    }
                }
            };

            registerReceiver(versionCheckReceiver, new IntentFilter(ResponsaDataManager.getIntentDataUpdatedName("version")));


            ResponsaApplication.responsaDataManager.load("version",versionName);



        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }
    }

    private void openAskUserArrivedDialog() {

        (new ArrivalQuestionDialogFragment()).show(getSupportFragmentManager(), "AriivalQuestionDialogFragment");
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (!chegueiDetectIsRegistered) {
            registerReceiver(chegueiDetectReceiver, new IntentFilter(ResponsaObject.TRIGGER_NEAREST_ALERT));
            chegueiDetectIsRegistered = true;
        }

        if (!requestPermissionsIsRegistered) {
            registerReceiver(requestPermissionsReceiver, new IntentFilter(REQUEST_PERMISSION_DIALOG_ACTION));
            requestPermissionsIsRegistered = true;
        }

        floatingActionsMenu.collapse();
        //Zera o numero de notificacoes pendentes de leitura
        ResponsaPushReceiver.unreadNotificationCount = 0;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        switch (requestCode) {
            case PERMISSIONS_LOCATION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    LocationService.restartLocationService(this);
                    //LocationCoordinates.requestLocationUpdate(true);
                }
                break;
            case PERMISSIONS_DIAL_PHONE:
                break;
        }

        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onPause() {
        super.onPause();

        if (chegueiDetectIsRegistered) {
            unregisterReceiver(chegueiDetectReceiver);
            chegueiDetectIsRegistered = false;
        }

        if (requestPermissionsIsRegistered) {
            unregisterReceiver(requestPermissionsReceiver);
            requestPermissionsIsRegistered = false;
        }

        ResponsaApplication.responsaDataManager.saveLastFetchedAt();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        if (extras != null) {
            String dialogMessage = extras.getString("dialogMessage");
            String dialogTitle = extras.getString("dialogTitle");

            if (dialogMessage != null)  {

                // custom dialog
                final Dialog dialog = new Dialog(this);
                dialog.setContentView(R.layout.custom_dialog);
                dialog.setTitle(dialogTitle != null ? dialogTitle : "Mensagem");

                // set the custom dialog components - text, image and button
                TextView text = (TextView) dialog.findViewById(R.id.dialogMessage);
                text.setText(dialogMessage);

                dialog.show();
            }
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        mToolbarView.inflateMenu(R.menu.menu_main);

        if (ParseUser.getCurrentUser() != null) {
            MenuItem item = menu.findItem(R.id.badge);
            MenuItemCompat.setActionView(item, R.layout.badge_steps);
            //notifCount = (Button) MenuItemCompat.getActionView(item);
            RelativeLayout badgeLayout = (RelativeLayout) MenuItemCompat.getActionView(item); //menu.findItem(R.id.badge).getActionView();
            final MainActivity self = this;
            badgeLayout.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(self, TrajectoryActivity.class);
                    startActivity(intent);
                }
            });

            stepsTextView = (TextView) badgeLayout.findViewById(R.id.actionbar_steps_number);
            stepsIcon = (FontIconTextView) badgeLayout.findViewById(R.id.actionbar_steps_icon);
            if (ParseUser.getCurrentUser().getInt("steps") > 0) {
                stepsTextView.setText(ResponsaUtil.humanFormatInteger(ParseUser.getCurrentUser().getInt("steps")));
                //stepsTextView.setText(ResponsaUtil.humanFormatInteger(10350));
            }
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            Intent intent = new Intent(this, SettingsActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_my_address) {
            Intent intent = new Intent(this, UserFormActivity.class);
            startActivity(intent);
            return true;
        } else if (id == R.id.action_help) {
            Intent intent = new Intent(this, TutorialActivity.class);
            startActivity(intent);
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onScrollChanged(int scrollY, boolean firstScroll, boolean dragging) {
        /*
        if (dragging) {
            int toolbarHeight = mToolbarView.getHeight();
            float currentHeaderTranslationY = ViewHelper.getTranslationY(mHeaderView);
            if (firstScroll) {
                if (-toolbarHeight < currentHeaderTranslationY) {
                    mBaseTranslationY = scrollY;
                }
            }
            float headerTranslationY = ScrollUtils.getFloat(-(scrollY - mBaseTranslationY), -toolbarHeight, 0);
            ViewPropertyAnimator.animate(mHeaderView).cancel();
            ViewHelper.setTranslationY(mHeaderView, headerTranslationY);
        }
        */
    }

    @Override
    public void onDownMotionEvent() {
    }

    @Override
    public void onUpOrCancelMotionEvent(ScrollState scrollState) {
        ResponsaUtil.log("ObservableListView", "onUpOrCancelMotionEvent");
        mBaseTranslationY = 0;

        Fragment fragment = getCurrentFragment();
        if (fragment == null) {
            return;
        }
        View view = fragment.getView();
        if (view == null) {
            return;
        }
        if (currentState.equals(State.SEARCH)) {
            return;
        }

        // ObservableXxxViews have same API
        // but currently they don't have any common interfaces.
        adjustToolbar(scrollState, view);
    }

    private void adjustToolbar(ScrollState scrollState, View view) {
        int toolbarHeight = mToolbarView.getHeight();
        final Scrollable scrollView = (Scrollable) view.findViewById(R.id.scroll);
        if (scrollView == null) {
            return;
        }
        int scrollY = scrollView.getCurrentScrollY();
        if (scrollState == ScrollState.DOWN) {
            showToolbar();
        } else if (scrollState == ScrollState.UP) {
            if (toolbarHeight <= scrollY) {
                hideToolbar();
            } else {
                showToolbar();
            }
        } else {
            // Even if onScrollChanged occurs without scrollY changing, toolbar should be adjusted
            if (toolbarIsShown() || toolbarIsHidden()) {
                // Toolbar is completely moved, so just keep its state
                // and propagate it to other pages
                propagateToolbarState(toolbarIsShown());
            } else {
                // Toolbar is moving but doesn't know which to move:
                // you can change this to hideToolbar()
                showToolbar();
            }
        }
    }

    private Fragment getCurrentFragment() {
        return mPagerAdapter.getItemAt(mPager.getCurrentItem());
    }

    private void propagateToolbarState(boolean isShown) {
        int toolbarHeight = mToolbarView.getHeight();

        // Set scrollY for the fragments that are not created yet
        mPagerAdapter.setScrollY(isShown ? 0 : toolbarHeight);

        // Set scrollY for the active fragments
        for (int i = 0; i < mPagerAdapter.getCount(); i++) {
            // Skip current item
            if (i == mPager.getCurrentItem()) {
                continue;
            }

            // Skip destroyed or not created item
            Fragment f = mPagerAdapter.getItemAt(i);
            if (f == null) {
                continue;
            }

            View view = f.getView();
            if (view == null) {
                continue;
            }
            propagateToolbarState(isShown, view, toolbarHeight);
        }
    }

    private void propagateToolbarState(boolean isShown, View view, int toolbarHeight) {
        Scrollable scrollView = (Scrollable) view.findViewById(R.id.scroll);
        if (scrollView == null) {
            return;
        }
        if (isShown) {
            // Scroll up
            if (0 < scrollView.getCurrentScrollY()) {
                scrollView.scrollVerticallyTo(0);
            }
        } else {
            // Scroll down (to hide padding)
            if (scrollView.getCurrentScrollY() < toolbarHeight) {
                scrollView.scrollVerticallyTo(toolbarHeight);
            }
        }
    }

    private boolean toolbarIsShown() {
        return ViewHelper.getTranslationY(mHeaderView) == 0;
    }

    private boolean toolbarIsHidden() {
        return ViewHelper.getTranslationY(mHeaderView) == -mToolbarView.getHeight();
    }

    private void showToolbar() {
        float headerTranslationY = ViewHelper.getTranslationY(mHeaderView);
        if (headerTranslationY != 0) {
            ViewPropertyAnimator.animate(mHeaderView).cancel();
            ViewPropertyAnimator.animate(mHeaderView).translationY(0).setDuration(200).start();
        }
        propagateToolbarState(true);
    }

    private void hideToolbar() {
        float headerTranslationY = ViewHelper.getTranslationY(mHeaderView);
        int toolbarHeight = mToolbarView.getHeight();
        if (headerTranslationY != -toolbarHeight) {
            ViewPropertyAnimator.animate(mHeaderView).cancel();
            ViewPropertyAnimator.animate(mHeaderView).translationY(-toolbarHeight).setDuration(200).start();
        }
        propagateToolbarState(false);
    }

    private final int[] PAGE_ICONS = new int[]{
            R.drawable.ic_action_notificacoes,
            R.drawable.ic_action_local,
            R.drawable.ic_action_desafios,
            R.drawable.ic_action_favorito_aberto
    };
    private final String[] PAGE_TITLES = new String[]{
            "notifications",
            "initiativesAndGatherings",
            "challenges",
            "favorites"
    };

    /**
     * Basic Definitions of the main app tabs
     */
    public static class appTabs {
        private static final int[] PAGE_ICONS = new int[]{
                R.drawable.ic_action_notificacoes,
                R.drawable.ic_action_local,
                R.drawable.ic_action_desafios,
                R.drawable.ic_action_favorito_aberto
        };
        private static final String[] PAGE_TITLES = new String[]{
                "notifications",
                "initiativesAndGatherings",
                "challenges",
                "favorites"
        };

        public static Fragment getFragment(Integer position, Integer mScrollY) {
            Fragment f;
            User user = ResponsaApplication.currentUser;

            if (position == 0) {
                f = new NotificationsFragment();

                if (0 < mScrollY) {
                    Bundle args = new Bundle();
                    args.putInt(NotificationsFragment.ARG_INITIAL_POSITION, 1);
                    f.setArguments(args);
                }
            } else if (position == 1) {
                f = new ResponsaObjectListFragment();
                Bundle args = new Bundle();
                args.putString("view", "initiativesAndGatherings");
                f.setArguments(args);
            } else if (position == 2) {
                //f = new ChallengesRecyclerFragment();
                f = new ChallengeListFragment();
            } else if (position == 3) {
                f = new FavoritesFragment();
                Bundle args = new Bundle();
                args.putString("view", "favorites");
                f.setArguments(args);
            } else { //Gatherings
                f = new ViewPagerTabListViewFragment();
                if (0 < mScrollY) {
                    Bundle args = new Bundle();
                    args.putInt(ViewPagerTabListViewFragment.ARG_INITIAL_POSITION, 1);
                    f.setArguments(args);
                }
            }

            return f;
        }
        public static String getViewName(Integer position) {
            return PAGE_TITLES[position];
        }
        public static Integer getIcon(Integer position) {
            return PAGE_ICONS[position];
        }
        public static Integer getCount() {
            return PAGE_TITLES.length;
        }
        public static void showFloatingActionButton(MainActivity activity, Integer position) {
            String viewName = getViewName(position);
            View floatingActionsMenuView = activity.findViewById(R.id.floating_action_button);
            View mapViewToggleView = activity.findViewById(R.id.mapview_toggle);

            int buttonHeight = mapViewToggleView.getHeight();

            if (!"initiativesAndGatherings".equals(viewName)) {

                mapViewToggleView.animate()
                        .translationY(buttonHeight +  fabmapBottomMargin);
                        //.setInterpolator(new LinearInterpolator())
                        //.start();
            } else {
                mapViewToggleView.animate()
                        .translationY(0);
                        //.setInterpolator(new LinearInterpolator())
                        //.start();
            }

            if ("notifications".equals(viewName) || "favorites".equals(viewName)) {
                floatingActionsMenuView.animate()
                        .translationY(buttonHeight + fabBottomMargin);
                        //.setInterpolator(new LinearInterpolator())
                        //.start();
                return;
            }


            if ("initiativesAndGatherings".equals(viewName)) {
                activity.findViewById(R.id.newInitiativeBtn).setVisibility(View.VISIBLE);
                activity.findViewById(R.id.newGatheringBtn).setVisibility(View.VISIBLE);
                activity.findViewById(R.id.newChallengeBtn).setVisibility(View.GONE);
                activity.findViewById(R.id.showDiscoveries).setVisibility(View.GONE);
                activity.findViewById(R.id.showChallenges).setVisibility(View.GONE);
                activity.findViewById(R.id.showChallengesAndDiscoveries).setVisibility(View.GONE);
                activity.findViewById(R.id.filterCategories).setVisibility(View.VISIBLE);
                activity.findViewById(R.id.filterbyTerritory).setVisibility(View.VISIBLE);
            } else if ("challenges".equals(viewName)) {
                activity.findViewById(R.id.newInitiativeBtn).setVisibility(View.GONE);
                activity.findViewById(R.id.newGatheringBtn).setVisibility(View.GONE);
                activity.findViewById(R.id.newChallengeBtn).setVisibility(View.VISIBLE);
                activity.findViewById(R.id.showDiscoveries).setVisibility(View.VISIBLE);
                activity.findViewById(R.id.showChallenges).setVisibility(View.VISIBLE);
                activity.findViewById(R.id.filterCategories).setVisibility(View.GONE);
                activity.findViewById(R.id.filterbyTerritory).setVisibility(View.GONE);
            }
            activity.floatingActionsMenu.collapse();
            floatingActionsMenuView.animate()
                    .translationY(0);
                    //.setInterpolator(new LinearInterpolator())
                    //.start();
        }
    }


    /**
     * This adapter provides two types of fragments as an example.
     * {@linkplain #createItem(int)} should be modified if you use this example for your app.
     */
    private class NavigationAdapter extends CacheFragmentStatePagerAdapter {

        private int mScrollY;

        public NavigationAdapter(FragmentManager fm) {
            super(fm);
        }

        public void setScrollY(int scrollY) {
            mScrollY = scrollY;
        }

        @Override
        protected Fragment createItem(int position) {
            return appTabs.getFragment(position, mScrollY);
        }

        @Override
        public int getCount() {
            return appTabs.getCount();
        }

        //http://stackoverflow.com/questions/23472759/add-icons-to-slidingtablayout-instead-of-text
        @Override
        public CharSequence getPageTitle(int position) {
            Drawable image = getResources().getDrawable(appTabs.getIcon(position));
            image.setBounds(0, 0, image.getIntrinsicWidth(), image.getIntrinsicHeight());
            SpannableString sb = new SpannableString(" ");
            ImageSpan imageSpan = new ImageSpan(image, ImageSpan.ALIGN_BOTTOM);
            sb.setSpan(imageSpan, 0, 1, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
            return sb;
        }
    }

    private class PageListener extends ViewPager.SimpleOnPageChangeListener {
        MainActivity activity;

        public void setActivity(MainActivity activity) {
            this.activity = activity;
        }

        @Override
        public void onPageSelected(int position) {
            showToolbar();
            appTabs.showFloatingActionButton(activity, position);
        }
    }

    public void openChallengeForm(View view) {
        floatingActionsMenu.collapse();

        Intent intent = new Intent(this, ChallengeFormActivity.class);
        startActivity(intent);
    }

    public void openInitiativeForm(View view) {
        floatingActionsMenu.collapse();

        Intent intent = new Intent(this, ResponsaObjectFormActivity.class);
        intent.putExtra("type","initiative");
        startActivity(intent);
    }

    public void openGatheringForm(View view) {
        floatingActionsMenu.collapse();

        Intent intent = new Intent(this, ResponsaObjectFormActivity.class);
        intent.putExtra("type", "gathering");
        startActivity(intent);
    }

    public void setState(State state) {
        if (state == State.SEARCH) {
            ResponsaUtil.log("MainActivityState","search");
            if (responsaViewPager != null) {
                responsaViewPager.lockSwipe();
                findViewById(R.id.sliding_tabs).setVisibility(View.GONE);
            }
            //Esconde as opções de menu (mapa) e filtros na busca
            if (mapViewToggleButton != null) {
                mapViewToggleButton.setVisibility(View.GONE);
                this.findViewById(R.id.filterCategories).setVisibility(View.GONE);
                this.findViewById(R.id.filterbyTerritory).setVisibility(View.GONE);
            }
        } else { //if (state == State.BROWSE)
            ResponsaUtil.log("MainActivityState","browse");
            if (responsaViewPager != null) {
                responsaViewPager.unlockSwipe();
                findViewById(R.id.sliding_tabs).setVisibility(View.VISIBLE);
            }
            //Mostra botão de mapa e filtros de categoria na navegação
            if (mapViewToggleButton != null) {
                mapViewToggleButton.setVisibility(View.VISIBLE);
                this.findViewById(R.id.filterCategories).setVisibility(View.VISIBLE);
                this.findViewById(R.id.filterbyTerritory).setVisibility(View.VISIBLE);
            }
        }
        currentState = state;
    }

    public void toggleMap(View view) {
        mapShown = !mapShown;
        if (responsaObjectListFragment != null) {
           responsaObjectListFragment.setMapShown(mapShown);
        }
        if (mapShown) {
            //this.findViewById(R.id.filterCategories).setVisibility(View.GONE);
            this.findViewById(R.id.filterbyTerritory).setVisibility(View.GONE);
            Drawable d = new IconicsDrawable(this)
                    .icon(FontAwesome.Icon.faw_list)
                    .color(getResources().getColor(R.color.white));
            mapViewToggleButton.setIconDrawable(d);
        } else {
            //this.findViewById(R.id.filterCategories).setVisibility(View.VISIBLE);
            this.findViewById(R.id.filterbyTerritory).setVisibility(View.VISIBLE);
            mapViewToggleButton.setIcon(R.drawable.ic_action_distancia);
        }
    }
}
