package br.org.eita.responsa.components;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;


//Context must implement scrollMap
public class MapScrollView extends ScrollView {
    public MapScrollView(Context context) {
        super(context);
    }

    public MapScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public MapScrollView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @Override
    protected void onScrollChanged(int l, int t, int oldl, int oldt) {
        super.onScrollChanged(l, t, oldl, oldt);
    }

    interface mapScrollHolder {
        public void scrollMap(int x, int y);
    }

}
