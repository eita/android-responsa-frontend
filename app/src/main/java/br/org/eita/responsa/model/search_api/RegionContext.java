package br.org.eita.responsa.model.search_api;


import android.content.Context;

import com.google.gson.annotations.SerializedName;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.HomeTerritory;

public class RegionContext {

    @SerializedName("initiatives_in_location")
    public Integer initiativesInLocation = 0;

    @SerializedName("people_in_location")
    public Integer peopleInLocation = 0;

    @SerializedName("initiatives_in_region")
    public Integer initiativesInRegion = 0;

    @SerializedName("people_in_region")
    public Integer peopleInRegion = 0;

    @SerializedName("initiatives_in_city")
    public Integer initiativesInCity = 0;

    @SerializedName("people_in_city")
    public Integer peopleInCity = 0;

    //FIXME put values in configuration file
    public Integer calculateSteps(int stepsOwner) {
        Integer result = stepsOwner;
        result += initiativesInLocation * 10;
        result += peopleInLocation * 5;
        result += initiativesInCity * 5;
        result += peopleInCity * 2;
        result += initiativesInRegion * 2;
        result += peopleInRegion * 1;
        return result;
    }

    public String getSignupShortMessage(Context context, HomeTerritory location, Integer stepsOwner) {
        StringBuffer resultMsg = new StringBuffer();

        HomeTerritory city   = ResponsaApplication.territoryDb.findTerritoryById(location.parentId);
        HomeTerritory region = ResponsaApplication.territoryDb.findTerritoryById(city.parentId);

        resultMsg.append(context.getString(R.string.n_signup_shortMessage, stepsOwner));

        Integer typesInitiatives = ((initiativesInRegion == 0) ? 0 : 1) + ((initiativesInLocation == 0) ? 0 : 1) + ((initiativesInCity == 0) ? 0 : 1);
        Integer typesPeople = ((peopleInRegion == 0) ? 0 : 1) + ((peopleInLocation == 0) ? 0 : 1) + ((peopleInCity == 0) ? 0 : 1);

        Boolean locFilled = false;

        //types Initiatives
        if (typesInitiatives == 1) {
            if (initiativesInLocation != 0) {
                resultMsg.append(context.getString(R.string.n_signup_initiatives_singular, initiativesInLocation, location.title));
            } else if (initiativesInCity != 0) {
                resultMsg.append(context.getString(R.string.n_signup_initiatives_singular, initiativesInCity, city.title));
            } else if (initiativesInRegion != 0) {
                resultMsg.append(context.getString(R.string.n_signup_initiatives_singular, initiativesInRegion, region.title));
            }
        } else if (typesInitiatives > 1) {
            resultMsg.append(context.getString(R.string.n_signup_initiatives, initiativesInRegion + initiativesInCity + initiativesInLocation));

            if (initiativesInLocation != 0) {
                locFilled = true;
                resultMsg.append(" ");
                resultMsg.append(context.getString(R.string.n_signup_count_in_place, initiativesInLocation, location.title));
            }

            if (initiativesInCity != 0) {
                if (locFilled) resultMsg.append((initiativesInRegion != 0) ? "," : " e");
                resultMsg.append(" ");
                resultMsg.append(context.getString(R.string.n_signup_count_in_place, initiativesInCity, city.title));
            }

            if (initiativesInRegion != 0) {
                resultMsg.append(" e ");
                resultMsg.append(context.getString(R.string.n_signup_count_in_place, initiativesInRegion, region.title));
            }
        }

        locFilled = false;

        if (typesPeople > 0 && typesInitiatives > 0) {
            resultMsg.append(context.getString(R.string.n_signup_other_things));
            resultMsg.append(" ");
        }

        //types People
        if (typesPeople == 1) {
            if (peopleInLocation != 0) {
                resultMsg.append(context.getString(R.string.n_signup_people_singular, peopleInLocation, location.title));
            } else if (peopleInCity != 0) {
                resultMsg.append(context.getString(R.string.n_signup_people_singular, peopleInCity, city.title));
            } else if (peopleInRegion != 0) {
                resultMsg.append(context.getString(R.string.n_signup_people_singular, peopleInRegion, region.title));
            }
        } else if (typesPeople > 1) {
            resultMsg.append(context.getString(R.string.n_signup_people, peopleInRegion + peopleInCity + peopleInLocation));

            if (peopleInLocation != 0) {
                locFilled = true;
                resultMsg.append(" ");
                resultMsg.append(context.getString(R.string.n_signup_count_in_place, peopleInLocation, location.title));
            }

            if (peopleInCity != 0) {
                if (locFilled) resultMsg.append((peopleInRegion != 0) ? "," : " e");
                resultMsg.append(" ");
                resultMsg.append(context.getString(R.string.n_signup_count_in_place, peopleInCity, city.title));
            }

            if (peopleInRegion != 0) {
                resultMsg.append(" e ");
                resultMsg.append(context.getString(R.string.n_signup_count_in_place, peopleInRegion, region.title));
            }
        }

        resultMsg.append(".");


        return resultMsg.toString();
    }
}
