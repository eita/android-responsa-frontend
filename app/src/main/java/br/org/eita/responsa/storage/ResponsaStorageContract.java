package br.org.eita.responsa.storage;

import android.provider.BaseColumns;

public final class ResponsaStorageContract extends BaseStorage {

    public ResponsaStorageContract() {}

    /* Inner class that defines the table contents */
    public static abstract class Notification implements BaseColumns {
        public static final String TABLE_NAME = "notification";
        //public static final String COLUMN_NAME_NOTIFICATION_ID = "id";
        public static final String COLUMN_NAME_MESSAGE_ID = "message_id";
        public static final String COLUMN_NAME_DATA = "data";
        public static final String COLUMN_NAME_TARGET = "target";
        public static final String COLUMN_NAME_READ = "read";
    }

    /* Inner class that defines the table contents */
    public static abstract class IbgeLocation implements BaseColumns {
        public static final String TABLE_NAME = "locations";
        public static final String COLUMN_NAME_ID = "_id";
        public static final String COLUMN_NAME_TYPE= "type";
        public static final String COLUMN_NAME_TITLE= "title";
        public static final String COLUMN_NAME_ALIAS = "alias";
        public static final String COLUMN_NAME_PARENT_ID = "parent_id";
        public static final String COLUMN_NAME_FIRST_CHILDS = "first_childs";
        public static final String COLUMN_NAME_LAT = "lat";
        public static final String COLUMN_NAME_LNG = "lng";
        public static final String COLUMN_NAME_NOACCENT = "label_noaccent";
        public static final String COLUMN_NAME_STATE = "state";

    }




}
