package br.org.eita.responsa.view.fragment;


import android.app.Fragment;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.components.TerritoryChooser;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.Notification;
import br.org.eita.responsa.model.validation.ValidationError;
import br.org.eita.responsa.view.activity.UserFormActivity;

public class UserFormFragment extends Fragment {

    EditText userNickname;
    TerritoryChooser userTerritory;
    Button userNameAndTerritorySubmit;

    HomeTerritory homeTerritory = null;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_user_form, container, false);

        findAllViewsByIds(view);

        userNameAndTerritorySubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validate().isEmpty()) {
                    ParseUser user = ParseUser.getCurrentUser();
                    String newHomeTerritoryId = userTerritory.getSelectedTerritory().id;
                    user.put("name", userNickname.getText().toString());
                    user.put("territoryId", newHomeTerritoryId);
                    user.put("territoryLabel", userTerritory.getSelectedTerritory().label);
                    user.put("territoryRegionId", userTerritory.getSelectedTerritory().getRegion().id);

                    //Unsubscribe from all previous territory channels
                    Notification.cleanChannelTypeSubscriptions(Notification.TERRITORY_CHANNEL);

                    //Subscribe in current location channel
                    Notification.subscribeToChannel(Notification.TERRITORY_CHANNEL, newHomeTerritoryId);

                    //FIXME alert aqui deveria ser feito so depois do "done" sem erros, mas quando isso acontece, Activity nao existe mais
                    Toast.makeText(ResponsaApplication.getAppContext(), getString(R.string.u_user_form), Toast.LENGTH_SHORT).show();

                    HomeTerritory.onUpdateTerritory();

                    user.saveEventually(new SaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            if (e == null) {
                            }
                        }
                    });

                    getActivity().finish();
                } else {
                    // Validation error
                    // TODO: print all validation errors to the user
                    Toast.makeText(getActivity(), getString(R.string.u_please_fill_all_data), Toast.LENGTH_SHORT).show();
                }
            }
        });

        populateData();
        validate();

        userNickname.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                validate();
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        userTerritory.onChangeSelectedTerritory(new TerritoryChooser.OnChangeTerritoryListener() {
            @Override
            public void onChange(HomeTerritory selectedTerritory) {
                validate();
            }
        });


        return view;
    }

    private void populateData() {
        userNickname.setText(ParseUser.getCurrentUser().getString("name"));
        userTerritory.setSelectedTerritory(ResponsaApplication.territoryDb.findTerritoryById(ParseUser.getCurrentUser().getString("territoryId")));
    }

    private void findAllViewsByIds(View parentView) {
        userNickname = (EditText) parentView.findViewById(R.id.userNickname);
        userTerritory = (TerritoryChooser) parentView.findViewById(R.id.userLocation);
        userNameAndTerritorySubmit = (Button) parentView.findViewById(R.id.userNameAndTerritorySubmit);
    }

    private ArrayList<ValidationError> validate() {
        String title = userNickname.getText().toString();
        ArrayList<ValidationError> errors = new ArrayList<>();
        if (title == null || title.isEmpty()) {
            errors.add(new ValidationError("User Form error", getString(R.string.v_user_no_nickname)));
        }
        errors.addAll(userTerritory.validate());
        userNameAndTerritorySubmit.setEnabled(errors.isEmpty());
        return errors;
    }
}
