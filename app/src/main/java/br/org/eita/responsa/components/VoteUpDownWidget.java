package br.org.eita.responsa.components;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.shamanland.fonticon.FontIconDrawable;
import com.shamanland.fonticon.FontIconTextView;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.Votable;

public class VoteUpDownWidget extends FrameLayout {

    private Votable votable;
    private FontIconTextView voteUpArrow;
    private FontIconTextView voteDownArrow;
    private TextView voteText;
    private TextView voteQtdLabel;

    private Integer voteState = 0;

    private Integer totalVotes = 0;

    private Integer baseValue = 0;

    private Boolean voteEnabled = true;
    private String disabledMessage;

    public VoteUpDownWidget(Context context) {
        this(context, null);
    }

    public VoteUpDownWidget(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VoteUpDownWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.vote_up_down, this);

        voteUpArrow = (FontIconTextView) view.findViewById(R.id.voteUpArrow) ;
        voteDownArrow = (FontIconTextView) view.findViewById(R.id.voteDownArrow);
        voteText = (TextView) view.findViewById(R.id.voteText);
        voteQtdLabel = (TextView) view.findViewById(R.id.voteQtdLabel);

        voteUpArrow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!voteEnabled) {
                    if (disabledMessage != null && !disabledMessage.isEmpty()) {
                        Toast.makeText(getContext(),disabledMessage,Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                //ResponsaApplication.vibrate();
                Integer newVoteState = (voteState == 1) ? 0 : 1;
                setState(newVoteState);
                if (votable != null) votable.setVoteState(newVoteState);
            }
        });

        voteDownArrow.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (!voteEnabled) {
                    if (disabledMessage != null && !disabledMessage.isEmpty()) {
                        Toast.makeText(getContext(),disabledMessage,Toast.LENGTH_SHORT).show();
                    }
                    return;
                }
                //ResponsaApplication.vibrate();
                Integer newVoteState = (voteState == -1) ? 0 : -1;
                setState(newVoteState);
                if (votable != null) votable.setVoteState(newVoteState);
            }
        });

        setState(voteState);
    }

    private void setState(Integer voteState) {
        this.voteState = voteState;
        Drawable icon = null;
        Drawable icon2 = null;
        if (voteEnabled) {
            icon = FontIconDrawable.inflate(getContext(), (voteState == 1 ? R.xml.ic_botao_up_on: R.xml.ic_botao_up_off));
            icon2 = FontIconDrawable.inflate(getContext(), (voteState == -1 ? R.xml.ic_botao_down_on: R.xml.ic_botao_down_off));
        } else {
            icon = FontIconDrawable.inflate(getContext(), R.xml.ic_botao_up_disabled);
            icon2 = FontIconDrawable.inflate(getContext(), R.xml.ic_botao_down_disabled);
        }
        voteUpArrow.setCompoundDrawables(null, icon, null, null);
        voteDownArrow.setCompoundDrawables(null, icon2, null, null);

        calcTotalVotes();
    }

    public void setVotable(Votable votable) {
        this.votable  = votable;
        populateVotable(votable);

        votable.onVotableChange(new Votable.OnVotableChange() {
            @Override
            public void onChange(Votable v) {
                populateVotable(v);
            }
        });
    }

    private void populateVotable(Votable votable) {
        this.setState(votable.getVoteState());
        this.setBaseValue(votable.getRelevance() - votable.getVoteState());
        this.calcTotalVotes();
        this.setEnabled(votable.getVotesEnabled());
    }

    private void setBaseValue(Integer baseValue) {
        this.baseValue = baseValue;
    }

    private void calcTotalVotes() {
        this.totalVotes = baseValue + voteState;
        voteText.setText(String.format("%d",totalVotes));
        voteQtdLabel.setText(getContext().getResources().getQuantityString(R.plurals.u_votes, totalVotes));

    }

    public void setVoteEnabled(Boolean voteEnabled, String disabledMessage) {
        this.voteEnabled = voteEnabled;
        setState(voteState);
        this.disabledMessage = disabledMessage;
    }

}
