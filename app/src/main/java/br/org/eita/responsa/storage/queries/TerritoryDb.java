package br.org.eita.responsa.storage.queries;

import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.preference.PreferenceManager;

import java.io.IOException;
import java.util.ArrayList;

import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.storage.ResponsaStorageContract;
import br.org.eita.responsa.storage.StaticDataDbHelper;

public class TerritoryDb {

    private static final String LOCATION_DB_STARTED = "br.org.eita.responsa.LOCATION_DB_STARTED";

    private StaticDataDbHelper dbHelper;
    private Context mContext;

    public TerritoryDb(Context context) {
        mContext = context;
        this.dbHelper = new StaticDataDbHelper(context);
    }


    public void start() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(mContext);

        //if (!preferences.getBoolean(LOCATION_DB_STARTED,false)) {
            try {
                this.dbHelper.createDataBase();
                SharedPreferences.Editor editor = preferences.edit();
                editor.putBoolean(LOCATION_DB_STARTED,true);
                editor.commit();

            } catch (IOException ioe) {
                throw new Error("Unable to create database");
            }
        //}
    }

    public ArrayList<HomeTerritory> queryCitiesByName(CharSequence searchTerm) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        //db.setLocale(Locale.forLanguageTag("pt_BR"));

        String sql = "";

        sql += "SELECT loc.*, ";
        sql += "loc.parent_id AS region_id, ";
        sql += "loc.title || ' / ' || loc.state AS CLABEL ";
        sql += "FROM locations loc, locations loc_child ";
        sql += "WHERE (loc_child.parent_id = loc._id AND loc_child.title=loc.title) ";
        sql += " AND ( loc_child.label_noaccent LIKE '"+searchTerm+"%' OR loc_child.label_noaccent LIKE '% "+searchTerm+"%' OR CLABEL LIKE '"+searchTerm+"%' OR CLABEL LIKE '% "+searchTerm+"%' ) ";
        sql += "AND loc.type = 'city' ORDER BY loc_child.label_noaccent ASC ";
        sql += "LIMIT 20";

        //ResponsaUtil.log("responsaQuery",sql);

        Cursor cursor = db.rawQuery(sql, null);

        ArrayList<HomeTerritory> recordsList = new ArrayList<HomeTerritory>();

        if (cursor.moveToFirst()) {
            do {
                recordsList.add(territoryFromCursor(cursor));
            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        return recordsList;
    }

    public ArrayList<HomeTerritory> queryTerritoriesByCity(HomeTerritory city) {
        ArrayList<HomeTerritory> recordsList = new ArrayList<HomeTerritory>();

        if (city !=null) {
            SQLiteDatabase db = dbHelper.getWritableDatabase();
            //db.setLocale(Locale.forLanguageTag("pt_BR"));

            String sql = "";
            sql += "SELECT loc.*, ";
            sql += "ploc.parent_id AS region_id, ";
            sql += "CASE WHEN loc.title = ploc.title ";
            sql += "  THEN ploc.title || ' / ' || ploc.state ";
            sql += "  ELSE ploc.title || ' / ' || ploc.state || ' - ' || loc.title ";
            sql += "  END AS CLABEL ";
            sql += "FROM locations loc ";
            sql += "JOIN locations ploc on ploc._id = loc.parent_id ";
            sql += "WHERE loc.parent_id='"+city.id+"' ";
            sql += "AND loc.type = 'subcity_region' ORDER BY loc.label_noaccent ASC ";
            //sql += "LIMIT 20";

            Cursor cursor = db.rawQuery(sql, null);

            if (cursor.moveToFirst()) {
                do {
                    recordsList.add(territoryFromCursor(cursor));
                } while (cursor.moveToNext());
            }
            cursor.close();
            db.close();
        }

        return recordsList;
    }

    public ArrayList<HomeTerritory> queryTerritoriesByName(CharSequence searchTerm) {

        // Gets the data repository in write mode
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.

        //db.setLocale(Locale.forLanguageTag("pt_BR"));
        
        String sql = "";

        sql += "SELECT loc.*, ";
        sql += "ploc.parent_id AS region_id, ";
        sql += "CASE WHEN loc.title = ploc.title ";
        sql += "  THEN ploc.title || ' / ' || ploc.state ";
        sql += "  ELSE ploc.title || ' / ' || ploc.state || ' - ' || loc.title ";
        sql += "  END AS CLABEL ";
        sql += "FROM locations loc ";
        sql += "JOIN locations ploc on ploc._id = loc.parent_id ";
        sql += "WHERE ( loc.label_noaccent LIKE '"+searchTerm+"%' OR loc.label_noaccent LIKE '% "+searchTerm+"%' OR CLABEL LIKE '"+searchTerm+"%' OR CLABEL LIKE '% "+searchTerm+"%' ) ";
        sql += "AND loc.type = 'subcity_region' ORDER BY loc.label_noaccent ASC ";
        sql += "LIMIT 20";

        //ResponsaUtil.log("responsaQuery",sql);

        Cursor cursor = db.rawQuery(sql, null);

        ArrayList<HomeTerritory> recordsList = new ArrayList<HomeTerritory>();

        //LocationCoordinates[] recordsList = new LocationCoordinates[] {};

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                // add to list
                recordsList.add(territoryFromCursor(cursor));

            } while (cursor.moveToNext());
        }

        cursor.close();
        db.close();

        // return the list of records
        return recordsList;
    }

    public HomeTerritory findTerritoryById(String territoryId) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.

        //db.setLocale(Locale.forLanguageTag("pt_BR"));

        // select query
        String sql = "";
        sql += "SELECT loc.*, ";
        sql += "CASE ";
        sql += "  WHEN (loc.type = 'city') ";
        sql += "  THEN loc.title || ' / ' || loc.state ";
        sql += "  ELSE ";
        sql += "    CASE ";
        sql += "      WHEN (loc.type = 'subcity_region' AND loc.title=ploc.title) ";
        sql += "      THEN ploc.title || ' / ' || ploc.state ";
        sql += "      ELSE";
        sql += "        CASE ";
        sql += "          WHEN loc.type = 'subcity_region' AND loc.title!=ploc.title ";
        sql += "          THEN ploc.title || ' / ' || ploc.state || ' - ' || loc.title ";
        sql += "          ELSE loc.title";
        sql += "        END ";
        sql += "    END ";
        sql += "END AS CLABEL ";
        sql += "FROM locations loc ";
        sql += "JOIN locations ploc on ploc._id = loc.parent_id ";
        sql += "WHERE loc._id = '" + territoryId + "'";

        Cursor cursor = db.rawQuery(sql, null);

        //LocationCoordinates[] recordsList = new LocationCoordinates[] {};

        HomeTerritory homeTerritory = null;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            homeTerritory = territoryFromCursor(cursor);
        }

        cursor.close();
        db.close();

        // return the list of records
        return homeTerritory;
    }

    private HomeTerritory territoryFromCursor(Cursor cursor) {
        HomeTerritory homeTerritory = new HomeTerritory();
        homeTerritory.id =  cursor.getString(cursor.getColumnIndex(ResponsaStorageContract.IbgeLocation.COLUMN_NAME_ID));
        homeTerritory.type = cursor.getString(cursor.getColumnIndex(ResponsaStorageContract.IbgeLocation.COLUMN_NAME_TYPE));
        homeTerritory.title = cursor.getString(cursor.getColumnIndex(ResponsaStorageContract.IbgeLocation.COLUMN_NAME_TITLE));
        homeTerritory.alias = cursor.getString(cursor.getColumnIndex(ResponsaStorageContract.IbgeLocation.COLUMN_NAME_ALIAS));
        homeTerritory.parentId = cursor.getString(cursor.getColumnIndex(ResponsaStorageContract.IbgeLocation.COLUMN_NAME_PARENT_ID));
        homeTerritory.firstChilds = cursor.getString(cursor.getColumnIndex(ResponsaStorageContract.IbgeLocation.COLUMN_NAME_FIRST_CHILDS));
        homeTerritory.state = cursor.getString(cursor.getColumnIndex(ResponsaStorageContract.IbgeLocation.COLUMN_NAME_STATE));
        homeTerritory.lat = cursor.getDouble(cursor.getColumnIndex(ResponsaStorageContract.IbgeLocation.COLUMN_NAME_LAT));
        homeTerritory.lng = cursor.getDouble(cursor.getColumnIndex(ResponsaStorageContract.IbgeLocation.COLUMN_NAME_LNG));
        if (cursor.getColumnIndex("CLABEL") > 0) {
            homeTerritory.label = cursor.getString(cursor.getColumnIndex("CLABEL"));
        }

        return homeTerritory;
    }

    public HomeTerritory findByCityState(String localidade, String uf) {
        SQLiteDatabase db = dbHelper.getWritableDatabase();
        // Define a projection that specifies which columns from the database
        // you will actually use after this query.

        //db.setLocale(Locale.forLanguageTag("pt_BR"));

        // select query
        String sql = "";
        sql += "SELECT loc.*, ";
        sql += "CASE WHEN loc.title = ploc.title ";
        sql += "  THEN ploc.title || ' / ' || ploc.state ";
        sql += "  ELSE ploc.title || ' / ' || ploc.state || ' - ' || loc.title ";
        sql += "  END AS CLABEL ";
        sql += "FROM locations loc ";
        sql += "JOIN locations ploc on ploc._id = loc.parent_id ";
        sql += "WHERE  loc.title = '" + localidade + "' AND loc.state = '"+ uf +"' AND loc.type='subcity_region' ";

        Cursor cursor = db.rawQuery(sql, null);

        //LocationCoordinates[] recordsList = new LocationCoordinates[] {};

        HomeTerritory homeTerritory = null;

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            homeTerritory = territoryFromCursor(cursor);
        }

        cursor.close();
        db.close();

        // return the list of records
        return homeTerritory;
    }

}
