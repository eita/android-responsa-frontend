package br.org.eita.responsa.model;


import android.app.*;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Parcel;
import android.provider.CalendarContract;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;

import com.mapbox.mapboxsdk.annotations.BaseMarkerOptions;
import com.mapbox.mapboxsdk.annotations.Icon;
import com.mapbox.mapboxsdk.annotations.IconFactory;
import com.mapbox.mapboxsdk.annotations.Marker;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.parse.GetCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.MainActivity;
import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.model.validation.ValidationError;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.view.activity.FichaSinteseActivity;


@ParseClassName("ResponsaObject")
public class ResponsaObject extends BaseResponsaModel implements Addressable{

    //Alert triggered when user has been in same location for a while (CHEGUEI)
    public static final String TRIGGER_NEAREST_ALERT = "br.org.eita.responsa.TRIGGER_NEAREST_ALERT";

    public UserResponsaObject userResponsaObject;


    public static Date lastRemoteFetchTrial;
    public static Date lastRemoteFetchCompleted;

    public static ResponsaObject currentResponsaObject;

    // This property asserts that the user has already received the cheguei notification, adn guarantees that it is not sent again
    public static String sentChegueiNotification = null;

    public static final Map<String, String> dataSources;
    static {
        Map<String, String> aMap = new HashMap<String, String>();
        aMap.put("experiencias_ean","Experiências de Educação Alimentar e Nutricional");
        aMap.put("cirandas","Cirandas.net - FBES");
        aMap.put("feiras_organicas","Feiras Orgânicas - IDEC");
        aMap.put("muda","Muda São Paulo");
        aMap.put("comsol","Rede de Comercialização Solidária - COMSOL");
        aMap.put("gcrs","Grupos de Consumo Responsável - Kairós");
        aMap.put("agrorede","Agroecologia em Rede - ANA e ABA");
        aMap.put("hortas_sp","Hortas de São Paulo");
        aMap.put("ecosampa","Roteiro EcoSampa");
        aMap.put("organicos_no_campo_e_na_mesa","Orgânicos no prato");
        aMap.put("responsa","Sugerido por usuário");

        dataSources = Collections.unmodifiableMap(aMap);
    }

    public ParseFile getAvatarImage() {
        return getParseFile("avatarImage");
    }

    public String getLocalId() {
        return getString("localId");
    }

    public void setLocalId(String localId) {
        if (localId == null) {
            this.put("localId", JSONObject.NULL);
        } else {
            this.put("localId", localId);
        }
    }

    public String getSource() {
        return getString("source");
    }

    public void setSource(String source) {
        if (source == null) {
            this.put("source", JSONObject.NULL);
        } else {
            this.put("source", source);
        }
    }

    public String getTerritoryId() {
        return getString("territoryId");
    }

    public void setTerritoryId(String territoryId) {
        if (territoryId == null) {
            this.put("territoryId", JSONObject.NULL);
        } else {
            this.put("territoryId", territoryId);
        }
    }

    public String getAvatar() {
        return getString("avatar");
    }

    public void setAvatar(String avatar) {
        if (avatar == null) {
            this.put("avatar", JSONObject.NULL);
        } else {
            this.put("avatar", avatar);
        }
    }

    public String getUrl() {
        return getString("url");
    }

    public void setUrl(String url) {
        if (url == null) {
            this.put("url", JSONObject.NULL);
        } else {
            this.put("url", url);
        }
    }

    public String getCountry() {
        return getString("country");
    }

    public void setCountry(String country) {
        if (country == null) {
            this.put("country", JSONObject.NULL);
        } else {
            this.put("country", country);
        }
    }

    public String getState() {
        HomeTerritory loc = ResponsaApplication.territoryDb.findTerritoryById(getTerritoryId());
        return loc.state;
    }


    public String getCity() {
        HomeTerritory loc = ResponsaApplication.territoryDb.findTerritoryById(getTerritoryId());
        return loc.title;
    }

    public UserResponsaObject getUserResponsaObject() {
        return userResponsaObject;
    }


    public void setUserResponsaObject(UserResponsaObject userResponsaObject) {
        this.userResponsaObject = userResponsaObject;
    }

    @Override
    public HomeTerritory getResponsaTerritory() {
        HomeTerritory territory = ResponsaApplication.territoryDb.findTerritoryById(getTerritoryId());
        return territory;
    }

    public void copyAddressFrom(ResponsaObject responsaObject) {
        setCep(responsaObject.getCep());
        setRuaENumero(responsaObject.getRuaENumero());
        setBairro(responsaObject.getBairro());
        setPhone1(responsaObject.getPhone1());
        setWebsite(responsaObject.getWebsite());
        setEmail(responsaObject.getEmail());
        setFacebook(responsaObject.getFacebook());
        setGeoLocation(responsaObject.getGeoLocation());
    }

    public String getTitle() {
        return getString("title");
    }

    public void setTitle(String title) {
        if (title == null) {
            this.put("title", JSONObject.NULL);
        } else {
            this.put("title", title);
        }
    }

    public String getDescription() {
        return getString("description");
    }

    public void setDescription(String description) {
        if (description == null) {
            this.put("description", JSONObject.NULL);
        } else {
            this.put("description", description);
        }
    }

    public ParseGeoPoint getGeoLocation() {
        return getParseGeoPoint("geoLocation");
    }

    public LocationCoordinates getLocationCoordinates() {
        return LocationCoordinates.fromParseGeoPoint(getGeoLocation(), System.currentTimeMillis());
    }

    public void setGeoLocation(ParseGeoPoint geoLocation) {
        if (geoLocation == null) {
            this.put("geoLocation", JSONObject.NULL);
        } else {
            this.put("geoLocation", geoLocation);
        }
    }

    public void setGeoLocation(LocationCoordinates locationCoordinates) {
        if (locationCoordinates == null) {
            this.put("geoLocation", JSONObject.NULL);
        } else {
            this.put("geoLocation", new ParseGeoPoint(locationCoordinates.getLatitude(),locationCoordinates.getLongitude()));
        }
    }

    public JSONArray getSources() {
        return getJSONArray("sources");
    }

    public void setSources(JSONArray sources) {
        if (sources == null) {
            this.put("sources", JSONObject.NULL);
        } else {
            this.put("sources", sources);
        }
    }

    public String getTitleSlug() {
        return getString("titleSlug");
    }

    public void setTitleSlug(String titleSlug) {
        if (titleSlug == null) {
            this.put("titleSlug", JSONObject.NULL);
        } else {
            this.put("titleSlug", titleSlug);
        }
    }

    public String getAddress() {
        return getString("address");
    }

    public void setAddress(String address) {
        if (address == null) {
            this.put("address", JSONObject.NULL);
        } else {
            this.put("address", address);
        }
    }

    public void setAddress(String logradouro, String bairro) {
        if (logradouro == null || logradouro.isEmpty()) {
            this.put("address", JSONObject.NULL);
        } else {
            String fullAddress = logradouro;
            if (bairro != null && !bairro.isEmpty()) {
                fullAddress += ", " + bairro;
            }
            this.put("address",fullAddress);
        }
    }

    public String getCategory() {
        return getString("category");
    }

    public void setCategory(String category) {
        if (category == null) {
            this.put("category", JSONObject.NULL);
        } else {
            this.put("category", category);
        }
    }

    public String getEmail() {
        return getString("email");
    }

    public void setEmail(String email) {
        if (email == null) {
            this.put("email", JSONObject.NULL);
        } else {
            this.put("email", email);
        }
    }

    public String getFacebook() {
        return getString("facebook");
    }

    public void setFacebook(String facebook) {
        if (facebook == null) {
            this.put("facebook", JSONObject.NULL);
        } else {
            this.put("facebook", facebook);
        }
    }

    public String getPhone1() {
        return getString("phone1");
    }

    public void setPhone1(String phone1) {
        if (phone1 == null) {
            this.put("phone1", JSONObject.NULL);
        } else {
            this.put("phone1", phone1);
        }
    }

    public String getPhone2() {
        return getString("phone2");
    }

    public void setPhone2(String phone2) {
        if (phone2 == null) {
            this.put("phone2", JSONObject.NULL);
        } else {
            this.put("phone2", phone2);
        }
    }

    public String getTwitter() {
        return getString("twitter");
    }

    public void setTwitter(String twitter) {
        if (twitter == null) {
            this.put("twitter", JSONObject.NULL);
        } else {
            this.put("twitter", twitter);
        }
    }

    public String getWebsite() {
        return getString("website");
    }

    public String getSite() {
        String website = getWebsite();
        if (website == null || website.isEmpty()) {
            website = getUrl();
        }
        return website;
    }

    public void setWebsite(String website) {
        if (website == null) {
            this.put("website", JSONObject.NULL);
        } else {
            this.put("website", website);
        }
    }

    public String getZipCode() {
        return getString("zipCode");
    }

    public void setZipCode(String zipCode) {
        if (zipCode == null) {
            this.put("zipCode", JSONObject.NULL);
        } else {
            this.put("zipCode", zipCode);
        }
    }

    public Integer getCountComments() {
        return getInt("countComments");
    }

    public void setCountComments(Integer countComments) {
        if (countComments == null) {
            this.put("countComments", JSONObject.NULL);
        } else {
            this.put("countComments", countComments);
        }
    }

    public Boolean getPublished() {
        return getBoolean("published");
    }

    public void setPublished(Boolean published) {
        if (published == null) {
            this.put("published", JSONObject.NULL);
        } else {
            this.put("published", published);
        }
    }

    public ParseUser getOwner() {
        return getParseUser("owner");
    }

    public void setOwner(ParseUser owner) {
        if (owner == null) {
            this.put("owner", JSONObject.NULL);
        } else {
            this.put("owner", owner);
        }
    }

    public Date getDateTimeStart() {
        return getDate("dateTimeStart");
    }

    public void setDateTimeStart(Date dateTimeStart) {
        if (dateTimeStart == null) {
            this.put("dateTimeStart", JSONObject.NULL);
        } else {
            this.put("dateTimeStart", dateTimeStart);
        }
    }

    public Date getDateTimeFinish() {
        return getDate("dateTimeFinish");
    }

    public void setDateTimeFinish(Date dateTimeFinish) {
        if (dateTimeFinish == null) {
            this.put("dateTimeFinish", JSONObject.NULL);
        } else {
            this.put("dateTimeFinish", dateTimeFinish);
        }
    }
    
    public String getType() {
        return getString("type");
    }

    public void setType(String type) {
        if (type == null) {
            this.put("type", JSONObject.NULL);
        } else {
            this.put("type", type);
        }
    }

    public ParseObject getPlaceInitiative() {
        return getParseObject("placeInitiative");
    }

    public void setPlaceInitiative(ResponsaObject placeInitiative) {
        if (placeInitiative == null) {
            this.put("placeInitiative", JSONObject.NULL);
        } else {
            this.put("placeInitiative", placeInitiative);
            this.setTerritory(placeInitiative.getTerritory());
        }
    }

    public Integer getCountFavorites() {
        return getInt("countFavorites");
    }

    public void setCountFavorites(Integer countFavorites) {
        if (countFavorites == null) {
            this.put("countFavorites", JSONObject.NULL);
        } else {
            this.put("countFavorites", countFavorites);
        }
    }

    public Integer getCountWillAttendGathering() {
        return getInt("countWillAttendGathering");
    }

    public void setCountWillAttendGathering(Integer countWillAttendGathering) {
        if (countWillAttendGathering == null) {
            this.put("countWillAttendGathering", JSONObject.NULL);
        } else {
            this.put("countWillAttendGathering", countWillAttendGathering);
        }
    }

    public String getTerritoryRegionId() {
        return getString("territoryRegionId");
    }

    public void setTerritoryRegionId(String territoryRegionId) {
        if (territoryRegionId == null) {
            this.put("territoryRegionId", JSONObject.NULL);
        } else {
            this.put("territoryRegionId", territoryRegionId);
        }
    }

    public String getTerritoryLabel() {
        return getString("territoryLabel");
    }

    public void setTerritoryLabel(String territoryLabel) {
        if (territoryLabel == null) {
            this.put("territoryLabel", JSONObject.NULL);
        } else {
            this.put("territoryLabel", territoryLabel);
        }
    }

    public String getHumanReadableTime() {
        return ResponsaUtil.humanReadableTimeForEvent(getDateTimeStart(), getDateTimeFinish());
    }

    public String getHumanReadableDate() {
        if (getDateTimeStart() == null || getDateTimeFinish() == null) return "";

        SimpleDateFormat sdf = new SimpleDateFormat("EEEE, d 'de' MMMM");

        if (ResponsaUtil.daysDiffer(getDateTimeStart(), getDateTimeFinish())) {
            return "de: " + sdf.format(getDateTimeStart()) + " às " + ResponsaUtil.humanReadableTime(getDateTimeStart()) +
                    "\n" +
                    "até: " + sdf.format(getDateTimeFinish()) + " às " + ResponsaUtil.humanReadableTime(getDateTimeFinish());
        } else {
            return sdf.format(getDateTimeStart()) + ", " + getHumanReadableTime();
        }
    }

    /**
     * Check if there is the need to send a broadcast telling that this object is a nearest initiative. If so, send the broadcast.
     * @param context
     */
    public void checkAndSendChegueiBroadcast(Context context) {
        UserResponsaObject userResponsaObject = getUserResponsaObject();
        if (userResponsaObject == null) {
            // New userResponsaObject will be created!
            userResponsaObject = ResponsaApplication.responsaDataManager.createUserResponsaObject(this);
            ResponsaUtil.log("ResponsaLocation","Had to create userResponsaObject since it is null for Responsa Object '"+getObjectId()+"'");
        }

        Boolean sendNotification = true;
        //Notification is only sent if it was never sent before or if it was sent more then "days_to_ativade_cheguei_again" days ago.
        if (getObjectId().equals(sentChegueiNotification)) {
            sendNotification = false;
        } else if (userResponsaObject.getLastAlertChegada() != null &&
                ResponsaUtil.daysBetween(userResponsaObject.getLastAlertChegada(), new Date()) < Constants.DAYS_TO_ACTIVATE_CHEGUEI_AGAIN) {
            sendNotification = false;
            ResponsaUtil.log("ResponsaLocationService", "already sent alert less than " +Constants.DAYS_TO_ACTIVATE_CHEGUEI_AGAIN+ " days ago to '" + getTitle() + "', userResponsaObject=="+userResponsaObject.getObjectId() + " lastAlertChegada: " + userResponsaObject.getLastAlertChegada());
        }

        if (sendNotification) {
            sentChegueiNotification = getObjectId();
            context.sendBroadcast(new Intent(ResponsaObject.TRIGGER_NEAREST_ALERT));
            if (!ResponsaApplication.RESPONSA_IS_IN_FOREGROUND) {
                sendChegueiNotification(context);
                ResponsaUtil.log("ResponsaLocationService", "sent notification because user is outside app");
            } else {
                ResponsaUtil.log("ResponsaLocationService", "did not send notification, because user is in app");
            }

            userResponsaObject.setLastAlertChegada(new Date());
            ResponsaApplication.responsaDataManager.saveObject("userResponsaObject",getObjectId(),userResponsaObject, null);
        }
    }

    private void sendChegueiNotification(Context context) {
        ResponsaObject currentNearest = this;

        NotificationCompat.Builder mBuilder =
                new NotificationCompat.Builder(context)
                        .setSmallIcon(R.drawable.ic_responsa_logo)
                        .setContentTitle(context.getString(R.string.u_ask_cheguei_dialog_title, currentNearest.getTitle()))
                        .setContentText(currentNearest.getDescription());

        // Creates an explicit intent for an Activity in your app
        Intent resultIntent = new Intent(context, FichaSinteseActivity.class);
        //resultIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP|Intent.FLAG_ACTIVITY_SINGLE_TOP);

        //ResponsaApplication.responsaViewManager.openFichaSintese(ResponsaApplication.getAppContext(),currentNearest);
        resultIntent.putExtra(FichaSinteseActivity.INTENT_OBJECT_ID_KEY,this.getObjectId());
        resultIntent.putExtra(FichaSinteseActivity.INTENT_VIEW_KEY, this.getType());
        resultIntent.putExtra(FichaSinteseActivity.INTENT_CHEGUEI, true);

        //resultIntent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP | Intent.FLAG_ACTIVITY_CLEAR_TOP);




        // The stack builder object will contain an artificial back stack for the
        // started Activity.
        // This ensures that navigating backward from the Activity leads out of
        // your application to the Home screen.
        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack for the Intent (but not the Intent itself)
        stackBuilder.addParentStack(MainActivity.class);
        // Adds the Intent that starts the Activity to the top of the stack
        stackBuilder.addNextIntent(resultIntent);

        PendingIntent pendingNotificationIntent = PendingIntent.getActivity(context, 1, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT);

        mBuilder.setContentIntent(pendingNotificationIntent);


        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(
                        0,
                        PendingIntent.FLAG_UPDATE_CURRENT
                );


        mBuilder.setContentIntent(resultPendingIntent);

        NotificationManager mNM = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);

        android.app.Notification not = mBuilder.build();


        mNM.notify(1, not);
    }

    public void chegar(Context context) {
        Notification.sendNotification(context, "chegueiInitiative", this);
    }

    public String getShareUrl() {
        return "http://mapa.consumoresponsavel.org.br/?id="+getSource()+"__"+getLocalId();
    }

    public void exportEvent(Context c) {
        Calendar beginTime = Calendar.getInstance();
        beginTime.setTime(getDateTimeStart());
        Calendar endTime = Calendar.getInstance();
        endTime.setTime(getDateTimeFinish());

        Intent intent = new Intent(Intent.ACTION_INSERT)
                .setData(CalendarContract.Events.CONTENT_URI)
                .putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME, beginTime.getTimeInMillis())
                .putExtra(CalendarContract.EXTRA_EVENT_END_TIME, endTime.getTimeInMillis())
                .putExtra(CalendarContract.Events.TITLE, getTitle())
                .putExtra(CalendarContract.Events.DESCRIPTION, getDescription())
                .putExtra(CalendarContract.Events.EVENT_LOCATION, getPlace())
                .putExtra(CalendarContract.Events.AVAILABILITY, CalendarContract.Events.AVAILABILITY_BUSY);
        c.startActivity(intent);
    }

    public interface GetUserResponsaObjectCallback {
        public void done(UserResponsaObject userResponsaObject, ParseException e);
    }

    //Validation
    public ArrayList<ValidationError> validate(Context c) {
        ArrayList<ValidationError> errors = new ArrayList<ValidationError>();

        String title = this.getTitle();
        if (title == null || title == JSONObject.NULL || (title instanceof String && title.isEmpty())) {
            errors.add(new ValidationError("title",c.getString(R.string.v_ro_no_title)));
        }

        String description = this.getDescription();
        if (description == null || description == JSONObject.NULL || (description instanceof String && description.isEmpty())) {
            errors.add(new ValidationError("description",c.getString(R.string.v_ro_no_description)));
        }

        String territoryId = this.getTerritoryId();
        if (territoryId == null || territoryId == JSONObject.NULL || (territoryId instanceof String && territoryId.isEmpty())) {
            errors.add(new ValidationError("territoryId",c.getString(R.string.v_ro_no_location_id)));
        }

        if (errors.size() > 0)
            return errors;

        return null;
    }


    //---------------------------------------------------------------------------
    //Addressable methods and temporary variables
    private String localRuaENumero = "";
    private String localBairro = "";


    @Override
    public void setCep(String cep) {
        setZipCode(cep);
    }

    @Override
    public void setRuaENumero(String logradouro) {
        this.localRuaENumero = logradouro;
        setAddress(localRuaENumero, localBairro);
    }

    @Override
    public void setBairro(String bairro) {
        this.localBairro = bairro;
        setAddress(localRuaENumero, localBairro);
    }

    @Override
    public void fromCepAddress(CepAddress cepAddress) {
        this.setZipCode(cepAddress.cep);
        this.setBairro(cepAddress.bairro);
        this.setRuaENumero(cepAddress.logradouro);
        this.setTerritory(cepAddress.getLocation());
    }

    private void setTerritory(HomeTerritory territory) {
        if (territory != null) {
            if (territory.lat != null && territory.lng != null) {
                this.setGeoLocation(new ParseGeoPoint(territory.lat, territory.lng));
            }
            this.setTerritoryId(territory.id);
        } else {
            this.setGeoLocation((ParseGeoPoint) null);
        }
    }

    public HomeTerritory getTerritory() {
        return ResponsaApplication.territoryDb.findTerritoryById(this.getTerritoryId());
    }

    @Override
    public void fromGeocoderAddress(GeocoderAddress geocoderAddress) {
        this.setGeoLocation(new ParseGeoPoint(geocoderAddress.lat, geocoderAddress.lon));
    }

    @Override
    public String getCep() {
        return getZipCode();
    }

    @Override
    public String getRuaENumero() {
        if ("".equals(localRuaENumero)) return getAddress();
        return localRuaENumero;
    }

    @Override
    public String getBairro() {
        return localBairro;
    }

    @Override
    public LocationCoordinates getCoordinates() {
        return getLocationCoordinates();
    }

    //Returns complete string with place (title & address)
    public String getPlace() {
        String res = "";
        if (getType().equals("initiative")) {
            res = getTitle() + " - " + getFullAddress();
        } else { //gathering
            ResponsaObject placeInitiative = (ResponsaObject) getPlaceInitiative();
            if (placeInitiative != null) {
                res = placeInitiative.getPlace();
            } else {
                res = getFullAddress();
            }
        }
        return res;
    }

    public String getFullAddress() {
        return getAddress() + ", " + getBairro() + ", " + getCity() + " - " + getState();
    }

    public int getDrawableIcon() {
        if ("gathering".equals(getType())) {
            return R.drawable.encontros;
        } else if (InitiativeCategory.FEIRAS.equals(getCategory())) {
            return R.drawable.feiras;
        } else if (InitiativeCategory.GRUPOS_DE_CONSUMO_RESPONSAVEL.equals(getCategory())) {
            return R.drawable.grupos_de_consumo_responsavel;
        } else if (InitiativeCategory.INICIATIVAS_DE_ECONOMIA_SOLIDARIA.equals(getCategory())) {
            return R.drawable.iniciativas_de_economia_solidaria;
        } else if (InitiativeCategory.INICIATIVAS_DE_AGROECOLOGIA.equals(getCategory())) {
            return R.drawable.iniciativas_de_agroecologia;
        } else if (InitiativeCategory.RESTAURANTES.equals(getCategory())) {
            return R.drawable.restaurantes;
        } else if (InitiativeCategory.AGRICULTURA_URBANA.equals(getCategory())) {
            return R.drawable.agricultura_urbana;
        } else if (InitiativeCategory.LOJAS.equals(getCategory())) {
            return R.drawable.lojas;
        } else if (InitiativeCategory.COLETIVOS_CULTURAIS.equals(getCategory())) {
            return R.drawable.coletivos_culturais;
        } else {
            return R.drawable.avatar_responsa;
        }
    }

    public Icon getMapboxIcon(Context c) {
        return IconFactory.getInstance(c).fromResource(getDrawableIcon());
    }

}
