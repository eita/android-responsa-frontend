package br.org.eita.responsa.model;


public interface Answerable {
    public Integer getTotalAnswers();

    public void onAnswerableChange(OnAnswerableChange onAnswerableChange);

    interface OnAnswerableChange {
        public void onChange(Answerable a);
    }
}
