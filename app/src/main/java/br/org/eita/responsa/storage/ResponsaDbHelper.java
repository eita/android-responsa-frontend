package br.org.eita.responsa.storage;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import br.org.eita.responsa.storage.ResponsaStorageContract.*;

public class ResponsaDbHelper extends SQLiteOpenHelper {
    public static final int DATABASE_VERSION = 6;
    public static final String DATABASE_NAME = "Responsa.db";

    public ResponsaDbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        // This database is only a cache for online data, so its upgrade policy is
        // to simply to discard the data and start over
        onCreate(db);
    }

    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        onUpgrade(db, oldVersion, newVersion);
    }

    /**********************************************************
     *                      QUERIES AND SQLs                  *
     **********************************************************/

    private static final String  TEXT_TYPE = " TEXT";
    private static final String  STRING_TYPE = " VARCHAR(255)";
    private static final String  INTEGER_TYPE = " INTEGER";
    private static final String  COMMA_SEP = ",";

    /**********************************************
     *                NOTIFICATIONS               *
     **********************************************/

    private static final String SQL_CREATE_NOTIFICATIONS =
            "CREATE TABLE " + Notification.TABLE_NAME + " (" +
                    Notification._ID + " INTEGER PRIMARY KEY," +
                    Notification.COLUMN_NAME_MESSAGE_ID + INTEGER_TYPE + COMMA_SEP +
                    Notification.COLUMN_NAME_DATA + STRING_TYPE + COMMA_SEP +
                    Notification.COLUMN_NAME_TARGET + STRING_TYPE + COMMA_SEP +
                    Notification.COLUMN_NAME_READ + INTEGER_TYPE + " DEFAULT 0 " +
                    " )";

    private static final String SQL_DELETE_NOTIFICATIONS =
            "DROP TABLE IF EXISTS " + Notification.TABLE_NAME;


}
