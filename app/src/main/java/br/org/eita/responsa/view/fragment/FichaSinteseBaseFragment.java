package br.org.eita.responsa.view.fragment;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.FragmentActivity;
import android.support.v7.app.AlertDialog;
import android.text.Layout;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.beardedhen.androidbootstrap.BootstrapButton;
import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.shamanland.fonticon.FontIconDrawable;
import com.shamanland.fonticon.FontIconTextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;

import br.org.eita.responsa.MainActivity;
import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.components.FolhaRostoFichaSintese;
import br.org.eita.responsa.components.DownloadMapViewTask;
import br.org.eita.responsa.components.TerritoryChooser;
import br.org.eita.responsa.components.icon_badge.IconBadgeComments;
import br.org.eita.responsa.components.icon_badge.ModelIconBadge;
import br.org.eita.responsa.components.icon_badge.IconBadgeWillAttend;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.view.activity.FeedbackActivity;
import br.org.eita.responsa.view.activity.FichaSinteseActivity;
import br.org.eita.responsa.view.activity.ResponsaObjectFormActivity;


public abstract class FichaSinteseBaseFragment extends BaseShowFragment {

    ResponsaObject responsaObject;

    FontIconTextView fsTextAddress;
    FontIconTextView fsTextPhone;
    FontIconTextView fsTextSite;
    FontIconTextView fsTextFacebook;
    FontIconTextView fsTextDistance;
    FontIconTextView fsTextWorkingTime;
    FontIconTextView fsDateStartEnd;
    FontIconTextView fsLocal;

    IconBadgeWillAttend badgeWillAttend;
    ModelIconBadge badgeFavorited;
    IconBadgeComments badgeComments;

    TextView responsaObjectPageDescription;
    TextView responsaObjectStatus;

    TextView fsDataSources;
    TextView fsTitle;
    TextView fsAuthor;
    TextView fsApontarErro;

    LinearLayout actionPanel;

    Boolean gostou = null;
    Boolean favoritou = null;
    Boolean marcouPresenca = null;


    FontIconTextView favoritarIcon = null;
    FontIconTextView compartilharIcon = null;
    FontIconTextView encontrosIcon = null;
    FontIconTextView marcarPresencaIcon = null;
    FontIconTextView exportarIcon = null;


    BootstrapCircleThumbnail fsLogo;
    BootstrapButton vejaMaisButton;
    Bitmap avatarBitmap;

    TextView commentsListTitle;

    View headerPadding;

    Button editarBtn;

    FolhaRostoFichaSintese imageFolha = null;

    Boolean descriptionHidden = true;

    protected abstract int getFragmentResource();

    LinearLayout chegueiIcon;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(getFragmentResource(), container, false);

        findAllViewsByIds(view);

        if (responsaObject != null) {
            populateViewData();
        }

        fsTextPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //TODO work in Android 6
                if (android.os.Build.VERSION.SDK_INT < Build.VERSION_CODES.M) {
                    Intent callIntent = new Intent(Intent.ACTION_CALL);
                    callIntent.setData(Uri.parse("tel:" + fsTextPhone.getText().toString().trim()));
                    startActivity(callIntent);
                }
            }
        });

        fsLogo.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                final FullImageDialogFragment fullImageDialogFragment = new FullImageDialogFragment();
                if (avatarBitmap != null) {
                    fullImageDialogFragment.setImage(avatarBitmap);
                    fullImageDialogFragment.show(getActivity().getSupportFragmentManager(), "view_image_fullscreen");
                }
            }
        });

        vejaMaisButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (descriptionHidden) {
                    ResponsaUtil.resetGradient(responsaObjectPageDescription);
                    responsaObjectPageDescription.setMaxLines(Integer.MAX_VALUE);
                    responsaObjectPageDescription.setEllipsize(null);
                    vejaMaisButton.setVisibility(View.GONE);
                }
            }
        });


        if (responsaObjectPageDescription != null) {
            responsaObjectPageDescription.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (descriptionHidden) {
                        ResponsaUtil.resetGradient(responsaObjectPageDescription);
                        responsaObjectPageDescription.setMaxLines(Integer.MAX_VALUE);
                        responsaObjectPageDescription.setEllipsize(null);
                        vejaMaisButton.setVisibility(View.GONE);
                    } else {
                        ResponsaUtil.applyGradient(responsaObjectPageDescription,getActivity());
                        responsaObjectPageDescription.setMaxLines(3);
                        responsaObjectPageDescription.setEllipsize(TextUtils.TruncateAt.END);
                    }
                    descriptionHidden = !descriptionHidden;
                }
            });
        }

        if (fsApontarErro != null) {
            fsApontarErro.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), FeedbackActivity.class);
                    intent.putExtra("view", getString(R.string.u_feedback_error_in_initiative));
                    intent.putExtra("viewObjectId", responsaObject.getObjectId());
                    intent.putExtra("viewClass",getClass().getSimpleName());
                    startActivity(intent);
                }
            });
        }


        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    protected void findAllViewsByIds(View parentView) {

        fsTextAddress = (FontIconTextView) parentView.findViewById(R.id.fsTextAddress);
        fsTextPhone = (FontIconTextView) parentView.findViewById(R.id.fsTextPhone);
        fsTextSite = (FontIconTextView) parentView.findViewById(R.id.fsTextSite);
        fsTextFacebook = (FontIconTextView) parentView.findViewById(R.id.fsTextFacebook);
        fsTextDistance = (FontIconTextView) parentView.findViewById(R.id.fsTextDistance);
        fsTextWorkingTime = (FontIconTextView) parentView.findViewById(R.id.fsTextWorkingTime);
        fsDateStartEnd = (FontIconTextView) parentView.findViewById(R.id.fsDateStartEnd);
        fsDataSources = (TextView) parentView.findViewById(R.id.fsDataSources);
        fsAuthor = (TextView) parentView.findViewById(R.id.fsAuthor);
        fsLocal = (FontIconTextView) parentView.findViewById(R.id.fsLocal);
        fsApontarErro = (TextView) parentView.findViewById(R.id.fsApontarErro);

        fsTitle = (TextView) parentView.findViewById(R.id.fsTitle);

        badgeWillAttend = (IconBadgeWillAttend) parentView.findViewById(R.id.badgeWillAttend);
        badgeFavorited = (ModelIconBadge) parentView.findViewById(R.id.badgeFavorited);
        badgeComments = (IconBadgeComments) parentView.findViewById(R.id.badgeComments);

        favoritarIcon = (FontIconTextView) parentView.findViewById(R.id.favoritarIcon);
        compartilharIcon = (FontIconTextView) parentView.findViewById(R.id.compartilharIcon);
        encontrosIcon = (FontIconTextView) parentView.findViewById(R.id.encontrosIcon);
        marcarPresencaIcon = (FontIconTextView) parentView.findViewById(R.id.marcarPresencaIcon);
        exportarIcon = (FontIconTextView) parentView.findViewById(R.id.exportarIcon);

        responsaObjectPageDescription = (TextView) parentView.findViewById(R.id.responsaObjectPageDescription);
        responsaObjectStatus = (TextView) parentView.findViewById(R.id.responsaObjectStatus);
        vejaMaisButton = (BootstrapButton) parentView.findViewById(R.id.vejaMaisButton);

        actionPanel = (LinearLayout) parentView.findViewById(R.id.fsActionPanel);


        fsLogo = (BootstrapCircleThumbnail) parentView.findViewById(R.id.fsLogo);

        commentsListTitle = (TextView) parentView.findViewById(R.id.commentsListTitle);

        imageFolha = (FolhaRostoFichaSintese) parentView.findViewById(R.id.imageFolha);

        headerPadding = (View) parentView.findViewById(R.id.headerPadding);

        chegueiIcon = (LinearLayout) parentView.findViewById(R.id.chegueiIcon);

        editarBtn = (Button) parentView.findViewById(R.id.editarBtn);

    }

    private void fixActionBarWidth() {

        //After responsaObjectType is drawn
        // http://android.phonesdevelopers.com/58_22916277/
        ViewTreeObserver vto = actionPanel.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                FontIconTextView icon = null;
                int width = compartilharIcon.getWidth();
                if (responsaObject.getType().equals("initiative")) {
                    icon = compartilharIcon;
                    width = compartilharIcon.getWidth();
                } else {
                    icon = favoritarIcon;
                    width = favoritarIcon.getWidth() + 30;

                }

                ResponsaUtil.removeOnGlobalLayoutListener(actionPanel, this);
                for (int i = 0; i < actionPanel.getChildCount(); i++) {
                    View v = actionPanel.getChildAt(i);
                    if (v instanceof FontIconTextView) {
                        if (!(v.getId() == icon.getId())) {
                            ((FontIconTextView) v).setWidth(width);
                        }
                    }
                }
            }
        });
    }

    @Override
    protected void populateViewData() {
        fixActionBarWidth();

        if ("initiative".equals(responsaObject.getType())) {

            ParseFile imgFile = responsaObject.getAvatarImage();

            if (imgFile != null) {
                imgFile.getDataInBackground(new GetDataCallback() {
                    @Override
                    public void done(byte[] bytes, ParseException e) {
                        if (e != null) {
                            ResponsaUtil.log("ImageOpenError",e.getMessage());
                        }
                        if (bytes != null) { //Problem when transfering endpoint and not yet files...
                            avatarBitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                            if (avatarBitmap != null) {
                                fsLogo.setImage(avatarBitmap);
                                fsLogo.setVisibility(View.VISIBLE);
                            }
                        }
                    }
                });
                if (imageFolha != null) {
                    imageFolha.setCategory(null);
                }
            } else {
                if (imageFolha != null) {
                    imageFolha.setCategory(responsaObject.getCategory());
                }
            }

            if (responsaObjectStatus != null && !responsaObject.getPublished()) {
                responsaObjectStatus.setVisibility(View.VISIBLE);
            }
        } else {
            compartilharIcon.setVisibility(View.GONE);
            marcarPresencaIcon.setVisibility(View.VISIBLE);
            populateTextView(responsaObject.getHumanReadableDate(), fsDateStartEnd);
            populateTextView(responsaObject.getTitle(), fsTitle);
            ParseUser owner = responsaObject.getOwner();
            if (owner != null) {
                fsAuthor.setText(getString(R.string.by_author, owner.getString("name")));
            }
            final ResponsaObject placeInitiative = (ResponsaObject) responsaObject.getPlaceInitiative();
            if (placeInitiative == null) {
                fsLocal.setVisibility(View.GONE);
            } else {
                populateTextView(placeInitiative.getTitle(), fsLocal);
                if (ResponsaObject.currentResponsaObject == null) {
                    fsLocal.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            ResponsaApplication.responsaViewManager.openFichaSintese(getActivity(),placeInitiative);
                        }
                    });
                }
            }

            //here populate badges
            badgeWillAttend.setModel(responsaObject);
            badgeWillAttend.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //Opens dialog if there is at least one person who attended:
                    if (responsaObject.getCountWillAttendGathering() > 0) {
                        UserListDialogFragment userListDialogFragment = new UserListDialogFragment();
                        userListDialogFragment.setData(FichaSinteseActivity.gatheringAttendeesList);
                        userListDialogFragment.show(((FragmentActivity) getActivity()).getSupportFragmentManager(), "peopleWhoConfirmedInResponsaObject");
                    }
                }
            });

            badgeComments.setModel(responsaObject);
            badgeFavorited.setModel(responsaObject);
        }

        //Address
        final StringBuffer addressBuffer = new StringBuffer();
        Boolean haveAddress = false;

        if (responsaObject.getAddress() != null && !responsaObject.getAddress().isEmpty()) {
            addressBuffer.append(responsaObject.getAddress());
            haveAddress = true;
        }

        HomeTerritory loc = responsaObject.getResponsaTerritory();
        if (loc != null) {
            if (haveAddress)
                addressBuffer.append(", ");
            addressBuffer.append(loc.label);
        }
        populateTextView(addressBuffer.toString(),fsTextAddress);
        fsTextAddress.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {

                    Intent intent = new Intent(Intent.ACTION_VIEW);
                    Uri geoLocation =  Uri.parse("geo:0,0?q=" + URLEncoder.encode(addressBuffer.toString(), "utf-8"));

                    intent.setData(geoLocation);
                    if (intent.resolveActivity(getActivity().getPackageManager()) != null) {
                        startActivity(intent);
                    }

                } catch (UnsupportedEncodingException e) {
                    e.printStackTrace();
                }

            }
        });

        //Phone
        StringBuffer phoneBuffer = new StringBuffer();
        if (responsaObject.getPhone1() != null && !responsaObject.getPhone1().isEmpty())
            phoneBuffer.append(responsaObject.getPhone1());


        if (responsaObject.getPhone2() != null && !responsaObject.getPhone2().isEmpty())
            phoneBuffer.append(" / " + responsaObject.getPhone2());

        populateTextView(phoneBuffer.toString(),fsTextPhone);
        populateTextView(responsaObject.getSite(),fsTextSite);
        populateTextView(responsaObject.getFacebook(),fsTextFacebook);

        populateTextView(LocationCoordinates.getHumanReadableDistance(responsaObject.getLocationCoordinates()),fsTextDistance);

        populateTextView(null,fsTextWorkingTime);

        String description = responsaObject.getDescription();
        if (description == null || description.isEmpty()) {
            responsaObjectPageDescription.setVisibility(View.GONE);
            vejaMaisButton.setVisibility(View.GONE);

        } else {
            responsaObjectPageDescription.setVisibility(View.VISIBLE);
            responsaObjectPageDescription.setText(responsaObject.getDescription());
            responsaObjectPageDescription.post(new Runnable() {
                @Override
                public void run() {
                    int lineCount = responsaObjectPageDescription.getLineCount();
                    if (lineCount > 3) {
                        applyInitialGradient(responsaObjectPageDescription);
                        vejaMaisButton.setVisibility(View.VISIBLE);
                    }
                }
            });
        }

        if (favoritou != null) populateFavoritar(favoritou);
        if (marcouPresenca != null) populateMarcarPresenca(marcouPresenca);

        Integer count = responsaObject.getCountComments();
        if (count == 0) {
            commentsListTitle.setText(getString(R.string.u_comments_title_zero));
        } else {
            commentsListTitle.setText(getResources().getQuantityString(R.plurals.u_comments_title, count, count));
        }

        //DataSources
        try {
            JSONArray dataSources = responsaObject.getSources();
            StringBuffer finalDataSources = new StringBuffer();
            JSONObject obj;
            if (dataSources != null && "initiative".equals(responsaObject.getType())) {
                fsDataSources.setVisibility(View.VISIBLE);
                for (int i=0; i < dataSources.length(); i++) {
                    if (finalDataSources.length() > 0) {
                        finalDataSources.append(" / ");
                    }
                    obj = dataSources.getJSONObject(i);

                    finalDataSources.append(ResponsaObject.dataSources.get(obj.getString("source")));
                }
                fsDataSources.setText(getActivity().getResources().getQuantityString(R.plurals.u_sources, dataSources.length()) + ": " + finalDataSources.toString());
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Initiative / Gathering
        if ("initiative".equals(responsaObject.getType())) {
            encontrosIcon.setVisibility(View.VISIBLE);
        } else { /* responsaObject.getType() == "gathering" */
            exportarIcon.setVisibility(View.VISIBLE);
        }

        if (editarBtn != null) {
            ParseUser owner = responsaObject.getOwner();
            if (owner != null && owner.equals(ParseUser.getCurrentUser())) {
                editarBtn.setVisibility(View.VISIBLE);
            }
            editarBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), ResponsaObjectFormActivity.class);
                    intent.putExtra("type", responsaObject.getType());
                    intent.putExtra("objectId",responsaObject.getObjectId());
                    intent.putExtra("cleanPlaceInitiativeOnClose",true);
                    ResponsaObject.currentResponsaObject = (ResponsaObject) responsaObject.getPlaceInitiative();
                    ResponsaObjectFormActivity.transientResponsaObject = responsaObject;
                    startActivity(intent);
                }
            });
        }

        //Cheguei
        /*if (getActivity()) {
            chegueiIcon
        }*/

        //commentsListTitle.setText(R.string.u_comments);
    }

    protected void populateTextView(String value, TextView view) {
        if (value != null && !value.isEmpty()) {
            view.setVisibility(View.VISIBLE);
            view.setText(value);
        } else {
            view.setVisibility(View.GONE);
        }
    }

    @Override
    protected void sweepViewData() {

    }

    public void updateResponsaObject(ResponsaObject responsaObject) {

        if (responsaObject != null) {
            this.responsaObject = responsaObject;
            if (fsTextAddress != null) {
                populateViewData();
            }
        }
    }

    public void setFavoritar(Boolean newFavoritarState) {
        this.favoritou = newFavoritarState;
        if (favoritarIcon != null) populateFavoritar(favoritou);
    }

    public void setMarcarPresenca(Boolean newMarcarPresencaState) {
        this.marcouPresenca = newMarcarPresencaState;
        if (marcarPresencaIcon != null) populateMarcarPresenca(marcouPresenca);
    }

    public void populateFavoritar(Boolean favoritarState) {
        if (getActivity() != null) {
            Drawable icon = FontIconDrawable.inflate(getActivity(), (favoritarState ? R.xml.ic_acao_favorito_on : R.xml.ic_acao_favorito_off));
            favoritarIcon.setCompoundDrawables(null, icon, null, null);
        }
    }

    public void populateMarcarPresenca(Boolean marcarPresencaState) {
        if (getActivity() != null) {
            Drawable icon = FontIconDrawable.inflate(getActivity(), (marcarPresencaState ? R.xml.ic_acao_euvou_on : R.xml.ic_acao_euvou_off));
            marcarPresencaIcon.setCompoundDrawables(null, icon, null, null);
        }
    }

    //Apply Gradient to reinforce elipsis
    public void applyInitialGradient(final TextView view) {
        ViewTreeObserver vto = view.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                Layout layout = view.getLayout();

                try {
                    if (layout != null && layout.getEllipsisCount(2) > 0) {
                        ResponsaUtil.applyGradient(view, getActivity());
                    }
                } catch (ArrayIndexOutOfBoundsException e) {

                }

            }
        });


    }

}
