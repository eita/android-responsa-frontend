package br.org.eita.responsa.model;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.backend_comm.CepApi;
import retrofit.Callback;
import retrofit.RestAdapter;

public class CepAddress {

    String cep;
    String logradouro;
    String complemento;
    String bairro;
    String localidade;
    String uf;
    String ibge;

    public static void buscaCep(String cep, Callback<CepAddress> cb) {
        if (cep == null) return;

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.cepServiceUrl)
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .build();

        CepApi cepApi = restAdapter.create(CepApi.class);

        String cepFormat = cep.replace(" ","").replace("-","");

        cepApi.buscaCep(cepFormat, cb);
    }

    public String toString() {
        return (this.logradouro == null ? "" : this.logradouro) + ", " + (this.localidade == null ? "" : this.localidade) + ", Brazil";
    }

    public HomeTerritory getLocation() {
        return ResponsaApplication.territoryDb.findByCityState(localidade, uf);
    }
}
