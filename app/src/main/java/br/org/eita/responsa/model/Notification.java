package br.org.eita.responsa.model;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.MainActivity;
import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.backend_comm.SearchApi;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.model.search_api.RegionContext;
import br.org.eita.responsa.model.util.ResponsaSaveCallback;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.view.activity.FichaSinteseActivity;
import br.org.eita.responsa.view.activity.TrajectoryActivity;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * To send a customMessage in dashboard:

 {
 "alert": "teste (alert) de notificação",
 "title": "teste de notificação",
 "targetView": "br.org.eita.responsa.MainActivity",
 "targetParams": "{dialogTitle: \"oi\", dialogMessage: \" visite http://consumoresponsavel.org.br \n ok? \" }",
 "pushType": "notification",
 "actionTrigger": "customMessage",
 "silent": false,
 "steps": 0
 }

 *
 */


@ParseClassName("Notification")
public class Notification extends ParseObject {

    public static JSONObject config = null;

    public Boolean getRead() {
        return getBoolean("read");
    }

    public void setRead(Boolean read) {
        if (read == null) {
            this.put("read", JSONObject.NULL);
        } else {
            this.put("read", read);
        }
    }

    public String getTarget() {
        return getString("target");
    }

    public void setTarget(String target) {
        if (target == null) {
            this.put("target", JSONObject.NULL);
        } else {
            this.put("target", target);
        }
    }

    public String getActionTrigger() {
        return getString("actionTrigger");
    }

    public void setActionTrigger(String actionTrigger) {
        if (actionTrigger == null) {
            this.put("actionTrigger", JSONObject.NULL);
        } else {
            this.put("actionTrigger", actionTrigger);
        }
    }

    public String getTitle() {
        return getString("title");
    }

    public void setTitle(String title) {
        if (title == null) {
            this.put("title", JSONObject.NULL);
        } else {
            this.put("title", title);
        }
    }

    public String getTitleChannel() {
        return getString("titleChannel");
    }

    public void setTitleChannel(String titleChannel) {
        if (titleChannel == null) {
            this.put("titleChannel", JSONObject.NULL);
        } else {
            this.put("titleChannel", titleChannel);
        }
    }

    public ParseUser getTargetUser() {
        return getParseUser("targetUser");
    }

    public void setTargetUser(ParseUser user) {
        if (user == null) {
            this.put("targetUser", JSONObject.NULL);
        } else {
            this.put("targetUser", user);
        }
    }

    public String getAction() {
        return getString("action");
    }

    public void setAction(String action) {
        if (action == null) {
            this.put("action", JSONObject.NULL);
        } else {
            this.put("action", action);
        }
    }

    public String getTargetView() {
        return getString("targetView");
    }

    public void setTargetView(String targetView) {
        if (targetView == null) {
            this.put("targetView", JSONObject.NULL);
        } else {
            this.put("targetView", targetView);
        }
    }

    public String getTargetParams() {
        return getString("targetParams");
    }

    public void setTargetParams(String targetParams) {
        if (targetParams == null) {
            this.put("targetParams", JSONObject.NULL);
        } else {
            this.put("targetParams", targetParams);
        }
    }

    public String getShortMessage() {
        return getString("shortMessage");
    }

    public void setShortMessage(String shortMessage) {
        if (shortMessage == null) {
            this.put("shortMessage", JSONObject.NULL);
        } else {
            this.put("shortMessage", shortMessage);
        }
    }

    public String getLongMessage() {
        return getString("longMessage");
    }

    public void setLongMessage(String longMessage) {
        if (longMessage == null) {
            this.put("longMessage", JSONObject.NULL);
        } else {
            this.put("longMessage", longMessage);
        }
    }

    public JSONArray getChannels() {
        return getJSONArray("channels");
    }

    public void setChannels(JSONArray channels) {
        if (channels == null) {
            this.put("channels", JSONObject.NULL);
        } else {
            this.put("channels", channels);
        }
    }

    public String getShortMessageChannel() {
        return getString("shortMessageChannel");
    }

    public void setShortMessageChannel(String shortMessageChannel) {
        if (shortMessageChannel == null) {
            this.put("shortMessageChannel", JSONObject.NULL);
        } else {
            this.put("shortMessageChannel", shortMessageChannel);
        }
    }

    public String getStepsOwner() {
        return getString("stepsOwner");
    }

    public void setStepsOwner(String stepsOwner) {
        if (stepsOwner == null) {
            this.put("stepsOwner", JSONObject.NULL);
        } else {
            this.put("stepsOwner", stepsOwner);
        }
    }

    public String getStepsChannel() {
        return getString("stepsChannel");
    }

    public void setStepsChannel(String stepsChannel) {
        if (stepsChannel == null) {
            this.put("stepsChannel", JSONObject.NULL);
        } else {
            this.put("stepsChannel", stepsChannel);
        }
    }

    public String getChannelIntent() {
        return getString("channelIntent");
    }

    public void setChannelIntent(String channelIntent) {
        if (channelIntent == null) {
            this.put("channelIntent", JSONObject.NULL);
        } else {
            this.put("channelIntent", channelIntent);
        }
    }

    public JSONObject getChannelIntentParams() {
        return getJSONObject("channelIntentParams");
    }

    public void setChannelIntentParams(JSONObject channelIntentParams) {
        if (channelIntentParams == null) {
            this.put("channelIntentParams", JSONObject.NULL);
        } else {
            this.put("channelIntentParams", channelIntentParams);
        }
    }

    public Date getLocalCreatedAt() {
        return getDate("localCreatedAt");
    }

    public void setLocalCreatedAt(Date localCreatedAt) {
        if (localCreatedAt == null) {
            this.put("localCreatedAt", JSONObject.NULL);
        } else {
            this.put("localCreatedAt", localCreatedAt);
        }
    }

    public Boolean getSilent() {
        return getBoolean("silent");
    }

    public void setSilent(Boolean silent) {
        if (silent == null) {
            this.put("silent", JSONObject.NULL);
        } else {
            this.put("silent", silent);
        }
    }

    public void openActivity(Context context) {
        Intent intent = null;
        String targetView = this.getTargetView();
        if (targetView == null || targetView.isEmpty()) return;
        try {
            intent = new Intent(context, Class.forName(this.getTargetView()));
            ResponsaUtil.log("ResponsaNotificationURI",intent.toUri(Intent.URI_ALLOW_UNSAFE));
            //intent = new Intent(context, Uri.parse(this.getTargetView()));
            JSONObject targetParams = null;
            try {
                targetParams = new JSONObject(this.getTargetParams());
                Iterator<String> keyIterator = targetParams.keys();
                String key;
                while (keyIterator.hasNext()) {
                    key = keyIterator.next();
                    intent.putExtra(key, targetParams.getString(key));
                }
                context.startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }



    public static void loadConfiguration() {
        String json = ResponsaUtil.loadJSONFromAsset("notification_config.json", ResponsaApplication.getAppContext());
        try {
            config = new JSONObject(json);
        } catch (JSONException e) {
            ResponsaUtil.log("ResponsaNotification", "Error loading notification_config.json");
            e.printStackTrace();
        }
    }

    public static void sendNotification(final Context context, final String actionTrigger, ParseObject parseObject) {
        sendNotification(context, actionTrigger, parseObject, null, null);
    }

    public static void sendNotification(final Context context, final String actionTrigger, ParseObject parseObject, final List<String> customStrings, final List<String> customStringsChannel) {
        /*
        answerChallengeChosen : {objectId: "ChallengeId"}
         */
        try {

            final JSONObject actionObject = config.getJSONObject(actionTrigger);
            final String target = actionObject.getString("target");

            final Notification notification = new Notification();

            //Define Channels
            JSONArray channelGroups = actionObject.getJSONArray("channelGroups");
            JSONArray channels = new JSONArray();

            for (int i=0; i<channelGroups.length(); i++) {
                String ch = channelGroups.getString(i);
                if ("territoryId".equals(ch)) {
                    channels.put(getChannel(TERRITORY_CHANNEL, parseObject.getString("territoryId")));
                } else if ("Challenge".equals(ch)) {
                    channels.put(getChannel(CHALLENGE_CHANNEL, parseObject.getObjectId()));
                } else if ("ResponsaObject".equals(ch)) {
                    channels.put(getChannel(RESPONSAOBJECT_CHANNEL, parseObject.getObjectId()));
                } else if ("all".equals(ch)) {
                    channels.put("all");
                }
            }
            notification.setChannels(channels);

            String targetView = null;
            String targetParams = null;

            //Define target view and params
            if ("Challenge".equals(target) || "ChallengeAnswer".equals(target)) {
                targetView = "br.org.eita.responsa.view.activity.ChallengeViewActivity";
                targetParams = "{\""+ FichaSinteseActivity.INTENT_OBJECT_ID_KEY+"\":\""+parseObject.getObjectId()+"\"}";
            } else if ("ResponsaObject".equals(target) || "ResponsaObjectComment".equals(target)) {
                targetView = "br.org.eita.responsa.view.activity.FichaSinteseActivity";
                targetParams = "{\""+ FichaSinteseActivity.INTENT_OBJECT_ID_KEY+"\":\""+parseObject.getObjectId()+"\",\""+FichaSinteseActivity.INTENT_VIEW_KEY+"\":\""+parseObject.get("type")+"\"}";
            }

            //Define Messages
            String[] messageTypes = new String[] {"shortMessage", "shortMessageChannel", "longMessage", "title", "titleChannel"};
            String[] params;
            String message;

            for (int i=0; i<messageTypes.length; i++) {
                params = prepareParamsForMessage(messageTypes[i], actionObject, parseObject, customStrings);
                /*
                String val = "";
                for (int j=0;j<params.length;j++) {
                    val += "    " + params[j];
                }
                ResponsaUtil.log("Notification","messageType: "+ messageTypes[i] + " value:" + val); */
                message = getMessage(messageTypes[i], actionTrigger, context, (String[]) params);
                if (message != null) notification.put(messageTypes[i], message);
            }

            String stepsOwner = actionObject.getString("stepsOwner");
            final Integer steps = actionObject.getInt("stepsOwner");
            String stepsChannel = actionObject.getString("stepsChannel");

            try {
                String channelIntent = actionObject.getString("channelIntent");
                notification.setChannelIntent(channelIntent);
                JSONObject intentParams = actionObject.getJSONObject("channelIntentParams");

                JSONObject processedParams = new JSONObject();
                String key;
                for (Iterator<String> intKeys = intentParams.keys(); intKeys.hasNext(); ) {
                    key = intKeys.next();
                    processedParams.put(key, prepareParam(intentParams.getString(key),actionObject, parseObject, customStringsChannel));
                }
                notification.setChannelIntentParams(processedParams);

            } catch (JSONException e) {

            }

            Boolean silent = false;

            try {
                silent = actionObject.getBoolean("silent");
            } catch (JSONException e) {

            }

            notification.setSilent(silent);
            notification.setActionTrigger(actionTrigger);
            notification.setTarget(target);
            notification.setTargetParams(targetParams);
            notification.setTargetUser(ParseUser.getCurrentUser());
            notification.setTargetView(targetView);
            notification.setStepsChannel(stepsChannel);
            notification.setStepsOwner(stepsOwner);
            notification.setLocalCreatedAt(new Date());

            ResponsaApplication.responsaDataManager.saveObject("notifications", null, notification, true, new ResponsaSaveCallback() {
                @Override
                public void done(ParseException e) {
                    ResponsaUtil.log("newdatamanagerload ResponsaNotification", "Saved Notification with target '" + target + "'");
                    addStepsToUser(ParseUser.getCurrentUser(), steps);
                }
            });

            /*
            notification.saveEventually(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    if (e == null) {
                        notification.pinInBackground(ResponsaDataManager.PINTAG_NOTIFICATIONS, new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                if (e == null) {
                                    ResponsaUtil.log("ResponsaDataManager","pinned new notification in background");
                                    ResponsaApplication.responsaDataManager.addNotificationToFrontPage(notification);
                                    addStepsToUser(ParseUser.getCurrentUser(), steps);
                                }
                            }
                        });
                    }
                }
            });
            */

        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    /**
     *
     * @param messageType
     * @param actionObject
     * @param dataObject - Objeto que contém uma propriedade "territoryId" caso um dos parâmetros inicie com "location"
     * @return
     * @throws JSONException
     */
    private static String[] prepareParamsForMessage(String messageType, JSONObject actionObject, ParseObject dataObject, List<String> customStrings) throws JSONException {
        JSONArray fields = actionObject.getJSONArray(messageType + "Fields");

        ArrayList<String> params = new ArrayList<String>();
        for (int i=0; i< fields.length(); i++) {
            params.add(prepareParam(fields.getString(i), actionObject, dataObject, customStrings));
        }

        return params.toArray(new String[]{});
    }

    private static String prepareParam(String param, JSONObject actionObject, ParseObject dataObject, List<String> customStrings) throws JSONException {
        String[] smf = param.split("_");
        String result = null;

        if ("CUSTOM".equals(smf[0])) {
            result = (smf.length>1) ? customStrings.get(Integer.parseInt(smf[1])) : customStrings.get(0);
        } else if ("Challenge".equals(smf[0])) {
            result = getMixedString(smf[1],smf,dataObject);
        } else if ("ResponsaObject".equals(smf[0])) {
            if ("dateTimeStart".equals(smf[1]) || "dateTimeFinish".equals(smf[1])) {
                SimpleDateFormat sdf = new SimpleDateFormat("EEE, d 'de' MMMM");
                result = sdf.format(dataObject.getDate(smf[1])) + ", " + ResponsaUtil.humanReadableTime(dataObject.getDate(smf[1]));
            } else {
                result = getMixedString(smf[1],smf,dataObject);
            }
        } else if (smf.length > 1 && "User".equals(smf[1])) {
            result = ParseUser.getCurrentUser().getString(smf[2]);
        } else if ("territory".equals(smf[0])) {
            HomeTerritory homeTerritory = ResponsaApplication.territoryDb.findTerritoryById(dataObject.getString("territoryId"));
            result = homeTerritory.get(smf[1]);
        } else if ("initiative".equals(smf[0])) { //Event created in initiative
            result = ((ResponsaObject) dataObject.get("placeInitiative")).getString(smf[1]);
        } else {
            result = getMixedString(smf[0], smf, actionObject);
        }

        return result;
    }

    private static String getMixedString (String dataField, String[] smf, JSONObject actionObject) throws JSONException {
        String dataType = "string";
        if (smf.length > 2) {
            dataType = smf[2];
        }

        if ("string".equals(dataType)) {
            return actionObject.getString(dataField);
        } else if ("integer".equals(dataType)) {
            return Integer.toString(actionObject.getInt(dataField));
        }
        return null;
    }

    private static String getMixedString (String dataField, String[] smf, ParseObject actionObject) throws JSONException {
        String dataType = "string";
        if (smf.length > 2) {
            dataType = smf[2];
        }

        if ("objectId".equals(dataField)) {
            return actionObject.getObjectId();
        } else if ("string".equals(dataType)) {
            return actionObject.getString(dataField);
        } else if ("integer".equals(dataType)) {
            return Integer.toString(actionObject.getInt(dataField));
        }
        return null;
    }


    private static String getMessage(String messageType, String action, Context context, String[] params) {
        int  messageResource = ResponsaUtil.getResourceIdByName("n_" + action + "_" + messageType, context);
        if (messageResource == 0) {
            return null;
        }
        return context.getString(messageResource, params);
    }

    private static String getStringResourceByName(String aString, Context c) {
        String packageName = c.getPackageName();
        int resId = c.getResources().getIdentifier(aString, "string", packageName);
        if (resId == 0) return null;
        return c.getString(resId);
    }

    public static void saveFromChannel(final JSONObject dataObject, Boolean isProvisory) {

        try {
            final Notification notification = new Notification();

            notification.setStepsOwner(dataObject.getString("steps"));
            final Integer steps = dataObject.getInt("steps");
            String targetParams = dataObject.getString("targetParams");

            notification.setTargetView(dataObject.getString("targetView"));
            notification.setTargetParams(targetParams);
            notification.setTitle(dataObject.getString("title"));
            notification.setShortMessage(dataObject.getString("alert"));
            notification.setTargetUser(ParseUser.getCurrentUser());
            notification.setLocalCreatedAt(new Date());

            try{
                notification.setSilent(dataObject.getBoolean("silent"));
            } catch (JSONException e) {
                notification.setSilent(false);
            }

            //channelIntent treatment
            /*
            try {
                if (dataObject.getString("onReceiveIntent") != null) {
                    JSONObject intentParams = dataObject.getJSONObject("onReceiveIntentParams");

                    Intent intent = new Intent(dataObject.getString("onReceiveIntent"));

                    String key;
                    for (Iterator<String> intKeys =  intentParams.keys(); intKeys.hasNext(); ) {
                        key = intKeys.next();
                        intent.putExtra(key, intentParams.getString(key));
                    }

                    ResponsaApplication.getAppContext().sendBroadcast(intent);
                }
            } catch (JSONException e) {

            }
            */

            //loads data with forceRemote
            try {

                JSONObject loadAfterUpdate = config.getJSONObject(dataObject.getString("actionTrigger")).getJSONObject("loadAfterUpdate");

                if (loadAfterUpdate != null) {
                    JSONObject targetParamsObject = new JSONObject(targetParams);

                    String callerView = loadAfterUpdate.getString("callerView");
                    String targetView = loadAfterUpdate.getString("targetView");
                    String targetViewObjectId = targetParamsObject.getString("objectId");

                    ResponsaApplication.responsaDataManager.load(callerView, targetViewObjectId, ResponsaDataManager.FORCE_REMOTE, targetView);
                }
            } catch (JSONException e) {

            }

            if (!isProvisory) {
                ResponsaApplication.responsaDataManager.saveObject("notifications", null, notification, true, new ResponsaSaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        addStepsToUser(ParseUser.getCurrentUser(), steps);
                    }
                });

            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    //TODO migrate this to MainActivity
    private static void addStepsToUser(ParseUser user, Integer steps) {
        if (steps <= 0) return;
        user.increment("steps", steps);
        user.saveEventually();

        Context context = ResponsaApplication.getAppContext();

        if (context != null && MainActivity.stepsTextView != null && MainActivity.stepsIcon != null && ResponsaApplication.RESPONSA_IS_IN_FOREGROUND) {
            final Animation anim = AnimationUtils.loadAnimation(context, R.anim.scale);
            MainActivity.stepsIcon.startAnimation(anim);
            if (user.getInt("steps") > 0) {
                MainActivity.stepsTextView.setText(String.format("%d", user.getInt("steps")));
            }
            ResponsaApplication.vibrate();
        }
    }

    public static void markAllLocalAsRead() {
        ParseQuery<Notification> query = ParseQuery.getQuery("Notification");
        query.fromLocalDatastore();
        query.findInBackground(new FindCallback<Notification>() {
            @Override
            public void done(List<Notification> list, ParseException e) {
                for (int i=0; i < list.size(); i++) {
                    list.get(i).setRead(true);
                }
                ResponsaApplication.responsaDataManager.saveList("notifications",null, list, false, null);
            }
        });


    }

    public static void sendSignupNotification(final Context context, final String territoryId) {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.searchServiceUrl)
                .setLogLevel(RestAdapter.LogLevel.BASIC)
                .build();

        SearchApi searchApi = restAdapter.create(SearchApi.class);

        searchApi.queryRegionContext(territoryId, new Callback<RegionContext>() {
            @Override
            public void success(RegionContext regionContext, Response response) {
                if (regionContext == null) return;

                try {
                    final JSONObject actionObject = config.getJSONObject("signup");

                    final Notification notification = new Notification();

                    //Define Channels
                    JSONArray channels = new JSONArray();
                    channels.put(getChannel(TERRITORY_CHANNEL, territoryId));
                    notification.setChannels(channels);


                    String targetView = "br.org.eita.responsa.MainActivity";

                    //Steps Owner: calculated based in several variables
                    final Integer stepsOwner = regionContext.calculateSteps(Integer.parseInt(actionObject.getString("stepsOwner")));
                    Integer stepsChannel = actionObject.getInt("stepsChannel");

                    HomeTerritory location = ResponsaApplication.territoryDb.findTerritoryById(territoryId);

                    String shortMessage = regionContext.getSignupShortMessage(context, location, stepsOwner);

                    //Short message channel
                    String shortMessageChannel = context.getString(R.string.n_signup_shortMessageChannel, location.title,
                            stepsChannel);

                    //Title
                    String title = context.getString(R.string.n_signup_title);

                    //Title channel
                    String titleChannel = context.getString(R.string.n_signup_titleChannel, ParseUser.getCurrentUser().get("name"));

                    notification.setShortMessage(shortMessage);
                    notification.setShortMessageChannel(shortMessageChannel);
                    notification.setTitle(title);
                    notification.setTitleChannel(titleChannel);

                    String targetParams = "{\"dialogTitle\":\""+title+"\",\"dialogMessage\":\""+shortMessage+"\"}";


                    notification.setActionTrigger("signup");
                    notification.setTarget("SignupDialog");
                    notification.setTargetParams(targetParams);
                    notification.setTargetUser(ParseUser.getCurrentUser());
                    notification.setTargetView(targetView);
                    notification.setStepsChannel(String.format("%d", stepsChannel));
                    notification.setStepsOwner(String.format("%d", stepsOwner));
                    notification.setLocalCreatedAt(new Date());

                    ResponsaApplication.responsaDataManager.saveObject("notifications", null, notification, true, new ResponsaSaveCallback() {
                        @Override
                        public void done(ParseException e) {
                            addStepsToUser(ParseUser.getCurrentUser(), stepsOwner);
                        }
                    });

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void failure(RetrofitError error) {

            }
        });

    }

    // declare channel types
    public static String TERRITORY_CHANNEL = "lo";
    public static String CITY_CHANNEL = "ct";
    public static String INITIATIVE_CHANNEL = "ro";
    public static String GATHERING_CHANNEL = "ro";
    public static String RESPONSAOBJECT_CHANNEL = "ro";
    public static String CHALLENGE_CHANNEL = "ch";

    public static String getChannel(String channelType, String id) {
        return getChannel(channelType, id, false);
    }
    public static String getChannel(String channelType, String id, Boolean isProvisory) {
        String channelName = channelType+id;
        if (isProvisory) {
            channelName += "_tmp";
        }
        return channelName;
    }
    public static void toggleChannelSubscription(String channelType, String id, Boolean isSubscription) {
        toggleChannelSubscription(channelType, id, isSubscription, false);
    }
    public static void toggleChannelSubscription(String channelType, String id, Boolean isSubscription, Boolean isProvisory) {
        String channel = getChannel(channelType, id, isProvisory);
        if (isSubscription) {
            ParsePush.subscribeInBackground(channel);
        } else {
            ParsePush.unsubscribeInBackground(channel);
        }
    }
    public static void subscribeToChannel(String channelType, String id) {
        toggleChannelSubscription(channelType, id, true, false);
    }
    public static void subscribeToChannel(String channelType, String id, boolean isProvisory) {
        toggleChannelSubscription(channelType, id, true, isProvisory);
    }
    public static void unsubscribeToChannel(String channelType, String id) {
        toggleChannelSubscription(channelType, id, false, false);
    }
    public static void unsubscribeToChannel(String channelType, String id, boolean isProvisory) {
        toggleChannelSubscription(channelType, id, false, isProvisory);
    }
    public static void cleanChannelTypeSubscriptions(String channelType) {
        List<String> subscribedChannels = ParseInstallation.getCurrentInstallation().getList("channels");
        if (subscribedChannels != null) {
            String channel;
            for(Iterator<String> i = subscribedChannels.iterator(); i.hasNext();) {
                channel = i.next();
                if (channel.substring(0,2).equals(channelType)) {
                    ParsePush.unsubscribeInBackground(channel);
                }
            }
        }
    }
}
