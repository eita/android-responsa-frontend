package br.org.eita.responsa.components;


import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;

import br.org.eita.responsa.R;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.util.ResponsaUtil;

public class InitiativeDetails extends RelativeLayout {

    TextView responsaObjectTitle;
    TextView responsaObjectAddress;
    TextView responsaObjectLocation;
    BootstrapCircleThumbnail responsaObjectImage;
    private ResponsaObject initiative;

    public InitiativeDetails(Context context) {
        this(context, null);
    }

    public InitiativeDetails(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public InitiativeDetails(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.component_initiative_details, this);

        findAllViewsByIds(view);
    }

    private void findAllViewsByIds(View parentView) {
        responsaObjectTitle = (TextView) parentView.findViewById(R.id.responsaObjectTitle);
        responsaObjectAddress = (TextView) parentView.findViewById(R.id.responsaObjectAddress);
        responsaObjectLocation = (TextView) parentView.findViewById(R.id.responsaObjectLocation);
        responsaObjectImage = (BootstrapCircleThumbnail) parentView.findViewById(R.id.responsaObjectImage);
    }

    private void populateView() {
        responsaObjectTitle.setText(this.initiative.getTitle());
        responsaObjectAddress.setText(this.initiative.getRuaENumero());
        if (this.initiative.getResponsaTerritory() != null) {
            responsaObjectLocation.setText(this.initiative.getResponsaTerritory().label);
        }

        if (this.initiative.getAddress() != null && !this.initiative.getAddress().isEmpty()) {
            responsaObjectAddress.setVisibility(VISIBLE);
        }

        ParseFile imgFile = this.initiative.getAvatarImage();
        if (imgFile != null) {
            imgFile.getDataInBackground(new GetDataCallback() {
                @Override
                public void done(byte[] bytes, ParseException e) {
                    if (e != null) {
                        ResponsaUtil.log("ImageOpenError", e.getMessage());
                    }
                    if (bytes != null) { //Problem when transfering endpoint and not yet files...
                        Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length);
                        if (bitmap != null) {
                            responsaObjectImage.setImage(bitmap);
                        }
                    }
                }
            });
        }

    }

    public void setInitiative(ResponsaObject responsaObject) {
        this.initiative = responsaObject;
        populateView();
    }
}
