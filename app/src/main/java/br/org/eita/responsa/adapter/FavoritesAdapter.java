package br.org.eita.responsa.adapter;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.parse.Parse;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.shamanland.fonticon.FontIconTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.components.VotableStatsWidget;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.InitiativeCategory;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.util.ResponsaUtil;

public class FavoritesAdapter extends ArrayAdapter<ParseObject> {

    public FavoritesAdapter(Context context) {
        super(context, 0, new ArrayList<ParseObject>());
    }

    public void populateAdapter(List list,Boolean clearList) {
        if (clearList) {
            this.clear();
        }
        this.addAll(list);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ParseObject parseObject = getItem(position);

        if ("UserResponsaObject".equals(parseObject.getClass().getSimpleName())) {

            final ResponsaObject responsaObject = (ResponsaObject) parseObject.getParseObject("responsaObject");

            if ("initiative".equals(responsaObject.getType())) {
                convertView = View.inflate(getContext(), R.layout.item_initiative, null);
            } else if ("gathering".equals(responsaObject.getType())) {
                convertView = View.inflate(getContext(), R.layout.item_gathering, null);
            }

            TextView titleView = (TextView) convertView.findViewById(R.id.responsaObjectTitle);
            titleView.setText(responsaObject.getString("title"));

            //TextView textView = (TextView) v.findViewById(R.id.iniciativaText);
            //textView.setText(responsaObject.getString("description"));

            TextView distanceView = (TextView) convertView.findViewById(R.id.responsaObjectDistance);

            if (LocationCoordinates.getVisibleLocation() == null) {
                distanceView.setVisibility(View.GONE);
            } else {
                distanceView.setText(LocationCoordinates.getHumanReadableDistance(responsaObject.getLocationCoordinates()));
            }

            TextView locationView = (TextView) convertView.findViewById(R.id.responsaObjectLocation);
            HomeTerritory loc = responsaObject.getTerritory();
            if (loc != null) {
                locationView.setText(loc.label);
            } else {
                locationView.setVisibility(View.GONE);
            }

            if ("gathering".equals(responsaObject.getType())) {
                TextView dateView = (TextView) convertView.findViewById(R.id.responsaObjectDate);
                if (responsaObject.getDateTimeStart() != null) {
                    SimpleDateFormat sdf = new SimpleDateFormat("EEE, d 'de' MMMM");
                    dateView.setText(sdf.format(responsaObject.getDateTimeStart()));
                }

                FontIconTextView fsTimeGathering = (FontIconTextView) convertView.findViewById(R.id.fsTimeGathering);
                fsTimeGathering.setText(responsaObject.getHumanReadableTime());
            }


            ParseFile imgFile = responsaObject.getAvatarImage();

            final BootstrapCircleThumbnail thumbnail = (BootstrapCircleThumbnail) convertView.findViewById(R.id.responsaObjectImage);


            if ("gathering".equals(responsaObject.getType())) {
                thumbnail.setImage(R.drawable.encontros);
            } else if (InitiativeCategory.FEIRAS.equals(responsaObject.getCategory())) {
                thumbnail.setImage(R.drawable.feiras);
            } else if (InitiativeCategory.GRUPOS_DE_CONSUMO_RESPONSAVEL.equals(responsaObject.getCategory())) {
                thumbnail.setImage(R.drawable.grupos_de_consumo_responsavel);
            } else if (InitiativeCategory.INICIATIVAS_DE_ECONOMIA_SOLIDARIA.equals(responsaObject.getCategory())) {
                thumbnail.setImage(R.drawable.iniciativas_de_economia_solidaria);
            } else if (InitiativeCategory.INICIATIVAS_DE_AGROECOLOGIA.equals(responsaObject.getCategory())) {
                thumbnail.setImage(R.drawable.iniciativas_de_agroecologia);
            } else if (InitiativeCategory.RESTAURANTES.equals(responsaObject.getCategory())) {
                thumbnail.setImage(R.drawable.restaurantes);
            } else {
                thumbnail.setImage(R.drawable.avatar_responsa);
            }
            //}

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ResponsaApplication.responsaViewManager.openFichaSintese(getContext(),responsaObject);
                }
            });

        } else {

            final Challenge challenge = (Challenge) parseObject.getParseObject("challenge");

            // Check if an existing view is being reused, otherwise inflate the view
            //if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.challenge_item_view, parent, false);
            //}

            TextView challengeTitle = (TextView) convertView.findViewById(R.id.challengeTitle);
            TextView challengeCreatedAt = (TextView) convertView.findViewById(R.id.challengeCreatedAt);
            TextView challengeDescription = (TextView) convertView.findViewById(R.id.challengeDescription);
            TextView challengeAuthor = (TextView) convertView.findViewById(R.id.challengeAuthor);
            VotableStatsWidget votableStatsWidget = (VotableStatsWidget) convertView.findViewById(R.id.votableStatsWidget);

            String authorLocation = challenge.getParseObject("author").getString("name");
            String territoryId = challenge.getString("territoryId");
            if (territoryId != null) {
                HomeTerritory location = ResponsaApplication.territoryDb.findTerritoryById(territoryId);
                if (location != null) {
                    authorLocation += " " + getContext().getString(R.string.in_location,location.label);
                }
            }

            challengeTitle.setText(challenge.getString("title"));

            challengeDescription.setText(challenge.getString("description"));

            if (challenge.getDate("createdAt") != null) {
                challengeCreatedAt.setText(DateUtils.getRelativeTimeSpanString(ResponsaUtil.getDateCurrentTimeZone(challenge.getDate("createdAt").getTime()), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS));
                //challengeCreatedAt.setText(ResponsaUtil.getDateCurrentTimeZone(summary.createdAt.getTime() / 1000));
            } else {
                challengeCreatedAt.setVisibility(View.GONE);
            }
            challengeAuthor.setText(authorLocation);

            votableStatsWidget.setVotableAnswerable(challenge);

            FontIconTextView challengeSelectedAnswer = (FontIconTextView) convertView.findViewById(R.id.challengeSelectedAnswer);
            if (challenge.getCorrectAnswer() != null) {
                challengeSelectedAnswer.setText(challenge.getCorrectAnswer().getString("description"));
            } else {
                challengeSelectedAnswer.setVisibility(View.GONE);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ResponsaApplication.responsaViewManager.openChallengeView(getContext(), challenge.getObjectId());
                }
            });
        }


        return convertView;
    }
}
