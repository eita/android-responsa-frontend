package br.org.eita.responsa.model.util;

import com.parse.ParseException;

import java.util.List;

public interface ResponsaSaveCallback {
    public void done(ParseException e);

}
