package br.org.eita.responsa.view.activity;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.TrajectoryAdapter;
import br.org.eita.responsa.adapter.data_manager.QueryResult;
import br.org.eita.responsa.components.LoadMoreComponent;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.util.ResponsaUtil;

public class TrajectoryActivity extends BaseActivity{

    private Boolean firstPopulate = null;

    ListView trajectoryListView;
    private LoadMoreComponent loadMoreComponent = null;

    BroadcastReceiver updatesReceiver;

    TrajectoryAdapter adapter;

    Toolbar rToolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_trajectory);
        firstPopulate = true;

        rToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(rToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.action_trajectory));


        // Declaração do commentsListView
        trajectoryListView = (ListView) findViewById(R.id.trajectoryListView);

        LayoutInflater inflater = getLayoutInflater();

        View padding = inflater.inflate(R.layout.padding, trajectoryListView, false);
        trajectoryListView.addHeaderView(padding);

        TextView header = (TextView) inflater.inflate(R.layout.list_title, null);
        header.setText(R.string.u_my_trajectory);
        trajectoryListView.addHeaderView(header);

        final TrajectoryActivity self = this;

        final TrajectoryAdapter adapter = new TrajectoryAdapter(this);

        loadMoreComponent = new LoadMoreComponent(this, null, R.layout.progress_load_more);
        loadMoreComponent.initParams("trajectory", null);
        trajectoryListView.addFooterView(loadMoreComponent);

        trajectoryListView.setAdapter(adapter);


        updatesReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                QueryResult.MetaData metaData = ResponsaApplication.responsaDataManager.retrieveLoadedMetaData("trajectory", null);
                adapter.refresh();

                loadMoreComponent.setPage(metaData.page);
                if (ResponsaApplication.responsaDataManager.hasMore("trajectory", null)) {
                    loadMoreComponent.showBtnLoadMore();
                } else {
                    loadMoreComponent.stopProgressBar();
                    loadMoreComponent.hideBtnLoadMore();
                }

                if (firstPopulate) {
                    firstPopulate = false;
                }
            }
        };
        this.registerReceiver(updatesReceiver, new IntentFilter(ResponsaDataManager.getIntentDataUpdatedName("trajectory")));

        ResponsaApplication.responsaDataManager.load("trajectory", null, ResponsaDataManager.FORCE_LOCAL);
    }

    @Override
    protected void onDestroy() {
        this.unregisterReceiver(updatesReceiver);

        super.onDestroy();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        rToolbar.inflateMenu(R.menu.menu_feedback);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_feedback:
                intent = new Intent(this, FeedbackActivity.class);
                intent.putExtra("view", "trajectory");
                intent.putExtra("viewObjectId",(String)null);
                intent.putExtra("viewClass",getClass().getSimpleName());
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}

