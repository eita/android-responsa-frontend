package br.org.eita.responsa.components;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.LightingColorFilter;
import android.graphics.PorterDuff;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;
import android.widget.Toast;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import github.ankushsachdeva.emojicon.EmojiconEditText;
import github.ankushsachdeva.emojicon.EmojiconGridView;
import github.ankushsachdeva.emojicon.EmojiconsPopup;
import github.ankushsachdeva.emojicon.emoji.Emojicon;

import android.view.inputmethod.InputMethodManager;

public class EmojiconKeyboard extends RelativeLayout{

    private OnEmojiKeyboardSubmitCallback onEmojiKeyboardSubmitCallback;
    private CameraChooserDialog cameraChooserDialog;
    private RelativeLayout loadingPanel;
    private View rootView;
    private ProgressBar emojiProgressBar;

    private Boolean photoEnabled = true;
    private Boolean enabled = true;
    private EmojiconEditText emojiconEditText;

    private ImageView cameraSubmitButton;

    public EmojiconKeyboard(Context context) {
        this(context, null);
    }

    public EmojiconKeyboard(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public EmojiconKeyboard(final Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        inflater.inflate(R.layout.emoji_keyboard, this);

        emojiconEditText = (EmojiconEditText) findViewById(R.id.emojicon_edit_text);
        rootView = findViewById(R.id.emoji_keyboard_root);
        final ImageView emojiButton = (ImageView) findViewById(R.id.emoji_btn);
        cameraSubmitButton = (ImageView) findViewById(R.id.camera_and_submit);
        loadingPanel = (RelativeLayout) findViewById(R.id.loadingPanel);
        emojiProgressBar = (ProgressBar) findViewById(R.id.emojiProgressBar);

        // Give the topmost view of your activity layout hierarchy. This will be used to measure soft keyboard height
        final EmojiconsPopup popup = new EmojiconsPopup(rootView, context);

        //Will automatically set size according to the soft keyboard size
        popup.setSizeForSoftKeyboard();

        //If the emoji popup is dismissed, change emojiButton to smiley icon
        popup.setOnDismissListener(new PopupWindow.OnDismissListener() {

            @Override
            public void onDismiss() {
                changeEmojiKeyboardIcon(emojiButton, R.drawable.smiley);
            }
        });

        //If the text keyboard closes, also dismiss the emoji popup
        popup.setOnSoftKeyboardOpenCloseListener(new EmojiconsPopup.OnSoftKeyboardOpenCloseListener() {

            @Override
            public void onKeyboardOpen(int keyBoardHeight) {

            }

            @Override
            public void onKeyboardClose() {
                if (popup.isShowing())
                    popup.dismiss();
            }
        });

        //On emoji clicked, add it to edittext
        popup.setOnEmojiconClickedListener(new EmojiconGridView.OnEmojiconClickedListener() {

            @Override
            public void onEmojiconClicked(Emojicon emojicon) {
                if (emojiconEditText == null || emojicon == null) {
                    return;
                }

                int start = emojiconEditText.getSelectionStart();
                int end = emojiconEditText.getSelectionEnd();
                if (start < 0) {
                    emojiconEditText.append(emojicon.getEmoji());
                } else {
                    emojiconEditText.getText().replace(Math.min(start, end),
                            Math.max(start, end), emojicon.getEmoji(), 0,
                            emojicon.getEmoji().length());
                }
            }
        });

        //On backspace clicked, emulate the KEYCODE_DEL key event
        popup.setOnEmojiconBackspaceClickedListener(new EmojiconsPopup.OnEmojiconBackspaceClickedListener() {

            @Override
            public void onEmojiconBackspaceClicked(View v) {
                KeyEvent event = new KeyEvent(
                        0, 0, 0, KeyEvent.KEYCODE_DEL, 0, 0, 0, 0, KeyEvent.KEYCODE_ENDCALL);
                emojiconEditText.dispatchKeyEvent(event);
            }
        });

        // To toggle between text keyboard and emoji keyboard keyboard(Popup)
        emojiButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {

                //If popup is not showing => emoji keyboard is not visible, we need to show it
                if(!popup.isShowing()){

                    //If keyboard is visible, simply show the emoji popup
                    if(popup.isKeyBoardOpen()){
                        popup.showAtBottom();
                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
                    }

                    //else, open the text keyboard first and immediately after that show the emoji popup
                    else{
                        emojiconEditText.setFocusableInTouchMode(true);
                        emojiconEditText.requestFocus();
                        popup.showAtBottomPending();
                        final InputMethodManager inputMethodManager = (InputMethodManager) context.getSystemService(Context.INPUT_METHOD_SERVICE);
                        inputMethodManager.showSoftInput(emojiconEditText, InputMethodManager.SHOW_IMPLICIT);
                        changeEmojiKeyboardIcon(emojiButton, R.drawable.ic_action_keyboard);
                    }
                }

                //If popup is showing, simply dismiss it to show the undelying text keyboard
                else{
                    popup.dismiss();
                }
            }
        });

        emojiconEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                if (photoEnabled && (charSequence == null || charSequence.length() == 0)) {
                    //Icone de câmera
                    cameraSubmitButton.setImageResource(R.drawable.ic_action_camera);
                } else {
                    //Icone de onSubmit
                    cameraSubmitButton.setImageResource(R.drawable.ic_action_send_now);
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        //On onSubmit, add the edittext text to listview and clear the edittext
        cameraSubmitButton.setOnClickListener(new OnClickListener() {

            @Override
            public void onClick(View v) {
                String newText = emojiconEditText.getText().toString();

                if (newText.isEmpty()) {
                    //Camera
                    if (photoEnabled) {
                        if (!ResponsaApplication.isNetworkAvailable(getContext())) {
                            Toast.makeText(getContext(),R.string.ur_internet_unavailable,Toast.LENGTH_SHORT).show();
                        } else if (cameraChooserDialog != null) {
                            cameraChooserDialog.openImageDialog(getContext());
                        }
                    }
                } else {
                    //Submit
                    if (onEmojiKeyboardSubmitCallback != null) {
                        onEmojiKeyboardSubmitCallback.onSubmit(newText);
                    }
                }
            }
        });
    }

    public void setOnSubmitListener(OnEmojiKeyboardSubmitCallback onEmojiKeyboardSubmitCallback) {
        this.onEmojiKeyboardSubmitCallback = onEmojiKeyboardSubmitCallback;
    }

    public void setCameraChooserDialog(CameraChooserDialog cameraChooserDialog) {
        this.cameraChooserDialog = cameraChooserDialog;
    }

    public void setActivityForCameraDialogResult(Activity resultActivity, CameraChooserDialog.CameraDialogReceiver receiver) {
        this.cameraChooserDialog = new CameraChooserDialog(resultActivity, receiver);
    }

    public void handleCameraResultIntent(Intent data) {
        this.cameraChooserDialog.handleActivityResult(data);
    }

    public void startProgressBar() {
        RelativeLayout.LayoutParams loadingParams = (RelativeLayout.LayoutParams) loadingPanel.getLayoutParams();

        // Changes the height and width to the specified *pixels*
        loadingParams.height = rootView.getHeight();
        loadingPanel.setLayoutParams(loadingParams);
        loadingPanel.setVisibility(VISIBLE);
        loadingPanel.requestLayout();
        emojiProgressBar.setIndeterminate(true);
        emojiProgressBar.setVisibility(VISIBLE);

    }

    public void stopProgressBar() {
        loadingPanel.setVisibility(GONE);
        emojiProgressBar.setIndeterminate(false);
        emojiProgressBar.setVisibility(GONE);
    }

    public void setPhotoEnabled(Boolean photoEnabled) {
        this.photoEnabled = photoEnabled;
        if (photoEnabled) {
            //Icone de câmera
            cameraSubmitButton.setImageResource(R.drawable.ic_action_camera);
        } else {
            //Icone de onSubmit
            cameraSubmitButton.setImageResource(R.drawable.ic_action_send_now);
        }
    }

    public void setHint(String hint) {
        emojiconEditText.setHint(hint);
    }

    public void setEmojiKeyboardEnabled(Boolean enabled) {
        this.enabled = enabled;
        this.setVisibility(enabled ? VISIBLE : GONE);
    }

    public void setText(String text) {
        emojiconEditText.setText(text);
    }

    public interface OnEmojiKeyboardSubmitCallback {
        public void onSubmit(String text);
    }

    private void changeEmojiKeyboardIcon(ImageView iconToBeChanged, int drawableResourceId){
        iconToBeChanged.setImageResource(drawableResourceId);
    }

    public void clearInput() {
        emojiconEditText.getText().clear();
    }

}