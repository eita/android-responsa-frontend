package br.org.eita.responsa.model.util;


import java.util.List;

import br.org.eita.responsa.model.search_api.ChallengeFrontPageIds;
import retrofit.client.Response;

public interface LoadChallengesCallback {
    public void done(List<String> challengeIds);
}
