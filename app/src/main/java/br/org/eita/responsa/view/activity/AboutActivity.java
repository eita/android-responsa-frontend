package br.org.eita.responsa.view.activity;


import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;

import br.org.eita.responsa.R;
import br.org.eita.responsa.view.fragment.AboutFragment;
import br.org.eita.responsa.view.fragment.FeedbackFormFragment;

public class AboutActivity extends BaseActivity {

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        Toolbar rToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(rToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.action_about));

        AboutFragment aboutFragment = new AboutFragment();

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, aboutFragment)
                .commit();

    }
}
