package br.org.eita.responsa.util;

import android.app.Dialog;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.LinearGradient;
import android.graphics.Matrix;
import android.graphics.Shader;
import android.media.ExifInterface;
import android.os.Build;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.view.ViewTreeObserver;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.IOException;
import java.io.InputStream;
import java.text.DateFormat;
import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;

public class ResponsaUtil {

    //true if DAY differ between two dates
    public static boolean daysDiffer(Date date1, Date date2) {
        if (date1 == null && date2 != null) return true;
        if (date1 != null && date2 == null) return true;
        if (date1 == null && date2 == null) return false;

        //Check if both are in the same day
        Calendar c1 = Calendar.getInstance();
        c1.setTime(date1);

        Calendar c2 = Calendar.getInstance();
        c2.setTime(date2);

        if (c1.get(Calendar.YEAR) == c2.get(Calendar.YEAR)
                && c1.get(Calendar.DAY_OF_YEAR) == c2.get(Calendar.DAY_OF_YEAR)) {
            return false;
        }
        return true;
    }

    public static Calendar getDatePart(Date date){
        Calendar cal = Calendar.getInstance();       // get calendar instance
        cal.setTime(date);
        cal.set(Calendar.HOUR_OF_DAY, 0);            // set hour to midnight
        cal.set(Calendar.MINUTE, 0);                 // set minute in hour
        cal.set(Calendar.SECOND, 0);                 // set second in minute
        cal.set(Calendar.MILLISECOND, 0);            // set millisecond in second

        return cal;                                  // return the date part
    }

    public static long daysBetween(Date startDate, Date endDate) {
        Calendar sDate = getDatePart(startDate);
        Calendar eDate = getDatePart(endDate);

        long daysBetween = 0;
        while (sDate.before(eDate)) {
            sDate.add(Calendar.DAY_OF_MONTH, 1);
            daysBetween++;
        }
        return daysBetween;
    }

    public static Date fromJsonDate(String jsonDate) {
        SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSS'Z'", Locale.US);
        return fmt.parse(jsonDate, new ParsePosition(0));
    }

    public static Long getDateCurrentTimeZone(long timestamp) {
        try{
            Calendar calendar = Calendar.getInstance();
            TimeZone tz = TimeZone.getDefault();
            calendar.setTimeInMillis(timestamp);
            calendar.add(Calendar.MILLISECOND, tz.getOffset(calendar.getTimeInMillis()));
            return  calendar.getTime().getTime();
        }catch (Exception e) {
        }
        return null;
    }

    public static String loadJSONFromAsset(String fileName, Context c) {
        String json = null;
        try {

            InputStream is = c.getAssets().open(fileName);

            int size = is.available();

            byte[] buffer = new byte[size];

            is.read(buffer);

            is.close();

            json = new String(buffer, "UTF-8");


        } catch (IOException ex) {
            ex.printStackTrace();
            return null;
        }
        return json;

    }

    public static String humanReadableTime(Date inputDate) {
        StringBuffer timeStr = new StringBuffer();

        if (inputDate != null) {
            SimpleDateFormat sdf = new SimpleDateFormat("H'h'mm");
            timeStr.append(sdf.format(inputDate).replace("h00","h"));
        }

        return timeStr.toString();
    }

    public static String humanFormatInteger(Integer steps) {
        if (steps == null)
            return "0";

        Float st = steps.floatValue();
        if (st/1000 < 1)
            return String.format("%d",steps);
        else {
            return (String.format("%.1f",(st/1000)) + "mil").replace(".",",").replace(",0","");
        }
    }

    /**
     * Equals, but works also if objects are null
     * @param a
     * @param b
     * @return
     */
    public static final boolean equalsN(Object a, Object b) {
        if (a==b) return true;
        if ((a==null)||(b==null)) return false;
        return a.equals(b);
    }

    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {

        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }
        try {
            Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
            bitmap.recycle();
            return bmRotated;
        }
        catch (OutOfMemoryError e) {
            e.printStackTrace();
            ResponsaUtil.log("Bitmap handler out of memory error", e.toString());
            return null;
        }
    }

    public static int getImageExifOrientation(String path) {
        ExifInterface exif = null;
        try {
            exif = new ExifInterface(path);
        } catch (IOException e) {
            e.printStackTrace();
        }
        return exif.getAttributeInt(ExifInterface.TAG_ORIENTATION,
                ExifInterface.ORIENTATION_UNDEFINED);
    }

    public static String humanReadableTimeForEvent(Date dateTimeStart, Date dateTimeFinish) {
        Date startDate = dateTimeStart;
        Date finishDate = dateTimeFinish;
        StringBuffer resultStr = new StringBuffer();

        if (startDate == null || finishDate == null) return "";

        long dateDiff = finishDate.getTime() - startDate.getTime();

        Calendar cal = Calendar.getInstance();

        if (dateDiff > 0 && dateDiff < 86400000L) { /* 24 hours */
            resultStr.append(ResponsaUtil.humanReadableTime(startDate));
            resultStr.append(" às ");
            resultStr.append(ResponsaUtil.humanReadableTime(finishDate));
        } else {
            resultStr.append(ResponsaApplication.getAppContext().getString(R.string.start_at_time, ResponsaUtil.humanReadableTime(startDate)));
        }

        return resultStr.toString();
    }

    public static String dateToSqlFormat(Date inputDate) {
        if (inputDate == null) return  null;
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return df.format(inputDate);
    }

    public static int getResourceIdByName(String aString, Context c) {
        String packageName = c.getPackageName();
        return c.getResources().getIdentifier(aString, "string", packageName);
    }

    public static void log(String title, String msg) {
        if (Constants.DEBUG) {
            Log.i(title, msg);
        }
    }

    public static void removeOnGlobalLayoutListener(View v, ViewTreeObserver.OnGlobalLayoutListener listener){
        if (Build.VERSION.SDK_INT < 16) {
            v.getViewTreeObserver().removeGlobalOnLayoutListener(listener);
        } else {
            v.getViewTreeObserver().removeOnGlobalLayoutListener(listener);
        }
    }

    public static int dpToPx(int dp, Context context) {
        DisplayMetrics displayMetrics = context.getResources().getDisplayMetrics();
        int px = Math.round(dp * (displayMetrics.xdpi / DisplayMetrics.DENSITY_DEFAULT));
        return px;
    }

    //Apply Gradient to reinforce elipsis
    public static void applyGradient(TextView view, Context context) {
        Shader textShader=new LinearGradient(0, ResponsaUtil.dpToPx(52,context), 0, ResponsaUtil.dpToPx(82,context),
                new int[]{Color.BLACK,Color.WHITE},
                new float[]{0, 1}, Shader.TileMode.CLAMP);
        ((TextView) view).getPaint().setShader(textShader);
    }

    public static void resetGradient(TextView view) {
        Shader textShader=new LinearGradient(0, 0, 0, 1,
                new int[]{Color.BLACK,Color.BLACK},
                new float[]{0, 1}, Shader.TileMode.CLAMP);
        ((TextView) view).getPaint().setShader(textShader);
    }

    public static Float daysFromNow(Date referenceDate) {
        Float countDaysFromNow = ((Long)((referenceDate.getTime() - (new Date()).getTime()))).floatValue()/(24*3600*1000);
        return countDaysFromNow;
    }

    //Ellipsis
    // http://stackoverflow.com/questions/3597550/ideal-method-to-truncate-a-string-with-ellipsis
    private final static String NON_THIN = "[^iIl1\\.,']";

    private static int textWidth(String str) {
        return (int) (str.length() - str.replaceAll(NON_THIN, "").length() / 2);
    }

    public static String ellipsize(String text, int max) {

        if (textWidth(text) <= max)
            return text;

        // Start by chopping off at the word before max
        // This is an over-approximation due to thin-characters...
        int end = text.lastIndexOf(' ', max - 3);

        // Just one long word. Chop it off.
        if (end == -1)
            return text.substring(0, max-3) + "...";

        // Step forward as long as textWidth allows.
        int newEnd = end;
        do {
            end = newEnd;
            newEnd = text.indexOf(' ', end + 1);

            // No more spaces.
            if (newEnd == -1)
                newEnd = text.length();

        } while (textWidth(text.substring(0, newEnd) + "...") < max);

        return text.substring(0, end) + "...";
    }
}
