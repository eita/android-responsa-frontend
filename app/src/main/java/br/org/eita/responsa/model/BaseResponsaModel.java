package br.org.eita.responsa.model;

import com.parse.ParseException;
import com.parse.ParseObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import br.org.eita.responsa.util.ResponsaUtil;

public class BaseResponsaModel extends ParseObject {
    HashMap<String, ArrayList<OnUpdateValueCallback>> updateCallbacks = new HashMap<String, ArrayList<OnUpdateValueCallback>>();

    public void addUpdateCallback(String field, OnUpdateValueCallback callback) {
        if (updateCallbacks.get(field) == null) {
            updateCallbacks.put(field, new ArrayList<OnUpdateValueCallback>());
        }

        updateCallbacks.get(field).add(callback);

        callback.onUpdate(get(field));
    }

    public void removeUpdateCallback(String field, OnUpdateValueCallback callback) {
        if (updateCallbacks.get(field) == null) return;

        updateCallbacks.get(field).remove(callback);
    }

    @Override
    public void put(String key, Object value) {
        Object oldValue = get(key);
        super.put(key, value);

        if (!ResponsaUtil.equalsN(oldValue, value)) {
            ArrayList<OnUpdateValueCallback> cbs = updateCallbacks.get(key);
            if (cbs != null) {
                for (int i = 0; i < cbs.size(); i++) {
                    cbs.get(i).onUpdate(value);
                }
            }
        }
    }

    @Override
    public void increment(String key) {
        super.increment(key);

        Integer newValue = getInt(key);
        ArrayList<OnUpdateValueCallback> cbs = updateCallbacks.get(key);
        if (cbs != null) {
            for (int i = 0; i < cbs.size(); i++) {
                cbs.get(i).onUpdate(newValue);
            }
        }
    }

    @Override
    public void increment(String key, Number amount) {
        super.increment(key, amount);

        Integer newValue = getInt(key);
        ArrayList<OnUpdateValueCallback> cbs = updateCallbacks.get(key);
        if (cbs != null) {
            for (int i = 0; i < cbs.size(); i++) {
                cbs.get(i).onUpdate(newValue);
            }
        }
    }

}
