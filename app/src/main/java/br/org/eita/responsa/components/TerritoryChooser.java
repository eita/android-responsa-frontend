package br.org.eita.responsa.components;


import android.content.Context;
import android.content.res.TypedArray;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.FrameLayout;
import android.widget.RelativeLayout;

import java.util.ArrayList;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.ResponsaTerritoryAutocompleteAdapter;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.validation.ValidationError;
import br.org.eita.responsa.storage.queries.TerritoryDb;
import br.org.eita.responsa.util.ResponsaUtil;

public class TerritoryChooser extends FrameLayout {

    TerritoryDb territoryDb;

    // adapter and view of cities auto-complete
    ResponsaTerritoryAutocompleteAdapter cityAutocompleteAdapter;
    ArrayList<HomeTerritory> cities = new ArrayList<HomeTerritory>();
    AutoCompleteTextView autoCompleteTextView;

    // views related to the sub-city-region (district or neighboorhood)
    SubCityRegionSpinner subCityRegionSpinnerView;
    RelativeLayout subCityRegionWrapperView;

    HomeTerritory selectedSubCityRegion = null;
    HomeTerritory selectedCity = null;

    HomeTerritory selectedTerritory = null;

    private OnChangeTerritoryListener onChangeTerritoryListener;

    private static Boolean changingLabelOnly = false;

    public TerritoryChooser(Context context) {
        this(context, null);
    }

    public TerritoryChooser(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public TerritoryChooser(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.territory_chooser, this);

        territoryDb = new TerritoryDb(context);

        //Inputs
        autoCompleteTextView = (AutoCompleteTextView) view.findViewById(R.id.territoryAutoCompleteTextView);
        subCityRegionSpinnerView = (SubCityRegionSpinner) view.findViewById(R.id.subCityRegionSpinner);

        //Wrapper (shown/hidden when territory is chosen/unchosen)
        subCityRegionWrapperView = (RelativeLayout) view.findViewById(R.id.subCityRegionWrapper);

        //Initial spinner populate: empty array of cities
        cityAutocompleteAdapter = new ResponsaTerritoryAutocompleteAdapter(getContext(), R.layout.simple_autocomplete_line, cities);
        autoCompleteTextView.setAdapter(cityAutocompleteAdapter);

        // When city is chosen by the user (autocomplete):
        autoCompleteTextView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                if (i >= 0 && cities != null && cities.size() > i) {
                    setSelectedTerritory(cities.get(i), false);
                } else {
                    setSelectedTerritory(null, false);
                }
            }
        });

        // When autocomplete has text changed, unset selected city
        autoCompleteTextView.addTextChangedListener(new CityAutoCompleteTextWatcher());

        // Behaviour when territory is chosen by the user (Spinner)
        SubCityRegionSpinner.subCityRegionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                setSelectedTerritory(SubCityRegionSpinner.subCityRegionSpinnerAdapter.territories.get(i), false);
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.TerritoryChooser);
        String hint = a.getString(R.styleable.TerritoryChooser_hint);
        if (hint != null) {
            autoCompleteTextView.setHint(hint);
        }

        ///------------

    }

    public void setSelectedTerritory(HomeTerritory selection) {
        setSelectedTerritory(selection, true);
    }

    private void setSelectedTerritory(HomeTerritory selection, Boolean forceSetSelected) {
        if (selection==null || "city".equals(selection.type)) {
            setSelectedCity(selection);
        } else if ("subcity_region".equals(selection.type)) {
            if (selectedCity==null) {
                setSelectedCity(selection.getCity());
            }
            setSelectedSubCityRegion(selection, forceSetSelected);
        }
    }

    public HomeTerritory getSelectedTerritory() {
        return selectedTerritory;
    }

    public ArrayList<ValidationError> validate() {
        ArrayList<ValidationError> errors = new ArrayList<ValidationError>();
        if (selectedTerritory == null) {
            errors.add(new ValidationError("Error choosing territory", ResponsaApplication.getAppContext().getString(R.string.v_territory_chooser_none_selected)));
        }
        return errors;
    }

    private void setSelectedCity(HomeTerritory newSelectedCity) {
        //City changed
        if (!ResponsaUtil.equalsN(newSelectedCity, selectedCity)) {
            selectedCity = newSelectedCity;
            changingLabelOnly = true;
            autoCompleteTextView.setText(selectedCity == null ? "" : selectedCity.label);
            changingLabelOnly = false;
            Boolean autoSelectedTerritory = false;
            if (selectedCity !=null) {
                SubCityRegionSpinner.refreshSubCityRegionSpinnerAdapter(selectedCity);
                autoSelectedTerritory = (SubCityRegionSpinner.subCityRegionSpinner.getCount() == 1);
                if (autoSelectedTerritory) {
                    setSelectedTerritory(SubCityRegionSpinner.subCityRegionSpinnerAdapter.getItem(0));
                }
            } else {
                setSelectedSubCityRegion(null, false);
            }
            subCityRegionWrapperView.setVisibility((newSelectedCity == null) || (autoSelectedTerritory)
                            ? GONE
                            : VISIBLE
            );
        }
    }

    public void focus() {
        //autoCompleteTextView.requestFocus();
        InputMethodManager imm = (InputMethodManager) getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);
    }


    public interface OnChangeTerritoryListener {
        public void onChange(HomeTerritory territory);
    }

    public void onChangeSelectedTerritory(OnChangeTerritoryListener listener) {
        this.onChangeTerritoryListener = listener;
    }

    private void setSelectedSubCityRegion(HomeTerritory newSubCityRegion, Boolean forceSetSelected) {
        if (!ResponsaUtil.equalsN(newSubCityRegion,selectedSubCityRegion)) {
            selectedSubCityRegion = newSubCityRegion;
            selectedTerritory = newSubCityRegion;
            if (onChangeTerritoryListener != null) {
                onChangeTerritoryListener.onChange(selectedTerritory);
            }
            if (forceSetSelected) {
                SubCityRegionSpinner.SetSelectedSubCityRegion(selectedTerritory);
            }
        }
    }

    public void setEditable(Boolean editable) {
        autoCompleteTextView.setEnabled(editable);
        SubCityRegionSpinner.subCityRegionSpinner.setEnabled(editable);
    }

    public void setVisible(Boolean visible) {
        Integer visibility = (visible) ? View.VISIBLE : View.GONE;
        autoCompleteTextView.setVisibility(visibility);
        SubCityRegionSpinner.subCityRegionSpinner.setVisibility(visibility);
    }


    public class CityAutoCompleteTextWatcher implements TextWatcher {

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
            if (changingLabelOnly) return;
            // query the database based on the user input
            for (HomeTerritory territory: cities) {
                if (territory.label.equals(charSequence.toString())) {
                    return;
                }
            }

            setSelectedTerritory(null, false);

            cities = territoryDb.queryCitiesByName(charSequence.toString());

            // update the adapater
            cityAutocompleteAdapter.notifyDataSetChanged();
            cityAutocompleteAdapter = new ResponsaTerritoryAutocompleteAdapter(getContext(), R.layout.simple_autocomplete_line , cities);
            autoCompleteTextView.setAdapter(cityAutocompleteAdapter);
        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    }




}
