package br.org.eita.responsa.model;

import com.parse.Parse;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONObject;

import java.util.Date;

@ParseClassName("ChallengeUserJoin")
public class ChallengeUserJoin extends ParseObject {

    public ParseUser getUser() {
        return getParseUser("user");
    }

    public void setUser(ParseUser user) {
        if (user == null) {
            this.put("user", JSONObject.NULL);
        } else {
            this.put("user", user);
        }
    }

    public ParseObject getChallenge() {
        return getParseObject("challenge");
    }

    public void setChallenge(Challenge challenge) {
        if (challenge == null) {
            this.put("challenge", JSONObject.NULL);
        } else {
            this.put("challenge", challenge);
        }
    }

    public ParseObject getChallengeAnswer() {
        return getParseObject("challengeAnswer");
    }

    public void setChallengeAnswer(ChallengeAnswer challengeAnswer) {
        if (challengeAnswer == null) {
            this.put("challengeAnswer", JSONObject.NULL);
        } else {
            this.put("challengeAnswer", challengeAnswer);
        }
    }

    public Integer getVote() {
        return getInt("vote");
    }

    public void setVote(Integer vote) {
        if (vote == null) {
            this.put("vote", JSONObject.NULL);
        } else {
            this.put("vote", vote);
        }
    }

    public Boolean getFavorite() {
        return getBoolean("favorite");
    }

    public void setFavorite(Boolean favorite) {
        if (favorite == null) {
            this.put("favorite", JSONObject.NULL);
        } else {
            this.put("favorite", favorite);
        }
        Notification.toggleChannelSubscription(Notification.CHALLENGE_CHANNEL, getChallenge().getObjectId(), favorite);
    }

    public Date getFavoriteChangedAt() {
        return getDate("favoriteChangedAt");
    }

    public void setFavoriteChangedAt(Date favoriteChangedAt) {
        if (favoriteChangedAt == null) {
            this.put("favoriteChangedAt", JSONObject.NULL);
        } else {
            this.put("favoriteChangedAt", favoriteChangedAt);
        }
    }
}
