package br.org.eita.responsa.adapter;

import android.content.Context;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Date;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.Notification;

public class TrajectoryAdapter extends ArrayAdapter<Notification> {

    public TrajectoryAdapter(Context context) {
        super(context, 0, new ArrayList<Notification>());

        refresh();
    }

    public void refresh() {
        clear();
        addAll(ResponsaApplication.responsaDataManager.retrieveLoadedData("trajectory", null));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Notification notification = getItem(position);

        if (convertView == null) {
            convertView = View.inflate(getContext(), R.layout.item_notification_trajectory, null);
        }

        TextView titleView = (TextView) convertView.findViewById(R.id.notificationTitle);
        titleView.setText(notification.getString("title"));

        TextView createdAtView = (TextView) convertView.findViewById(R.id.notificationDate);
        Date createdAt = notification.getCreatedAt();
        String createdAtText = (createdAt != null) ? DateUtils.getRelativeTimeSpanString(notification.getCreatedAt().getTime()).toString() : "";
        createdAtView.setText(createdAtText);

        TextView stepsNumber = (TextView) convertView.findViewById(R.id.stepsNumber);
        stepsNumber.setText(notification.getStepsOwner());


        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                notification.openActivity(getContext());
            }
        });

        return convertView;
    }
}