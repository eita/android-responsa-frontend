package br.org.eita.responsa.view.activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;

import java.util.Calendar;

import br.org.eita.responsa.R;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.view.fragment.ResponsaObjectFormFragment;


public class ResponsaObjectFormActivity extends BaseActivity {

    private String responsaObjectType;
    private String objectId;
    private Boolean cleanPlaceInitiativeOnClose = false;

    public static ResponsaObject transientResponsaObject;
    ResponsaObject placeInitiative;

    Toolbar rToolbar;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        rToolbar.inflateMenu(R.menu.menu_feedback);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_feedback:
                intent = new Intent(this, FeedbackActivity.class);
                intent.putExtra("view", (String)null);
                intent.putExtra("viewObjectId",(String)null);
                intent.putExtra("viewClass",getClass().getSimpleName());
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_responsa_object_form);

        placeInitiative = ResponsaObject.currentResponsaObject;

        responsaObjectType = getIntent().getExtras().getString("type");

        //edit...
        objectId = getIntent().getExtras().getString("objectId");

        cleanPlaceInitiativeOnClose = getIntent().getExtras().getBoolean("cleanPlaceInitiativeOnClose");



        rToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(rToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        if (objectId == null) {
            if ("initiative".equals(responsaObjectType)) {
                actionBar.setTitle(getResources().getString(R.string.action_new_initiative));
            } else { /* if responsaObjectType == "gathering" */
                String title = getResources().getString(R.string.action_new_gathering);
                if (placeInitiative != null) {
                    title += " " + getResources().getString(R.string.gathering_in_place, placeInitiative.getTitle());
                }
                actionBar.setTitle(title);
            }
        } else {
            if ("initiative".equals(responsaObjectType)) {
                actionBar.setTitle(getResources().getString(R.string.action_edit_initiative));
            } else { /* if responsaObjectType == "gathering" */
                String title = getResources().getString(R.string.action_edit_gathering);
                if (placeInitiative != null) {
                    title += " " + getResources().getString(R.string.gathering_in_place, placeInitiative.getTitle());
                }
                actionBar.setTitle(title);
            }
        }


        ResponsaObjectFormFragment f = new ResponsaObjectFormFragment();

        Bundle args = new Bundle();
        args.putString("type",responsaObjectType);
        args.putString("objectId", objectId);

        f.setArguments(args);

        // Display the fragment as the main content.
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.content_frame, f)
                .commit();
    }

    @Override
    protected void onDestroy() {
        if (cleanPlaceInitiativeOnClose) {
            ResponsaObject.currentResponsaObject = null;
        }
        super.onDestroy();
    }


}
