package br.org.eita.responsa.view.fragment;

import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ListView;

import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.UserListAdapter;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.model.UserResponsaObject;

public class UserListDialogFragment extends DialogFragment {

    UserListAdapter userListAdapter;
    BroadcastReceiver dataReceiver = null;
    List<ParseUser> users;

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        AlertDialog.Builder alertDialog = new AlertDialog.Builder(getActivity());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        View convertView = (View) inflater.inflate(R.layout.fragment_users_list, null);
        alertDialog.setView(convertView);
        alertDialog.setTitle(R.string.participants_list_title);
        ListView lv = (ListView) convertView.findViewById(R.id.listView1);

        userListAdapter = new UserListAdapter(getActivity());

        lv.setAdapter(userListAdapter);
        populateList();

        dataReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                populateList();
            }
        };

        return alertDialog.create();
    }

    @Override
    public void onDismiss(DialogInterface dialog) {
        super.onDismiss(dialog);
    }

    public void setData(List<ParseUser> users) {
        this.users = users;
    }

    private void populateList() {
        userListAdapter.populateAdapter(users);
    }
}
