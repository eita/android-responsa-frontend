package br.org.eita.responsa.adapter;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.text.format.DateUtils;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.beardedhen.androidbootstrap.BootstrapCircleThumbnail;
import com.parse.GetDataCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.shamanland.fonticon.FontIconTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.data_manager.QueryResult;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.model.InitiativeCategory;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.util.ResponsaUtil;

//Gets pinned responsa objects near location
public class ResponsaObjectListAdapter extends ArrayAdapter<ResponsaObject> {

    public ResponsaObjectListAdapter(Context context) {
        super(context, 0, new ArrayList<ResponsaObject>());
    }

    public void populateAdapter(List list) {
        this.clear();
        this.addAll(list);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ResponsaObject responsaObject = getItem(position);

        if ("initiative".equals(responsaObject.getType())) {
            convertView = View.inflate(getContext(), R.layout.item_initiative, null);
        } else if ("gathering".equals(responsaObject.getType())) {
            convertView = View.inflate(getContext(), R.layout.item_gathering, null);
        }

        TextView titleView = (TextView) convertView.findViewById(R.id.responsaObjectTitle);
        titleView.setText(responsaObject.getString("title"));

        //TextView textView = (TextView) v.findViewById(R.id.iniciativaText);
        //textView.setText(responsaObject.getString("description"));

        TextView distanceView = (TextView) convertView.findViewById(R.id.responsaObjectDistance);

        if (LocationCoordinates.getVisibleLocation()==null || LocationCoordinates.getVisibleLocation().getCoordinateType().equals(LocationCoordinates.CoordinateType.BY_TERRITORY_ID)) {
            distanceView.setVisibility(View.GONE);
        } else {
            distanceView.setText(LocationCoordinates.getHumanReadableDistance(responsaObject.getLocationCoordinates()));
        }

        TextView locationView = (TextView) convertView.findViewById(R.id.responsaObjectLocation);
        HomeTerritory loc = responsaObject.getTerritory();
        if (loc != null) {
            locationView.setText(loc.label);
        } else {
            locationView.setVisibility(View.GONE);
        }

        if ("gathering".equals(responsaObject.getType())) {
            TextView dateView = (TextView) convertView.findViewById(R.id.responsaObjectDate);
            if (responsaObject.getDateTimeStart() != null) {
                //SimpleDateFormat sdf = new SimpleDateFormat("EEE, d 'de' MMMM");
                //dateView.setText(sdf.format(responsaObject.getDateTimeStart()));
                dateView.setText(DateUtils.getRelativeTimeSpanString(responsaObject.getDateTimeStart().getTime()));
            }

            FontIconTextView fsTimeGathering = (FontIconTextView) convertView.findViewById(R.id.fsTimeGathering);
            fsTimeGathering.setText(responsaObject.getHumanReadableTime());
        }

        final BootstrapCircleThumbnail thumbnail = (BootstrapCircleThumbnail) convertView.findViewById(R.id.responsaObjectImage);

        thumbnail.setImage(responsaObject.getDrawableIcon());

        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ResponsaUtil.log("obj_debug", "clicked on ficha sintese list with responsaObject: " + responsaObject.getObjectId() + " - " + responsaObject.getTitle());

                ResponsaApplication.responsaViewManager.openFichaSintese(getContext(),responsaObject);
            }
        });

        return convertView;
    }
}
