package br.org.eita.responsa.view.fragment;

import android.app.Fragment;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.Date;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.Feedback;
import br.org.eita.responsa.model.Version;
import br.org.eita.responsa.model.util.ResponsaSaveCallback;
import br.org.eita.responsa.util.ResponsaUtil;


public class UpgradeLockFragment extends Fragment {

    TextView upgradeLockText;
    Button updateNowButton;
    Button updateLaterButton;


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)   {
        View inflatedView = inflater.inflate(R.layout.fragment_upgrade_lock, container, false);

        findAllViewsByIds(inflatedView);
        PackageInfo pInfo = null;

        try {
            pInfo = getActivity().getPackageManager().getPackageInfo(getActivity().getPackageName(), 0);

            final String versionName = pInfo.versionName;

            List<Version> versions = ResponsaApplication.responsaDataManager.retrieveLoadedData("version", versionName);
            if (versions.size() == 0) return null;
            Version ver = versions.get(0);


            if (ver.getExpires().after(new Date())) {
                updateLaterButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        getActivity().finish();
                    }
                });

                int daysToExpire = (int) Math.floor(ResponsaUtil.daysFromNow(ver.getExpires()));

                upgradeLockText.setText(getString(R.string.upgrade_optional_text, daysToExpire));
            } else {
                updateLaterButton.setVisibility(View.GONE);
                upgradeLockText.setText(R.string.upgrade_mandatory_text);
            }

            updateNowButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse
                            ("market://details?id=br.org.eita.responsa")));
                }
            });
        } catch (PackageManager.NameNotFoundException e) {
            e.printStackTrace();
        }


        return inflatedView;
    }

    private void findAllViewsByIds(View parentView) {
        upgradeLockText = (TextView) parentView.findViewById(R.id.upgradeLockText);
        updateNowButton = (Button) parentView.findViewById(R.id.updateNowButton);
        updateLaterButton = (Button) parentView.findViewById(R.id.updateLaterButton);
    }
}
