package br.org.eita.responsa.backend_comm;

import br.org.eita.responsa.model.CepAddress;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

//http://viacep.com.br
public interface CepApi {

    @GET("/ws/{cep}/json/")
    void buscaCep(@Path("cep") String cep, Callback<CepAddress> cb);

}
