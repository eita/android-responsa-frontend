package br.org.eita.responsa.view.fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.ArrayList;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.util.TouchImageView;
import br.org.eita.responsa.view.activity.FichaSinteseActivity;

public class FullImageDialogFragment extends DialogFragment{

    public View fullSizeImageDialogView;
    //public static ImageView fullImageView;
    public static TouchImageView fullImageView;
    private static Bitmap image = null;

    public FullImageDialogFragment() {
        //Set Arguments here if needed for dialog auto recreation on screen rotation
        this.setStyle(DialogFragment.STYLE_NO_TITLE, R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen_Animated);
        this.setCancelable(true);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View fullImageDialogView = (View)inflater.inflate(R.layout.view_image_fullscreen, container, false);
        fullImageView = (TouchImageView) fullImageDialogView.findViewById(R.id.view_image_fullscreen);
        Button btnClose = (Button) fullImageDialogView.findViewById(R.id.btnClose);
        RelativeLayout allScreen = (RelativeLayout) fullImageDialogView.findViewById(R.id.view_image_fullscreen_wrapper);
        final FullImageDialogFragment self = this;
        btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                self.dismiss();
            }
        });
        allScreen.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                self.dismiss();
            }
        });

        return fullImageDialogView;
    }

    @Override
    public void onStart() {
        super.onStart();
        setImage(image);
    }

    public void setImage(Bitmap image) {
        this.image = image;
        if (fullImageView!=null) {
            fullImageView.setImageBitmap(image);
        }
    }
}
