package br.org.eita.responsa.adapter;


import android.content.Context;
import android.text.format.DateUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.parse.ParseUser;
import com.shamanland.fonticon.FontIconTextView;

import java.util.ArrayList;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.adapter.data_manager.ChallengeFrontPageData;
import br.org.eita.responsa.components.VotableStatsWidget;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.util.ResponsaUtil;

public class ChallengesFirstPageAdapter extends ArrayAdapter<Challenge>  {

    ChallengeFrontPageData frontPageData;

    public ChallengesFirstPageAdapter(Context context, ChallengeFrontPageData frontPageData) {
        super(context, 0, new ArrayList<Challenge>());

        /*
        this.frontPageData = frontPageData;

        this.addAll(frontPageData.challenges);

        Challenge title = new Challenge();
        title.layoutTitleViewString = R.string.u_discoveries;
        this.add(title);

        this.addAll(frontPageData.discoveries);
        */
    }

    public void populate(List<Challenge> challenges) {
        this.clear();

        ChallengeFrontPageData frontPageData = new ChallengeFrontPageData();
        frontPageData.loadFromChallengeList(challenges);

        this.addAll(frontPageData.getDataForView());
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Challenge challenge = getItem(position);

        if (challenge.getLayoutTitleViewString() != null) {
            // Check if an existing view is being reused, otherwise inflate the view
            //if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_title, parent, false);
            //}

            TextView listTitle = (TextView) convertView.findViewById(R.id.listTitle);

            listTitle.setText(challenge.getLayoutTitleViewString());

        } else {
            // Check if an existing view is being reused, otherwise inflate the view
            //if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.challenge_item_view, parent, false);
            //}

            TextView challengeTitle = (TextView) convertView.findViewById(R.id.challengeTitle);
            TextView challengeCreatedAt = (TextView) convertView.findViewById(R.id.challengeCreatedAt);
            TextView challengeDescription = (TextView) convertView.findViewById(R.id.challengeDescription);
            TextView challengeAuthor = (TextView) convertView.findViewById(R.id.challengeAuthor);
            VotableStatsWidget votableStatsWidget = (VotableStatsWidget) convertView.findViewById(R.id.votableStatsWidget);

            String authorLocation = challenge.getAuthor().getString("name");
            if (challenge.getTerritoryId() != null) {
                HomeTerritory territory = ResponsaApplication.territoryDb.findTerritoryById(challenge.getTerritoryId());
                if (territory != null) {
                    authorLocation += " " + getContext().getString(R.string.in_location,territory.label);
                }
            }

            challengeTitle.setText(challenge.getTitle());

            challengeDescription.setText(challenge.getDescription());

            if (challenge.getCreatedAt() != null) {
                challengeCreatedAt.setText(DateUtils.getRelativeTimeSpanString(ResponsaUtil.getDateCurrentTimeZone(challenge.getCreatedAt().getTime()), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS));
                //challengeCreatedAt.setText(ResponsaUtil.getDateCurrentTimeZone(summary.createdAt.getTime() / 1000));
            } else {
                challengeCreatedAt.setVisibility(View.GONE);
            }
            challengeAuthor.setText(authorLocation);

            votableStatsWidget.setVotableAnswerable(challenge);


            FontIconTextView challengeSelectedAnswer = (FontIconTextView) convertView.findViewById(R.id.challengeSelectedAnswer);
            if (challenge.getCorrectAnswer() != null) {
                ResponsaUtil.log("ResponsaDataManager", "desafio tem resposta");
                challengeSelectedAnswer.setText(challenge.getCorrectAnswer().getString("description"));
            } else {
                challengeSelectedAnswer.setVisibility(View.GONE);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ResponsaApplication.responsaViewManager.openChallengeView(getContext(), challenge);
                }
            });
        }

        return convertView;

    }
}
