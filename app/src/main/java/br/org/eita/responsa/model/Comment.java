package br.org.eita.responsa.model;

import android.content.ContentResolver;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;

import com.parse.DeleteCallback;
import com.parse.FindCallback;
import com.parse.ParseClassName;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.util.Date;
import java.util.List;

import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.util.ResponsaUtil;

@ParseClassName("Comment")
public class Comment  extends ParseObject {

    private final static Integer THUMBNAIL_MIN_WIDTH = 450;
    private final static Integer IMAGE_MIN_WIDTH = 900;

    private final static Integer THUMBNAIL_JPEG_QUALITY = 90;
    private final static Integer IMAGE_JPEG_QUALITY = 75;

    //if one image was saved - normal or thumbnail, this is true
    //this is required because of asyncronous saving of both image and thumbnail
    private Boolean savedImage = false;



    public ParseObject getResponsaObject() {
        return getParseObject("responsaObject");
    }

    public void setResponsaObject(ResponsaObject responsaObject) {
        if (responsaObject == null) {
            this.put("responsaObject", JSONObject.NULL);
        } else {
            this.put("responsaObject", responsaObject);
        }
    }

    public ParseObject getUser() {
        return getParseObject("user");
    }

    public void setUser(ParseObject user) {
        if (user == null) {
            this.put("user", JSONObject.NULL);
        } else {
            this.put("user", user);
        }
    }

    public String getComment() {
        return getString("comment");
    }

    public void setComment(String comment) {
        if (comment == null) {
            this.put("comment", JSONObject.NULL);
        } else {
            this.put("comment", comment);
        }
    }

    public ParseFile getImage() {
        return getParseFile("image");
    }

    public void setImage(ParseFile image) {
        if (image == null) {
            this.put("image", JSONObject.NULL);
        } else {
            this.put("image", image);
        }
    }

    public ParseFile getThumbnail() {
        return getParseFile("thumbnail");
    }

    public void setThumbnail(ParseFile thumbnail) {
        if (thumbnail == null) {
            this.put("thumbnail", JSONObject.NULL);
        } else {
            this.put("thumbnail", thumbnail);
        }
    }

    public Date getLocalCreatedAt() {
        return getDate("localCreatedAt");
    }

    public void setLocalCreatedAt(Date localCreatedAt) {
        if (localCreatedAt == null) {
            this.put("localCreatedAt", JSONObject.NULL);
        } else {
            this.put("localCreatedAt", localCreatedAt);
        }
    }

    public Integer getImageOrientation() {
        return getInt("imageOrientation");
    }

    public void setImageFromUri(Uri uri, ContentResolver resolver, final AfterImageSetCallback cb) {
        reduceAndSaveImage(uri, cb, false, resolver);
        reduceAndSaveImage(uri, cb, true, resolver);
    }

    private void reduceAndSaveImage(Uri uri, final AfterImageSetCallback cb, Boolean thumbnail, ContentResolver resolver) {
        int minWidth;
        int quality;
        String filename;
        String imageFieldName;

        int imageOrientation = ResponsaUtil.getImageExifOrientation(uri.getPath());

        //save image
        if (thumbnail) {
            minWidth = THUMBNAIL_MIN_WIDTH;
            quality = THUMBNAIL_JPEG_QUALITY;
            filename = "img_"+ ParseUser.getCurrentUser().getObjectId() + "_" + System.currentTimeMillis() + "_thumb.jpg";
            //filename = uri.getLastPathSegment().replace(".","_thumb.");
            imageFieldName = "thumbnail";
        } else {
            minWidth = IMAGE_MIN_WIDTH;
            quality = IMAGE_JPEG_QUALITY;
            filename = "img_"+ ParseUser.getCurrentUser().getObjectId() + "_" + System.currentTimeMillis() + ".jpg";
            //filename = uri.getLastPathSegment();
            imageFieldName = "image";
        }

        Bitmap bmp = decodeFile(uri, minWidth, resolver);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        Bitmap bmp2 = ResponsaUtil.rotateBitmap(bmp,imageOrientation);
        bmp2.compress(Bitmap.CompressFormat.JPEG, quality, stream);
        byte[] byteArray = stream.toByteArray();
        if (byteArray != null) {
            final ParseFile parseFile = new ParseFile(filename, byteArray);
            final Comment self = this;
            self.put(imageFieldName, parseFile);
            parseFile.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
                    //only must be executed in the second time
                    if (!self.savedImage) {
                        self.savedImage = true;
                    } else {
                        cb.executeAfterImageSet();
                        self.savedImage = false;
                    }
                }
            });
        }
    }

    public interface AfterImageSetCallback {
        public void executeAfterImageSet();
    }

    // Decodes image and scales it to reduce memory consumption
    // requiredSize is the new size we want to scale to
    private Bitmap decodeFile(Uri uri, final int requiredSize, ContentResolver resolver) {

        try {

            String uriString = uri.toString();
            InputStream inputStream;
            InputStream inputStream2;

            if (uriString.contains("content://")) {
                inputStream = resolver.openInputStream(uri);
                inputStream2 = resolver.openInputStream(uri);
            } else {
                inputStream = new FileInputStream(new File(uri.getPath()));
                inputStream2 = new FileInputStream(new File(uri.getPath()));
            }

            // Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inputStream, null, o);

            // Find the correct scale value. It should be the power of 2.
            int scale = 1;
            while (o.outWidth / scale / 2 >= requiredSize &&
                    o.outHeight / scale / 2 >= requiredSize) {
                scale *= 2;
            }

            // Decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize = scale;
            return BitmapFactory.decodeStream(inputStream2, null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }

    public interface FindCommentsCallback {
        public void done(List<Comment> list, ParseException e);
    }
}
