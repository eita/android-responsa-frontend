package br.org.eita.responsa.components.icon_badge;

import android.content.Context;
import android.util.AttributeSet;

import br.org.eita.responsa.model.BaseResponsaModel;
import br.org.eita.responsa.model.OnUpdateValueCallback;

public class ModelIconBadge extends FontIconBadge {
    private BaseResponsaModel model;
    private OnUpdateValueCallback<Integer> updateCallback;
    private Boolean onUpdateCallbackAttached = false;
    private String field = null;


    public ModelIconBadge(Context context) {
        this(context, null);
    }

    public ModelIconBadge(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ModelIconBadge(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        this.updateCallback = new OnUpdateValueCallback<Integer>() {
            @Override
            public void onUpdate(Integer newValue) {
                setValue(newValue);
            }
        };
    }

    @Override
    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
        if (!this.onUpdateCallbackAttached && this.model != null) {
            this.model.addUpdateCallback(field, updateCallback);
            this.onUpdateCallbackAttached = true;
        }
    }

    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        if (this.onUpdateCallbackAttached && this.model != null) {
            this.model.removeUpdateCallback(field, updateCallback);
            this.onUpdateCallbackAttached = false;
        }
    }

    public void setModel(BaseResponsaModel model) {
        this.model = model;
        this.model.addUpdateCallback(field, updateCallback);
        this.onUpdateCallbackAttached = true;
    }

    protected void setModelField(String field) {
        this.field = field;
    }
}
