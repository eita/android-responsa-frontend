package br.org.eita.responsa.adapter;


import android.content.Context;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.shamanland.fonticon.FontIconTextView;

import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.components.VotableStatsWidget;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.search_api.ChallengeSummary;
import br.org.eita.responsa.util.ResponsaUtil;

public abstract class ChallengesAdapter extends ArrayAdapter<ChallengeSummary> {

    public ChallengesAdapter(Context context, int resource, List<ChallengeSummary> objects) {
        super(context, resource, objects);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final ChallengeSummary summary = getItem(position);

        if (summary.layoutTitleViewString != null) {
            // Check if an existing view is being reused, otherwise inflate the view
            //if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.list_title, parent, false);
            //}

            TextView listTitle = (TextView) convertView.findViewById(R.id.listTitle);

            listTitle.setText(summary.layoutTitleViewString);

        } else {

            // Check if an existing view is being reused, otherwise inflate the view
            //if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.challenge_item_view, parent, false);
            //}

            TextView challengeTitle = (TextView) convertView.findViewById(R.id.challengeTitle);
            TextView challengeCreatedAt = (TextView) convertView.findViewById(R.id.challengeCreatedAt);
            TextView challengeDescription = (TextView) convertView.findViewById(R.id.challengeDescription);
            TextView challengeAuthor = (TextView) convertView.findViewById(R.id.challengeAuthor);
            VotableStatsWidget votableStatsWidget = (VotableStatsWidget) convertView.findViewById(R.id.votableStatsWidget);

            String authorLocation = summary.authorName;
            if (summary.territoryId != null) {
                HomeTerritory location = ResponsaApplication.territoryDb.findTerritoryById(summary.territoryId);
                if (location != null) {
                    authorLocation += " " + getContext().getString(R.string.in_location,location.label);
                }
            }

            challengeTitle.setText(summary.title);

            challengeDescription.setText(summary.description);

            if (summary.createdAt != null) {
                challengeCreatedAt.setText(DateUtils.getRelativeTimeSpanString(ResponsaUtil.getDateCurrentTimeZone(summary.createdAt.getTime()), System.currentTimeMillis(), DateUtils.MINUTE_IN_MILLIS));
                //challengeCreatedAt.setText(ResponsaUtil.getDateCurrentTimeZone(summary.createdAt.getTime() / 1000));
            } else {
                challengeCreatedAt.setVisibility(View.GONE);
            }
            challengeAuthor.setText(authorLocation);

            votableStatsWidget.setVotableAnswerable(summary);

            FontIconTextView challengeSelectedAnswer = (FontIconTextView) convertView.findViewById(R.id.challengeSelectedAnswer);
            if (summary.correctAnswerDescription != null && !summary.correctAnswerDescription.isEmpty()) {
                challengeSelectedAnswer.setText(summary.correctAnswerDescription);
            } else {
                challengeSelectedAnswer.setVisibility(View.GONE);
            }

            convertView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ResponsaApplication.responsaViewManager.openChallengeView(getContext(), summary.objectId);
                }
            });

        }

        return convertView;

    }
}
