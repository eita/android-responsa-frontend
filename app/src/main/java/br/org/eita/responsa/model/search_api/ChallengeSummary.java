package br.org.eita.responsa.model.search_api;

import java.util.ArrayList;
import java.util.Date;

import br.org.eita.responsa.model.VotableAnswerable;

public class ChallengeSummary implements VotableAnswerable {
    public Integer relevance;
    public Integer upVotes;
    public Integer answerCount;
    public String title;
    public String territoryId;
    public Date   createdAt;
    public String authorName;
    public String authorId;
    public String  correctAnswerId;
    public String objectId;
    public String description;
    public String correctAnswerDescription;

    //To be used by subheader list
    public Integer layoutTitleViewString;

    @Override
    public Integer getTotalAnswers() {
        return answerCount;
    }

    @Override
    public Integer getTotalVotes() {
        return 2 * this.upVotes - this.relevance;

    }

    @Override
    public Integer getRelevance() {
        return relevance;
    }

    @Override
    public void onAnswerableChange(OnAnswerableChange onAnswerableChange) {

    }

    @Override
    public void setVoteState(Integer newValue) {

    }

    @Override
    public Integer getVoteState() {
        return null;
    }

    @Override
    public void onVotableChange(OnVotableChange onVotableChange) {

    }

    @Override
    public boolean getVotesEnabled() {
        return false;
    }
}
