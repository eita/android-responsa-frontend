package br.org.eita.responsa.model;

public interface OnUpdateValueCallback<T> {
    public void onUpdate(T newValue);
}
