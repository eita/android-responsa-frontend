package br.org.eita.responsa.push;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.NotificationCompat;

import com.parse.ParseAnalytics;
import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Date;
import java.util.Iterator;
import java.util.Locale;
import java.util.Random;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.managers.ResponsaDataManager;
import br.org.eita.responsa.model.Notification;
import br.org.eita.responsa.util.ResponsaUtil;


//http;//stackoverflow.com/questions/26154855/exception-when-opening-parse-push-notification
public class ResponsaPushReceiver extends ParsePushBroadcastReceiver {

    public static Date lastReceivedDate = null;
    public static Integer unreadNotificationCount = 0;

    @Override
    protected void onPushReceive(Context context, Intent intent) {
        ResponsaUtil.log("ResponsaPushReceiver NewDataManager", "RECEIVED PUSH NOTIFICATION");

        Boolean silent = false;

        String pushData = intent.getExtras().getString(KEY_PUSH_DATA);
        String pushChannel = intent.getExtras().getString(KEY_PUSH_CHANNEL);

        JSONObject dataObject = null;

        try {
            dataObject = new JSONObject(pushData);
            ResponsaUtil.log("ResponsaPushReceiver NewDataManager","pushChannel: " + (pushChannel == null ? "null" : pushChannel));

            ResponsaUtil.log("ResponsaPushReceiver NewDataManager", "  (PUSH N) TYPE="+dataObject.getString("pushType"));

            try {
                //silent = dataObject.getBoolean("silent");
                silent = dataObject.getString("title").isEmpty();
            } catch (JSONException e) {}

            if ("notification".equals(dataObject.getString("pushType"))) {
                ResponsaUtil.log("ResponsaPushReceiver NewDataManager", "  (PUSH N): saveFromChannel, data=" + pushData);
                Notification.saveFromChannel(dataObject, false);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }

        Boolean notificationsEnabled = false;

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        if (preferences != null) {
            notificationsEnabled = preferences.getBoolean("p_show_notifications",false);
        }



        if (ResponsaApplication.isAppInForeground()) {
            Intent notificationIntent = new Intent(Constants.NOTIFICATION_UPDATE_ACTION);
            ResponsaApplication.getAppContext().sendBroadcast(notificationIntent);
            if (!silent) {
                ResponsaApplication.vibrate();
            }
            ParseAnalytics.trackAppOpenedInBackground(intent);
        } else if (notificationsEnabled && !silent) {
            String actionTrigger = this.getActionTrigger(intent);
            if (!"customMessage".equals(actionTrigger)) {
                unreadNotificationCount += 1;
            }
            this.origOnPushReceive(context,intent);
        }
    }

    protected void origOnPushReceive(Context context, Intent intent) {
        JSONObject pushData = null;

        try {
            pushData = new JSONObject(intent.getStringExtra("com.parse.Data"));
        } catch (JSONException var7) {
            //PLog.e("com.parse.ParsePushReceiver", "Unexpected JSONException when receiving push data: ", var7);
        }

        String action = null;
        if(pushData != null) {
            action = pushData.optString("action", (String)null);
        }

        if(action != null) {
            Bundle extras = intent.getExtras();
            Intent broadcastIntent = new Intent();
            broadcastIntent.putExtras(extras);
            broadcastIntent.setAction(action);
            broadcastIntent.setPackage(context.getPackageName());
            context.sendBroadcast(broadcastIntent);
        }

        android.app.Notification notification = this.getNotification(context, intent);
        if(notification != null) {
            this.showNotification(context, notification, intent);
        }
    }

    //Shows notification in android dock
    public void showNotification(Context context, android.app.Notification notification, Intent intent) {
        if(context != null && notification != null) {
            NotificationManager nm = (NotificationManager)context.getSystemService(Context.NOTIFICATION_SERVICE);

            String actionTrigger = this.getActionTrigger(intent);

            Integer notificationId = null;
            if ("customMessage".equals(actionTrigger)) {
                notificationId = (int)System.currentTimeMillis();
            } else {
                notificationId = Constants.RESPONSA_DEFAULT_NOTIFICATION_ID;
            }

            //Integer notificationId = Constants.RESPONSA_DEFAULT_NOTIFICATION_ID;

            try {
                nm.notify(notificationId, notification);
            } catch (SecurityException var6) {
                notification.defaults = android.app.Notification.DEFAULT_LIGHTS | android.app.Notification.DEFAULT_SOUND;
                nm.notify(notificationId, notification);
            }

        }

    }



    private JSONObject getPushData(Intent intent) {
        try {
            return new JSONObject(intent.getStringExtra("com.parse.Data"));
        } catch (JSONException var3) {
            //PLog.e("com.parse.ParsePushReceiver", "Unexpected JSONException when receiving push data: ", var3);
            return null;
        }
    }

    private String getActionTrigger(Intent intent) {
        try {
            JSONObject jsonObject = new JSONObject(intent.getStringExtra("com.parse.Data"));
            return jsonObject.getString("actionTrigger");
        } catch (JSONException var3) {
            //PLog.e("com.parse.ParsePushReceiver", "Unexpected JSONException when receiving push data: ", var3);
            return null;
        }
    }

    @Override
    protected android.app.Notification getNotification(Context context, Intent intent) {
        JSONObject pushData = this.getPushData(intent);
        if(pushData != null && (pushData.has("alert") || pushData.has("title"))) {
            String title = pushData.optString("title", "Responsa");
            String alert = pushData.optString("alert", "Notification received.");
            String tickerText = String.format(Locale.getDefault(), "%s: %s", new Object[]{title, alert});
            Bundle extras = intent.getExtras();
            Random random = new Random();
            int contentIntentRequestCode = random.nextInt();
            int deleteIntentRequestCode = random.nextInt();
            String packageName = context.getPackageName();
            Intent contentIntent = new Intent("com.parse.push.intent.OPEN");
            contentIntent.putExtras(extras);
            contentIntent.setPackage(packageName);
            Intent deleteIntent = new Intent("com.parse.push.intent.DELETE");
            deleteIntent.putExtras(extras);
            deleteIntent.setPackage(packageName);
            PendingIntent pContentIntent = PendingIntent.getBroadcast(context, contentIntentRequestCode, contentIntent, PendingIntent.FLAG_UPDATE_CURRENT);
            PendingIntent pDeleteIntent = PendingIntent.getBroadcast(context, deleteIntentRequestCode, deleteIntent, PendingIntent.FLAG_UPDATE_CURRENT);

            String actionTrigger  = this.getActionTrigger(intent);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

            if ((!"customMessage".equals(actionTrigger)) && unreadNotificationCount > 1) {
                title = "Responsa";
                alert = "Você tem " + unreadNotificationCount + " novidades.";
            }

            builder.setContentTitle(title)
                    .setContentText(alert)
                    .setTicker(tickerText)
                    .setSmallIcon(R.drawable.ic_responsa_logo)
                    .setLargeIcon(this.getLargeIcon(context, intent))
                    .setContentIntent(pContentIntent)
                    .setDeleteIntent(pDeleteIntent)
                    .setAutoCancel(true)
                    .setOnlyAlertOnce(true)
                    .setDefaults(-1);


            //if(alert != null && alert.length() > 38) {
            //    builder.setStyle((new com.parse.NotificationCompat.Builder.BigTextStyle()).bigText(alert));
            //}

            return builder.build();
        } else {
            return null;
        }
    }

    @Override
    public void onPushOpen(Context context, Intent intent) {
        ParseAnalytics.trackAppOpenedInBackground(intent);

        String pushData = intent.getExtras().getString("com.parse.Data");
        try {
            JSONObject dataObject = new JSONObject(pushData);

            String actionTrigger = this.getActionTrigger(intent);

            String activityName = null;
            JSONObject targetParams = null;
            if (!"customMessage".equals(actionTrigger) && unreadNotificationCount > 1) {
                activityName = "br.org.eita.responsa.MainActivity";
                targetParams = new JSONObject();
            } else {
                activityName = dataObject.getString("targetView");
                targetParams = new JSONObject(dataObject.getString("targetParams"));
            }

            if (!"customMessage".equals(actionTrigger)) {
                unreadNotificationCount = 0;
            }

            if (activityName != null && !activityName.isEmpty()) {
                Intent activityIntent = new Intent(context,Class.forName(activityName));

                Iterator<String> pIterator = targetParams.keys();
                String value, key;
                while (pIterator.hasNext()) {
                    key = pIterator.next();
                    value = targetParams.getString(key);
                    activityIntent.putExtra(key,value);
                }
                activityIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_SINGLE_TOP);
                context.startActivity(activityIntent);
            }

        } catch (JSONException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

}

