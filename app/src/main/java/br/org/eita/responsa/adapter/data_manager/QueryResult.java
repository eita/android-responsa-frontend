package br.org.eita.responsa.adapter.data_manager;

import com.mapbox.mapboxsdk.geometry.LatLngBounds;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import br.org.eita.responsa.location.LocationCoordinates;

public class QueryResult {

    public static String HAS_NO_NEW_DATA = "has_no_new_data";
    public static String HAS_NEW_DATA = "has_new_data";

    public QueryResult(List newData, List previousData, Origin origin, LocationCoordinates locationCoordinates, Date lastFetchedAt, Integer page, Boolean hasMore) {
        this.metaData = new MetaData();
        if (previousData==null) {
            this.metaData.updateStatus = HAS_NEW_DATA;
        } else {
            this.metaData.updateStatus = newData.equals(previousData)
                    ? HAS_NO_NEW_DATA
                    : HAS_NEW_DATA;
        }
        this.metaData.origin = origin;
        this.metaData.visibleLocationWhenFetched = locationCoordinates;
        this.metaData.lastFetchedAt = lastFetchedAt;
        this.metaData.countResults = newData.size();
        this.metaData.page = page;
        this.metaData.hasMore = hasMore;

        if (this.metaData.updateStatus==HAS_NEW_DATA && previousData != null && page > 0) {
            List mergedData = previousData;;
            for (int i=0; i < newData.size(); i++) {
                mergedData.add(newData.get(i));
            }
            this.data = mergedData;
        } else {
            this.data = newData;
        }
    }

    public static class MetaData {
        public String updateStatus;
        public Origin origin;
        public Date lastFetchedAt;
        public Date lastRemoteFetchedAt;
        public LocationCoordinates visibleLocationWhenFetched;
        public Integer countResults;
        public Integer page;
        public Boolean hasMore;
    }

    public static class Origin {
        public static final Origin LOCAL =  new Origin();
        public static final Origin REMOTE = new Origin();
        public static final Origin NEW_OBJECT = new Origin();
    }

    //----------------------------------------------------------------------//

    private List data = new ArrayList();
    private MetaData metaData;

    public List getData() {
        return data;
    }

    public void setData(List data) {
        this.data = data;
    }

    public MetaData getMetaData() {
        return metaData;
    }

    public void setMetaData(MetaData metaData) {
        this.metaData = metaData;
    }

    public void updateData(QueryResult queryResult) {
        this.data.addAll(0, queryResult.getData());

        MetaData newMetaData = queryResult.getMetaData();
        if (newMetaData.origin.equals(Origin.REMOTE)) metaData.lastRemoteFetchedAt = newMetaData.lastFetchedAt;
    }

    public int size() {
        return data.size();
    }

    public void setUpdateStatus(String updateStatus) {
        this.metaData.updateStatus = updateStatus;
    }

    public void limitResults(List list, Integer limit, Integer page) {
        Integer begin = page * limit;
        Integer end = begin + limit;
        if (list.size()>end) {
            List limitedList = new ArrayList();
            for (int i = begin; i < end; i++) {
                limitedList.add(list.get(i));
            }
            this.setData(limitedList);
        }
    }
}
