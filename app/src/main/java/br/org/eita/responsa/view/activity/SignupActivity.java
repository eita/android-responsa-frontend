package br.org.eita.responsa.view.activity;

import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.inputmethod.InputMethodManager;

import br.org.eita.responsa.MainActivity;
import br.org.eita.responsa.R;
import br.org.eita.responsa.view.fragment.SignupFragment;

public class SignupActivity extends BaseActivity {

    Boolean locReceiverIsRegistered = false;

    Toolbar rToolbar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        rToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(rToolbar);
        ActionBar actionBar = getSupportActionBar();

        actionBar.setTitle(getString(R.string.u_signup_title));

        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new SignupFragment())
                .commit();

        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0);

    }

    public void openMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        rToolbar.inflateMenu(R.menu.menu_feedback);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_feedback:
                intent = new Intent(this, FeedbackActivity.class);
                intent.putExtra("view", (String)null);
                intent.putExtra("viewObjectId",(String)null);
                intent.putExtra("viewClass",getClass().getSimpleName());
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
