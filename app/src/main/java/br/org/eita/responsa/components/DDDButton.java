package br.org.eita.responsa.components;


import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.org.eita.responsa.R;
import br.org.eita.responsa.view.activity.BaseActivity;
import br.org.eita.responsa.view.activity.SignupActivity;

public class DDDButton extends Button implements View.OnClickListener {

    private Boolean dddSelected = false;
    private OnDDDChangeListener listener;

    public Boolean getDddSelected() {
        return dddSelected;
    }

    public void setDddSelected(Boolean dddSelected) {
        this.dddSelected = dddSelected;
    }

    public DDDButton(Context context) {
        super(context);
        init();
    }

    public DDDButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public DDDButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    public void onClick(View view) {
        final DDDButton self = this;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.u_select_ddd)
                .setItems(R.array.ddds_brasil, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Resources res = getResources();
                        String[] ddds = res.getStringArray(R.array.ddds_brasil);

                        String dddOriginal = ddds[which];
                        Pattern p = Pattern.compile("[0-9]{2}");
                        Matcher m = p.matcher(dddOriginal);
                        String ddd = "DDD";
                        while (m.find()) {
                            ddd = m.group(0);
                        }
                        self.setText(ddd);
                        self.setDddSelected(!ddd.equals("DDD"));
                        if (self.listener != null) {
                            self.listener.onDDDChange(ddd, self.dddSelected);
                        }
                    }
                });
        AlertDialog dddSelector = builder.create();
        dddSelector.show();
    }

    public void setOnDDDChangeListener(OnDDDChangeListener listener) {
        this.listener = listener;
    }

    private void init() {
        setOnClickListener(this);
    }

    public interface OnDDDChangeListener {
        public void onDDDChange(String ddd, Boolean isDddFilled);
    }
}
