package br.org.eita.responsa.util;

/**
 * Created by vinicius on 31/07/15.
 */
public interface OnAjaxLoadCompletedCallback {
    public void onComplete();
}
