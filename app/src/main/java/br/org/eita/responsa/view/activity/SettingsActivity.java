package br.org.eita.responsa.view.activity;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.Toast;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.view.fragment.SettingsFragment;


public class SettingsActivity extends BaseActivity implements SharedPreferences.OnSharedPreferenceChangeListener {

    Toolbar rToolbar;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        rToolbar.inflateMenu(R.menu.menu_feedback);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent intent;
        switch (item.getItemId()) {
            case android.R.id.home:
                finish();
                return true;
            case R.id.action_feedback:
                intent = new Intent(this, FeedbackActivity.class);
                intent.putExtra("view", (String)null);
                intent.putExtra("viewObjectId",(String)null);
                intent.putExtra("viewClass",getClass().getSimpleName());
                startActivity(intent);
                return true;
            case R.id.action_about:
                intent = new Intent(this, AboutActivity.class);
                startActivity(intent);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);

        rToolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(rToolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        actionBar.setTitle(getResources().getString(R.string.action_settings));


        // Display the fragment as the main content.
        getFragmentManager().beginTransaction()
                .replace(R.id.content_frame, new SettingsFragment())
                .commit();
    }

    @Override
    protected void onResume() {
        super.onResume();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences.registerOnSharedPreferenceChangeListener(this);

    }

    @Override
    protected void onPause() {
        super.onPause();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences.unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences,
                                          String key) {

        final Context self = this;

        if ("p_show_notifications".equals(key)) {
            if (sharedPreferences.getBoolean(key,false)) {
                Toast.makeText(this,R.string.p_notifications_enabled,Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(this,R.string.p_notifications_disabled,Toast.LENGTH_SHORT).show();
            }
        }
    }

}
