package br.org.eita.responsa.model.util;

import com.parse.ParseException;

import java.util.List;


public interface ResponsaGetCallback<T> {
    public void done(T object, ParseException e);

}
