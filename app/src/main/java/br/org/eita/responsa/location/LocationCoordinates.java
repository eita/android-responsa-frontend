package br.org.eita.responsa.location;

import android.content.Intent;
import android.content.SharedPreferences;
import android.location.Location;
import android.preference.PreferenceManager;
import android.util.Log;

import com.google.gson.Gson;
import com.mapbox.mapboxsdk.geometry.LatLng;
import com.parse.ParseGeoPoint;
import com.parse.ParseUser;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.GeocoderReverseAddress;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.receivers.PeriodicTaskReceiver;
import br.org.eita.responsa.util.ResponsaUtil;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class LocationCoordinates {

    /**
     * Visible location used in application; can be the location fetched in a past day, stored
     * in preferences
     */
    private static LocationCoordinates visibleLocation;

    /**
     * Current location fetched from LocationService - atualizado todo momento
     */
    private static LocationCoordinates currentLocation;



    public  static ResponsaObject latestNearest;

    //Time the user 'entered' in the nearest initiative, to make 'cheguei'
    public static Long enteredInNearest;


    /**
     * Only filled when requested
     */
    public static GeocoderReverseAddress currentGeocoderReverseAddress;

    public static String CURRENT_LOCATION_PREFERENCE_KEY = "br.org.eita.responsa.LAST_LOCATION";

    private static Boolean useNextLocationAsVisible = false;

    public static enum CoordinateType {BY_TERRITORY_ID, BY_GPS, OTHER}

    private Double latitude;
    private Double longitude;
    private Long fetchTime;
    private Float accuracy;

    private CoordinateType coordinateType;
    private HomeTerritory location;


    public LocationCoordinates() {
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public ParseGeoPoint toParseGeoPoint() {
        //time information is lost
        return new ParseGeoPoint(latitude,longitude);
    }

    public Location toLocation() {
        Location location = new Location("responsa");
        location.setLatitude(this.latitude);
        location.setLongitude(this.longitude);
        location.setTime(this.fetchTime);
        location.setAccuracy(this.accuracy);
        return location;
    }

    public static LocationCoordinates fromString(String serialized) {
        try {
            return new Gson().fromJson(serialized, LocationCoordinates.class);
        } catch (Exception e) {
            return null;
        }
    }

    public static LocationCoordinates fromParseGeoPoint(ParseGeoPoint geoPoint, long fetchTime) {
        if (geoPoint == null) return null;

        LocationCoordinates locationCoordinates = new LocationCoordinates();
        locationCoordinates.latitude = geoPoint.getLatitude();
        locationCoordinates.longitude = geoPoint.getLongitude();
        locationCoordinates.fetchTime = fetchTime;
        locationCoordinates.accuracy = 0F;
        locationCoordinates.coordinateType = CoordinateType.OTHER;
        return locationCoordinates;
    }

    public static ParseGeoPoint toParseGeopoint(LatLng latLng) {
        return new ParseGeoPoint(latLng.getLatitude(),latLng.getLongitude());
    }

    public static LocationCoordinates fromLocation(Location location) {
        if (location == null) return null;

        LocationCoordinates locationCoordinates = new LocationCoordinates();
        locationCoordinates.latitude = location.getLatitude();
        locationCoordinates.longitude = location.getLongitude();
        locationCoordinates.fetchTime = location.getTime();
        locationCoordinates.accuracy = location.getAccuracy();
        locationCoordinates.coordinateType = CoordinateType.OTHER;
        return locationCoordinates;
    }

    public Long getFetchTime() {
        return fetchTime;
    }

    public void setFetchTime(Long fetchTime) {
        this.fetchTime = fetchTime;
    }

    public Double getLatitude() {
        return latitude;
    }

    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    public Double getLongitude() {
        return longitude;
    }

    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }

    public LatLng getLatLng() {
        return new LatLng(getLatitude(),getLongitude());
    }

    public Float getAccuracy() {
        return accuracy;
    }

    public void setAccuracy(Float accuracy) {
        this.accuracy = accuracy;
    }

    public CoordinateType getCoordinateType() {
        return coordinateType;
    }

    public void setCoordinateType(CoordinateType coordinateType) {
        this.coordinateType = coordinateType;
    }

    public HomeTerritory getLocation() {
        return location;
    }

    public void setLocation(HomeTerritory location) {
        this.location = location;
    }

    public Double getDistanceInKilometersTo(LocationCoordinates coordinates) {
        if (coordinates == null) return null;

        return coordinates.toParseGeoPoint().distanceInKilometersTo(toParseGeoPoint());
    }

    public static void setVisibleLocation(LocationCoordinates newVisibleLocation) {
        ResponsaUtil.log("responsaLocation", "sets current location");

        LocationCoordinates currentVisibleLocation = getVisibleLocation();
        Boolean updateLocation = false;

        if (currentVisibleLocation == null) {
            updateLocation = true;
        } else if (currentVisibleLocation.getCoordinateType().equals(CoordinateType.BY_TERRITORY_ID)) {
            updateLocation = true;
        } else if (currentVisibleLocation.getCoordinateType().equals(CoordinateType.BY_GPS)
                && newVisibleLocation.getCoordinateType().equals(CoordinateType.BY_GPS)) {
            updateLocation = true;
        }

        if (updateLocation) {
            //Save chosen location in preferences
            SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ResponsaApplication.getAppContext());
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString(CURRENT_LOCATION_PREFERENCE_KEY, newVisibleLocation.toString());
            editor.commit();

            LocationCoordinates.visibleLocation = newVisibleLocation;

            ResponsaApplication.responsaDataManager.load("initiativesAndGatherings", null);

            Intent locationIntent = new Intent(Constants.LOCATION_CHANGED);
            ResponsaApplication.getAppContext().sendBroadcast(locationIntent);
        }
    }


    public static LocationCoordinates getVisibleLocation() {
        //If visible Location is null, fetches it from preferences - last visible location
        if (visibleLocation == null) {
            ResponsaUtil.log("responsaLocation", "   loading from preferences");
            //loads from preferences
            final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ResponsaApplication.getAppContext());
            LocationCoordinates lastLocation = LocationCoordinates.fromString(preferences.getString(CURRENT_LOCATION_PREFERENCE_KEY, ""));

            //if location in preferences is older than two hours, it is invalid
            if (lastLocation == null || lastLocation.fetchTime < System.currentTimeMillis() - Constants.VISIBLE_LOCATION_EXPIRE_TIME) {
                ParseUser currentUser = ParseUser.getCurrentUser();

                //loads visibleLocation from user territory
                if (currentUser != null && ResponsaApplication.territoryDb != null) {
                    HomeTerritory userTerritory = ResponsaApplication.territoryDb.findTerritoryById(currentUser.getString("territoryId"));

                    if (userTerritory != null) {
                        visibleLocation = userTerritory.getLocationCoordinates();
                    }
                }
            } else {
                visibleLocation = lastLocation;
            }
        }

        return visibleLocation;
    }


    public static void requestLocationUpdate(Boolean forceLocationService)   {
        //Must force a new location update from LocationService
        if (forceLocationService) {
            Intent intent = new Intent(PeriodicTaskReceiver.INTENT_FETCH_LOCATION_NOW);
            ResponsaApplication.getAppContext().sendBroadcast(intent);
        }

        ResponsaUtil.log("responsaLocation", "request location update");
        LocationCoordinates lastLocation = getCurrentLocation();

        //If last location has not expired, use it
        if (lastLocation != null && lastLocation.fetchTime > System.currentTimeMillis() - Constants.CURRENT_LOCATION_EXPIRE_TIME) {
            setVisibleLocation(getCurrentLocation());
        } else {
            ResponsaUtil.log("responsaLocation", "   will use next location to be found");
            //Flag for next location found
            useNextLocationAsVisible = true;
        }
    }

    public static LocationCoordinates getCurrentLocation() {
        return currentLocation;
    }

    public static String getHumanReadableDistance(LocationCoordinates location) {
        if (location == null || LocationCoordinates.getVisibleLocation() == null ) {
            Log.e("responsaError","LocationCoordinates getHumanReadableDistance is receiving null param");
            return "";
        }
        Double distance = location.toParseGeoPoint().distanceInKilometersTo(LocationCoordinates.getVisibleLocation().toParseGeoPoint());

        if (distance < 0.05) {
            return ResponsaApplication.getAppContext().getString(R.string.here);
        } else if (distance < 1) {
            return String.format("%.0f",distance*1000) + " m";
        } else {
            return String.format("%.1f",distance) + " km";
        }

    }

    //Called mainly from service
    public static void setCurrentLocation(LocationCoordinates currentLocation) {
        LocationCoordinates.currentLocation = currentLocation;
        if (useNextLocationAsVisible) {
            setVisibleLocation(currentLocation);
            useNextLocationAsVisible = false;
        }
    }

    public static void requestAddressFromVisibleLocation() {
        final LocationCoordinates currentLocation = getVisibleLocation();
        if (getVisibleLocation() != null) {
            GeocoderReverseAddress.reverseGeocode(currentLocation.getLatitude(), currentLocation.getLongitude(), new Callback<GeocoderReverseAddress>() {
                @Override
                public void success(GeocoderReverseAddress geocoderReverseAddress, Response response) {
                    //ResponsaUtil.log("responsa_location", "Got location by position: " + geocoderReverseAddress.displayName);
                    //Toast.makeText(context, geocoderReverseAddress.displayName, Toast.LENGTH_SHORT).show();
                    geocoderReverseAddress.lat = currentLocation.getLatitude();
                    geocoderReverseAddress.lng = currentLocation.getLongitude();
                    geocoderReverseAddress.location = currentLocation;
                    LocationCoordinates.currentGeocoderReverseAddress = geocoderReverseAddress;

                    Intent intent = new Intent(Constants.INQUIRY_ADDRESS_ACTION);
                    ResponsaApplication.getAppContext().sendBroadcast(intent);
                }

                @Override
                public void failure(RetrofitError error) {
                    //Toast.makeText(context, "Erro ao receber dados de geocoding reverso: " + error.getKind().toString(), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(Constants.INQUIRY_ADDRESS_ACTION);
                    ResponsaApplication.getAppContext().sendBroadcast(intent);
                }
            });
        }
    }
}
