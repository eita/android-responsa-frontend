package br.org.eita.responsa.components;


import android.content.Context;
import android.content.DialogInterface;
import android.content.res.Resources;
import android.support.v7.app.AlertDialog;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import br.org.eita.responsa.R;

public class StateButton extends Button implements View.OnClickListener {

    private Boolean stateSelected = false;
    private OnStateChangeListener listener;

    public Boolean getStateSelected() {
        return stateSelected;
    }

    public void setStateSelected(Boolean stateSelected) {
        this.stateSelected = stateSelected;
    }

    public StateButton(Context context) {
        super(context);
        init();
    }

    public StateButton(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public StateButton(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    @Override
    public void onClick(View view) {
        final StateButton self = this;
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle(R.string.u_select_state)
                .setItems(R.array.states_brasil, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Resources res = getResources();
                        String[] states = res.getStringArray(R.array.states_brasil);

                        String stateOriginal = states[which];
                        Pattern p = Pattern.compile("[A-Z]{2}");
                        Matcher m = p.matcher(stateOriginal);
                        String state = "Estado";
                        if (m.find()) {
                            state = m.group(0);
                        }
                        self.setText(state);
                        self.setStateSelected(!state.equals("Estado"));
                        if (self.listener != null) {
                            self.listener.onStateChange(state, self.stateSelected);
                        }
                    }
                });
        AlertDialog stateSelector = builder.create();
        stateSelector.show();
    }

    public void setOnStateChangeListener(OnStateChangeListener listener) {
        this.listener = listener;
    }

    private void init() {
        setOnClickListener(this);
    }

    public interface OnStateChangeListener {
        public void onStateChange(String state, Boolean isStateFilled);
    }
}
