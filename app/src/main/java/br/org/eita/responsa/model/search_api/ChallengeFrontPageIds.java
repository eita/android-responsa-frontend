package br.org.eita.responsa.model.search_api;


import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;

import java.util.List;

import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.util.ResponsaUtil;

public class ChallengeFrontPageIds {
    public List<ChallengeRankedId> challenges;
    public List<ChallengeRankedId> discoveries;

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    public static ChallengeFrontPageIds fromString(String serialized) {
        try {
            return new Gson().fromJson(serialized, ChallengeFrontPageIds.class);
        } catch (Exception e) {
            return null;
        }
    }

    public void saveToPreferences() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ResponsaApplication.getAppContext());

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString("challengeFrontPageIds", toString());

        editor.commit();
    }

    public static ChallengeFrontPageIds loadFromPreferences() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ResponsaApplication.getAppContext());

        return fromString(preferences.getString("challengeFrontPageIds",null));

    }
}
