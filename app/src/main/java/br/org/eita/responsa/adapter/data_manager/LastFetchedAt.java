package br.org.eita.responsa.adapter.data_manager;

import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.google.gson.Gson;
import com.mapbox.mapboxsdk.geometry.LatLngBounds;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.util.ExtraFilters;
import br.org.eita.responsa.util.ResponsaObjectsMap;
import br.org.eita.responsa.util.ResponsaUtil;

/**
 * Stores the last fetched at objects
 */
public class LastFetchedAt {

    private HashMap<String, Date> fetchedDates = new HashMap<>();

    private HashMap<String, LocationCoordinates> fetchedPositions = new HashMap<>();

    // Keep track of the page of the last fetch:
    private HashMap<String, Integer> fetchedPages = new HashMap<>();

    private HashMap<String, List<LatLngBounds>> fetchedLatLngBounds = new HashMap<>();

    public HashMap<String, Boolean> readyToPopulate = new HashMap<>();


    public static LastFetchedAt fromString(String serialized) {
        try {
            return new Gson().fromJson(serialized, LastFetchedAt.class);
        } catch (Exception e) {
            return null;
        }
    }

    @Override
    public String toString() {
        return new Gson().toJson(this);
    }

    /*
        Generic Methods
     */

    /**
     * When data is loaded from the cloud, we store the update status to avoid repeating same loads unnecessarily. So this function helps to optimize remote loading.
     * @param key The view key, which identifies what kind of data is being handled.The keys are defined in data_config.json
     * @param extraFilters The filters applied to this specific query. Each compound of key + extraFields yealds an unique lastFetchedAt string
     * @param visibleLocationWhenFetched Where was the user when initiatives and gatherings were last fetched.
     * @param threadTokenKey The loading thread
     * @param page The page for handling "load more"
     * @param latLngBoundsWhenFetched The map boundaries in device when data was last fetched.
     */
    public void updateLastFetchedAt(String key, ExtraFilters extraFilters, LocationCoordinates visibleLocationWhenFetched, String threadTokenKey, Integer page, LatLngBounds latLngBoundsWhenFetched) {
        if (extraFilters != null) {
            String compoundKey = extraFilters.getUniqueId();
            ResponsaUtil.log("aiaiai", compoundKey);
        }
        setLastFetchedAtDate(key, extraFilters, new Date());
        setFetchedPosition(key, extraFilters, visibleLocationWhenFetched);
        Integer fetchedMaxPage = getLastFetchedAtPage(key, extraFilters);
        if (fetchedMaxPage == null || page > fetchedMaxPage) {
            removeLastFetchedAtDate(key, extraFilters);
            setLastFetchedAtPage(key, extraFilters, page);
        }
        ResponsaUtil.log("colchaderetalhos", "----------------");
        ResponsaUtil.log("colchaderetalhos", "updateLastFetchedAt called");

        addFetchedLatLngBounds(key, extraFilters, latLngBoundsWhenFetched);

        readyToPopulate.put(threadTokenKey, true);
    }

    private void addFetchedLatLngBounds(String key, ExtraFilters extraFilters, LatLngBounds newFetchedLatLngBounds) {
        ResponsaUtil.log("colchaderetalhos", "-------------");
        ResponsaUtil.log("colchaderetalhos", "entered addFetchedLatLngBounds. unique key="+getCompoundKey(key, extraFilters));
        if (newFetchedLatLngBounds == null) {
            ResponsaUtil.log("colchaderetalhos", "newFetchedLatLngBounds is empty. bye.");
            return;
        }
        List<LatLngBounds> fetchedLatLngBoundsListOfThisView = getFetchedLatLngBounds(key, extraFilters);
        if (fetchedLatLngBoundsListOfThisView == null) {
            fetchedLatLngBoundsListOfThisView = new ArrayList<LatLngBounds>();
        }
        fetchedLatLngBoundsListOfThisView.add(newFetchedLatLngBounds);
        ResponsaUtil.log("colchaderetalhos", "newFetchedLatLngBounds = "+newFetchedLatLngBounds);
        ResponsaUtil.log("colchaderetalhos", "fetchedLatLngBoundsListOfThisView = "+fetchedLatLngBoundsListOfThisView);

        fetchedLatLngBoundsListOfThisView = ResponsaObjectsMap.cleanLatLngBoundsArray(fetchedLatLngBoundsListOfThisView);
        ResponsaUtil.log("colchaderetalhos", "Cleaned: fetchedLatLngBoundsListOfThisView = "+fetchedLatLngBoundsListOfThisView);
        setFetchedLatLngBounds(key,extraFilters, fetchedLatLngBoundsListOfThisView);
    }

    public void resetReadyToPopulate(String threadTokenKey) {
        readyToPopulate.remove(threadTokenKey);
    }

    public void resetReadyToPopulate(ArrayList threadTokenKeys) {
        for (int i=0; i<threadTokenKeys.size(); i++) {
            readyToPopulate.remove(threadTokenKeys.get(i));
        }
    }

    public Boolean needRemoteFetch(String key) {
        return needRemoteFetch(key,null, 0);
    }

    public Boolean needRemoteFetch(String key, ExtraFilters extraFilters, Integer page) {

        Integer lastFetchedPage = getLastFetchedAtPage(key, extraFilters);

        ResponsaUtil.log("newdatamanagerload","Checking if need remote fetch: key="+ getCompoundKey(key, extraFilters) + ", page=" + ((page==null)?"null":page.toString()));


        if (page > lastFetchedPage) {
            removeLastFetchedAtDate(key, extraFilters);
            ResponsaUtil.log("newdatamanagerload","Yes, needs remote fetch");
            return true;
        }

        if ("initiativesAndGatherings".equals(key) || "nearestInitiative".equals(key)) {
            LocationCoordinates lastFetchedPosition = getFetchedPosition(key, extraFilters);

            if (lastFetchedPosition ==  null) {
                ResponsaUtil.log("newdatamanagerload","Yes, needs remote fetch because there is no lastFetchedPosition");
                return true;
            }

            if (LocationCoordinates.getVisibleLocation().getDistanceInKilometersTo(lastFetchedPosition) > Constants.DISTANCE_IN_KMS_TO_FETCH_FROM_REMOTE
              /*  || coordinates.getCoordinateType().equals(LocationCoordinates.CoordinateType.BY_GPS) */) {
                //last date becomes irrelevant because the user moved
                removeLastFetchedAtDate(key, extraFilters);
                ResponsaApplication.responsaDataManager.clearLocal(key, extraFilters);
                ResponsaUtil.log("newdatamanagerload","Yes, needs remote fetch because user moved");
                return true;
            }
        }

        if ("initiativesAndGatheringsWithinGeoBox".equals(key)) {
            if (!ResponsaObjectsMap.containsLatLngBounds(getFetchedLatLngBounds(key, extraFilters), ResponsaObjectsMap.mapViewBounds)) {
                removeLastFetchedAtDate(key, extraFilters);
                //ResponsaApplication.responsaDataManager.clearLocal(key);
                ResponsaUtil.log("newdatamanagerload","Yes, needs remote fetch because mapViewBounds is not contained in data LatLngBounds");
                return true;
            }
        }

        Date lastFetchedAt = getLastFetchedAtDate(key, extraFilters);
        if (lastFetchedAt == null) {
            ResponsaUtil.log("newdatamanagerload","Yes, needs remote fetch because lastFetchedAt is null");
            return true;
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(ResponsaApplication.getAppContext());
        Integer hoursToExpire = Integer.parseInt(preferences.getString("p_hours_to_check_for_updates", "6"));
        if (System.currentTimeMillis() > lastFetchedAt.getTime() + 1000 * 60 * 3600 * hoursToExpire) {
            ResponsaUtil.log("newdatamanagerload","Yes, needs remote fetch because data is older than "+hoursToExpire+" hours");
            // If data is old, the fetchedLatLngBounds must be cleared for next moves
            removeFetchedLatLngBounds(key, extraFilters);
            return true;
        }

        ResponsaUtil.log("newdatamanagerload","No, does not need remote fetch");
        return false;
    }

    /**
     * Generates an unique key that takes into consideration the view+objectId ("key") and the filters applied (extraFilters). This unique key keeps track of
     * how the user is interacting with the network (lastfetchedat dates, positions, latlngbounds, etc...)
     * @param key The info about the view and eventually the objectId
     * @param extraFilters The info about eventual filters applied to this view
     * @return the unique ID, a string
     */
    public static String getCompoundKey(String key, ExtraFilters extraFilters) {
        String ret = "key_"+key;
        if (extraFilters != null) {
            String uniqueId = extraFilters.getUniqueId();
            if (uniqueId!=null && uniqueId!="") {
                ret += "_"+uniqueId;
            }
        }
        return ret;
    }

    // handle lastFetchedAtDate
    public Date getLastFetchedAtDate(String key, ExtraFilters extraFilters) {
        return fetchedDates.get(getCompoundKey(key, extraFilters));
    }
    public void setLastFetchedAtDate(String key, ExtraFilters extraFilters, Date date) {
        fetchedDates.put(getCompoundKey(key, extraFilters), date);
    }
    public void removeLastFetchedAtDate(String key, ExtraFilters extraFilters) {
        fetchedDates.remove(getCompoundKey(key, extraFilters));
    }

    // handle lastFetchedAtPage
    public Integer getLastFetchedAtPage(String key, ExtraFilters extraFilters) {
        return (fetchedPages.get(getCompoundKey(key, extraFilters)) == null)
                ? 0
                : fetchedPages.get(getCompoundKey(key, extraFilters));
    }
    public void setLastFetchedAtPage(String key, ExtraFilters extraFilters, Integer page) {
        fetchedPages.put(getCompoundKey(key, extraFilters), page);
    }

    // Handle fetchedPositions
    public LocationCoordinates getFetchedPosition(String key, ExtraFilters extraFilters) {
        return fetchedPositions.get(getCompoundKey(key, extraFilters));
    }
    public void setFetchedPosition(String key, ExtraFilters extraFilters, LocationCoordinates locationCoordinate) {
        fetchedPositions.put(getCompoundKey(key, extraFilters), locationCoordinate);
    }
    public void removeFetchedPosition(String key, ExtraFilters extraFilters) {
        fetchedPositions.remove(getCompoundKey(key, extraFilters));
    }

    // Handle fetchedLatLngBounds
    public List<LatLngBounds> getFetchedLatLngBounds(String key, ExtraFilters extraFilters) {
        return fetchedLatLngBounds.get(getCompoundKey(key, extraFilters));
    }
    public void setFetchedLatLngBounds(String key, ExtraFilters extraFilters, List<LatLngBounds> latLngBoundsList) {
        fetchedLatLngBounds.put(getCompoundKey(key, extraFilters), latLngBoundsList);
    }
    public void removeFetchedLatLngBounds(String key, ExtraFilters extraFilters) {
        fetchedLatLngBounds.remove(getCompoundKey(key, extraFilters));
    }

}
