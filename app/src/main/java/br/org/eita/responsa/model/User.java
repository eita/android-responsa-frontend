package br.org.eita.responsa.model;


import android.content.Context;
import android.content.SharedPreferences;
import android.os.Build;
import android.preference.PreferenceManager;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.annotations.SerializedName;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.Serializable;
import java.util.Date;

import br.org.eita.responsa.Constants;
import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.backend_comm.BackendApi;
import br.org.eita.responsa.components.ResponsaPhoneNumber;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.util.RestError;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class User implements Serializable {

    private final static Integer SMS_TIMEOUT_MINUTES = 1;
    private final static Integer SMS_FETCH_STATUS_SECONDS = 10;

    public final static String USER_BACKEND_ID = "backendId";
    public final static String USER_ID_PREFERENCE = "userId";
    public final static String USER_TOKEN_PREFERENCE = "userToken";
    public final static String PARSE_USERNAME = "parseUsername";
    public final static String PARSE_PASSWORD = "parsePassword";

    public final static String USER_TERRITORY_ID = "userLocationId";
    public final static String USER_NAME        = "userName";
    public final static String TUTORIAL_APPEARED = "tutorialAppeared";

    public final static String LAST_CONFIRMATION_SMS_DATE = "lastSmsDate";

    public void setId(Integer id) {
        this.id = id;
    }

    public void setUid(String uid) {
        this.uid = uid;
    }

    public void setAppToken(String appToken) {
        this.appToken = appToken;
    }

    public String getUid() {
        return uid;
    }

    public Integer getId() {
        return id;
    }

    public String getAppToken() {
        return appToken;
    }

    private boolean timeoutsChecking = false;

    public void clearSmsFailureHistory() {
        this.smsStatusGroup = null;
        this.smsErrorGroup = null;
        this.smsStatusName = null;
        this.smsErrorName = null;
        stopSmsTimeoutTimer();
    }


    public static class State {

        public final static State NEW = new State();
        public final static State PENDING_NAME_AND_TERRITORY_INFO = new State();
        public final static State PENDING_CONFIRMATION = new State();
        public final static State CONFIRMATION_TIMEOUT =  new State();
        public final static State SMS_SENDING_FAILURE = new State();
        public final static State CONFIRMED = new State();
        public final static State PHONE_NUMBER_RESETED = new State();
    }


    private Context context = null;


    @SerializedName("id")
    public Integer id;

    @SerializedName("uid")
    public String uid;

    @SerializedName("app_token")
    public String appToken;

    @SerializedName("parse_username")
    public String parseUsername;

    @SerializedName("parse_password")
    public String parsePassword;

    @SerializedName("user_name")
    public String userName;

    @SerializedName("territory_id")
    public String territoryId;

    @SerializedName("tutorial_appeared")
    public Boolean tutorialAppeared = false;

    @SerializedName("sms_status_name")
    public String smsStatusName;

    @SerializedName("sms_status_group")
    public String smsStatusGroup;

    @SerializedName("sms_error_name")
    public String smsErrorName;

    @SerializedName("sms_error_group")
    public String smsErrorGroup;

    public User(Context context) {
        this.context = context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public static User getCurrentUser(Context context) {
        if (ResponsaApplication.currentUser != null) {
            return ResponsaApplication.currentUser;
        }
        User user = new User(context);
        user.loadDataFromPreferences();
        return user;
    }

    /***
     * @return User's current state
     */
    public State getState() {

        //Estado:
        //usuário forneceu telefone -> em espera pelo cód confirmação
        //5 minutos em espera -> opção para enviar código novamente ou mudar telefone (volta p inicio)

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        if (preferences.getString(USER_ID_PREFERENCE,null) == null) {
            if (preferences.getString(USER_BACKEND_ID,null) == null) {
                return State.NEW;
            } else {
                return State.PHONE_NUMBER_RESETED;
            }
        }

        if (preferences.getString(USER_TOKEN_PREFERENCE,null) != null) {
            return State.CONFIRMED;
        }

        if (preferences.getString(USER_NAME,null) == null) {
            return State.PENDING_NAME_AND_TERRITORY_INFO;
        }

        if ("UNDELIVERABLE".equals(smsStatusGroup) || "EXPIRED".equals(smsStatusGroup) || "REJECTED".equals(smsStatusGroup)) {
            return State.SMS_SENDING_FAILURE;
        }

        Gson gson = new Gson();
        Date lastConfirmationSmsDate;

        //Um erro muito estranho ocorreu para um usuário na hora de recuperar esta data, a biblioteca GSON não conseguiu desserializar
        //uma data que ela mesma tinha serializado. Aqui faz o tratamento caso este erro ocorra de novo.
        try {
            lastConfirmationSmsDate = gson.fromJson(preferences.getString(LAST_CONFIRMATION_SMS_DATE, null), Date.class);
        } catch (Exception e) {
            return State.CONFIRMATION_TIMEOUT;
        }

        Date actualDate = new Date();

        if (actualDate.getTime() - lastConfirmationSmsDate.getTime() > (1000 * 60 * SMS_TIMEOUT_MINUTES)) {
            return State.CONFIRMATION_TIMEOUT;
        }

        return State.PENDING_CONFIRMATION;

    }

    public int getSmsFailureMessage() {

        Integer res = 0;
        if ("UNDELIVERABLE".equals(smsStatusGroup)) {
            res = R.string.sms_failure_undeliverable;
        } else if ("EXPIRED".equals(smsStatusGroup)) {
            res = R.string.sms_failure_undeliverable;
        } else if ("REJECTED".equals(smsStatusGroup)) {
            res = R.string.sms_failure_rejected;
        }
        return res;
    }

    /***
     * Save user variables in preferences
     */
    public void savePreferences() {
        Gson gson = new Gson();

        if (context == null) {
            context = ResponsaApplication.currentUser.context;
        }

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(USER_BACKEND_ID, this.id.toString());
        editor.putString(USER_ID_PREFERENCE, this.uid );
        editor.putString(USER_TOKEN_PREFERENCE, this.appToken);
        if (this.parseUsername != null) {
            editor.putString(PARSE_USERNAME,this.parseUsername);
            editor.putString(PARSE_PASSWORD,this.parsePassword);
        }
        if (this.userName != null) {
            editor.putString(USER_NAME,this.userName);
            editor.putString(USER_TERRITORY_ID,this.territoryId);
        }

        editor.putBoolean(TUTORIAL_APPEARED,Boolean.TRUE.equals(this.tutorialAppeared));

        editor.commit();

        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
        installation.add("user_id", this.id.toString());
        installation.add("user_uid", this.uid);
        //installation.add("androidVersion", android.os.Build.VERSION.SDK_INT);
        installation.put("osVersion", android.os.Build.VERSION.SDK_INT);
        installation.saveInBackground(new SaveCallback() {
            @Override
            public void done(ParseException e) {
                /*
                String msg = "ok!";
                if (e != null) {
                    msg = e.getMessage();
                }
                Toast.makeText(ResponsaApplication.currentUser.context,msg,Toast.LENGTH_LONG).show();
                */

            }
        });
    }

    private void loadDataFromPreferences() {
        if (context == null)
            return;
        //throw new Exception("Context not defined for user");

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        try{
            this.id = Integer.parseInt(preferences.getString(USER_BACKEND_ID, null));
        } catch (NumberFormatException e) {
            this.id = null;
        }
        this.uid = preferences.getString(USER_ID_PREFERENCE,null);
        this.appToken = preferences.getString(USER_TOKEN_PREFERENCE,null);

        this.parseUsername = preferences.getString(PARSE_USERNAME,null);
        this.parsePassword = preferences.getString(PARSE_PASSWORD,null);

        this.userName = preferences.getString(USER_NAME,null);
        this.territoryId = preferences.getString(USER_TERRITORY_ID,null);
        this.tutorialAppeared = preferences.getBoolean(TUTORIAL_APPEARED,false);
    }


    public void storeNameAndTerritory(String name, HomeTerritory selectedLocation) {
        this.userName = name;
        this.territoryId = selectedLocation.id;
        savePreferences();
    }

    public void storeNumber(String uid, final Registrator registrator) {
        if (this.id == null) {
            register(uid,registrator);
        } else {
            changeNumber(uid, registrator);
        }
    }

    public void parseLogout() {
        ParseUser.logOut();
    }

    public void parseLogInBackground(final ResponsaUserParseLoginCallback cb) {
        if (this.parseUsername != null) {
            final Context userContext = context;

            ParseUser.logInInBackground(this.parseUsername, this.parsePassword, new LogInCallback() {
                public void done(ParseUser user, ParseException e) {
                    if (e != null) {
                        ResponsaUtil.log("LOGIN", "ParseUser.loginInBackground Error: " + e.getMessage());
                        

                    }
                    if (user != null) {
                        // Hooray! The user is logged in.
                        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                        installation.put("user", user);
                        //installation.add("androidVersion", android.os.Build.VERSION.SDK_INT);
                        installation.put("osVersion", android.os.Build.VERSION.SDK_INT);
                        installation.saveInBackground(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {
                                /*
                                String msg = "ok!";
                                if (e != null) {
                                    msg = e.getMessage();
                                }
                                Toast.makeText(ResponsaApplication.currentUser.context,msg,Toast.LENGTH_LONG).show();
                                */

                            }
                        });
                        //Toast.makeText(userContext, "Logged in into PARSE", Toast.LENGTH_SHORT).show();
                    } else {
                        // Signup failed. Look at the ParseException to see what happened.
                        //Toast.makeText(userContext, "Error in login into PARSE", Toast.LENGTH_SHORT).show();
                    }
                    if (cb != null) {
                        cb.done(user,e);
                    }
                }
            });
        }
    }


    //Registers user in the backend; sets preference on success, toast notification on failure
    private void register(String uid, final Registrator registrator) {
        if (context == null)
            return;
        //throw new Exception("Context not defined for user");

        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.backendUrl)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        BackendApi backendApi = restAdapter.create(BackendApi.class);

        final String reqUserId = uid;

        backendApi.createUser(reqUserId, ResponsaApplication.installationObjectId, "", new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                user.context = ResponsaApplication.currentUser.context;
                ResponsaApplication.currentUser = user;
                user.savePreferences();
                ResponsaApplication.currentUser.setNowAsLastSmsTime(registrator);
                ResponsaApplication.currentUser.startSmsTimeoutTimer(registrator);
                registrator.onUserRegistrationRequestSuccess();
            }

            @Override
            public void failure(RetrofitError error) {
                //Tratar erro
                registrator.onUserRegistrationRequestFailure(error);
            }
        });
    }

    public void confirm(String confirmationCode, final Registrator registrator)  {
        if (context == null)
            return;
            //throw new Exception("Context not defined for user");

        if (uid != null) {
            ResponsaApplication.currentUser.stopSmsTimeoutTimer();
            RestAdapter restAdapter = new RestAdapter.Builder()
                    .setEndpoint(Constants.backendUrl)
                    .setLogLevel(RestAdapter.LogLevel.FULL)
                    .build();

            final BackendApi backendApi = restAdapter.create(BackendApi.class);

            backendApi.confirmUser(uid,confirmationCode, "", new Callback<User>() {
                @Override
                public void success(User user, Response response) {
                    user.savePreferences();
                    ResponsaApplication.currentUser.loadDataFromPreferences();
                    ResponsaApplication.currentUser.parseLogInBackground(getFirstUserSetupCallback(backendApi,registrator));
                }

                @Override
                public void failure(RetrofitError error) {
                    if (error.getResponse() != null) {
                        RestError body = (RestError) error.getBodyAs(RestError.class);
                        registrator.onUserConfirmationFailure(body.errorDetails);
                    } else {
                        registrator.onUserConfirmationFailure(context.getString(R.string.u_error_network));
                    }
                }
            });
        }
    }

    /***
     * Used when number has changed
     * @param newUid
     * @param registrator
     */
    private void changeNumber(String newUid, final Registrator registrator) {
        if (context == null)
            return; //throws

        if (id == null)
            return; //throws

        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.backendUrl)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        BackendApi backendApi = restAdapter.create(BackendApi.class);

        backendApi.changeNumber(newUid, this.id, "", new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                user.savePreferences();
                ResponsaApplication.currentUser.loadDataFromPreferences();
                registrator.onUserRegistrationRequestSuccess();
            }

            @Override
            public void failure(RetrofitError error) {
                registrator.onUserRegistrationRequestFailure(error);
            }
        });
    }

    public void resendSms(final Registrator registrator) {
        if (context == null)
            return; //throws

        if (id == null)
            return; //throws

        if (uid == null)
            return;


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.backendUrl)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        BackendApi backendApi = restAdapter.create(BackendApi.class);

        backendApi.resendSms(this.id, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                ResponsaApplication.currentUser.setNowAsLastSmsTime(registrator);
                ResponsaApplication.currentUser.startSmsTimeoutTimer(registrator);
                registrator.onSmsResendSuccess();
            }

            @Override
            public void failure(RetrofitError error) {
                registrator.onSmsResendFailure(error);
            }
        });
    }

    //Fetches the SMS status in the server (did it find the proper route or not?)
    public void fetchSmsStatus(final Registrator registrator) {
        if (context == null)
            return; //throws

        if (id == null)
            return; //throws

        if (uid == null)
            return;


        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(Constants.backendUrl)
                .setLogLevel(RestAdapter.LogLevel.FULL)
                .build();

        BackendApi backendApi = restAdapter.create(BackendApi.class);

        backendApi.smsStatus(this.id, new Callback<User>() {
            @Override
            public void success(User user, Response response) {
                User currentUser = ResponsaApplication.currentUser;

                currentUser.smsStatusGroup = user.smsStatusGroup;
                currentUser.smsStatusName = user.smsStatusName;
                currentUser.smsErrorGroup = user.smsErrorGroup;
                currentUser.smsErrorName =  user.smsErrorName;

                //Já deu problema, status está resolvido, fecha o timer do status
                if ("UNDELIVERABLE".equals(currentUser.smsStatusGroup) || "EXPIRED".equals(currentUser.smsStatusGroup) || "REJECTED".equals(currentUser.smsStatusGroup)) {
                    stopSmsTimeoutTimer();
                }

                if (registrator != null) {
                    registrator.onSmsFetchStatusSuccess(user.smsStatusGroup,user.smsStatusName,user.smsErrorGroup,user.smsErrorName);
                }
            }

            @Override
            public void failure(RetrofitError error) {
                if (registrator != null) {
                    registrator.onSmsFetchStatusFailure(error);
                }
            }
        });

    }

    public void resetNumber() {
        this.uid = null;
        savePreferences();
    }

    private void setNowAsLastSmsTime(final Registrator registrator) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(context);
        final Date lastConfirmationDate = new Date();
        final User currentUser = this;

        Gson gson = new Gson();
        SharedPreferences.Editor editor = preferences.edit();
        editor.putString(LAST_CONFIRMATION_SMS_DATE, gson.toJson(lastConfirmationDate));
        editor.commit();

        //Set timeout to call confirmation timeout
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        if (currentUser.getState() == State.CONFIRMATION_TIMEOUT) {
                            registrator.onUserConfirmationTimeout();
                        }
                    }
                },
                (1000 * 60 * SMS_TIMEOUT_MINUTES + 100));

    }

    /**
     * Timeout Timers: Check every 10 seconds about the status of the last sms sent
     */
    private void startSmsTimeoutTimer(final Registrator registrator) {
        if (timeoutsChecking) return; //Already running
        timeoutsChecking = true;
        scheduleTimeoutChecking(registrator);
    }

    private void scheduleTimeoutChecking(final Registrator registrator) {
        if (timeoutsChecking ==  false || registrator == null) return;

        //Set timeout to call confirmation timeout
        new android.os.Handler().postDelayed(
                new Runnable() {
                    public void run() {
                        if (timeoutsChecking ==  false || registrator == null) return;

                        fetchSmsStatus(registrator);
                        scheduleTimeoutChecking(registrator);
                    }
                },
                (1000 * SMS_FETCH_STATUS_SECONDS));
    }

    private void stopSmsTimeoutTimer() {
        timeoutsChecking = false;
    }

    public ResponsaPhoneNumber getPhoneNumber() {
        if (this.uid == null)
            return null;

        ResponsaPhoneNumber phoneNumber = new ResponsaPhoneNumber(this.context);
        if (! phoneNumber.fromString(this.uid.replaceAll("dummy_","")) ) {
            return null;
        }
        return phoneNumber;
    }

    public interface Registrator {
        public void onUserRegistrationRequestSuccess();
        public void onUserRegistrationRequestFailure(RetrofitError error);
        public void onUserConfirmationSuccess();
        public void onUserConfirmationFailure(String error);
        public void onUserConfirmationTimeout();
        public void onSmsResendSuccess();
        public void onSmsResendFailure(RetrofitError error);
        public void onSmsFetchStatusSuccess(String smsStatusGroup, String smsStatusName, String smsErrorGroup, String smsErrorName);
        public void onSmsFetchStatusFailure(RetrofitError error);
    }

    public interface ResponsaUserParseLoginCallback {
        public void done(ParseUser user, ParseException e);
    }

    public ResponsaUserParseLoginCallback getFirstUserSetupCallback(final BackendApi backendApi, final Registrator registrator) {
        return new ResponsaUserParseLoginCallback() {
            @Override
            public void done(ParseUser parseUser, ParseException e) {
                if (parseUser != null) {
                    if (backendApi != null) {
                        backendApi.wipeDummy(uid, new Callback<User>() {
                            @Override
                            public void success(User user, Response response) {

                            }

                            @Override
                            public void failure(RetrofitError error) {

                            }
                        });
                    }

                    if (registrator != null) {
                        registrator.onUserConfirmationSuccess();
                    }
                    parseUser.put("name", ResponsaApplication.currentUser.userName);
                    parseUser.put("territoryId", ResponsaApplication.currentUser.territoryId);

                    //Region id
                    HomeTerritory loc = ResponsaApplication.territoryDb.findTerritoryById(ResponsaApplication.currentUser.territoryId);
                    parseUser.put("territoryRegionId",loc.getRegion().id);
                    parseUser.put("territoryLabel",loc.label);

                    if (!parseUser.getBoolean("loggedInOnce")) {
                        parseUser.put("loggedInOnce", true);
                        parseUser.saveEventually(new SaveCallback() {
                            @Override
                            public void done(ParseException e) {

                            }
                        });
                        Notification.sendSignupNotification(context, parseUser.getString("territoryId"));
                    } else {
                        parseUser.saveEventually();
                    }

                    //populate responsaObjects with responsaLocation coordinates
                    //HomeTerritory responsaLocation = ResponsaApplication.territoryDb.findTerritoryById(parseUser.getString("territoryId"));
                    //LocationCoordinates.setCurrentLocation(responsaLocation.getLocationCoordinates());

                    ResponsaApplication.responsaDataManager.loadInitialData(parseUser);

                    //Unsubscribe from all previous territory channels
                    Notification.cleanChannelTypeSubscriptions(Notification.TERRITORY_CHANNEL);

                    //Subscribe in current territory channel
                    Notification.subscribeToChannel(Notification.TERRITORY_CHANNEL, territoryId);
                } else {
                    Toast.makeText(ResponsaApplication.getAppContext(), R.string.e_some_error_in_user_confirmation,Toast.LENGTH_SHORT).show();
                }

            }
        };
    }
}
