package br.org.eita.responsa.components;


import android.content.Context;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.TextView;

import br.org.eita.responsa.R;
import br.org.eita.responsa.model.Answerable;
import br.org.eita.responsa.model.Votable;
import br.org.eita.responsa.model.VotableAnswerable;

public class VotableStatsWidget extends RelativeLayout{

    private TextView totalAnswers;
    private TextView totalAnswersLabel;

    private TextView relevance;

    public VotableStatsWidget(Context context) {
        this(context,null);
    }

    public VotableStatsWidget(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public VotableStatsWidget(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

        TypedArray a = getContext().obtainStyledAttributes(attrs, R.styleable.VotableStatsWidget);


        Boolean simple = a.getBoolean(R.styleable.VotableStatsWidget_voteStatsSimple, false);
        //Boolean simple = attrs.getAttributeBooleanValue("android","voteStatsSimple",false);
        int viewResource = simple ? R.layout.votable_stats_widget_simple : R.layout.votable_stats_widget;

        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(viewResource, this);


        totalAnswers = (TextView) findViewById(R.id.totalAnswers);
        totalAnswersLabel = (TextView) findViewById(R.id.totalAnswersLabel);
        relevance = (TextView) findViewById(R.id.relevance);
    }

    public void setVotableAnswerable(VotableAnswerable votableAnswerable) {
        populateVotable(votableAnswerable);
        populateAnswerable(votableAnswerable);

        votableAnswerable.onVotableChange(new Votable.OnVotableChange() {
            @Override
            public void onChange(Votable v) {
                populateVotable(v);
            }
        });

        votableAnswerable.onAnswerableChange(new Answerable.OnAnswerableChange() {
            @Override
            public void onChange(Answerable a) {
                populateAnswerable(a);
            }
        });
    }

    private void populateVotable(Votable votable) {
        Resources resources = getContext().getResources();

        if (relevance != null) {
            relevance.setText(String.format("%d", votable.getRelevance()));
        }
    }

    private void populateAnswerable(Answerable answerable) {
        Resources resources = getContext().getResources();
        totalAnswers.setText(String.format("%d", answerable.getTotalAnswers()));
        totalAnswersLabel.setText(resources.getQuantityString(R.plurals.u_answers, answerable.getTotalAnswers()));
    }
}
