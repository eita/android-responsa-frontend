package br.org.eita.responsa.model.search_api;


import java.util.List;

public class ChallengeFrontPageSummary {
    public List<ChallengeSummary> challenges;
    public List<ChallengeSummary> discoveries;
}
