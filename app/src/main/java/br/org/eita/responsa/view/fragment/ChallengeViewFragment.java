package br.org.eita.responsa.view.fragment;


import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.support.annotation.Nullable;


import android.support.v4.app.Fragment;
import android.text.Spannable;
import android.text.SpannableStringBuilder;
import android.text.format.DateUtils;
import android.text.style.StyleSpan;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.parse.ParseUser;
import com.shamanland.fonticon.FontIconDrawable;
import com.shamanland.fonticon.FontIconTextView;

import java.util.Date;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.components.VotableStatsWidget;
import br.org.eita.responsa.components.VoteUpDownWidget;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.model.ChallengeUserJoin;
import br.org.eita.responsa.model.HomeTerritory;
import br.org.eita.responsa.model.ResponsaObject;
import br.org.eita.responsa.view.activity.ChallengeFormActivity;
import br.org.eita.responsa.view.activity.ResponsaObjectFormActivity;


public class ChallengeViewFragment  extends Fragment {

    Challenge challenge;
    ChallengeUserJoin challengeUserJoin;

    TextView challengeTitle;
    TextView challengeDescription;
    VoteUpDownWidget voteUpDownWidget;
    ProgressBar voteUpDownProgress;
    VotableStatsWidget votableStatsWidget;

    FontIconTextView favoritarIcon = null;

    TextView userName;
    TextView challengeDate;
    TextView challengeLocation;

    Button editarBtn;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_challenge_view, container, false);

        findAllViewsByIds(view);

        if (challenge != null && challengeUserJoin != null) {
            populateViewData(challenge, challengeUserJoin);
        }

        return view;
    }

    private void populateViewData(final Challenge challenge, ChallengeUserJoin challengeUserJoin) {
        challengeTitle.setText(challenge.getTitle());
        challengeDescription.setText(challenge.getDescription());
        voteUpDownWidget.setVotable(challenge);
        votableStatsWidget.setVotableAnswerable(challenge);

        Boolean userIsChallengeAuthor = ParseUser.getCurrentUser().equals(challenge.getAuthor());
        String disabledMessage = (userIsChallengeAuthor) ? getString(R.string.u_vote_disabled_user_is_author) : null;
        voteUpDownWidget.setVoteEnabled(!userIsChallengeAuthor,disabledMessage);

        userName.setText(challenge.getAuthor().getString("name"));

        Date createdAtTime = challenge.getCreatedAt();
        challengeDate.setText((createdAtTime == null) ? ResponsaApplication.getAppContext().getString(R.string.u_created_now) : DateUtils.getRelativeTimeSpanString(createdAtTime.getTime()));

        if (challenge.getTerritoryId() != null) {
            HomeTerritory location = ResponsaApplication.territoryDb.findTerritoryById(challenge.getTerritoryId());
            String locationText = ResponsaApplication.getAppContext().getString(R.string.challenge_in_location, location.label);
            SpannableStringBuilder sb = new SpannableStringBuilder(locationText);
            StyleSpan bss = new StyleSpan(android.graphics.Typeface.BOLD);
            sb.setSpan(bss, locationText.length() - location.label.length(), locationText.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
            challengeLocation.setText(sb);
            challengeLocation.setVisibility(View.VISIBLE);
        }

        voteUpDownProgress.setVisibility(View.GONE);
        voteUpDownWidget.setVisibility(View.VISIBLE);
        populateFavoritar(challengeUserJoin.getFavorite());

        if (editarBtn != null) {
            ParseUser author = challenge.getAuthor();
            if (author != null && author.equals(ParseUser.getCurrentUser())) {
                editarBtn.setVisibility(View.VISIBLE);
            }
            editarBtn.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent intent = new Intent(getActivity(), ChallengeFormActivity.class);
                    intent.putExtra("objectId",challenge.getObjectId());
                    ChallengeFormActivity.transientChallenge = challenge;
                    startActivity(intent);
                }
            });
        }

    }

    private void findAllViewsByIds(View parentView) {
        challengeTitle = (TextView) parentView.findViewById(R.id.challengeTitle);
        challengeDescription  = (TextView) parentView.findViewById(R.id.challengeDescription);
        voteUpDownWidget = (VoteUpDownWidget) parentView.findViewById(R.id.voteUpDownWidget);
        voteUpDownProgress = (ProgressBar) parentView.findViewById(R.id.voteUpDownProgress);
        votableStatsWidget = (VotableStatsWidget) parentView.findViewById(R.id.votableStatsWidget);
        favoritarIcon = (FontIconTextView) parentView.findViewById(R.id.favoritarIcon);
        userName = (TextView) parentView.findViewById(R.id.userName);
        challengeDate = (TextView) parentView.findViewById(R.id.challengeDate);
        challengeLocation = (TextView) parentView.findViewById(R.id.challengeLocation);
        editarBtn = (Button) parentView.findViewById(R.id.editarBtn);
    }

    public void updateChallenge(Challenge challenge, ChallengeUserJoin challengeUserJoin) {
        if (challenge != null) {
            this.challenge = challenge;
            this.challengeUserJoin = challengeUserJoin;
            if (challengeTitle != null) {
                populateViewData(challenge, challengeUserJoin);
            }
        }
    }

    public void populateFavoritar(Boolean favoritarState) {
        if (getActivity() != null) {
            Drawable icon = FontIconDrawable.inflate(getActivity(), (favoritarState ? R.xml.ic_acao_favorito_on : R.xml.ic_acao_favorito_off));
            favoritarIcon.setCompoundDrawables(null, icon, null, null);
        }
    }
}
