package br.org.eita.responsa.adapter.data_manager;

import android.util.Log;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import br.org.eita.responsa.R;
import br.org.eita.responsa.ResponsaApplication;
import br.org.eita.responsa.model.Challenge;
import br.org.eita.responsa.model.search_api.ChallengeFrontPageIds;
import br.org.eita.responsa.model.search_api.ChallengeRankedId;
import br.org.eita.responsa.model.search_api.ChallengeSummary;

public class ChallengeFrontPageData {
    public List<Challenge> challenges = new ArrayList<Challenge>();
    public List<Challenge> discoveries = new ArrayList<Challenge>();

    private String title;

    public void loadFromChallengeList(List<Challenge> chs) {
        challenges.clear();
        discoveries.clear();

        addFromChallengeList(chs);
    }

    public void addFromChallengeList(List<Challenge> chs) {
        ChallengeFrontPageIds rankInformation = ChallengeFrontPageIds.loadFromPreferences();

        if (rankInformation == null || rankInformation.challenges==null || rankInformation.discoveries==null) {
            return;
        }

        List<Challenge> chals = new ArrayList<>(chs);

        Iterator<ChallengeRankedId> iranksc = rankInformation.challenges.iterator();

        //Add challenges in order defined by rank
        while (iranksc.hasNext()) {
            Iterator<Challenge> ichals = chals.iterator();
            ChallengeRankedId chRank = iranksc.next();

            while (ichals.hasNext()) {
                Challenge ch = ichals.next();
                if (chRank.objectId.equals(ch.getObjectId())) {
                    if (ch != null) {
                        challenges.add(ch);
                    }
                    ichals.remove();
                }
            }
        }

        Iterator<ChallengeRankedId> iranksd = rankInformation.discoveries.iterator();

        //Add discoveries in order defined by rank
        while (iranksd.hasNext()) {
            Iterator<Challenge> idiscs = chals.iterator();
            ChallengeRankedId disRank = iranksd.next();

            while (idiscs.hasNext()) {
                Challenge ch = idiscs.next();
                if (disRank.objectId.equals(ch.getObjectId())) {
                    if (ch != null) {
                        discoveries.add(ch);
                    }
                    idiscs.remove();
                }
            }
        }
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public ArrayList<Challenge> getDataForView() {
        ArrayList<Challenge> chs = new ArrayList<Challenge>();

        Challenge title = new Challenge();

        if (challenges.size()>0) {
            title.setLayoutTitleViewString(ResponsaApplication.getAppContext().getString(R.string.u_challenges));
            chs.add(title);
            chs.addAll(challenges);
        }

        if (discoveries.size()>0) {
            title = new Challenge();
            title.setLayoutTitleViewString(ResponsaApplication.getAppContext().getString(R.string.u_discoveries));
            chs.add(title);
            chs.addAll(discoveries);
        }

        return chs;
    }
}
