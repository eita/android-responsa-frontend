package br.org.eita.responsa.managers;

import android.util.Log;

import com.mapbox.mapboxsdk.geometry.LatLngBounds;
import com.mapbox.mapboxsdk.maps.MapView;
import com.parse.ParseGeoPoint;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import br.org.eita.responsa.location.LocationCoordinates;
import br.org.eita.responsa.util.ResponsaObjectsMap;
import br.org.eita.responsa.util.ResponsaUtil;
import br.org.eita.responsa.util.ExtraFilters;


public class ResponsaQueryBuilder {

    public static ParseQuery buildLocalQuery(String view, JSONObject dataOptions, String pinName, String objectId, Integer page, ExtraFilters extraFilters) {
        if (pinName == null) {
            Log.e("ResponsaDataManager","falta pinName");
        }

        ResponsaUtil.log("QUERY", "--- BUILD LOCAL QUERY: "+view+" ---");


        try {
            ParseQuery query;

            //OR must be treated first, because it cannot be inserted after.
            if (dataOptions.has("OR")) {
                JSONObject orConstraints = dataOptions.getJSONObject("OR");
                ArrayList<ParseQuery<ParseObject>> resultQueries = new ArrayList<ParseQuery<ParseObject>>();

                for (Iterator<String> itOrConstraint = orConstraints.keys(); itOrConstraint.hasNext();) {
                    String constraintField = itOrConstraint.next();

                    //Each constraint field is an array or arrays
                    JSONArray constraintsForField = orConstraints.getJSONArray(constraintField);

                    for (int i=0; i < constraintsForField.length(); i++) {
                        Boolean useConstraint = false;
                        JSONArray constraint = constraintsForField.getJSONArray(i);

                        if (useConstraintInContext(constraint,"L")) {
                            ParseQuery<ParseObject> orConstraintQuery = new ParseQuery<ParseObject>(dataOptions.getString("className"));
                            addConstraintFromConfiguration(orConstraintQuery, constraintField, constraint, objectId, null, null, false);
                            resultQueries.add(orConstraintQuery);
                        }
                    }
                }
                query = ParseQuery.or(resultQueries);

            } else {
                query = new ParseQuery(dataOptions.getString("className"));
            }

            query.fromPin(pinName);

            // hasTerritoryFilter is necessary to avoid constraining by "near from" in the query below.
            Boolean hasTerritoryFilter = addConstraintFromFilters(query, extraFilters);

            for (Iterator<String> itKeys = dataOptions.keys(); itKeys.hasNext();) {
                String key = itKeys.next();
                if ("order".equals(key)) {
                    String orderType = dataOptions.getString("orderType");
                    if ("desc".equals(orderType)) {
                        query.orderByDescending(dataOptions.getString(key));
                    } else {
                        query.orderByAscending(dataOptions.getString(key));
                    }
                } else if ("limit".equals(key)) {
                    Integer limit = dataOptions.getInt(key);
                    query.setLimit(limit+1);
                    if (page>0) {
                        query.setSkip(page*limit);
                    }
                } else if ("includes".equals(key)) {
                    for (int j=0; j < dataOptions.getJSONArray(key).length(); j++) {
                        query.include(dataOptions.getJSONArray(key).getString(j));
                    }
                } else if ("constraints".equals(key)) {
                    JSONObject constraints = dataOptions.getJSONObject(key);
                    for (Iterator<String> itConstraint = constraints.keys(); itConstraint.hasNext();) {
                        /**
                         * Constraints to consider in local:
                         *  * comparator = 'near'
                         *  * constraintField = type
                         */
                        String constraintField = itConstraint.next();

                        if (useConstraintInContext(constraints.getJSONArray(constraintField),"L")) {
                            addConstraintFromConfiguration(query, constraintField, constraints.getJSONArray(constraintField), objectId, null, null, hasTerritoryFilter);
                        }
                    }
                }
            }



            return query;

        } catch (JSONException e) {
            ResponsaUtil.log("ResponsaQueryBuilder", "exception in building query");
            e.printStackTrace();
        }
        return null;
    }

    public static Boolean useConstraintInContext(JSONArray constraint, String context) {
        Boolean useConstraint = false;

        try {
            String constraintOptions = constraint.getString(2);
            if (constraintOptions != null && constraintOptions.contains(context)) {
                useConstraint = true;
            }
            if ("near".equals(constraint.getString(0))) {
                useConstraint = true;
            }
        } catch (JSONException e) { }

        return useConstraint;
    }

    public static ParseQuery buildRemoteQuery(String view, JSONObject dataOptions, String objectId, Date lastFetchedDate, List<String> outsourcedIds, List parents, Integer page, ExtraFilters extraFilters) {

        ResponsaUtil.log("QUERY", "--- BUILD REMOTE QUERY: "+view+" ---");

        try {
            ParseQuery query;

            //OR must be treated first, because it cannot be inserted after.
            if (dataOptions.has("OR")) {
                JSONObject orConstraints = dataOptions.getJSONObject("OR");
                ArrayList<ParseQuery<ParseObject>> resultQueries = new ArrayList<ParseQuery<ParseObject>>();
                ResponsaUtil.log("QUERY", "THIS IS AN 'OR' QUERY");
                ResponsaUtil.log("QUERY", "------------------------");

                for (Iterator<String> itOrConstraint = orConstraints.keys(); itOrConstraint.hasNext();) {
                    String constraintField = itOrConstraint.next();

                    //Each constraint field is an array or arrays
                    JSONArray constraintsForField = orConstraints.getJSONArray(constraintField);

                    for (int i=0; i < constraintsForField.length(); i++) {
                        JSONArray constraint = constraintsForField.getJSONArray(i);
                        ParseQuery<ParseObject> orConstraintQuery = new ParseQuery<ParseObject>(dataOptions.getString("className"));
                        addConstraintFromConfiguration(orConstraintQuery, constraintField, constraint, objectId, outsourcedIds, parents, false);
                        resultQueries.add(orConstraintQuery);
                    }
                }
                query = ParseQuery.or(resultQueries);

            } else {
                ResponsaUtil.log("QUERY", "THIS IS AN 'AND' QUERY");
                ResponsaUtil.log("QUERY", "------------------------");
                query = new ParseQuery(dataOptions.getString("className"));
            }

            // hasTerritoryFilter is necessary to avoid constraining by "near from" in the query below.
            Boolean hasTerritoryFilter = addConstraintFromFilters(query, extraFilters);

            for (Iterator<String> itFields = dataOptions.keys(); itFields.hasNext();) {
                String field = itFields.next();
                if ("order".equals(field)) {
                    String orderType = dataOptions.getString("orderType");
                    if ("desc".equals(orderType)) {
                        query.orderByDescending(dataOptions.getString(field));
                    } else {
                        query.orderByAscending(dataOptions.getString(field));
                    }
                } else if ("limit".equals(field)) {
                    Integer limit = dataOptions.getInt(field);
                    query.setLimit(limit+1);
                    if (page>0) {
                        query.setSkip(page*limit);
                    }
                } else if ("includes".equals(field)) {
                    JSONArray dataOptionsArray = dataOptions.getJSONArray(field);
                    for (int j=0; j < dataOptionsArray.length(); j++) {
                        String value = dataOptionsArray.getString(j);
                        query.include(value);
                        ResponsaUtil.log("QUERY", "includes "+value);
                    }
                } else if ("constraints".equals(field)) {
                    JSONObject constraints = dataOptions.getJSONObject(field);
                    ResponsaUtil.log("QUERY", "These are 'AND' constraints:");
                    for (Iterator<String> itConstraint = constraints.keys(); itConstraint.hasNext();) {
                        String constraintField = itConstraint.next();
                        addConstraintFromConfiguration(query, constraintField, constraints.getJSONArray(constraintField), objectId, outsourcedIds, parents, hasTerritoryFilter);
                    }
                }
            }

            if (lastFetchedDate != null) {
                query.whereGreaterThan("updatedAt",lastFetchedDate);
                ResponsaUtil.log("QUERY", "updatedAt. whereGreaterThan="+lastFetchedDate);
            }

            return query;

        } catch (JSONException e) {
            ResponsaUtil.log("ResponsaQueryBuilder","exception in building query");
            e.printStackTrace();
        }
        return null;
    }


    private static void addConstraintFromConfiguration(ParseQuery query, String field, JSONArray constraint, String objectId, List<String> outsourcedIds, List parents, Boolean hasTerritoryFilter) throws JSONException {
        String operation = constraint.getString(0);
        Object value = null;

        if (constraint.length() > 1) {
            value = constraint.get(1);
        }

        if ("=".equals(operation)) {
            query.whereEqualTo(field,translateValue(value, objectId));
            ResponsaUtil.log("QUERY", field+". whereEqualTo="+translateValue(value, objectId));
        } else if ("!=".equals(operation)) {
            query.whereNotEqualTo(field, translateValue(value, objectId));
            ResponsaUtil.log("QUERY", field+". whereNotEqualTo="+translateValue(value, objectId));
        } else if (">=".equals(operation)) {
            query.whereGreaterThanOrEqualTo(field, translateValue(value, objectId));
            ResponsaUtil.log("QUERY", field+". whereGreaterThanOrEqualTo="+translateValue(value, objectId));
        } else if ("near".equals(operation) && !hasTerritoryFilter) {
            // We can only use "near" constraint if the user has not filtered by territory.
            query.whereNear(field, (ParseGeoPoint) translateValue(value, objectId));
            ResponsaUtil.log("QUERY", field+". whereNearValue="+translateValue(value, objectId));
        } else if ("withinGeoBox".equals(operation)) {
            LatLngBounds mapViewBounds = (LatLngBounds) translateValue(value, objectId);
            query.whereWithinGeoBox(field, new ParseGeoPoint(mapViewBounds.getLatSouth(),mapViewBounds.getLonWest()), new ParseGeoPoint(mapViewBounds.getLatNorth(),mapViewBounds.getLonEast()));
            ResponsaUtil.log("QUERY", field+". whereWithinGeoBoxValue="+translateValue(value, objectId));

        } else if ("id=".equals(operation)) {
            //Operator id= uses third parameter: class name
            ParseQuery subQuery = new ParseQuery(constraint.getString(2));
            subQuery.whereEqualTo("objectId", translateValue(value, objectId));
            query.whereMatchesQuery(field, subQuery);
            ResponsaUtil.log("QUERY", field+". whereEqualTo="+translateValue(value, objectId)+"; whereMatchesQuery="+constraint.getString(2));
        } else if ("in_OUTSOURCED_IDS".equals(operation)) {
            query.whereContainedIn(field, outsourcedIds);
            ResponsaUtil.log("QUERY", field+". whereContainedIn="+ outsourcedIds);
        } else if ("in_PARENTS".equals(operation)) {
            query.whereContainedIn(field, parents);
            ResponsaUtil.log("QUERY", field+". whereContainedInValue="+value+"; whereContainedInObjectId="+objectId);
            ResponsaUtil.log("QUERY", field+". whereContainedIn="+value+"; parents="+parents.toString());
        } else if ("NOT_NULL".equals(operation)) {
            query.whereExists(field);
            ResponsaUtil.log("QUERY", field+". whereExists="+field);
        } else if ("NOT_NULL_DATE".equals(operation)) {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            try {
                Date d = sdf.parse("21/12/2012");
                query.whereGreaterThan(field,d);
                ResponsaUtil.log("QUERY", field+". whereGreaterThanDate="+d);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } else if ("NULL".equals(operation)) {
            query.whereDoesNotExist(field);
            ResponsaUtil.log("QUERY", field+". whereDoesNotExist");
        } else if ("STARTS_WITH".equals(operation)) {
            ResponsaUtil.log("QUERY", field+". whereStartsWith="+translateValue(value, objectId));
            query.whereStartsWith(field,(String)translateValue(value, objectId));
        }
    }

    private static Boolean addConstraintFromFilters(ParseQuery query, ExtraFilters extraFilters) {
        if (extraFilters == null) return false;
        ResponsaUtil.log("QUERY", "Filters:");
        Boolean hasTerritoryFilter = false;
        Iterator<ExtraFilters.Argument> iterator = extraFilters.iterator();
        while (iterator.hasNext()) {
            ExtraFilters.Argument argument = iterator.next();
            if ("territoryId".equals(argument.field)) {
                hasTerritoryFilter = true;
            }
            if ("in".equals(argument.operation)) {
                query.whereContainedIn(argument.field,(List)argument.value);
                ResponsaUtil.log("QUERY", argument.field + ". whereContainedIn: "+argument.value);
            } else if ("IS NULL".equals(argument.operation)) {
                query.whereDoesNotExist(argument.field);
                ResponsaUtil.log("QUERY", argument.field + ". whereDoesNot Exist");
            } else if ("IS NOT NULL".equals(argument.operation)) {
                query.whereExists(argument.field);
                ResponsaUtil.log("QUERY", argument.field + ". whereExists");
            } else if ("STARTS_WITH".equals(argument.operation)) {
                query.whereStartsWith(argument.field, (String)argument.value);
                ResponsaUtil.log("QUERY", argument.field + ". whereStartsWith: "+argument.value);
            }
        }
        return hasTerritoryFilter;
    }

    private static Object translateValue(Object value, String objectId) throws JSONException {

        if (!(value instanceof String)) {
            return value;
        }

        if ("NOW".equals(value)) {
            return new Date();
        } else if ("CURRENT_USER".equals(value)) {
            return ParseUser.getCurrentUser();
        } else if ("OBJECT_ID".equals(value)) {
            return objectId;
        } else if ("USER_LOCATION".equals(value)) {
            return LocationCoordinates.getVisibleLocation().toParseGeoPoint();
        } else if ("CURRENT_LOCATION".equals(value)) {
            return LocationCoordinates.getCurrentLocation().toParseGeoPoint();
        } else if ("MAPVIEW_BOUNDS".equals(value)) {
            return ResponsaObjectsMap.mapViewBounds;
        } else if ("true".equals(value)) {
            return true;
        } else if ("false".equals(value)) {
            return false;
        } else {
            return value;
        }
    }
}
