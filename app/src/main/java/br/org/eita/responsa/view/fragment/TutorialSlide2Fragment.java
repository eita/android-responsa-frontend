package br.org.eita.responsa.view.fragment;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import br.org.eita.responsa.R;


/**
 * A simple {@link Fragment} subclass.
 * Use the {@link TutorialSlide2Fragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class TutorialSlide2Fragment extends Fragment {


    public TutorialSlide2Fragment() {
        // Required empty public constructor
    }

    public static TutorialSlide2Fragment newInstance() {
        TutorialSlide2Fragment fragment = new TutorialSlide2Fragment();
        return fragment;
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View fragmentView = inflater.inflate(R.layout.tutorial_slide_2, container, false);


        return fragmentView;
    }

}
