package br.org.eita.responsa.model;


import com.parse.ParseClassName;
import com.parse.ParseObject;

import org.json.JSONObject;

@ParseClassName("Chegada")
public class Chegada extends ParseObject {
    public ParseObject getResponsaObject() {
        return getParseObject("responsaObject");
    }

    public void setResponsaObject(ParseObject responsaObject) {
        if (responsaObject == null) {
            this.put("responsaObject", JSONObject.NULL);
        } else {
            this.put("responsaObject", responsaObject);
        }
    }

    public ParseObject getUser() {
        return getParseObject("user");
    }

    public void setUser(ParseObject user) {
        if (user == null) {
            this.put("user", JSONObject.NULL);
        } else {
            this.put("user", user);
        }
    }
}
