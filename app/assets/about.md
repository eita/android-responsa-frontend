O aplicativo Responsa é uma iniciativa do Instituto Kairós (http://institutokairos.net/) e da Cooperativa de Trabalho EITA (http://eita.org.br), com o objetivo de aumentar o grau de consciência sobre o consumo responsável e transformar hábitos de consumo individuais e coletivos.

Com o lema “passos rumo ao bem viver individual e coletivo”, o aplicativo oferece ferramentas eficientes e práticas para encontrar e cadastrar novas iniciativas de consumo responsável, tais como feiras orgânicas, grupos de consumo responsável, empreendimentos solidários, experiências agroecológicas e restaurantes orgânicos.

No Responsa, é possível também se comunicar e marcar encontros com outras pessoas, além de compartilhar dúvidas e descobertas relacionadas às práticas de consumo responsável.

A nossa base de dados parte de 10 plataformas online existentes no Brasil, e ela se amplia a cada dia mais com a contribuição de cada usuária e usuário do Responsa. A lista de fontes iniciais das iniciativas segue abaixo.

Ajude a melhorar o Responsa! Sugestões de melhorias e informações sobre problemas do aplicativo podem ser enviadas a qualquer momento no menu principal do canto superior direito. Agradecemos a sua ajuda!

Futuramente o Responsa será usado também como canal de divulgação por coletivos, feiras, empreendimentos e ONGs relacionados ao tema.

**Lista de plataformas parceiras que servem de fonte inicial dos dados iniciais do Responsa:**

1. Cirandas.net: portal do Fórum Brasileiro de Economia Solidária onde se encontram iniciativas de economia solidária e seus produtos e serviços;
2. Mapa dos Grupos de Consumo Responsável, organizado pelo Instituto Kairós;
3. Mapa de Feiras Orgânicas, coordenado pelo IDEC
4. Mapa da Rede de Comercialização Solidária, uma iniciativa do Instituto Marista de Solidariedade
5. AgroecologiaEmRede: experiências de agroecologia, uma plataforma criada pela Articulação Nacional de Agroecologia (ANA) e pela Associação Brasileira de Agroecologia (ABA)
6. Plataforma Ideias na Mesa, iniciativa da Coordenação Geral de Educação Alimentar e Nutricional do Ministério do Desenvolvimento Social e Combate à Fome (CGEAN/MDS) e do Observatório de Políticas de Segurança Alimentar e Nutrição – Universidade de Brasília (OPSAN/UnB).
7. Plataforma da articulação Muda-SP, com mapa de iniciativas ligadas ao consumo responsável em São Paulo
8. Mapa de hortas urbanas de São Paulo
9. Roteiro EcoSampa, iniciativa da Prefeitura Municipal de São Paulo
10. Orgânicos no Prato: mapa de restaurantes orgânicos em São Paulo, iniciativa do Instituto Kairós